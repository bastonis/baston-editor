// @Author: Ultraxime
//
// this file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

#[cfg(test)]
#[cfg(feature = "gui")]
mod test_gui {
    use assert_cmd::cmd::Command;
    use predicates::str::contains;

    fn cmd() -> Command {
        let _ = env_logger::builder().is_test(true).try_init();

        let mut cmd = Command::cargo_bin("baston_editor").expect("A test should never fail");

        cmd.arg("--dry-run")
            .args(&["--log-level", "debug"])
            .arg("gui");

        cmd
    }

    #[test]
    fn test_no_cmd() {
        let _ = env_logger::builder().is_test(true).try_init();

        let mut cmd = Command::cargo_bin("baston_editor").expect("A test should never fail");

        cmd.arg("--dry-run").args(&["--log-level", "debug"]);

        cmd.assert()
            .success()
            .stderr(contains("Dry run, the GUI was not started"))
            .stderr(contains("Starting the GUI"));
    }

    #[test]
    fn test_no_input() {
        let mut cmd = cmd();

        cmd.assert()
            .success()
            .stderr(contains("Dry run, the GUI was not started"))
            .stderr(contains("Starting the GUI"));
    }

    #[test]
    fn test_input() {
        let mut cmd = cmd();

        cmd.arg("tests/input.yml");

        cmd.assert()
            .success()
            .stderr(contains("Dry run, the GUI was not started"))
            .stderr(contains("Starting the GUI"))
            .stderr(contains("Opening card tests/input.yml"));
    }

    #[test]
    fn test_input_dir() {
        let mut cmd = cmd();

        cmd.arg("tests");

        cmd.assert()
            .failure()
            .stderr(contains("The GUI does not support input directories"));
    }

    #[test]
    fn test_multi_inputs() {
        let mut cmd = cmd();

        cmd.args(&["tests/input.yml", "tests/input_ugly.yml"]);

        cmd.assert()
            .failure()
            .stderr(contains("unexpected argument 'tests/input_ugly.yml' found"));
    }

    #[test]
    fn test_wrong_input() {
        let mut cmd = cmd();

        cmd.arg("tests/cli.rs");

        cmd.assert()
            .failure()
            .stderr(contains("Input file tests/cli.rs is not a YAML file"));
    }

    #[test]
    fn test_input_does_not_exist() {
        let mut cmd = cmd();

        cmd.arg("tests/bla");

        cmd.assert()
            .failure()
            .stderr(contains("Input tests/bla does not exist"));
    }
}

#[cfg(test)]
#[cfg(feature = "gui")]
mod test_color_picker {
    use assert_cmd::cmd::Command;
    use predicates::str::contains;

    use baston_editor::color::Color;
    use baston_editor::error::Error;

    fn cmd() -> Command {
        let _ = env_logger::builder().is_test(true).try_init();

        let mut cmd = Command::cargo_bin("baston_editor").expect("A test should never fail");

        cmd.arg("--dry-run")
            .args(&["--log-level", "debug"])
            .arg("color-picker");

        cmd
    }

    #[test]
    fn test_no_input() {
        let mut cmd = cmd();

        cmd.assert()
            .success()
            .stderr(contains("Dry run, color picker GUI was not started"));
    }

    #[test]
    fn test_black_input() {
        let mut cmd = cmd();

        cmd.arg(serde_yaml::to_string(&Color::Black).expect("A test should never fail"));

        cmd.assert()
            .success()
            .stderr(contains("Dry run, color picker GUI was not started"));
    }

    #[test]
    fn test_custom_input() {
        let mut cmd = cmd();

        cmd.arg(
            serde_yaml::to_string(&Color::Custom(12, 34, 56, 78))
                .expect("A test should never fail"),
        );

        cmd.assert()
            .success()
            .stderr(contains("Dry run, color picker GUI was not started"));
    }

    #[test]
    fn test_invalid_input() {
        let mut cmd = cmd();

        cmd.arg("Something wrong");

        cmd.assert().failure().stderr(contains("Invalid color"));
    }

    #[test]
    fn test_serde_error_input() {
        let mut cmd = cmd();

        cmd.arg("!Custom\n- 10");

        cmd.assert().failure().stderr(contains(format!(
            "Invalid color: {}",
            Error::LengthError {
                expected_size: 4,
                current_size: 1
            }
        )));
    }
}

#[cfg(test)]
mod test_headless {
    use assert_cmd::cmd::Command;

    fn cmd() -> Command {
        let _ = env_logger::builder().is_test(true).try_init();

        let mut cmd = Command::cargo_bin("baston_editor").expect("A test should never fail");

        cmd.arg("--dry-run")
            .args(&["--log-level", "debug"])
            .arg("headless");

        cmd
    }

    mod test_input {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn no_input() -> Command {
            super::cmd()
        }

        fn single() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("tests/input.yml");

            cmd
        }

        fn multiple() -> Command {
            let mut cmd = super::cmd();

            cmd.args(&["tests/input.yml", "tests/input_ugly.yml"]);

            cmd
        }

        fn dir() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("tests");

            cmd
        }

        fn empty_dir() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("tests/images");

            cmd
        }

        fn wrong() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("tests/cli.rs");

            cmd
        }

        fn non_existing() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("bla");

            cmd
        }

        #[test]
        fn test_no_input() {
            let mut cmd = no_input();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_single() {
            let mut cmd = single();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_multiple() {
            let mut cmd = multiple();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));
        }

        #[test]
        fn test_dir() {
            let mut cmd = dir();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));
        }

        #[test]
        fn test_empty_dir() {
            let mut cmd = empty_dir();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_wrong() {
            let mut cmd = wrong();

            cmd.assert()
                .failure()
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));
        }

        #[test]
        fn test_non_existing() {
            let mut cmd = non_existing();

            cmd.assert()
                .failure()
                .stderr(contains("Input bla does not exist"));
        }

        #[test]
        fn test_recursive() {
            let mut cmd = no_input();

            cmd.arg("-r");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));

            let mut cmd = single();

            cmd.arg("-r");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.arg("-r");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.arg("-r");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.arg("-r");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        fn test_jobs(n: usize) {
            let mut cmd = no_input();

            cmd.args(&["-j", &n.to_string()]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));

            let mut cmd = single();

            cmd.args(&["-j", &n.to_string()]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.args(&["-j", &n.to_string()]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.args(&["-j", &n.to_string()]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.args(&["-j", &n.to_string()]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_1_job() {
            test_jobs(1)
        }

        #[test]
        fn test_2_jobs() {
            test_jobs(2)
        }

        #[test]
        fn test_0_job() {
            test_jobs(0)
        }

        #[test]
        fn test_job_none() {
            let mut cmd = no_input();

            cmd.arg("-j");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));

            let mut cmd = single();

            cmd.arg("-j");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.arg("-j");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.arg("-j");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.arg("-j");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_update() {
            let mut cmd = no_input();

            cmd.arg("-U");

            cmd.assert().failure().stderr(contains(
                "error: the following required arguments were not provided:\n  <FILE, FOLDER>...",
            ));

            let mut cmd = single();

            cmd.arg("-U");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.arg("-U");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.arg("-U");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.arg("-U");

            cmd.assert()
                .failure()
                .stderr(contains("No input were found despite the update option"));
        }

        #[test]
        fn test_show() {
            let mut cmd = no_input();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));

            let mut cmd = single();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        fn test_outputs(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--out");
                args.push(*output);
            }

            let mut cmd = no_input();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));

            let mut cmd = single();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.args(&args);

            cmd.assert().failure().stderr(contains(
                "error: Multiple inputs cannot be used with output",
            ));

            let mut cmd = dir();

            cmd.args(&args);

            cmd.assert().failure().stderr(contains(
                "error: Multiple inputs cannot be used with output",
            ));

            let mut cmd = empty_dir();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_single_output() {
            test_outputs(&vec!["tests/output.yml"])
        }

        #[test]
        fn test_multiple_outputs() {
            test_outputs(&vec!["tests/output.yml", "tests/output.pdf"])
        }

        fn test_output_types(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--output-type");
                args.push(*output);
            }

            let mut cmd = no_input();

            cmd.args(&args);

            cmd.assert().failure().stderr(contains(
                "error: the following required arguments were not provided:\n  <FILE, FOLDER>...",
            ));

            let mut cmd = single();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.args(&args);

            cmd.assert().failure().stderr(contains(
                "No input were found despite the output-type option",
            ));
        }

        #[test]
        fn test_single_output_type() {
            test_output_types(&vec!["yml"])
        }

        #[test]
        fn test_multiple_output_types() {
            test_output_types(&vec!["yml", "pdf"])
        }

        #[test]
        fn test_wrong_output_type() {
            let mut cmd = no_input();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                .failure()
                .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));

            let mut cmd = single();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                .failure()
                .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));

            let mut cmd = multiple();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                .failure()
                .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));

            let mut cmd = dir();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                .failure()
                .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));

            let mut cmd = empty_dir();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                .failure()
                .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
        }

        #[test]
        fn test_output_dir() {
            let mut cmd = no_input();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"]);

            cmd.assert().failure().stderr(contains(
                "error: the following required arguments were not provided:\n  <FILE, FOLDER>...",
            ));

            let mut cmd = single();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));

            let mut cmd = multiple();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"));

            let mut cmd = dir();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"))
                .stderr(contains("Opening card tests/input_ugly.yml"))
                .stderr(contains("Opening card tests/input_ugly_char.yml"))
                .stderr(contains("Input file tests/cli.rs is not a YAML file"));

            let mut cmd = empty_dir();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"]);

            cmd.assert().failure().stderr(contains(
                "No input were found despite the output-type option",
            ));
        }

        #[test]
        fn test_output_dir_not_dir() {
            let mut cmd = no_input();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"]);

            cmd.assert().failure().stderr(contains(
                "error: the following required arguments were not provided:\n  <FILE, FOLDER>...",
            ));

            let mut cmd = single();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"]);

            cmd.assert()
                .failure()
                .stderr(contains("The output-dir : tests.yml is not a directory"));

            let mut cmd = multiple();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"]);

            cmd.assert()
                .failure()
                .stderr(contains("The output-dir : tests.yml is not a directory"));

            let mut cmd = dir();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"]);

            cmd.assert()
                .failure()
                .stderr(contains("The output-dir : tests.yml is not a directory"));

            let mut cmd = empty_dir();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"]);

            cmd.assert().failure().stderr(contains(
                "No input were found despite the output-type option",
            ));
        }
    }

    mod test_recursive {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn cmd() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("-r");

            cmd
        }

        #[test]
        fn test_recursive() {
            cmd()
                .assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_job() {
            for j in [0, 1, 2] {
                let mut cmd = cmd();

                cmd.args(&["-j", &j.to_string()]);

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"));
            }
            let mut cmd = cmd();

            cmd.arg("-j");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_update() {
            let mut cmd = cmd();

            cmd.arg("-U").arg("tests/input.yml");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_show() {
            let mut cmd = cmd();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        fn test_outputs(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--out");
                args.push(*output);
            }

            let mut cmd = cmd();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_single_output() {
            test_outputs(&vec!["tests/output.yml"])
        }

        #[test]
        fn test_multiple_outputs() {
            test_outputs(&vec!["tests/output.yml", "tests/output.pdf"])
        }

        fn test_output_types(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--output-type");
                args.push(*output);
            }

            let mut cmd = cmd();

            cmd.args(&args).arg("tests/input.yml");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_single_output_type() {
            test_output_types(&vec!["yml"])
        }

        #[test]
        fn test_multiple_output_types() {
            test_output_types(&vec!["yml", "pdf"])
        }

        #[test]
        fn test_wrong_output_type() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                    .failure()
                    .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
        }

        #[test]
        fn test_output_dir() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"])
                .arg("tests/input.yml");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_output_dir_not_dir() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"])
                .arg("tests/input.yml");

            cmd.assert()
                .failure()
                .stderr(contains("The output-dir : tests.yml is not a directory"));
        }
    }

    mod test_jobs {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn cmds() -> Vec<Command> {
            let mut cmds = vec![];

            let mut cmd = super::cmd();

            cmd.arg("-j");

            cmds.push(cmd);

            for i in [0, 1, 2] {
                let mut cmd = super::cmd();

                cmd.args(&["-j", &i.to_string()]);

                cmds.push(cmd);
            }

            cmds
        }

        #[test]
        fn test_job() {
            for mut cmd in cmds() {
                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"));
            }
        }

        #[test]
        fn test_update() {
            for mut cmd in cmds() {
                cmd.arg("-U").arg("tests/input.yml");

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"))
                    .stderr(contains("Opening card tests/input.yml"));
            }
        }

        #[test]
        fn test_show() {
            for mut cmd in cmds() {
                cmd.arg("-s");

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"));
            }
        }

        fn test_outputs(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--out");
                args.push(*output);
            }

            for mut cmd in cmds() {
                cmd.args(&args);

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"));
            }
        }

        #[test]
        fn test_single_output() {
            test_outputs(&vec!["tests/output.yml"])
        }

        #[test]
        fn test_multiple_outputs() {
            test_outputs(&vec!["tests/output.yml", "tests/output.pdf"])
        }

        fn test_output_types(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--output-type");
                args.push(*output);
            }

            for mut cmd in cmds() {
                cmd.args(&args).arg("tests/input.yml");

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"))
                    .stderr(contains("Opening card tests/input.yml"));
            }
        }

        #[test]
        fn test_single_output_type() {
            test_output_types(&vec!["yml"])
        }

        #[test]
        fn test_multiple_output_types() {
            test_output_types(&vec!["yml", "pdf"])
        }

        #[test]
        fn test_wrong_output_type() {
            for mut cmd in cmds() {
                cmd.args(&["--output-type", "something wrong"]);

                cmd.assert()
                    .failure()
                    .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
            }
        }

        #[test]
        fn test_output_dir() {
            for mut cmd in cmds() {
                cmd.args(&["--output-type", "yml"])
                    .args(&["--output-dir", "tests"])
                    .arg("tests/input.yml");

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"))
                    .stderr(contains("Opening card tests/input.yml"));
            }
        }

        #[test]
        fn test_output_dir_not_dir() {
            for mut cmd in cmds() {
                cmd.args(&["--output-type", "yml"])
                    .args(&["--output-dir", "tests.yml"])
                    .arg("tests/input.yml");

                cmd.assert()
                    .failure()
                    .stderr(contains("The output-dir : tests.yml is not a directory"));
            }
        }
    }

    mod test_update {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn cmd() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("-U").arg("tests/input.yml");

            cmd
        }

        #[test]
        fn test_update() {
            let mut cmd = cmd();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_show() {
            let mut cmd = cmd();

            cmd.arg("-s");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        fn test_outputs(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--out");
                args.push(*output);
            }

            let mut cmd = cmd();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_single_output() {
            test_outputs(&vec!["tests/output.yml"])
        }

        #[test]
        fn test_multiple_outputs() {
            test_outputs(&vec!["tests/output.yml", "tests/output.pdf"])
        }

        fn test_output_types(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--output-type");
                args.push(*output);
            }

            let mut cmd = cmd();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_single_output_type() {
            test_output_types(&vec!["yml"])
        }

        #[test]
        fn test_multiple_output_types() {
            test_output_types(&vec!["yml", "pdf"])
        }

        #[test]
        fn test_wrong_output_type() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                    .failure()
                    .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
        }

        #[test]
        fn test_output_dir() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"]);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_output_dir_not_dir() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"]);

            cmd.assert()
                .failure()
                .stderr(contains("The output-dir : tests.yml is not a directory"));
        }
    }

    mod test_show {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn cmd() -> Command {
            let mut cmd = super::cmd();

            cmd.arg("-s");

            cmd
        }

        #[test]
        fn test_show() {
            let mut cmd = cmd();

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        fn test_outputs(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--out");
                args.push(*output);
            }

            let mut cmd = cmd();

            cmd.args(&args);

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"));
        }

        #[test]
        fn test_single_output() {
            test_outputs(&vec!["tests/output.yml"])
        }

        #[test]
        fn test_multiple_outputs() {
            test_outputs(&vec!["tests/output.yml", "tests/output.pdf"])
        }

        fn test_output_types(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--output-type");
                args.push(*output);
            }

            let mut cmd = cmd();

            cmd.args(&args).arg("tests/input.yml");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_single_output_type() {
            test_output_types(&vec!["yml"])
        }

        #[test]
        fn test_multiple_output_types() {
            test_output_types(&vec!["yml", "pdf"])
        }

        #[test]
        fn test_wrong_output_type() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "something wrong"]);

            cmd.assert()
                    .failure()
                    .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
        }

        #[test]
        fn test_output_dir() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests"])
                .arg("tests/input.yml");

            cmd.assert()
                .success()
                .stderr(contains("Dry run, nothing was modified"))
                .stderr(contains("Opening card tests/input.yml"));
        }

        #[test]
        fn test_output_dir_not_dir() {
            let mut cmd = cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-dir", "tests.yml"])
                .arg("tests/input.yml");

            cmd.assert()
                .failure()
                .stderr(contains("The output-dir : tests.yml is not a directory"));
        }
    }

    mod test_output {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn cmds() -> Vec<Command> {
            let mut cmds = vec![];

            let mut cmd = super::cmd();

            cmd.args(&["--out", "test/output.yml"]);

            cmds.push(cmd);

            let mut cmd = super::cmd();

            cmd.args(&["--out", "test/output.yml"]);
            cmd.args(&["--out", "test/output.pdf"]);

            cmds.push(cmd);

            cmds
        }

        #[test]
        fn test_outputs() {
            for mut cmd in cmds() {
                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"));
            }
        }

        fn test_output_types(outputs: &[&str]) {
            let mut args = vec![];

            for output in outputs {
                args.push("--output-type");
                args.push(*output);
            }

            for mut cmd in cmds() {
                cmd.args(&args).arg("tests/input.yml");

                cmd.assert()
                    .failure()
                    .stderr(contains("error: the argument '--outputs <FILE>' cannot be used with '--output-type <TYPE>'"));
            }
        }

        #[test]
        fn test_single_output_type() {
            test_output_types(&vec!["yml"])
        }

        #[test]
        fn test_multiple_output_types() {
            test_output_types(&vec!["yml", "pdf"])
        }

        #[test]
        fn test_wrong_output_type() {
            for mut cmd in cmds() {
                cmd.args(&["--output-type", "something wrong"]);

                cmd.assert()
                    .failure()
                    .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
            }
        }

        #[test]
        fn test_output_dir() {
            for mut cmd in cmds() {
                cmd.args(&["--output-type", "yml"])
                    .args(&["--output-dir", "tests"])
                    .arg("tests/input.yml");

                cmd.assert()
                    .failure()
                    .stderr(contains("error: the argument '--outputs <FILE>' cannot be used with:\n  --output-type <TYPE>\n  --output-dir <FOLDER>"));
            }
        }

        #[test]
        fn test_output_dir_not_dir() {
            for mut cmd in cmds() {
                cmd.args(&["--output-type", "yml"])
                    .args(&["--output-dir", "tests.yml"])
                    .arg("tests/input.yml");

                cmd.assert()
                    .failure()
                    .stderr(contains("error: the argument '--outputs <FILE>' cannot be used with:\n  --output-type <TYPE>\n  --output-dir <FOLDER>"));
            }
        }
    }

    mod test_output_type {
        use assert_cmd::cmd::Command;
        use predicates::str::contains;

        fn cmds() -> Vec<Command> {
            let mut cmds = vec![];

            let mut cmd = super::cmd();

            cmd.args(&["--output-type", "yml"]).arg("tests/input.yml");

            cmds.push(cmd);

            let mut cmd = super::cmd();

            cmd.args(&["--output-type", "yml"])
                .args(&["--output-type", "pdf"])
                .arg("tests/input.yml");

            cmds.push(cmd);

            cmds
        }

        #[test]
        fn test_output_types() {
            for mut cmd in cmds() {
                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"))
                    .stderr(contains("Opening card tests/input.yml"));
            }
        }

        #[test]
        fn test_wrong_output_type() {
            let mut cmd = super::cmd();

            cmd.args(&["--output-type", "something wrong"])
                .arg("tests/input.yml");

            cmd.assert()
                    .failure()
                    .stderr(contains("error: invalid value 'something wrong' for '--output-type <TYPE>'\n  [possible values: pdf, svg, png, yml]"));
        }

        #[test]
        fn test_output_dir() {
            for mut cmd in cmds() {
                cmd.args(&["--output-dir", "tests"]);

                cmd.assert()
                    .success()
                    .stderr(contains("Dry run, nothing was modified"))
                    .stderr(contains("Opening card tests/input.yml"));
            }
        }

        #[test]
        fn test_output_dir_not_dir() {
            for mut cmd in cmds() {
                cmd.args(&["--output-dir", "tests.yml"]);

                cmd.assert()
                    .failure()
                    .stderr(contains("The output-dir : tests.yml is not a directory"));
            }
        }
    }

    mod test_output_dir {
        use predicates::str::contains;

        #[test]
        fn test_output_dir() {
            let mut cmd = super::cmd();
            cmd.args(&["--output-dir", "tests"]).arg("tests/input.yml");

            cmd.assert()
                    .failure()
                    .stderr(contains("error: the following required arguments were not provided:\n  --output-type <TYPE>"));
        }

        #[test]
        fn test_output_dir_not_dir() {
            let mut cmd = super::cmd();
            cmd.args(&["--output-dir", "tests.yml"])
                .arg("tests/input.yml");

            cmd.assert()
                    .failure()
                    .stderr(contains("error: the following required arguments were not provided:\n  --output-type <TYPE>"));
        }
    }
}
