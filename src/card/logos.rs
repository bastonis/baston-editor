// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for parts of a card that are similar to a logo

use crate::error::{Error, LogError};
use crate::render::Group;
use crate::render::{
    config::Config,
    images::{Image, RasterImage, Svg},
    Layer, Render, RenderedCard,
};

use image::ImageFormat;
use serde::{Deserialize, Serialize};
use strum_macros::{Display, EnumIter};
use usvg::Transform;

/// Represents the Faction of a Card
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum Faction {
    /// The Loyal Faction
    #[serde(alias = "LAW")]
    Law,
    /// The Neutral Faction
    #[serde(alias = "BALANCE")]
    #[default]
    Balance,
    /// The Chaotic Faction
    #[serde(alias = "CHAOS")]
    Chaos,
    /// No Faction
    #[serde(alias = "EMPTY")]
    Empty,
}

/// Represents the Gender of a Card
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum Gender {
    /// Non-Binary gender
    NonBinary,
    /// Male gender
    Male,
    /// Female gender
    Female,
    /// No gender applicable
    NonGenderable,
    /// Random gender
    #[default]
    Random,
}

/// Represents the Era of a Card
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum Era {
    /// From a past age
    Past,
    /// From a contemporaneous age
    Present,
    /// From a futuristic age
    #[serde(alias = "Futur")]
    Future,
    /// From the past and the present
    PastPresent,
    /// From the past and the future
    PastFuture,
    /// From the present and the future
    PresentFuture,
    /// From all ages
    #[default]
    All,
}

/// Represents the Archetype of a Card
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum Archetype {
    /// The wanderer archetype
    Wanderer,
    /// The necropolis archetype
    Necropolis,
    /// The trader archetype
    Trader,
    /// The warmonger archetype
    Warmonger,
    /// The magic archetype
    Magic,
    /// The religious archetype
    Religious,
    /// The intellectual archetype
    Intellectual,
    /// The industrial archetype
    Industrial,
    /// Anything else
    #[default]
    Uncivilized,
}

/// Represents the extra deck of a Card
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum ExtraDeck {
    /// The extra deck
    #[serde(alias = "true")]
    ExtraDeck,
    /// The cities
    City,
    /// The machines
    Machine,
    /// The buildings and assimilations
    Building,
    /// Is not extra deck
    #[serde(alias = "false")]
    #[default]
    None,
}

/// Trait for object represented by a logo
pub trait Logo {
    /// Gets the logo
    ///
    /// Can return none if the logo does not exist
    fn get_image(&self) -> Option<impl Image>;
}

/// Trait for object represented by a logo in SVG
pub trait SvgLogo {
    /// Gets the logo
    fn get_image(&self) -> Svg;
}

impl<L: SvgLogo> Logo for L {
    fn get_image(&self) -> Option<impl Image> {
        Some(SvgLogo::get_image(self))
    }
}

impl Logo for Faction {
    fn get_image(&self) -> Option<impl Image> {
        Some(match self {
            Self::Empty => RasterImage::from_bytes(include_bytes!("../logos/faction/Empty.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/faction/Empty.png, this file is embed in the code so it should not fail"),
            Self::Law => RasterImage::from_bytes(include_bytes!("../logos/faction/Law.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/faction/Law.png, this file is embed in the code so it should not fail"),
            Self::Balance => RasterImage::from_bytes(include_bytes!("../logos/faction/Balance.png"), None).critical("Error while loading src/logos/faction/Balance.png, this file is embed in the code so it should not fail"),
            Self::Chaos => RasterImage::from_bytes(include_bytes!("../logos/faction/Chaos.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/faction/Chaos.png, this file is embed in the code so it should not fail"),
        })
    }
}

impl Logo for ExtraDeck {
    fn get_image(&self) -> Option<impl Image> {
        match self {
            Self::ExtraDeck => Some(RasterImage::from_bytes(include_bytes!("../logos/extra_deck/ExtraDeck.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/extra_deck/ExtraDeck.png, this file is embed in the code so it should not fail")),
            Self::City => Some(RasterImage::from_bytes(include_bytes!("../logos/extra_deck/City.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/extra_deck/City.png, this file is embed in the code so it should not fail")),
            Self::Machine => Some(RasterImage::from_bytes(include_bytes!("../logos/extra_deck/Machine.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/extra_deck/Machine.png, this file is embed in the code so it should not fail")),
            Self::Building => Some(RasterImage::from_bytes(include_bytes!("../logos/extra_deck/Building.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/extra_deck/Building.png, this file is embed in the code so it should not fail")),
            Self::None => None,
        }
    }
}

impl SvgLogo for Gender {
    fn get_image(&self) -> Svg {
        match self {
            Self::NonBinary => Svg::from_str(
                include_str!("../logos/genders/NonBinary.svg"),
                Layer::Gender,
            ),
            Self::Male => Svg::from_str(include_str!("../logos/genders/Male.svg"), Layer::Gender),
            Self::Female => {
                Svg::from_str(include_str!("../logos/genders/Female.svg"), Layer::Gender)
            }
            Self::Random => {
                Svg::from_str(include_str!("../logos/genders/Random.svg"), Layer::Gender)
            }
            Self::NonGenderable => Svg::from_str(
                include_str!("../logos/genders/NonGenderable.svg"),
                Layer::Gender,
            ),
        }
    }
}

impl SvgLogo for Era {
    fn get_image(&self) -> Svg {
        match self {
            Self::Past => Svg::from_str(include_str!("../logos/eras/Past.svg"), Layer::Era),
            Self::Present => Svg::from_str(include_str!("../logos/eras/Present.svg"), Layer::Era),
            Self::Future => Svg::from_str(include_str!("../logos/eras/Future.svg"), Layer::Era),
            Self::PastPresent => {
                Svg::from_str(include_str!("../logos/eras/PastPresent.svg"), Layer::Era)
            }
            Self::PastFuture => {
                Svg::from_str(include_str!("../logos/eras/PastFuture.svg"), Layer::Era)
            }
            Self::PresentFuture => {
                Svg::from_str(include_str!("../logos/eras/PresentFuture.svg"), Layer::Era)
            }
            Self::All => Svg::from_str(include_str!("../logos/eras/All.svg"), Layer::Era),
        }
    }
}

impl SvgLogo for Archetype {
    fn get_image(&self) -> Svg {
        match self {
            Self::Uncivilized => Svg::from_str(
                include_str!("../logos/archetypes/Uncivilized.svg"),
                Layer::Archetype,
            ),
            Self::Wanderer => Svg::from_str(
                include_str!("../logos/archetypes/Wanderer.svg"),
                Layer::Archetype,
            ),
            Self::Necropolis => Svg::from_str(
                include_str!("../logos/archetypes/Necropolis.svg"),
                Layer::Archetype,
            ),
            Self::Trader => Svg::from_str(
                include_str!("../logos/archetypes/Trader.svg"),
                Layer::Archetype,
            ),
            Self::Warmonger => Svg::from_str(
                include_str!("../logos/archetypes/Warmonger.svg"),
                Layer::Archetype,
            ),
            Self::Magic => Svg::from_str(
                include_str!("../logos/archetypes/Magic.svg"),
                Layer::Archetype,
            ),
            Self::Religious => Svg::from_str(
                include_str!("../logos/archetypes/Religious.svg"),
                Layer::Archetype,
            ),
            Self::Intellectual => Svg::from_str(
                include_str!("../logos/archetypes/Intellectual.svg"),
                Layer::Archetype,
            ),
            Self::Industrial => Svg::from_str(
                include_str!("../logos/archetypes/Industrial.svg"),
                Layer::Archetype,
            ),
        }
    }
}

impl<L: Logo> Render for L {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        match self.get_image() {
            Some(image) => Ok(image
                .to_width_node(
                    layer,
                    config.get_position(layer)?,
                    Some(config.get_width(layer)?),
                )
                .into()),
            None => Ok(Group::new(layer, Transform::identity()).into()),
        }
    }
}

#[cfg(test)]
mod test_module_logos {
    #[cfg(test)]
    mod test_enum_faction {

        use super::super::Faction;
        use super::super::Logo;

        use crate::card::illustration::{Mode, Orientation};
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde() {
            assert_eq!(
                serde_yaml::from_str::<Faction>("Empty").expect("A test should never fail"),
                Faction::Empty
            );
            assert_eq!(
                serde_yaml::from_str::<Faction>("EMPTY").expect("A test should never fail"),
                Faction::Empty
            );
            assert_eq!(
                serde_yaml::to_string(&Faction::Empty).expect("A test should never fail"),
                "Empty\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Faction>("Law").expect("A test should never fail"),
                Faction::Law
            );
            assert_eq!(
                serde_yaml::from_str::<Faction>("LAW").expect("A test should never fail"),
                Faction::Law
            );
            assert_eq!(
                serde_yaml::to_string(&Faction::Law).expect("A test should never fail"),
                "Law\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Faction>("Chaos").expect("A test should never fail"),
                Faction::Chaos
            );
            assert_eq!(
                serde_yaml::from_str::<Faction>("CHAOS").expect("A test should never fail"),
                Faction::Chaos
            );
            assert_eq!(
                serde_yaml::to_string(&Faction::Chaos).expect("A test should never fail"),
                "Chaos\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Faction>("Balance").expect("A test should never fail"),
                Faction::Balance
            );
            assert_eq!(
                serde_yaml::from_str::<Faction>("BALANCE").expect("A test should never fail"),
                Faction::Balance
            );
            assert_eq!(
                serde_yaml::to_string(&Faction::Balance).expect("A test should never fail"),
                "Balance\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Faction>("Anything else").expect_err("A test should never fail").to_string(),
                "unknown variant `Anything else`, expected one of `LAW`, `Law`, `BALANCE`, `Balance`, `CHAOS`, `Chaos`, `EMPTY`, `Empty`"
            );
        }

        #[test]
        fn test_clone() {
            assert_eq!(Faction::Law, Faction::Law.clone());
            assert_eq!(Faction::Chaos, Faction::Chaos.clone());
            assert_eq!(Faction::Balance, Faction::Balance.clone());
            assert_eq!(Faction::Empty, Faction::Empty.clone());
        }

        #[test]
        fn test_display() {
            assert_eq!(format!("{}", Faction::Law), "Law");
            assert_eq!(format!("{}", Faction::Chaos), "Chaos");
            assert_eq!(format!("{}", Faction::Balance), "Balance");
            assert_eq!(format!("{}", Faction::Empty), "Empty");

            assert_eq!(format!("{:?}", Faction::Law), "Law");
            assert_eq!(format!("{:?}", Faction::Chaos), "Chaos");
            assert_eq!(format!("{:?}", Faction::Balance), "Balance");
            assert_eq!(format!("{:?}", Faction::Empty), "Empty");
        }

        #[test]
        fn test_get_image() {
            for faction in Faction::iter() {
                faction.get_image();
            }
        }

        #[test]
        fn test_render_faction() {
            let mut config = Config::default();
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    config.set_orientation(orientation);
                    config.set_mode(mode);
                    for faction in Faction::iter() {
                        let render = faction
                            .render(&Layer::Faction, &config, None)
                            .expect("A test should never fail");
                        assert!(render.has_node(&Layer::Faction));
                    }
                }
            }
        }
    }

    #[cfg(test)]
    mod test_enum_gender {
        use super::super::Gender;
        use super::super::Logo;

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde() {
            assert_eq!(
                serde_yaml::from_str::<Gender>("NonGenderable").expect("A test should never fail"),
                Gender::NonGenderable
            );
            assert_eq!(
                serde_yaml::to_string(&Gender::NonGenderable).expect("A test should never fail"),
                "NonGenderable\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Gender>("Male").expect("A test should never fail"),
                Gender::Male
            );
            assert_eq!(
                serde_yaml::to_string(&Gender::Male).expect("A test should never fail"),
                "Male\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Gender>("Female").expect("A test should never fail"),
                Gender::Female
            );
            assert_eq!(
                serde_yaml::to_string(&Gender::Female).expect("A test should never fail"),
                "Female\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Gender>("Random").expect("A test should never fail"),
                Gender::Random
            );
            assert_eq!(
                serde_yaml::to_string(&Gender::Random).expect("A test should never fail"),
                "Random\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Gender>("NonBinary").expect("A test should never fail"),
                Gender::NonBinary
            );
            assert_eq!(
                serde_yaml::to_string(&Gender::NonBinary).expect("A test should never fail"),
                "NonBinary\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Gender>("Anything else").expect_err("A test should never fail").to_string(),
                "unknown variant `Anything else`, expected one of `NonBinary`, `Male`, `Female`, `NonGenderable`, `Random`"
            );
        }

        #[test]
        fn test_clone() {
            assert_eq!(Gender::Male, Gender::Male.clone());
            assert_eq!(Gender::Female, Gender::Female.clone());
            assert_eq!(Gender::Random, Gender::Random.clone());
            assert_eq!(Gender::NonBinary, Gender::NonBinary.clone());
            assert_eq!(Gender::NonGenderable, Gender::NonGenderable.clone());
        }

        #[test]
        fn test_display() {
            assert_eq!(format!("{}", Gender::Male), "Male");
            assert_eq!(format!("{}", Gender::Female), "Female");
            assert_eq!(format!("{}", Gender::Random), "Random");
            assert_eq!(format!("{}", Gender::NonBinary), "NonBinary");
            assert_eq!(format!("{}", Gender::NonGenderable), "NonGenderable");

            assert_eq!(format!("{:?}", Gender::Male), "Male");
            assert_eq!(format!("{:?}", Gender::Female), "Female");
            assert_eq!(format!("{:?}", Gender::Random), "Random");
            assert_eq!(format!("{:?}", Gender::NonBinary), "NonBinary");
            assert_eq!(format!("{:?}", Gender::NonGenderable), "NonGenderable");
        }

        #[test]
        fn test_get_image() {
            for gender in Gender::iter() {
                gender.get_image();
            }
        }
    }

    #[cfg(test)]
    mod test_enum_era {
        use super::super::Era;
        use super::super::Logo;

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde() {
            assert_eq!(
                serde_yaml::from_str::<Era>("Past").expect("A test should never fail"),
                Era::Past
            );
            assert_eq!(
                serde_yaml::to_string(&Era::Past).expect("A test should never fail"),
                "Past\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("Present").expect("A test should never fail"),
                Era::Present
            );
            assert_eq!(
                serde_yaml::to_string(&Era::Present).expect("A test should never fail"),
                "Present\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("Future").expect("A test should never fail"),
                Era::Future
            );
            assert_eq!(
                serde_yaml::from_str::<Era>("Futur").expect("A test should never fail"),
                Era::Future
            );
            assert_eq!(
                serde_yaml::to_string(&Era::Future).expect("A test should never fail"),
                "Future\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("PastPresent").expect("A test should never fail"),
                Era::PastPresent
            );
            assert_eq!(
                serde_yaml::to_string(&Era::PastPresent).expect("A test should never fail"),
                "PastPresent\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("PastFuture").expect("A test should never fail"),
                Era::PastFuture
            );
            assert_eq!(
                serde_yaml::to_string(&Era::PastFuture).expect("A test should never fail"),
                "PastFuture\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("PresentFuture").expect("A test should never fail"),
                Era::PresentFuture
            );
            assert_eq!(
                serde_yaml::to_string(&Era::PresentFuture).expect("A test should never fail"),
                "PresentFuture\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("All").expect("A test should never fail"),
                Era::All
            );
            assert_eq!(
                serde_yaml::to_string(&Era::All).expect("A test should never fail"),
                "All\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Era>("Anything else").expect_err("A test should never fail").to_string(),
                "unknown variant `Anything else`, expected one of `Past`, `Present`, `Futur`, `Future`, `PastPresent`, `PastFuture`, `PresentFuture`, `All`"
            );
        }

        #[test]
        fn test_clone() {
            assert_eq!(Era::Past, Era::Past.clone());
            assert_eq!(Era::Present, Era::Present.clone());
            assert_eq!(Era::Future, Era::Future.clone());
            assert_eq!(Era::PastPresent, Era::PastPresent.clone());
            assert_eq!(Era::PastFuture, Era::PastFuture.clone());
            assert_eq!(Era::PresentFuture, Era::PresentFuture.clone());
            assert_eq!(Era::All, Era::All.clone());
        }

        #[test]
        fn test_display() {
            assert_eq!(format!("{}", Era::Past), "Past");
            assert_eq!(format!("{}", Era::Present), "Present");
            assert_eq!(format!("{}", Era::Future), "Future");
            assert_eq!(format!("{}", Era::PastPresent), "PastPresent");
            assert_eq!(format!("{}", Era::PastFuture), "PastFuture");
            assert_eq!(format!("{}", Era::PresentFuture), "PresentFuture");
            assert_eq!(format!("{}", Era::All), "All");

            assert_eq!(format!("{:?}", Era::Past), "Past");
            assert_eq!(format!("{:?}", Era::Present), "Present");
            assert_eq!(format!("{:?}", Era::Future), "Future");
            assert_eq!(format!("{:?}", Era::PastPresent), "PastPresent");
            assert_eq!(format!("{:?}", Era::PastFuture), "PastFuture");
            assert_eq!(format!("{:?}", Era::PresentFuture), "PresentFuture");
            assert_eq!(format!("{:?}", Era::All), "All");
        }

        #[test]
        fn test_get_image() {
            for era in Era::iter() {
                era.get_image();
            }
        }
    }

    #[cfg(test)]
    mod test_enum_archetype {
        use super::super::Archetype;
        use super::super::Logo;

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde() {
            assert_eq!(
                serde_yaml::from_str::<Archetype>("Uncivilized").expect("A test should never fail"),
                Archetype::Uncivilized
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Uncivilized).expect("A test should never fail"),
                "Uncivilized\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Wanderer").expect("A test should never fail"),
                Archetype::Wanderer
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Wanderer).expect("A test should never fail"),
                "Wanderer\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Necropolis").expect("A test should never fail"),
                Archetype::Necropolis
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Necropolis).expect("A test should never fail"),
                "Necropolis\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Trader").expect("A test should never fail"),
                Archetype::Trader
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Trader).expect("A test should never fail"),
                "Trader\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Warmonger").expect("A test should never fail"),
                Archetype::Warmonger
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Warmonger).expect("A test should never fail"),
                "Warmonger\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Magic").expect("A test should never fail"),
                Archetype::Magic
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Magic).expect("A test should never fail"),
                "Magic\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Religious").expect("A test should never fail"),
                Archetype::Religious
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Religious).expect("A test should never fail"),
                "Religious\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Intellectual")
                    .expect("A test should never fail"),
                Archetype::Intellectual
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Intellectual).expect("A test should never fail"),
                "Intellectual\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Industrial").expect("A test should never fail"),
                Archetype::Industrial
            );
            assert_eq!(
                serde_yaml::to_string(&Archetype::Industrial).expect("A test should never fail"),
                "Industrial\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Archetype>("Anything else")
                    .expect_err("A test should never fail").to_string(),
                "unknown variant `Anything else`, expected one of `Wanderer`, `Necropolis`, `Trader`, `Warmonger`, `Magic`, `Religious`, `Intellectual`, `Industrial`, `Uncivilized`"
            );
        }

        #[test]
        fn test_clone() {
            for archetype in Archetype::iter() {
                assert_eq!(archetype, archetype.clone());
            }
        }

        #[test]
        fn test_display() {
            assert_eq!(format!("{}", Archetype::Uncivilized), "Uncivilized");
            assert_eq!(format!("{}", Archetype::Wanderer), "Wanderer");
            assert_eq!(format!("{}", Archetype::Necropolis), "Necropolis");
            assert_eq!(format!("{}", Archetype::Trader), "Trader");
            assert_eq!(format!("{}", Archetype::Warmonger), "Warmonger");
            assert_eq!(format!("{}", Archetype::Magic), "Magic");
            assert_eq!(format!("{}", Archetype::Religious), "Religious");
            assert_eq!(format!("{}", Archetype::Intellectual), "Intellectual");
            assert_eq!(format!("{}", Archetype::Industrial), "Industrial");
        }

        #[test]
        fn test_get_image() {
            for archetype in Archetype::iter() {
                archetype.get_image();
            }
        }
    }

    mod test_enum_extra_deck {
        use super::super::ExtraDeck;
        use super::super::Logo;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde() {
            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("ExtraDeck").expect("A test should never fail"),
                ExtraDeck::ExtraDeck
            );
            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("true").expect("A test should never fail"),
                ExtraDeck::ExtraDeck
            );
            assert_eq!(
                serde_yaml::to_string(&ExtraDeck::ExtraDeck).expect("A test should never fail"),
                "ExtraDeck\n"
            );

            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("City").expect("A test should never fail"),
                ExtraDeck::City
            );
            assert_eq!(
                serde_yaml::to_string(&ExtraDeck::City).expect("A test should never fail"),
                "City\n"
            );

            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("Machine").expect("A test should never fail"),
                ExtraDeck::Machine
            );
            assert_eq!(
                serde_yaml::to_string(&ExtraDeck::Machine).expect("A test should never fail"),
                "Machine\n"
            );

            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("Building").expect("A test should never fail"),
                ExtraDeck::Building
            );
            assert_eq!(
                serde_yaml::to_string(&ExtraDeck::Building).expect("A test should never fail"),
                "Building\n"
            );

            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("None").expect("A test should never fail"),
                ExtraDeck::None
            );
            assert_eq!(
                serde_yaml::from_str::<ExtraDeck>("false").expect("A test should never fail"),
                ExtraDeck::None
            );
            assert_eq!(
                serde_yaml::to_string(&ExtraDeck::None).expect("A test should never fail"),
                "None\n"
            );

            assert!(serde_yaml::from_str::<ExtraDeck>("Anything").is_err());
        }

        #[test]
        fn test_debug() {
            assert_eq!(format!("{:?}", ExtraDeck::ExtraDeck), "ExtraDeck");
            assert_eq!(format!("{:?}", ExtraDeck::City), "City");
            assert_eq!(format!("{:?}", ExtraDeck::Machine), "Machine");
            assert_eq!(format!("{:?}", ExtraDeck::Building), "Building");
            assert_eq!(format!("{:?}", ExtraDeck::None), "None");
        }

        #[test]
        fn test_clone() {
            assert_eq!(ExtraDeck::ExtraDeck, ExtraDeck::ExtraDeck.clone());
            assert_eq!(ExtraDeck::City, ExtraDeck::City.clone());
            assert_eq!(ExtraDeck::Machine, ExtraDeck::Machine.clone());
            assert_eq!(ExtraDeck::Building, ExtraDeck::Building.clone());
            assert_eq!(ExtraDeck::None, ExtraDeck::None.clone());
        }

        #[test]
        fn test_display() {
            assert_eq!(format!("{}", ExtraDeck::ExtraDeck), "ExtraDeck");
            assert_eq!(format!("{}", ExtraDeck::City), "City");
            assert_eq!(format!("{}", ExtraDeck::Machine), "Machine");
            assert_eq!(format!("{}", ExtraDeck::Building), "Building");
            assert_eq!(format!("{}", ExtraDeck::None), "None");
        }

        #[test]
        fn test_get_image() {
            for extra_deck in ExtraDeck::iter() {
                assert_eq!(
                    extra_deck.get_image().is_some(),
                    extra_deck != ExtraDeck::None
                );
            }
        }

        #[test]
        fn test_render() {
            for extra_deck in ExtraDeck::iter() {
                assert!(extra_deck
                    .render(&Layer::ExtraDeck, &Config::default(), None)
                    .expect("A test should never fail")
                    .has_node(&Layer::ExtraDeck));
            }
        }

        #[test]
        fn test_default() {
            assert_eq!(ExtraDeck::default(), ExtraDeck::None);
        }
    }
}
