// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the pictograms

use super::logos::SvgLogo;
use crate::card::fields::PowerField;
use crate::color::Color;
use crate::error::{Error, LogError};
use crate::render::RenderedCard;
use crate::render::{config::Config, images::Svg, Group, Layer, Render};

use log::warn;
use serde::{de::Error as SerdeError, Deserialize, Serialize};
use serde_yaml::{Mapping, Value};
use strum_macros::{Display, EnumIter};

/// Represents a Pictogram
#[derive(Serialize, Debug, Clone, PartialEq)]
pub struct Pictogram {
    /// The effect of the pictogram (logo shown)
    pub effect: Effect,
    /// The type of the pictogram (color of the logo)
    pub r#type: Type,
    /// The value of the pictogram (text shown)
    pub value: String,
    /// The number of stars of the pictogram should be between 0 and 3 (included)
    pub stars: u8,
    /// The color of the border of the pictogram
    pub border: Color,
    /// The color of the text of the pictogram
    pub text_color: Color,
    /// The part of the card, the pictogram is about
    pub reference: Reference,
}

/// Represents the effect carried by a Pictogram
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum Effect {
    /// The [Pictogram] is negative
    Negative,
    /// The [Pictogram] is mandatory
    Mandatory,
    /// The [Pictogram] is an action
    Action,
    /// The [Pictogram] is passive
    Passive,
    /// The [Pictogram] is triggered
    Triggered,
    /// The [Pictogram] is an object
    Object,
    /// The [Pictogram] is a device
    Device,
    /// The [Pictogram] is a city
    City,
    /// The [Pictogram] is something else
    #[serde(other)]
    #[default]
    Other,
}

/// Represents the type of effect carried by a Pictogram
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Display, EnumIter, Default)]
pub enum Type {
    /// The [Pictogram] is an attack
    Attack,
    /// The [Pictogram] is a defense
    Defense,
    /// The [Pictogram] is an internal action
    Intern,
    /// The [Pictogram] is an external action
    Extern,
    /// The [Pictogram] is a Charm
    Charm,
    /// The [Pictogram] is used when outside the hand
    Outside,
    /// The [Pictogram] is a safeguard
    Safeguard,
    /// The [Pictogram] is used on validation
    OnValidation,
    /// The [Pictogram] is a heal
    Heal,
    /// The [Pictogram] acts on another card
    OtherCard,
    /// The [Pictogram] is negative
    Negative,
    /// The [Pictogram] is something else
    #[serde(other)]
    #[default]
    Other,
}

/// Represents the part of the card, the pictogram is about
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Default)]
pub struct Reference {
    /// The type of part of the card, the pictogram is about
    #[serde(default)]
    pub r#type: ReferenceType,
    /// The value of the reference
    #[serde(default)]
    pub value: String,
}

/// Represents the type of part of the card, the pictogram is about
#[derive(Serialize, Deserialize, Debug, Display, Copy, Clone, PartialEq, Default, EnumIter)]
pub enum ReferenceType {
    /// The reference is about a [Power](crate::card::field::PowerField)
    Power,
    /// The reference is about a [Field](crate::card::field::BaseField)
    #[default]
    Field,
}

impl Pictogram {
    /// Gets the color this pictogramme should have given its type
    pub fn get_color(&self) -> Color {
        self.r#type.into()
    }

    /// Check that the reference of the pictogram is valid
    ///
    /// Returns `true` if the reference is not valid and was modified to a valid one
    pub fn check_ref(&mut self, powers: &[PowerField]) -> bool {
        if self.reference.r#type == ReferenceType::Power
            && !powers.iter().any(|p| p.value == self.reference.value)
        {
            warn!(
                "The pictogram {self:?} references a power that does not exist ({})",
                self.reference.value
            );
            self.reference.value = format!("Wrong Power {}", self.reference.value);
            self.reference.r#type = ReferenceType::Field;
            true
        } else {
            false
        }
    }
}

impl<'de> Deserialize<'de> for Pictogram {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let value = Value::deserialize(deserializer)?;

        match value {
            Value::String(s) => s.as_str().try_into().map_err(D::Error::custom),
            Value::Mapping(map) => map.try_into().map_err(D::Error::custom),
            _ => Err(D::Error::custom(Error::ValueError {
                expected: "a pictogram".to_owned(),
                current: format!("{:?}", value),
            })),
        }
    }
}

impl TryFrom<&str> for Pictogram {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let fields: Vec<&str> = value.split(',').collect();
        if fields.len() < 3 {
            return Err(Error::LengthError {
                expected_size: 3,
                current_size: fields.len(),
            });
        }
        let value = if fields.len() == 3 {
            fields[1].to_string()
        } else {
            fields[1..fields.len() - 1].join(",")
        };
        let stars: u8 = fields[fields.len() - 1].parse()?;
        if fields[0] == "VIDE" {
            return Ok(Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value,
                stars,
                border: Color::default(),
                text_color: Color::default(),
                reference: Reference::default(),
            });
        }
        let characteristic: Vec<&str> = fields[0].split('_').collect();
        if characteristic.len() != 2 {
            return Err(Error::LengthError {
                expected_size: 2,
                current_size: characteristic.len(),
            });
        }
        let effect = match characteristic[0] {
            "NEG" => Effect::Mandatory,
            "ACT" => Effect::Action,
            "PAS" => Effect::Passive,
            "DEC" => Effect::Triggered,
            "OBJ" => Effect::Object,
            "MAC" => Effect::Device,
            "VIL" => Effect::City,
            s => {
                return Err(Error::ValueError {
                    expected: "NEG | ACT | PAS | DEC | OBJ | MAC | VIL".to_owned(),
                    current: s.to_string(),
                })
            }
        };
        let r#type = match characteristic[1] {
                    "ATT" | "OFF" => Type::Attack,
                    "DEF" => Type::Defense,
                    "INT" | "BUF" => Type::Intern,
                    "EXT" => match effect{
                        Effect::Triggered => Type::OnValidation,
                        Effect::Mandatory => Type::Extern,
                        _ => Type::Outside,
                    }
                    "DEB" => Type::Extern,
                    "DON" => Type::OtherCard,
                    "ADV" => Type::Charm,
                    "SAV" => Type::Safeguard,
                    "HEA" => Type::Heal,
                    "NEG" => Type::Negative,
                    "OTH" | "BLA" => Type::Other,
                    s => return Err(Error::ValueError{
                        expected: "ATT | OFF | DEF | INT | BUF | EXT | DEB | DON | ADV | SAV | HEA | NEG | OTH | BLA".to_owned(),
                        current: s.to_string()})
                };
        Ok(Pictogram {
            effect,
            r#type,
            value,
            stars,
            border: Color::default(),
            text_color: Color::default(),
            reference: Reference::default(),
        })
    }
}

impl TryFrom<Mapping> for Pictogram {
    type Error = Error;

    fn try_from(value: Mapping) -> Result<Self, Self::Error> {
        let effect = match value.get("effect") {
            Some(effect) => serde_yaml::from_value(effect.clone())?,
            None => Effect::default(),
        };

        let r#type = match value.get("type") {
            Some(r#type) => serde_yaml::from_value(r#type.clone())?,
            None => Type::default(),
        };

        let stars = match value.get("stars") {
            Some(stars) => serde_yaml::from_value(stars.clone())?,
            None => u8::default(),
        };

        let border = match value.get("border") {
            Some(border) => serde_yaml::from_value(border.clone())?,
            None => Color::default(),
        };

        let text_color = match value.get("text_color") {
            Some(text_color) => serde_yaml::from_value(text_color.clone())?,
            None => Color::default(),
        };

        let reference = match value.get("reference") {
            Some(reference) => serde_yaml::from_value(reference.clone())?,
            None => Reference::default(),
        };

        let value = match value.get("value") {
            Some(value) => match value {
                Value::String(value) => value.to_owned(),
                Value::Null => "".to_owned(),
                Value::Number(n) => n.to_string(),
                _ => {
                    return Err(Error::ValueError {
                        expected: "a string".to_owned(),
                        current: format!("{:?}", value),
                    })
                }
            },

            None => "".to_owned(),
        };

        Ok(Pictogram {
            effect,
            r#type,
            value,
            stars,
            border,
            text_color,
            reference,
        })
    }
}

impl super::Updatable for Pictogram {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.stars > 3 {
            log::warn!(
                "The number of stars of a pictogram is too high ({}), switching to 3",
                self.stars
            );
            self.stars = 3;
            res = true
        }
        if self.border == Color::default() {
            log::warn!(
                "The color of the border of a pictogram is not defined, using default value : {}",
                default.border
            );
            self.border = default.border;
            res = true
        }
        let closest_color = self.border.closest();
        if self.border != closest_color {
            log::warn!(
                "The color of the border of a pictogram {} was too close to {}, replacing",
                self.border,
                closest_color
            );
            res = true;
            self.border = closest_color;
        }
        if self.text_color == Color::default() {
            log::warn!(
                "The color of the text of a pictogram is not defined, using default value : {}",
                default.text_color
            );
            self.text_color = default.text_color;
            res = true
        }
        let closest_color = self.text_color.closest();
        if self.text_color != closest_color {
            log::warn!(
                "The color of the text of a pictogram {} was too close to {}, replacing",
                self.text_color,
                closest_color
            );
            res = true;
            self.text_color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Pictogram::default()
    }
}

impl Default for Pictogram {
    fn default() -> Self {
        Self {
            effect: Effect::Other,
            r#type: Type::Other,
            value: "".to_owned(),
            stars: 0,
            border: Color::Transparent,
            text_color: Color::Black,
            reference: Reference::default(),
        }
    }
}

impl Render for Vec<Pictogram> {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let mut pictograms = self.clone();
        pictograms.sort_by(|pictogram1, pictogram2| pictogram2.stars.cmp(&pictogram1.stars));

        let pictograms = if pictograms.len() > config.get_max_pictograms() {
            log::warn!(
                "Too many pictograms in the card, only the first {} will be rendered",
                config.get_max_pictograms() - 1
            );
            let mut vec = pictograms[0..config.get_max_pictograms() - 1].to_vec();
            let mut stars = 0;
            for pictogram in pictograms[config.get_max_pictograms() - 1..].iter() {
                stars += 5_u32.pow(pictogram.stars as u32);
            }
            let stars = stars.ilog(5) as u8;
            vec.push(Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars,
                border: Color::Transparent,
                text_color: Color::Black,
                reference: Reference::default(),
            });
            vec
        } else {
            pictograms.to_vec()
        };
        let mut node = Group::new(layer, config.get_position(&Layer::CharacteristicsBand)?);
        let font = config.get_font(layer)?;
        let shape = config.get_pictogram_shape();
        let scale = config.get_scale(layer);
        let positions = config.get_pictogram_positions(
            pictograms
                .len()
                .try_into()
                .critical("More than 255 pictograms is not handle by the software"),
        )?;
        let stars = config.get_pictogram_stars();
        for i in 0..pictograms.len() {
            let mut pictogram = Group::new(
                format!("{}{}", layer, i),
                usvg::Transform::from_scale(scale, scale),
            );

            pictogram.append(Group::from(pictograms[i].get_image()));
            if !pictograms[i].value.is_empty() {
                pictogram.append(usvg::Node::Text(Box::new(RenderedCard::create_text(
                    &format!("{}{}_text", layer, i),
                    &pictograms[i].value,
                    pictograms[i].text_color,
                    shape.text_size,
                    &font,
                    shape.text_position,
                    usvg::TextAnchor::Start,
                ))))
            }

            if pictograms[i].stars > 0 {
                if let Some(positions) = stars.get(&pictograms[i].stars) {
                    let mut star = Group::new(
                        format!("{}{}Star", layer, i),
                        usvg::Transform::from_scale(shape.stars_scale, shape.stars_scale),
                    );
                    star.append(Svg::from_str(
                        include_str!("../logos/pictograms/Star.svg"),
                        "Star",
                    ));
                    for position in positions {
                        let mut position = Group::new(
                            format!("{}{}Star", layer, i),
                            *position - (star.size().critical("The file src/logos/pictograms/Star.svg is not valid: size missing") * shape.stars_scale / 2.0).into(),
                        );
                        position.append(star.clone());
                        pictogram.append(position)
                    }
                } else {
                    return Err(Error::MissingValueError(format!(
                        "{} stars for a pictogram in the config",
                        pictograms[i].stars
                    )));
                }
            }

            let mut tmp = Group::new(format!("{}{} Position", layer, i), positions[i]);
            tmp.append(pictogram);
            node.append(tmp)
        }
        Ok(node.into())
    }
}

impl From<Type> for Color {
    fn from(r#type: Type) -> Self {
        match r#type {
            Type::Attack => Self::Red,
            Type::Defense => Self::Blue,
            Type::Intern => Self::Purple,
            Type::Extern | Type::Charm => Self::Green,
            Type::Outside | Type::OnValidation | Type::OtherCard => Self::Orange,
            Type::Safeguard | Type::Heal => Self::Gray,
            Type::Negative => Self::Brown,
            Type::Other => Self::Black,
        }
    }
}

impl SvgLogo for Effect {
    fn get_image(&self) -> Svg {
        Svg::from_str(
            match self {
                Self::Mandatory => include_str!("../logos/pictograms/Mandatory.svg"),
                Self::Negative => include_str!("../logos/pictograms/Negative.svg"),
                Self::Action => include_str!("../logos/pictograms/Action.svg"),
                Self::Passive => include_str!("../logos/pictograms/Passive.svg"),
                Self::Triggered => include_str!("../logos/pictograms/Triggered.svg"),
                Self::Object => include_str!("../logos/pictograms/Object.svg"),
                Self::Device => include_str!("../logos/pictograms/Device.svg"),
                Self::City => include_str!("../logos/pictograms/City.svg"),
                Self::Other => include_str!("../logos/pictograms/Other.svg"),
            },
            self,
        )
    }
}

impl SvgLogo for Pictogram {
    fn get_image(&self) -> Svg {
        self.effect
            .get_image()
            .set_fill_color(self.get_color())
            .set_stroke_color(self.border)
    }
}

#[cfg(test)]
mod test_module_pictogram {
    #[cfg(test)]
    mod test_enum_effect {
        use super::super::Effect;

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_effect() {
            assert_eq!(
                Effect::Negative,
                serde_yaml::from_str("Negative").expect("A test should never fail")
            );
            assert_eq!(
                "Negative\n",
                serde_yaml::to_string(&Effect::Negative).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Mandatory,
                serde_yaml::from_str("Mandatory").expect("A test should never fail")
            );
            assert_eq!(
                "Mandatory\n",
                serde_yaml::to_string(&Effect::Mandatory).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Action,
                serde_yaml::from_str("Action").expect("A test should never fail")
            );
            assert_eq!(
                "Action\n",
                serde_yaml::to_string(&Effect::Action).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Passive,
                serde_yaml::from_str("Passive").expect("A test should never fail")
            );
            assert_eq!(
                "Passive\n",
                serde_yaml::to_string(&Effect::Passive).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Triggered,
                serde_yaml::from_str("Triggered").expect("A test should never fail")
            );
            assert_eq!(
                "Triggered\n",
                serde_yaml::to_string(&Effect::Triggered).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Object,
                serde_yaml::from_str("Object").expect("A test should never fail")
            );
            assert_eq!(
                "Object\n",
                serde_yaml::to_string(&Effect::Object).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Device,
                serde_yaml::from_str("Device").expect("A test should never fail")
            );
            assert_eq!(
                "Device\n",
                serde_yaml::to_string(&Effect::Device).expect("A test should never fail")
            );

            assert_eq!(
                Effect::City,
                serde_yaml::from_str("City").expect("A test should never fail")
            );
            assert_eq!(
                "City\n",
                serde_yaml::to_string(&Effect::City).expect("A test should never fail")
            );

            assert_eq!(
                Effect::Other,
                serde_yaml::from_str("Other").expect("A test should never fail")
            );
            assert_eq!(
                Effect::Other,
                serde_yaml::from_str("Anything else").expect("A test should never fail")
            );
            assert_eq!(
                "Other\n",
                serde_yaml::to_string(&Effect::Other).expect("A test should never fail")
            );
        }

        #[test]
        fn test_display_effect() {
            assert_eq!("Negative", Effect::Negative.to_string());
            assert_eq!("Mandatory", Effect::Mandatory.to_string());
            assert_eq!("Action", Effect::Action.to_string());
            assert_eq!("Passive", Effect::Passive.to_string());
            assert_eq!("Triggered", Effect::Triggered.to_string());
            assert_eq!("Object", Effect::Object.to_string());
            assert_eq!("Device", Effect::Device.to_string());
            assert_eq!("City", Effect::City.to_string());
            assert_eq!("Other", Effect::Other.to_string());

            assert_eq!("Negative", format!("{:?}", Effect::Negative));
            assert_eq!("Mandatory", format!("{:?}", Effect::Mandatory));
            assert_eq!("Action", format!("{:?}", Effect::Action));
            assert_eq!("Passive", format!("{:?}", Effect::Passive));
            assert_eq!("Triggered", format!("{:?}", Effect::Triggered));
            assert_eq!("Object", format!("{:?}", Effect::Object));
            assert_eq!("Device", format!("{:?}", Effect::Device));
            assert_eq!("City", format!("{:?}", Effect::City));
            assert_eq!("Other", format!("{:?}", Effect::Other));
        }

        #[test]
        fn test_iter_effect() {
            assert_eq!(
                Effect::Negative,
                Effect::iter().nth(0).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Mandatory,
                Effect::iter().nth(1).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Action,
                Effect::iter().nth(2).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Passive,
                Effect::iter().nth(3).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Triggered,
                Effect::iter().nth(4).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Object,
                Effect::iter().nth(5).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Device,
                Effect::iter().nth(6).expect("A test should never fail")
            );
            assert_eq!(
                Effect::City,
                Effect::iter().nth(7).expect("A test should never fail")
            );
            assert_eq!(
                Effect::Other,
                Effect::iter().nth(8).expect("A test should never fail")
            );
        }

        #[test]
        fn test_clone_effect() {
            for effect in Effect::iter() {
                assert_eq!(effect, effect.clone());
            }
        }
    }

    #[cfg(test)]
    mod test_enum_type {
        use super::super::Type;

        use crate::color::Color;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_type() {
            assert_eq!(
                Type::Attack,
                serde_yaml::from_str("Attack").expect("A test should never fail")
            );
            assert_eq!(
                "Attack\n",
                serde_yaml::to_string(&Type::Attack).expect("A test should never fail")
            );

            assert_eq!(
                Type::Defense,
                serde_yaml::from_str("Defense").expect("A test should never fail")
            );
            assert_eq!(
                "Defense\n",
                serde_yaml::to_string(&Type::Defense).expect("A test should never fail")
            );

            assert_eq!(
                Type::Intern,
                serde_yaml::from_str("Intern").expect("A test should never fail")
            );
            assert_eq!(
                "Intern\n",
                serde_yaml::to_string(&Type::Intern).expect("A test should never fail")
            );

            assert_eq!(
                Type::Extern,
                serde_yaml::from_str("Extern").expect("A test should never fail")
            );
            assert_eq!(
                "Extern\n",
                serde_yaml::to_string(&Type::Extern).expect("A test should never fail")
            );

            assert_eq!(
                Type::Charm,
                serde_yaml::from_str("Charm").expect("A test should never fail")
            );
            assert_eq!(
                "Charm\n",
                serde_yaml::to_string(&Type::Charm).expect("A test should never fail")
            );

            assert_eq!(
                Type::Outside,
                serde_yaml::from_str("Outside").expect("A test should never fail")
            );
            assert_eq!(
                "Outside\n",
                serde_yaml::to_string(&Type::Outside).expect("A test should never fail")
            );

            assert_eq!(
                Type::Safeguard,
                serde_yaml::from_str("Safeguard").expect("A test should never fail")
            );
            assert_eq!(
                "Safeguard\n",
                serde_yaml::to_string(&Type::Safeguard).expect("A test should never fail")
            );

            assert_eq!(
                Type::OnValidation,
                serde_yaml::from_str("OnValidation").expect("A test should never fail")
            );
            assert_eq!(
                "OnValidation\n",
                serde_yaml::to_string(&Type::OnValidation).expect("A test should never fail")
            );

            assert_eq!(
                Type::Heal,
                serde_yaml::from_str("Heal").expect("A test should never fail")
            );
            assert_eq!(
                "Heal\n",
                serde_yaml::to_string(&Type::Heal).expect("A test should never fail")
            );

            assert_eq!(
                Type::OtherCard,
                serde_yaml::from_str("OtherCard").expect("A test should never fail")
            );
            assert_eq!(
                "OtherCard\n",
                serde_yaml::to_string(&Type::OtherCard).expect("A test should never fail")
            );

            assert_eq!(
                Type::Negative,
                serde_yaml::from_str("Negative").expect("A test should never fail")
            );
            assert_eq!(
                "Negative\n",
                serde_yaml::to_string(&Type::Negative).expect("A test should never fail")
            );

            assert_eq!(
                Type::Other,
                serde_yaml::from_str("Other").expect("A test should never fail")
            );
            assert_eq!(
                Type::Other,
                serde_yaml::from_str("Anything else").expect("A test should never fail")
            );
            assert_eq!(
                "Other\n",
                serde_yaml::to_string(&Type::Other).expect("A test should never fail")
            );
        }

        #[test]
        fn test_debug_type() {
            assert_eq!("Attack", format!("{:?}", Type::Attack));
            assert_eq!("Defense", format!("{:?}", Type::Defense));
            assert_eq!("Intern", format!("{:?}", Type::Intern));
            assert_eq!("Extern", format!("{:?}", Type::Extern));
            assert_eq!("Charm", format!("{:?}", Type::Charm));
            assert_eq!("Outside", format!("{:?}", Type::Outside));
            assert_eq!("Safeguard", format!("{:?}", Type::Safeguard));
            assert_eq!("OnValidation", format!("{:?}", Type::OnValidation));
            assert_eq!("Heal", format!("{:?}", Type::Heal));
            assert_eq!("OtherCard", format!("{:?}", Type::OtherCard));
            assert_eq!("Negative", format!("{:?}", Type::Negative));
            assert_eq!("Other", format!("{:?}", Type::Other));
        }

        #[test]
        fn test_iter_type() {
            assert_eq!(
                Type::Attack,
                Type::iter().nth(0).expect("A test should never fail")
            );
            assert_eq!(
                Type::Defense,
                Type::iter().nth(1).expect("A test should never fail")
            );
            assert_eq!(
                Type::Intern,
                Type::iter().nth(2).expect("A test should never fail")
            );
            assert_eq!(
                Type::Extern,
                Type::iter().nth(3).expect("A test should never fail")
            );
            assert_eq!(
                Type::Charm,
                Type::iter().nth(4).expect("A test should never fail")
            );
            assert_eq!(
                Type::Outside,
                Type::iter().nth(5).expect("A test should never fail")
            );
            assert_eq!(
                Type::Safeguard,
                Type::iter().nth(6).expect("A test should never fail")
            );
            assert_eq!(
                Type::OnValidation,
                Type::iter().nth(7).expect("A test should never fail")
            );
            assert_eq!(
                Type::Heal,
                Type::iter().nth(8).expect("A test should never fail")
            );
            assert_eq!(
                Type::OtherCard,
                Type::iter().nth(9).expect("A test should never fail")
            );
            assert_eq!(
                Type::Negative,
                Type::iter().nth(10).expect("A test should never fail")
            );
            assert_eq!(
                Type::Other,
                Type::iter().nth(11).expect("A test should never fail")
            );
        }

        #[test]
        fn test_clone_type() {
            for type_ in Type::iter() {
                assert_eq!(type_, type_.clone());
            }
        }

        #[test]
        fn test_color_type() {
            assert_eq!(Color::from(Type::Attack), Color::Red);
            assert_eq!(Color::from(Type::Defense), Color::Blue);
            assert_eq!(Color::from(Type::Intern), Color::Purple);
            assert_eq!(Color::from(Type::Extern), Color::Green);
            assert_eq!(Color::from(Type::Outside), Color::Orange);
            assert_eq!(Color::from(Type::OnValidation), Color::Orange);
            assert_eq!(Color::from(Type::OtherCard), Color::Orange);
            assert_eq!(Color::from(Type::Safeguard), Color::Gray);
            assert_eq!(Color::from(Type::Heal), Color::Gray);
            assert_eq!(Color::from(Type::Negative), Color::Brown);
            assert_eq!(Color::from(Type::Other), Color::Black);
        }
    }

    mod test_enum_reference_type {
        use super::super::ReferenceType;

        use strum::IntoEnumIterator;

        #[test]
        fn test_debug() {
            assert_eq!("Field", format!("{:?}", ReferenceType::Field));
            assert_eq!("Power", format!("{:?}", ReferenceType::Power));
        }

        #[test]
        fn test_iter() {
            let mut iter = ReferenceType::iter();

            assert_eq!(
                ReferenceType::Power,
                iter.next().expect("A test should never fail")
            );
            assert_eq!(
                ReferenceType::Field,
                iter.next().expect("A test should never fail")
            );
        }

        #[test]
        fn test_clone() {
            for reference_type in ReferenceType::iter() {
                assert_eq!(reference_type, reference_type.clone());
            }
        }

        #[test]
        fn test_serde() {
            assert_eq!(
                "Field\n",
                serde_yaml::to_string(&ReferenceType::Field).expect("A test should never fail")
            );
            assert_eq!(
                "Power\n",
                serde_yaml::to_string(&ReferenceType::Power).expect("A test should never fail")
            );
            assert_eq!(
                ReferenceType::Field,
                serde_yaml::from_str("Field").expect("A test should never fail")
            );
            assert_eq!(
                ReferenceType::Power,
                serde_yaml::from_str("Power").expect("A test should never fail")
            );
        }

        #[test]
        fn test_display() {
            assert_eq!("Field", format!("{}", ReferenceType::Field));
            assert_eq!("Power", format!("{}", ReferenceType::Power));
        }
    }

    mod test_struct_reference {
        use super::super::Reference;

        use super::super::ReferenceType;

        #[test]
        fn test_serde() {
            let reference = Reference {
                r#type: ReferenceType::Field,
                value: "test".to_owned(),
            };

            assert_eq!(
                "type: Field\nvalue: test\n",
                serde_yaml::to_string(&reference).expect("A test should never fail")
            );
            assert_eq!(
                reference,
                serde_yaml::from_str("type: Field\nvalue: test").expect("A test should never fail")
            );

            let reference = Reference {
                r#type: ReferenceType::Field,
                value: "test".to_owned(),
            };

            assert_eq!(
                "type: Field\nvalue: test\n",
                serde_yaml::to_string(&reference).expect("A test should never fail")
            );
            assert_eq!(
                reference,
                serde_yaml::from_str("value: test").expect("A test should never fail")
            );

            let reference = Reference {
                r#type: ReferenceType::Field,
                value: "".to_owned(),
            };

            assert_eq!(
                "type: Field\nvalue: ''\n",
                serde_yaml::to_string(&reference).expect("A test should never fail")
            );
            assert_eq!(
                reference,
                serde_yaml::from_str("type: Field\n").expect("A test should never fail")
            );
        }

        #[test]
        fn test_debug() {
            assert_eq!(
                format!(
                    "Reference {{ type: {}, value: \"test\" }}",
                    ReferenceType::Field
                ),
                format!(
                    "{:?}",
                    Reference {
                        r#type: ReferenceType::Field,
                        value: "test".to_owned()
                    }
                )
            );
        }

        #[test]
        fn test_clone() {
            assert_eq!(
                Reference {
                    r#type: ReferenceType::Field,
                    value: "test".to_owned()
                }
                .clone(),
                Reference {
                    r#type: ReferenceType::Field,
                    value: "test".to_owned()
                }
            );
        }

        #[test]
        fn test_default() {
            assert_eq!(
                Reference::default(),
                Reference {
                    r#type: ReferenceType::Field,
                    value: "".to_owned()
                }
            );
        }
    }

    #[cfg(test)]
    mod test_struct_pictogram {
        use super::super::Pictogram;

        use super::super::super::Updatable;
        use super::super::{Effect, Reference, ReferenceType, Type};
        use crate::card::logos::SvgLogo;
        use crate::color::Color;
        use crate::error::Error;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_pictogram() {
            let pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "".to_owned(),
                stars: 0,
                border: Color::Black,
                text_color: Color::Pink,
                reference: Reference {
                    r#type: ReferenceType::Field,
                    value: "test".to_owned(),
                },
            };

            let s = "effect: Negative\ntype: Negative\nvalue: ''\nstars: 0\nborder: Black\ntext_color: Pink\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(!test.update(&pictogram));
            assert_eq!(test, pictogram);
            assert_eq!(
                s,
                serde_yaml::to_string(&pictogram).expect("A test should never fail")
            );

            let s = "type: Negative\nvalue: \nstars: 0\nborder: Black\ntext_color: Pink\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(!test.update(&pictogram));
            assert_eq!(
                test,
                Pictogram {
                    effect: Effect::Other,
                    ..pictogram.clone()
                }
            );

            let s = "effect: Negative\nvalue: \nstars: 0\nborder: Black\ntext_color: Pink\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(!test.update(&pictogram));
            assert_eq!(
                test,
                Pictogram {
                    r#type: Type::Other,
                    ..pictogram.clone()
                }
            );

            let s = "effect: Negative\ntype: Negative\nstars: 0\nborder: Black\ntext_color: Pink\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(!test.update(&pictogram));
            assert_eq!(test, pictogram);

            let s = "effect: Negative\ntype: Negative\nvalue: \nborder: Black\ntext_color: Pink\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(!test.update(&pictogram));
            assert_eq!(test, pictogram);

            let s = "effect: Negative\ntype: Negative\nvalue: \nstars: 0\ntext_color: Pink\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(test.update(&pictogram));
            assert_eq!(test, pictogram);

            let s = "effect: Negative\ntype: Negative\nvalue: ''\nstars: 0\nborder: Black\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(test.update(&pictogram));
            assert_eq!(test, pictogram);

            let s = "effect: Negative\ntype: Negative\nvalue: ''\nstars: 0\nborder: Black\ntext_color: Pink\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(!test.update(&pictogram));
            assert_eq!(
                test,
                Pictogram {
                    reference: Reference::default(),
                    ..pictogram.clone()
                }
            );

            let s = "effect: Negative\ntype: Negative\nvalue: 10\nstars: 0\nborder: Black\nreference:\n  type: Field\n  value: test\n";
            let mut test = serde_yaml::from_str::<Pictogram>(s).expect("A test should never fail");
            assert!(test.update(&pictogram));
            assert_eq!(
                test,
                Pictogram {
                    value: "10".to_owned(),
                    ..pictogram
                }
            );

            let s = "effect: Negative\ntype: Negative\nvalue: []\nstars: 0\nborder: Black\ntext_color: Pink\n";
            assert!(serde_yaml::from_str::<Pictogram>(s).is_err());

            let s =
                "- effect: Negative\n- type: Negative\n- value: \n- stars: 0\n- border: Black\n";
            assert!(serde_yaml::from_str::<Pictogram>(s).is_err());
        }

        #[test]
        fn test_debug_pictogram() {
            let pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "".to_owned(),
                stars: 0,
                border: Color::Black,
                text_color: Color::Pink,
                reference: Reference {
                    r#type: ReferenceType::Field,
                    value: "test".to_owned(),
                },
            };
            assert_eq!(format!("{:?}", pictogram), format!("Pictogram {{ effect: Negative, type: Negative, value: \"\", stars: 0, border: Black, text_color: Pink, reference: {:?} }}",Reference {
                r#type: ReferenceType::Field,
                value: "test".to_owned(),
            }) );
        }

        #[test]
        fn test_default_pictogram() {
            let pictogram = Pictogram::default();
            assert_eq!(
                pictogram,
                Pictogram {
                    effect: Effect::Other,
                    r#type: Type::Other,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::Transparent,
                    text_color: Color::Black,
                    reference: Reference::default()
                }
            );

            assert_eq!(Pictogram::default(), Pictogram::get_default(),)
        }

        #[test]
        fn test_get_color_pictogram() {
            let pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "".to_owned(),
                stars: 0,
                border: Color::Transparent,
                text_color: Color::Black,
                reference: Reference::default(),
            };
            assert_eq!(pictogram.get_color(), Type::Negative.into());
        }

        #[test]
        fn test_get_image_pictogram() {
            let mut stars = 0;
            for effect in Effect::iter() {
                for r#type in Type::iter() {
                    let _ = Pictogram {
                        effect,
                        r#type,
                        value: effect.to_string(),
                        stars,
                        border: Color::Black,
                        text_color: Color::Black,
                        reference: Reference::default(),
                    }
                    .get_image();
                    stars = (stars + 1) % 4
                }
            }
        }

        #[test]
        fn test_deserialize_old() {
            let pictogram =
                serde_yaml::from_str::<Pictogram>("VIDE,,0").expect("A test should never fail");
            assert_eq!(
                pictogram,
                Pictogram {
                    effect: Effect::Other,
                    r#type: Type::Other,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default()
                }
            );

            assert_eq!(
                serde_yaml::from_str::<Pictogram>("VIDE,,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Other,
                    r#type: Type::Other,
                    value: ",".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default()
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("VIDE,0").map_err(Error::from),
                Err(Error::LengthError {
                    expected_size: 3,
                    current_size: 2
                })
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("VIDE,,a").map_err(|e| e.to_string()),
                Err("invalid digit found in string".to_owned())
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG,,0").map_err(Error::from),
                Err(Error::LengthError {
                    expected_size: 2,
                    current_size: 1
                })
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_NEG_NEG,,0").map_err(Error::from),
                Err(Error::LengthError {
                    expected_size: 2,
                    current_size: 3
                })
            );

            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("ACT_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Action,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("PAS_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Passive,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("DEC_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Triggered,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("OBJ_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Object,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("MAC_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Device,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("VIL_ATT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::City,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("_ATT,,0").map_err(Error::from),
                Err(Error::ValueError {
                    expected: "NEG | ACT | PAS | DEC | OBJ | MAC | VIL".to_owned(),
                    current: "".to_owned(),
                })
            );

            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_OFF,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_DEF,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Defense,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_INT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Intern,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_BUF,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Intern,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_EXT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Extern,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("DEC_EXT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Triggered,
                    r#type: Type::OnValidation,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("ACT_EXT,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Action,
                    r#type: Type::Outside,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_DEB,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Extern,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_DON,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::OtherCard,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_ADV,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Charm,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_SAV,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Safeguard,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_HEA,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Heal,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_NEG,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Negative,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_OTH,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Other,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_BLA,,0").expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Other,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
            assert_eq!(
                serde_yaml::from_str::<Pictogram>("NEG_,,0").map_err(Error::from),
                Err(Error::ValueError{
                            expected: "ATT | OFF | DEF | INT | BUF | EXT | DEB | DON | ADV | SAV | HEA | NEG | OTH | BLA".to_owned(),
                            current: "".to_owned()}
                )
            );

            assert_eq!(
                serde_yaml::from_str::<Pictogram>(
                    "effect: Mandatory\ntype: Attack\nvalue: \"\"\nstars: 0"
                )
                .expect("A test should never fail"),
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Attack,
                    value: "".to_owned(),
                    stars: 0,
                    border: Color::default(),
                    text_color: Color::default(),
                    reference: Reference::default(),
                }
            );
        }
    }

    #[cfg(test)]
    mod test_struct_vec_pictogram {
        use super::super::Pictogram;

        use super::super::{Effect, Reference, Type};
        use crate::card::illustration::{Mode, Orientation};
        use crate::color::Color;
        use crate::error::Error;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use log_tester::LogTester;
        use strum::IntoEnumIterator;

        #[test]
        fn test_render_vec_pictogram() {
            let pictograms = vec![
                Pictogram {
                    effect: Effect::Negative,
                    r#type: Type::Negative,
                    value: "test".to_owned(),
                    stars: 0,
                    border: Color::Transparent,
                    text_color: Color::Black,
                    reference: Reference::default(),
                },
                Pictogram {
                    effect: Effect::Mandatory,
                    r#type: Type::Defense,
                    value: "test".to_owned(),
                    stars: 1,
                    border: Color::Red,
                    text_color: Color::Purple,
                    reference: Reference::default(),
                },
                Pictogram {
                    effect: Effect::Triggered,
                    r#type: Type::Attack,
                    value: "test".to_owned(),
                    stars: 2,
                    border: Color::Pink,
                    text_color: Color::Green,
                    reference: Reference::default(),
                },
                Pictogram {
                    effect: Effect::Action,
                    r#type: Type::Charm,
                    value: "test".to_owned(),
                    stars: 3,
                    border: Color::Blue,
                    text_color: Color::White,
                    reference: Reference::default(),
                },
            ];

            let mut config = Config::default();

            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    config.set_orientation(orientation);
                    config.set_mode(mode);
                    let render = pictograms
                        .render(&Layer::Pictograms, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::Pictograms));

                    let pictograms: Vec<Pictogram> = pictograms
                        .iter()
                        .cycle()
                        .take(config.get_max_pictograms() + 1)
                        .map(|elt| elt.clone())
                        .collect();
                    let render = pictograms
                        .render(&Layer::Pictograms, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::Pictograms));
                }
            }

            let pictograms = vec![Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 4,
                border: Color::Black,
                text_color: Color::Black,
                reference: Reference::default(),
            }];

            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    config.set_orientation(orientation);
                    config.set_mode(mode);
                    let render = pictograms.render(&Layer::Pictograms, &config, None);

                    assert!(render.is_err());
                    assert_eq!(
                        render.expect_err("A test should never fail"),
                        Error::MissingValueError(
                            "4 stars for a pictogram in the config".to_owned()
                        )
                    );
                }
            }
        }

        #[test]
        fn test_to_many_pictogram() {
            LogTester::start();
            let config = Config::default();
            let pictograms = vec![Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::Black,
                text_color: Color::Black,
                reference: Reference::default(),
            }];
            let pictograms: Vec<Pictogram> = pictograms
                .iter()
                .cycle()
                .take(config.get_max_pictograms() + 1)
                .map(|elt| elt.clone())
                .collect();
            let render = pictograms
                .render(&Layer::Pictograms, &config, None)
                .expect("A test should never fail");
            assert!(render.has_node(&Layer::Pictograms));

            assert!(LogTester::contains(
                log::Level::Warn,
                &format!(
                    "Too many pictograms in the card, only the first {} will be rendered",
                    config.get_max_pictograms() - 1
                )
            ));
        }
    }

    mod test_log_updatable {
        use log::Level;
        use log_tester::LogTester;

        use super::super::super::Updatable;
        use super::super::{Effect, Pictogram, Reference, Type};
        use crate::color::Color;

        #[test]
        fn test_stars_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::Blue,
                text_color: Color::Purple,
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            for stars in 0..=3 {
                pictogram.stars = stars;
                assert!(!pictogram.update(&default));
                assert_eq!(pictogram.effect, Effect::Negative);
                assert_eq!(pictogram.r#type, Type::Negative);
                assert_eq!(pictogram.value, "test");
                assert_eq!(pictogram.stars, stars);
                assert_eq!(pictogram.border, Color::Blue);
                assert_eq!(pictogram.text_color, Color::Purple);
                assert_eq!(LogTester::len(), 0);
            }

            pictogram.stars = 4;
            assert!(pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::Negative);
            assert_eq!(pictogram.r#type, Type::Negative);
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Blue);
            assert_eq!(pictogram.text_color, Color::Purple);
            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The number of stars of a pictogram is too high (4), switching to 3"
            ));
        }

        #[test]
        fn test_border_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::default(),
                text_color: Color::Purple,
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            assert!(pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::Negative);
            assert_eq!(pictogram.r#type, Type::Negative);
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Yellow);
            assert_eq!(pictogram.text_color, Color::Purple);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the border of a pictogram is not defined, using default value : Yellow"));
        }

        #[test]
        fn test_close_border_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::Custom(0, 0, 0, 0),
                text_color: Color::Purple,
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            assert!(pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::Negative);
            assert_eq!(pictogram.r#type, Type::Negative);
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Transparent);
            assert_eq!(pictogram.text_color, Color::Purple);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the border of a pictogram Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_text_color_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::Purple,
                text_color: Color::default(),
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            assert!(pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::Negative);
            assert_eq!(pictogram.r#type, Type::Negative);
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Purple);
            assert_eq!(pictogram.text_color, Color::Pink);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the text of a pictogram is not defined, using default value : Pink"
            ));
        }

        #[test]
        fn test_close_text_color_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::Purple,
                text_color: Color::Custom(0, 0, 0, 0),
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            assert!(pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::Negative);
            assert_eq!(pictogram.r#type, Type::Negative);
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Purple);
            assert_eq!(pictogram.text_color, Color::Transparent);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the text of a pictogram Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_effect_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::default(),
                r#type: Type::Negative,
                value: "test".to_owned(),
                stars: 3,
                border: Color::Purple,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            assert!(!pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::default());
            assert_eq!(pictogram.r#type, Type::Negative);
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Purple);
            assert_eq!(pictogram.text_color, Color::Pink);

            assert_eq!(LogTester::len(), 0);
        }

        #[test]
        fn test_type_pictogram() {
            LogTester::start();
            let mut pictogram = Pictogram {
                effect: Effect::Negative,
                r#type: Type::default(),
                value: "test".to_owned(),
                stars: 3,
                border: Color::Purple,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            let default = Pictogram {
                effect: Effect::Other,
                r#type: Type::Other,
                value: "".to_owned(),
                stars: 0,
                border: Color::Yellow,
                text_color: Color::Pink,
                reference: Reference::default(),
            };

            assert!(!pictogram.update(&default));
            assert_eq!(pictogram.effect, Effect::Negative);
            assert_eq!(pictogram.r#type, Type::default());
            assert_eq!(pictogram.value, "test");
            assert_eq!(pictogram.stars, 3);
            assert_eq!(pictogram.border, Color::Purple);
            assert_eq!(pictogram.text_color, Color::Pink);

            assert_eq!(LogTester::len(), 0);
        }
    }
}
