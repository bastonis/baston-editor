// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the config for rendering the symbols

use super::Symbol;
use crate::color::Color;
use crate::error::LogError;
use crate::render::Point;

use std::collections::HashMap;

use lazy_static::lazy_static;
use serde::Deserialize;

lazy_static! {
    /// The configuration file
    static ref CONFIG: Config = serde_yaml::from_str::<Config>(include_str!("../../logos/symbols/Config.yml")).critical("Error during deserializing the file src/logos/symbols/Config.yml, this should not have happen.");
}

/// Represents the configuration for every symbol
#[derive(Deserialize, Clone)]
#[serde(transparent)]
pub struct Config {
    /// The configuration for every symbol
    symbols: HashMap<Symbol, SymbolConfig>,
}

#[derive(Deserialize, Clone, Copy)]
/// Represents the configuration for a symbol image
pub struct SymbolConfig {
    /// The center point of the symbol
    pub center: Point,
    /// The diameter of the symbol (if specified)
    pub diameter: Option<u32>,
    /// The position of the text (if specified)
    pub text_position: Option<Point>,
    /// The size of the text (if specified)
    pub text_size: Option<f32>,
    /// The color of the text
    ///
    /// If not specified or equal to [Color::Default], [Color::RedStat] will be used
    #[serde(default)]
    pub text_color: Color,
}

impl Config {
    /// Get the configuration for a symbol
    pub fn get_symbol(&self, symbol: &Symbol) -> &SymbolConfig {
        self.symbols.get(symbol).unwrap_or_else(|| {
            panic!(
                "There is an error in the src/logos/symbols/Config.yml for the symbol : {}.",
                symbol
            )
        })
    }
}

impl Default for Config {
    fn default() -> Self {
        CONFIG.clone()
    }
}

#[cfg(test)]
mod test_module_symbol_config {
    use super::Config;

    use crate::card::symbol::Symbol;
    use strum::IntoEnumIterator;

    #[test]
    fn test_yaml() {
        let config = Config::default();
        for symbol in Symbol::iter() {
            config.get_symbol(&symbol);
        }
    }
}

#[cfg(test)]
mod test_module_symbol_show {
    use super::Config as SymbolConfig;
    use crate::card::symbol::{Symbol, SymbolField};
    use crate::error::Error;
    use crate::render::{config::Config, Group, Layer};

    use std::fs::File;
    use std::io::Write;
    use strum::IntoEnumIterator;
    use tempfile::Builder;
    use usvg::{AspectRatio, NonZeroRect, Size, Tree, ViewBox};
    use usvg::{PostProcessingSteps, TreePostProc, TreeWriting, XmlOptions};

    fn convert_text(mut tree: Tree) -> Result<Tree, Error> {
        tree.postprocess(
            PostProcessingSteps {
                convert_text_into_paths: true,
            },
            &Config::default().get_font_database()?,
        );
        Ok(tree)
    }

    fn render(symbol: &SymbolField, open_output: bool) {
        let symbol_config = SymbolConfig::default().get_symbol(&symbol.symbol).clone();
        let config = Config::default();
        let font = config
            .get_font(&Layer::Symbols)
            .expect("A test should never fail");
        let mut group = Group::default();
        let width = config
            .get_width(&Layer::Symbols)
            .expect("A test should never fail");
        group.append(symbol.render(&config, &symbol_config, &font, width));
        let tree = convert_text(Tree {
            size: Size::from_wh(width, width).expect("A test should never fail"),
            view_box: ViewBox {
                rect: NonZeroRect::from_ltrb(-width / 2., -width / 2., width / 2., width / 2.)
                    .expect("A test should never fail"),
                aspect: AspectRatio::default(),
            },
            root: group.into(),
        })
        .expect("A test should never fail");
        let tempfile = Builder::new()
            .suffix(".svg")
            .prefix(&symbol.symbol.to_string())
            .tempfile()
            .expect("A test should never fail");
        File::create(tempfile.path().to_str().expect("A test should never fail"))
            .expect("A test should never fail")
            .write_all(tree.to_string(&XmlOptions::default()).as_bytes())
            .expect("A test should never fail");
        if open_output {
            let (_, path) = tempfile.keep().expect("A test should never fail");
            open::that(path).expect("A test should never fail")
        }
    }

    fn render_all(symbol: SymbolField, open_output: bool) {
        render(&symbol, open_output);
        render(
            &SymbolField {
                value: 10.into(),
                ..symbol.clone()
            },
            open_output,
        );
        render(
            &SymbolField {
                value: "D10".try_into().expect("A test should never fail"),
                ..symbol
            },
            open_output,
        );
    }

    #[test]
    fn test_render() {
        for symbol in Symbol::iter() {
            render_all(symbol.into(), false);
        }
    }

    #[test]
    #[ignore]
    fn test_arc() {
        render_all(Symbol::Arc.into(), true)
    }

    #[test]
    #[ignore]
    fn test_casque() {
        render_all(Symbol::Casque.into(), true)
    }

    #[test]
    #[ignore]
    fn test_couronne() {
        render_all(Symbol::Couronne.into(), true)
    }

    #[test]
    #[ignore]
    fn test_serviteur() {
        render_all(Symbol::Serviteur.into(), true)
    }

    #[test]
    #[ignore]
    fn test_crocs() {
        render_all(Symbol::Crocs.into(), true)
    }

    #[test]
    #[ignore]
    fn test_epee() {
        render_all(Symbol::Epee.into(), true)
    }

    #[test]
    #[ignore]
    fn test_poing() {
        render_all(Symbol::Poing.into(), true)
    }

    #[test]
    #[ignore]
    fn test_etendard() {
        render_all(Symbol::Etendard.into(), true)
    }

    #[test]
    #[ignore]
    fn test_flamme() {
        render_all(Symbol::Flamme.into(), true)
    }

    #[test]
    #[ignore]
    fn test_hurlante() {
        render_all(Symbol::Hurlante.into(), true)
    }

    #[test]
    #[ignore]
    fn test_medic() {
        render_all(Symbol::Medic.into(), true)
    }

    #[test]
    #[ignore]
    fn test_oeil() {
        render_all(Symbol::Oeil.into(), true)
    }

    #[test]
    #[ignore]
    fn test_collision() {
        render_all(Symbol::Collision.into(), true)
    }

    #[test]
    #[ignore]
    fn test_ecrasement() {
        render_all(Symbol::Ecrasement.into(), true)
    }

    #[test]
    #[ignore]
    fn test_fanatique() {
        render_all(Symbol::Fanatique.into(), true)
    }

    #[test]
    #[ignore]
    fn test_fuite() {
        render_all(Symbol::Fuite.into(), true)
    }

    #[test]
    #[ignore]
    fn test_gel() {
        render_all(Symbol::Gel.into(), true)
    }

    #[test]
    #[ignore]
    fn test_killer() {
        render_all(Symbol::Killer.into(), true)
    }

    #[test]
    #[ignore]
    fn test_necromancie() {
        render_all(Symbol::Necromancie.into(), true)
    }

    #[test]
    #[ignore]
    fn test_poison() {
        render_all(Symbol::Poison.into(), true)
    }

    #[test]
    #[ignore]
    fn test_raz_de_maree() {
        render_all(Symbol::RazDeMaree.into(), true)
    }

    #[test]
    #[ignore]
    fn test_tornade() {
        render_all(Symbol::Tornade.into(), true)
    }

    #[test]
    #[ignore]
    fn test_eblouissement() {
        render_all(Symbol::Eblouissement.into(), true)
    }

    #[test]
    #[ignore]
    fn test_tenebre() {
        render_all(Symbol::Tenebre.into(), true)
    }

    #[test]
    #[ignore]
    fn test_incendie() {
        render_all(Symbol::Incendie.into(), true)
    }

    #[test]
    #[ignore]
    fn test_baton_magique() {
        render_all(Symbol::BatonMagique.into(), true)
    }

    #[test]
    #[ignore]
    fn test_providence() {
        render_all(Symbol::Providence.into(), true)
    }

    #[test]
    #[ignore]
    fn test_magie_demoniaque() {
        render_all(Symbol::MagieDemoniaque.into(), true)
    }

    #[test]
    #[ignore]
    fn test_magie_incantatoire() {
        render_all(Symbol::MagieIncantatoire.into(), true)
    }

    #[test]
    #[ignore]
    fn test_devastator() {
        render_all(Symbol::Devastator.into(), true)
    }

    #[test]
    #[ignore]
    fn test_fusil_elite() {
        render_all(Symbol::FusilElite.into(), true)
    }

    #[test]
    #[ignore]
    fn test_magie_necromantique() {
        render_all(Symbol::MagieNecromantique.into(), true)
    }

    #[test]
    #[ignore]
    fn test_paix() {
        render_all(Symbol::Paix.into(), true)
    }

    #[test]
    #[ignore]
    fn test_portail() {
        render_all(Symbol::Portail.into(), true)
    }

    #[test]
    #[ignore]
    fn test_prescience() {
        render_all(Symbol::Prescience.into(), true)
    }

    #[test]
    #[ignore]
    fn test_sablier() {
        render_all(Symbol::Sablier.into(), true)
    }

    #[test]
    #[ignore]
    fn test_blaster() {
        render_all(Symbol::Blaster.into(), true)
    }

    #[test]
    #[ignore]
    fn test_paralyseur() {
        render_all(Symbol::Paralyseur.into(), true)
    }

    #[test]
    #[ignore]
    fn test_sabre_bleu() {
        render_all(Symbol::SabreBleu.into(), true)
    }

    #[test]
    #[ignore]
    fn test_sabre_rouge() {
        render_all(Symbol::SabreRouge.into(), true)
    }

    #[test]
    #[ignore]
    fn test_sabre_violet() {
        render_all(Symbol::SabreViolet.into(), true)
    }

    #[test]
    #[ignore]
    fn test_sabre_vert() {
        render_all(Symbol::SabreVert.into(), true)
    }

    #[test]
    #[ignore]
    fn test_dragon_ball_1() {
        render_all(Symbol::DragonBall1.into(), true)
    }

    #[test]
    #[ignore]
    fn test_dragon_ball_2() {
        render_all(Symbol::DragonBall2.into(), true)
    }

    #[test]
    #[ignore]
    fn test_dragon_ball_3() {
        render_all(Symbol::DragonBall3.into(), true)
    }

    #[test]
    #[ignore]
    fn test_dragon_ball_4() {
        render_all(Symbol::DragonBall4.into(), true)
    }

    #[test]
    #[ignore]
    fn test_force_lumineuse() {
        render_all(Symbol::ForceLumineuse.into(), true)
    }

    #[test]
    #[ignore]
    fn test_force_obscure() {
        render_all(Symbol::ForceObscure.into(), true)
    }

    #[test]
    #[ignore]
    fn test_wakfu() {
        render_all(Symbol::Wakfu.into(), true)
    }

    #[test]
    #[ignore]
    fn test_sorcellerie() {
        render_all(Symbol::Sorcellerie.into(), true)
    }

    #[test]
    #[ignore]
    fn test_credits() {
        render_all(Symbol::Credits.into(), true)
    }

    #[test]
    #[ignore]
    fn test_bouclier() {
        render_all(Symbol::Bouclier.into(), true)
    }

    #[test]
    #[ignore]
    fn test_bloque() {
        render_all(Symbol::Bloque.into(), true)
    }

    #[test]
    #[ignore]
    fn test_retourne() {
        render_all(Symbol::Retourne.into(), true)
    }

    #[test]
    #[ignore]
    fn test_flop_flop() {
        render_all(Symbol::FlopFlop.into(), true)
    }

    #[test]
    #[ignore]
    fn test_zizanie() {
        render_all(Symbol::Zizanie.into(), true)
    }

    #[test]
    #[ignore]
    fn test_traitre() {
        render_all(Symbol::Traitre.into(), true)
    }

    #[test]
    #[ignore]
    fn test_attaque_sournoise() {
        render_all(Symbol::AttaqueSournoise.into(), true)
    }

    #[test]
    #[ignore]
    fn test_pitie() {
        render_all(Symbol::Pitie.into(), true)
    }

    #[test]
    #[ignore]
    fn test_null() {
        render_all(Symbol::Null.into(), true)
    }

    #[test]
    #[ignore]
    fn test_empty() {
        render_all(Symbol::Empty.into(), true)
    }
}
