// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the old symbols, the python's ones and the simple rust ones

use crate::color::Color;
use crate::error::{Error, LogError};
use crate::types::CharacteristicValue;

use regex::RegexBuilder;
use serde::{de::Error as SerdeError, Deserialize, Deserializer};
use serde_yaml::{Mapping, Value};
use strum_macros::EnumIter;

/// Represents a OldSymbol
#[derive(Debug, Clone, PartialEq, EnumIter, Default)]
pub enum OldSymbol {
    Arc,
    Casque,
    Couronne,
    Crocs,
    Epee,
    Poing,
    Etendard,
    Flamme,
    Hurlante,
    Medic,
    Oeil,
    Collision,
    Ecrasement,
    Fanatique,
    Fuite,
    Gel,
    Killer,
    Necromancie,
    Poison(CharacteristicValue),
    RazDeMaree,
    Tornade,
    Eblouissement,
    Tenebre,
    BatonMagique,
    Providence,
    MagieDemoniaque,
    MagieIncantatoire,
    Devastator,
    FusilElite,
    MagieNecromantique,
    Paix,
    Portail,
    Prescience(CharacteristicValue),
    Sablier,
    Blaster,
    Paralyseur,
    SabreBleu,
    SabreRouge,
    SabreViolet,
    SabreVert,
    DragonBall1,
    DragonBall2,
    DragonBall3,
    DragonBall4,
    ForceLumineuse(CharacteristicValue),
    ForceObscure(CharacteristicValue),
    Wakfu(CharacteristicValue),
    Sorcellerie(CharacteristicValue),
    Credits(CharacteristicValue),
    Bouclier(CharacteristicValue),
    Bloque,
    Retourne,
    FlopFlop,
    Zizanie,
    Traitre,
    AttaqueSournoise,
    Pitie,
    #[default]
    Null,
}

impl OldSymbol {
    /// Gets the text of this Symbol if it has one
    pub fn get_text(&self) -> Option<String> {
        match self {
            Self::ForceLumineuse(value)
            | Self::ForceObscure(value)
            | Self::Wakfu(value)
            | Self::Sorcellerie(value)
            | Self::Credits(value)
            | Self::Bouclier(value)
            | Self::Prescience(value)
            | Self::Poison(value) => Some(value.to_string()),
            _ => None,
        }
    }

    /// Gets the value of this Symbol if it has one
    pub fn get_value(&self) -> Option<CharacteristicValue> {
        match self {
            Self::ForceLumineuse(value)
            | Self::ForceObscure(value)
            | Self::Wakfu(value)
            | Self::Sorcellerie(value)
            | Self::Credits(value)
            | Self::Bouclier(value)
            | Self::Prescience(value)
            | Self::Poison(value) => Some(*value),
            _ => None,
        }
    }
}

impl TryFrom<String> for OldSymbol {
    type Error = Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Self::try_from(value.as_str())
    }
}

impl<'de> Deserialize<'de> for OldSymbol {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value = Value::deserialize(deserializer)?;

        match value {
            Value::String(s) => Self::try_from(s).map_err(D::Error::custom),
            Value::Mapping(m) => Self::try_from(m).map_err(D::Error::custom),
            _ => Err(D::Error::custom(Error::ValueError {
                expected: "a symbol".to_owned(),
                current: format!("{:?}", value),
            })),
        }
    }
}

impl TryFrom<&str> for OldSymbol {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            "arc" => Ok(Self::Arc),
            "casque" => Ok(Self::Casque),
            "couronne" => Ok(Self::Couronne),
            "crocs" => Ok(Self::Crocs),
            "epee" => Ok(Self::Epee),
            "poing" => Ok(Self::Poing),
            "etendard" => Ok(Self::Etendard),
            "flamme" => Ok(Self::Flamme),
            "hurlante" => Ok(Self::Hurlante),
            "medic" => Ok(Self::Medic),
            "oeil" => Ok(Self::Oeil),
            "collision" => Ok(Self::Collision),
            "ecrasement" | "ecraser" => Ok(Self::Ecrasement),
            "fanatique" => Ok(Self::Fanatique),
            "fuite" => Ok(Self::Fuite),
            "gel" => Ok(Self::Gel),
            "killer" => Ok(Self::Killer),
            "necromancie" => Ok(Self::Necromancie),
            "razdemaree" | "raz de maree" | "raz_de_maree" | "raz-de-maree" => Ok(Self::RazDeMaree),
            "tornade" => Ok(Self::Tornade),
            "eblouissement" | "aveuglement" => Ok(Self::Eblouissement),
            "tenebre" => Ok(Self::Tenebre),
            "batonmagique" | "baton magique" | "baton_magique" | "baton-magique" => {
                Ok(Self::BatonMagique)
            }
            "providence" | "chance" => Ok(Self::Providence),
            "magiedemoniaque" | "magie demoniaque" | "magie_demoniaque" | "magie-demoniaque"
            | "demon" => Ok(Self::MagieDemoniaque),
            "magieincantatoire" | "magie incantatoire" | "magie_incantatoire"
            | "magie-incantatoire" => Ok(Self::MagieIncantatoire),
            "devastator" => Ok(Self::Devastator),
            "fusilelite" | "fusil elite" | "fusil_elite" | "fusil-elite" | "elite" => {
                Ok(Self::FusilElite)
            }
            "magienecromantique"
            | "magie necromantique"
            | "magie_necromantique"
            | "magie-necromantique"
            | "necromancien" => Ok(Self::MagieNecromantique),
            "paix" | "pax" => Ok(Self::Paix),
            "portail" => Ok(Self::Portail),
            "sablier" => Ok(Self::Sablier),
            "blaster" => Ok(Self::Blaster),
            "paralyseur" => Ok(Self::Paralyseur),
            "sabrebleu" | "sabre bleu" | "sabre_bleu" | "sabre-bleu" => Ok(Self::SabreBleu),
            "sabrerouge" | "sabre rouge" | "sabre_rouge" | "sabre-rouge" => Ok(Self::SabreRouge),
            "sabreviolet" | "sabre violet" | "sabre_violet" | "sabre-violet" => {
                Ok(Self::SabreViolet)
            }
            "sabrevert" | "sabre vert" | "sabre_vert" | "sabre-vert" => Ok(Self::SabreVert),
            "dragonball1" | "db1" => Ok(Self::DragonBall1),
            "dragonball2" | "db2" => Ok(Self::DragonBall2),
            "dragonball3" | "db3" => Ok(Self::DragonBall3),
            "dragonball4" | "db4" => Ok(Self::DragonBall4),
            "bloque" => Ok(Self::Bloque),
            "retourne" => Ok(Self::Retourne),
            "flopflop" | "flop flop" | "flipflop" | "flip flop" | "flop-flop" | "flip-flop" => {
                Ok(Self::FlopFlop)
            }
            "zizanie" => Ok(Self::Zizanie),
            "traitre" => Ok(Self::Traitre),
            "attaquesournoise" | "attaque sournoise" | "attaque_sournoise"
            | "attaque-sournoise" => Ok(Self::AttaqueSournoise),
            "pitie" => Ok(Self::Pitie),
            "null" | "laser" | "vide" | "empty" => Ok(Self::Null),
            _ => {
                let re = RegexBuilder::new(r"^(?<variant>[^\(\)]+)\((?<value>[^\)]+)\)$")
                    .ignore_whitespace(true)
                    .build()
                    .critical("Error with the regex, should not happen");
                if let Some(capture) = re.captures(value) {
                    let mut map = Mapping::new();
                    map.insert(capture["variant"].into(), capture["value"].into());
                    map.try_into()
                } else {
                    Err(Error::ConversionError)
                }
            }
        }
    }
}

impl TryFrom<Mapping> for OldSymbol {
    type Error = Error;

    fn try_from(map: Mapping) -> Result<Self, Self::Error> {
        if map.len() != 1 {
            return Err(Error::LengthError {
                expected_size: 1,
                current_size: map.len(),
            });
        }
        let (key, value) = map
            .into_iter()
            .next()
            .critical("Should never fail as we check before it was of length 1");
        let value = serde_yaml::from_value::<CharacteristicValue>(value.clone())?;
        let key = key.as_str().ok_or(Error::ConversionError)?;
        match key.to_lowercase().as_str() {
            "poison" => Ok(Self::Poison(value)),
            "prescience" => Ok(Self::Prescience(value)),
            "forcelumineuse" | "force lumineuse" | "force_lumineuse" | "force-lumineuse" => Ok(Self::ForceLumineuse(value)),
            "forceobscure" | "force obscure" | "force_obscure" | "force-obscure" => Ok(Self::ForceObscure(value)),
            "wakfu" => Ok(Self::Wakfu(value)),
            "sorcellerie" => Ok(Self::Sorcellerie(value)),
            "credits" | "credit" => Ok(Self::Credits(value)),
            "bouclier" => Ok(Self::Bouclier(value)),
            _ => Err(Error::ValueError{
                expected: "Poison, Prescience, ForceLumineuse, ForceObscure, Wakfu, Sorcellerie, Credits or Bouclier".to_owned(),
                current: key.to_owned(),
            }),
        }
    }
}

impl From<OldSymbol> for super::Symbol {
    fn from(symbol: OldSymbol) -> Self {
        match symbol {
            OldSymbol::Arc => Self::Arc,
            OldSymbol::Casque => Self::Casque,
            OldSymbol::Couronne => Self::Couronne,
            OldSymbol::Crocs => Self::Crocs,
            OldSymbol::Epee => Self::Epee,
            OldSymbol::Poing => Self::Poing,
            OldSymbol::Poison(_) => Self::Poison,
            OldSymbol::Prescience(_) => Self::Prescience,
            OldSymbol::ForceLumineuse(_) => Self::ForceLumineuse,
            OldSymbol::ForceObscure(_) => Self::ForceObscure,
            OldSymbol::Wakfu(_) => Self::Wakfu,
            OldSymbol::Sorcellerie(_) => Self::Sorcellerie,
            OldSymbol::Credits(_) => Self::Credits,
            OldSymbol::Bouclier(_) => Self::Bouclier,
            OldSymbol::Etendard => Self::Etendard,
            OldSymbol::Flamme => Self::Flamme,
            OldSymbol::Hurlante => Self::Hurlante,
            OldSymbol::Medic => Self::Medic,
            OldSymbol::Oeil => Self::Oeil,
            OldSymbol::Collision => Self::Collision,
            OldSymbol::Ecrasement => Self::Ecrasement,
            OldSymbol::Fanatique => Self::Fanatique,
            OldSymbol::Fuite => Self::Fuite,
            OldSymbol::Gel => Self::Gel,
            OldSymbol::Killer => Self::Killer,
            OldSymbol::Necromancie => Self::Necromancie,
            OldSymbol::RazDeMaree => Self::RazDeMaree,
            OldSymbol::Tornade => Self::Tornade,
            OldSymbol::Tenebre => Self::Tenebre,
            OldSymbol::BatonMagique => Self::BatonMagique,
            OldSymbol::Providence => Self::Providence,
            OldSymbol::MagieDemoniaque => Self::MagieDemoniaque,
            OldSymbol::MagieIncantatoire => Self::MagieIncantatoire,
            OldSymbol::Devastator => Self::Devastator,
            OldSymbol::FusilElite => Self::FusilElite,
            OldSymbol::MagieNecromantique => Self::MagieNecromantique,
            OldSymbol::Eblouissement => Self::Eblouissement,
            OldSymbol::AttaqueSournoise => Self::AttaqueSournoise,
            OldSymbol::DragonBall1 => Self::DragonBall1,
            OldSymbol::DragonBall2 => Self::DragonBall2,
            OldSymbol::DragonBall3 => Self::DragonBall3,
            OldSymbol::DragonBall4 => Self::DragonBall4,
            OldSymbol::SabreBleu => Self::SabreBleu,
            OldSymbol::SabreRouge => Self::SabreRouge,
            OldSymbol::SabreViolet => Self::SabreViolet,
            OldSymbol::SabreVert => Self::SabreVert,
            OldSymbol::Paralyseur => Self::Paralyseur,
            OldSymbol::Pitie => Self::Pitie,
            OldSymbol::Retourne => Self::Retourne,
            OldSymbol::FlopFlop => Self::FlopFlop,
            OldSymbol::Zizanie => Self::Zizanie,
            OldSymbol::Traitre => Self::Traitre,
            OldSymbol::Bloque => Self::Bloque,
            OldSymbol::Null => Self::Null,
            OldSymbol::Sablier => Self::Sablier,
            OldSymbol::Paix => Self::Paix,
            OldSymbol::Portail => Self::Portail,
            OldSymbol::Blaster => Self::Blaster,
        }
    }
}

impl From<OldSymbol> for super::SymbolField {
    fn from(symbol: OldSymbol) -> Self {
        let value = symbol.get_value().unwrap_or_default();
        Self {
            symbol: symbol.into(),
            value,
            text_color: Color::Default,
            border_color: Color::Default,
        }
    }
}

#[cfg(test)]
mod test_module_symbol {

    #[cfg(test)]
    mod test_enum_symbol {

        use super::super::OldSymbol;

        use crate::card::symbol::{Symbol, SymbolField};
        use crate::color::Color;
        use crate::error::Error;
        use crate::types::CharacteristicValue;

        use std::str::FromStr;
        use strum::IntoEnumIterator;

        #[test]
        fn test_deserialize_symbol_value() {
            let s = "POISON(10)";
            assert_eq!(
                OldSymbol::Poison(10.into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
            let s = "POIson(10)";
            assert_eq!(
                OldSymbol::Poison(10.into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
            let s = "Poison: \"10\"";
            assert_eq!(
                OldSymbol::Poison(10.into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
            let s = "Poison: 10";
            assert_eq!(
                OldSymbol::Poison(10.into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
            let s = "Poison: \"10.5\"";
            assert_eq!(
                OldSymbol::Poison((10.5).into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
            let s = "Poison: 10.5";
            assert_eq!(
                OldSymbol::Poison((10.5).into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
            let s = "Poison: 10\nPrescience: 10";
            assert_eq!(
                Err(Error::LengthError {
                    expected_size: 1,
                    current_size: 2
                }),
                serde_yaml::from_str::<OldSymbol>(s).map_err(Error::from)
            );
            let s = "";
            assert_eq!(
                Err(Error::ValueError {
                    expected: "a symbol".to_owned(),
                    current: "Null".to_owned()
                }),
                serde_yaml::from_str::<OldSymbol>(s).map_err(Error::from)
            );

            let s = "PRESCIENCE(10)";
            assert_eq!(
                OldSymbol::Prescience(10.into()),
                serde_yaml::from_str(s).expect("A test should never fail")
            );
        }

        #[test]
        fn fail_old() {
            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("POISON)").map_err(Error::from),
                Err(Error::ConversionError)
            );
            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("POISON(").map_err(Error::from),
                Err(Error::ConversionError)
            );
            assert_eq!(
            serde_yaml::from_str::<OldSymbol>("ARC(10)").map_err(Error::from),
            Err(Error::ValueError {
                expected: "Poison, Prescience, ForceLumineuse, ForceObscure, Wakfu, Sorcellerie, Credits or Bouclier".to_owned(),
                current: "ARC".to_owned()
            })
        );
        }

        #[test]
        fn good_old_symbol() {
            let s = "ARC";
            assert_eq!(
                OldSymbol::Arc,
                serde_yaml::from_str(s).expect("A test should never fail")
            );
        }

        #[test]
        fn test_default_old_symbol() {
            assert_eq!(OldSymbol::default(), OldSymbol::Null);
        }

        #[test]
        fn test_debug_old_symbol() {
            assert_eq!(format!("{:?}", OldSymbol::Arc), "Arc");
            assert_eq!(format!("{:?}", OldSymbol::Casque), "Casque");
            assert_eq!(format!("{:?}", OldSymbol::Couronne), "Couronne");
            assert_eq!(format!("{:?}", OldSymbol::Crocs), "Crocs");
            assert_eq!(format!("{:?}", OldSymbol::Epee), "Epee");
            assert_eq!(format!("{:?}", OldSymbol::Poing), "Poing");
            assert_eq!(format!("{:?}", OldSymbol::Etendard), "Etendard");
            assert_eq!(format!("{:?}", OldSymbol::Flamme), "Flamme");
            assert_eq!(format!("{:?}", OldSymbol::Hurlante), "Hurlante");
            assert_eq!(format!("{:?}", OldSymbol::Medic), "Medic");
            assert_eq!(format!("{:?}", OldSymbol::Oeil), "Oeil");
            assert_eq!(format!("{:?}", OldSymbol::Collision), "Collision");
            assert_eq!(format!("{:?}", OldSymbol::Ecrasement), "Ecrasement");
            assert_eq!(format!("{:?}", OldSymbol::Fanatique), "Fanatique");
            assert_eq!(format!("{:?}", OldSymbol::Fuite), "Fuite");
            assert_eq!(format!("{:?}", OldSymbol::Gel), "Gel");
            assert_eq!(format!("{:?}", OldSymbol::Killer), "Killer");
            assert_eq!(format!("{:?}", OldSymbol::Necromancie), "Necromancie");
            assert_eq!(
                format!("{:?}", OldSymbol::Poison(10.into())),
                format!("Poison({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(format!("{:?}", OldSymbol::RazDeMaree), "RazDeMaree");
            assert_eq!(format!("{:?}", OldSymbol::Tornade), "Tornade");
            assert_eq!(format!("{:?}", OldSymbol::Eblouissement), "Eblouissement");
            assert_eq!(format!("{:?}", OldSymbol::Tenebre), "Tenebre");
            assert_eq!(format!("{:?}", OldSymbol::BatonMagique), "BatonMagique");
            assert_eq!(format!("{:?}", OldSymbol::Providence), "Providence");
            assert_eq!(
                format!("{:?}", OldSymbol::MagieDemoniaque),
                "MagieDemoniaque"
            );
            assert_eq!(
                format!("{:?}", OldSymbol::MagieIncantatoire),
                "MagieIncantatoire"
            );
            assert_eq!(format!("{:?}", OldSymbol::Devastator), "Devastator");
            assert_eq!(format!("{:?}", OldSymbol::FusilElite), "FusilElite");
            assert_eq!(
                format!("{:?}", OldSymbol::MagieNecromantique),
                "MagieNecromantique"
            );
            assert_eq!(format!("{:?}", OldSymbol::Paix), "Paix");
            assert_eq!(format!("{:?}", OldSymbol::Portail), "Portail");
            assert_eq!(
                format!("{:?}", OldSymbol::Prescience(10.into())),
                format!("Prescience({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(format!("{:?}", OldSymbol::Sablier), "Sablier");
            assert_eq!(format!("{:?}", OldSymbol::Blaster), "Blaster");
            assert_eq!(format!("{:?}", OldSymbol::Paralyseur), "Paralyseur");
            assert_eq!(format!("{:?}", OldSymbol::SabreBleu), "SabreBleu");
            assert_eq!(format!("{:?}", OldSymbol::SabreRouge), "SabreRouge");
            assert_eq!(format!("{:?}", OldSymbol::SabreViolet), "SabreViolet");
            assert_eq!(format!("{:?}", OldSymbol::SabreVert), "SabreVert");
            assert_eq!(format!("{:?}", OldSymbol::DragonBall1), "DragonBall1");
            assert_eq!(format!("{:?}", OldSymbol::DragonBall2), "DragonBall2");
            assert_eq!(format!("{:?}", OldSymbol::DragonBall3), "DragonBall3");
            assert_eq!(format!("{:?}", OldSymbol::DragonBall4), "DragonBall4");
            assert_eq!(
                format!("{:?}", OldSymbol::ForceLumineuse(10.into())),
                format!("ForceLumineuse({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(
                format!("{:?}", OldSymbol::ForceObscure(10.into())),
                format!("ForceObscure({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(
                format!("{:?}", OldSymbol::Wakfu(10.into())),
                format!("Wakfu({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(
                format!("{:?}", OldSymbol::Sorcellerie(10.into())),
                format!("Sorcellerie({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(
                format!("{:?}", OldSymbol::Credits(10.into())),
                format!("Credits({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(
                format!("{:?}", OldSymbol::Bouclier(10.into())),
                format!("Bouclier({:?})", CharacteristicValue::from(10))
            );
            assert_eq!(format!("{:?}", OldSymbol::Bloque), "Bloque");
            assert_eq!(format!("{:?}", OldSymbol::Retourne), "Retourne");
            assert_eq!(format!("{:?}", OldSymbol::FlopFlop), "FlopFlop");
            assert_eq!(format!("{:?}", OldSymbol::Zizanie), "Zizanie");
            assert_eq!(format!("{:?}", OldSymbol::Traitre), "Traitre");
            assert_eq!(
                format!("{:?}", OldSymbol::AttaqueSournoise),
                "AttaqueSournoise"
            );
            assert_eq!(format!("{:?}", OldSymbol::Pitie), "Pitie");
            assert_eq!(format!("{:?}", OldSymbol::Null), "Null");
        }

        #[test]
        fn test_serde_old_symbol() {
            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Arc").expect("A test should never fail"),
                OldSymbol::Arc
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Casque").expect("A test should never fail"),
                OldSymbol::Casque
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Couronne").expect("A test should never fail"),
                OldSymbol::Couronne
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Crocs").expect("A test should never fail"),
                OldSymbol::Crocs
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Epee").expect("A test should never fail"),
                OldSymbol::Epee
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Poing").expect("A test should never fail"),
                OldSymbol::Poing
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Etendard").expect("A test should never fail"),
                OldSymbol::Etendard
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Flamme").expect("A test should never fail"),
                OldSymbol::Flamme
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Hurlante").expect("A test should never fail"),
                OldSymbol::Hurlante
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Medic").expect("A test should never fail"),
                OldSymbol::Medic
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Oeil").expect("A test should never fail"),
                OldSymbol::Oeil
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Collision").expect("A test should never fail"),
                OldSymbol::Collision
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Ecrasement").expect("A test should never fail"),
                OldSymbol::Ecrasement
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Fanatique").expect("A test should never fail"),
                OldSymbol::Fanatique
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Fuite").expect("A test should never fail"),
                OldSymbol::Fuite
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Gel").expect("A test should never fail"),
                OldSymbol::Gel
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Killer").expect("A test should never fail"),
                OldSymbol::Killer
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Necromancie").expect("A test should never fail"),
                OldSymbol::Necromancie
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Poison: 10").expect("A test should never fail"),
                OldSymbol::Poison(10.into())
            );
            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Poison: 10.5")
                    .expect("A test should never fail"),
                OldSymbol::Poison((10.5).into())
            );
            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Poison: D12").expect("A test should never fail"),
                OldSymbol::Poison(
                    CharacteristicValue::from_str("D12").expect("A test should never fail")
                )
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("RazDeMaree").expect("A test should never fail"),
                OldSymbol::RazDeMaree
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Tornade").expect("A test should never fail"),
                OldSymbol::Tornade
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Eblouissement")
                    .expect("A test should never fail"),
                OldSymbol::Eblouissement
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Tenebre").expect("A test should never fail"),
                OldSymbol::Tenebre
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("BatonMagique")
                    .expect("A test should never fail"),
                OldSymbol::BatonMagique
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Providence").expect("A test should never fail"),
                OldSymbol::Providence
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("MagieDemoniaque")
                    .expect("A test should never fail"),
                OldSymbol::MagieDemoniaque
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("MagieIncantatoire")
                    .expect("A test should never fail"),
                OldSymbol::MagieIncantatoire
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Devastator").expect("A test should never fail"),
                OldSymbol::Devastator
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("FusilElite").expect("A test should never fail"),
                OldSymbol::FusilElite
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("MagieNecromantique")
                    .expect("A test should never fail"),
                OldSymbol::MagieNecromantique
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("MagieNecromantique")
                    .expect("A test should never fail"),
                OldSymbol::MagieNecromantique
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Paix").expect("A test should never fail"),
                OldSymbol::Paix
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Portail").expect("A test should never fail"),
                OldSymbol::Portail
            );

            assert_eq!(
                serde_yaml::from_str::<OldSymbol>("Prescience: 10")
                    .expect("A test should never fail"),
                OldSymbol::Prescience(10.into())
            );
        }

        #[test]
        fn test_clone_old_symbol() {
            for old_symbol in OldSymbol::iter() {
                assert_eq!(old_symbol.clone(), old_symbol);
            }
        }

        #[test]
        fn test_get_text() {
            assert_eq!(
                OldSymbol::Poison(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::Prescience(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::ForceLumineuse(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::ForceObscure(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::Wakfu(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::Sorcellerie(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::Credits(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(
                OldSymbol::Bouclier(10.into()).get_text(),
                Some("10".to_owned())
            );
            assert_eq!(OldSymbol::Arc.get_text(), None);
            assert_eq!(OldSymbol::Casque.get_text(), None);
            assert_eq!(OldSymbol::Couronne.get_text(), None);
            assert_eq!(OldSymbol::Crocs.get_text(), None);
            assert_eq!(OldSymbol::Epee.get_text(), None);
            assert_eq!(OldSymbol::Poing.get_text(), None);
            assert_eq!(OldSymbol::Etendard.get_text(), None);
            assert_eq!(OldSymbol::Flamme.get_text(), None);
            assert_eq!(OldSymbol::Hurlante.get_text(), None);
            assert_eq!(OldSymbol::Medic.get_text(), None);
            assert_eq!(OldSymbol::Oeil.get_text(), None);
            assert_eq!(OldSymbol::Collision.get_text(), None);
            assert_eq!(OldSymbol::Ecrasement.get_text(), None);
            assert_eq!(OldSymbol::Fanatique.get_text(), None);
            assert_eq!(OldSymbol::Fuite.get_text(), None);
            assert_eq!(OldSymbol::Gel.get_text(), None);
            assert_eq!(OldSymbol::Killer.get_text(), None);
            assert_eq!(OldSymbol::Necromancie.get_text(), None);
            assert_eq!(OldSymbol::RazDeMaree.get_text(), None);
            assert_eq!(OldSymbol::Tornade.get_text(), None);
            assert_eq!(OldSymbol::Eblouissement.get_text(), None);
            assert_eq!(OldSymbol::Tenebre.get_text(), None);
            assert_eq!(OldSymbol::BatonMagique.get_text(), None);
            assert_eq!(OldSymbol::Providence.get_text(), None);
            assert_eq!(OldSymbol::MagieDemoniaque.get_text(), None);
            assert_eq!(OldSymbol::MagieIncantatoire.get_text(), None);
            assert_eq!(OldSymbol::Devastator.get_text(), None);
            assert_eq!(OldSymbol::FusilElite.get_text(), None);
            assert_eq!(OldSymbol::MagieNecromantique.get_text(), None);
            assert_eq!(OldSymbol::Paix.get_text(), None);
            assert_eq!(OldSymbol::Portail.get_text(), None);
            assert_eq!(OldSymbol::Sablier.get_text(), None);
            assert_eq!(OldSymbol::Blaster.get_text(), None);
            assert_eq!(OldSymbol::Paralyseur.get_text(), None);
            assert_eq!(OldSymbol::SabreBleu.get_text(), None);
            assert_eq!(OldSymbol::SabreRouge.get_text(), None);
            assert_eq!(OldSymbol::SabreViolet.get_text(), None);
            assert_eq!(OldSymbol::SabreVert.get_text(), None);
            assert_eq!(OldSymbol::DragonBall1.get_text(), None);
            assert_eq!(OldSymbol::DragonBall2.get_text(), None);
            assert_eq!(OldSymbol::DragonBall3.get_text(), None);
            assert_eq!(OldSymbol::DragonBall4.get_text(), None);
            assert_eq!(OldSymbol::Bloque.get_text(), None);
            assert_eq!(OldSymbol::Retourne.get_text(), None);
            assert_eq!(OldSymbol::FlopFlop.get_text(), None);
            assert_eq!(OldSymbol::Zizanie.get_text(), None);
            assert_eq!(OldSymbol::Traitre.get_text(), None);
            assert_eq!(OldSymbol::AttaqueSournoise.get_text(), None);
            assert_eq!(OldSymbol::Pitie.get_text(), None);
            assert_eq!(OldSymbol::Null.get_text(), None);
        }

        #[test]
        fn test_from_old_symbol() {
            assert_eq!(Symbol::Arc, OldSymbol::Arc.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Arc,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Arc.into()
            );

            assert_eq!(Symbol::Casque, OldSymbol::Casque.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Casque,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Casque.into(),
            );

            assert_eq!(Symbol::Couronne, OldSymbol::Couronne.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Couronne,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Couronne.into(),
            );

            assert_eq!(Symbol::Crocs, OldSymbol::Crocs.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Crocs,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Crocs.into(),
            );

            assert_eq!(Symbol::Epee, OldSymbol::Epee.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Epee,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Epee.into(),
            );

            assert_eq!(Symbol::Poing, OldSymbol::Poing.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Poing,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Poing.into(),
            );

            assert_eq!(Symbol::Etendard, OldSymbol::Etendard.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Etendard,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Etendard.into(),
            );

            assert_eq!(Symbol::Flamme, OldSymbol::Flamme.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Flamme,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Flamme.into(),
            );

            assert_eq!(Symbol::Hurlante, OldSymbol::Hurlante.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Hurlante,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Hurlante.into(),
            );

            assert_eq!(Symbol::Medic, OldSymbol::Medic.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Medic,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Medic.into(),
            );

            assert_eq!(Symbol::Oeil, OldSymbol::Oeil.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Oeil,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Oeil.into(),
            );

            assert_eq!(Symbol::Collision, OldSymbol::Collision.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Collision,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Collision.into(),
            );

            assert_eq!(Symbol::Ecrasement, OldSymbol::Ecrasement.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Ecrasement,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Ecrasement.into(),
            );

            assert_eq!(Symbol::Fanatique, OldSymbol::Fanatique.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Fanatique,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Fanatique.into(),
            );

            assert_eq!(Symbol::Fuite, OldSymbol::Fuite.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Fuite,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Fuite.into(),
            );

            assert_eq!(Symbol::Gel, OldSymbol::Gel.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Gel,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Gel.into(),
            );

            assert_eq!(Symbol::Killer, OldSymbol::Killer.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Killer,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Killer.into(),
            );

            assert_eq!(Symbol::Necromancie, OldSymbol::Necromancie.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Necromancie,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Necromancie.into(),
            );

            assert_eq!(Symbol::Poison, OldSymbol::Poison(10.into()).into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Poison,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Poison(10.into()).into(),
            );

            assert_eq!(Symbol::RazDeMaree, OldSymbol::RazDeMaree.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::RazDeMaree,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::RazDeMaree.into(),
            );

            assert_eq!(Symbol::Tornade, OldSymbol::Tornade.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Tornade,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Tornade.into(),
            );

            assert_eq!(Symbol::Eblouissement, OldSymbol::Eblouissement.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Eblouissement,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Eblouissement.into(),
            );

            assert_eq!(Symbol::Tenebre, OldSymbol::Tenebre.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Tenebre,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Tenebre.into(),
            );

            assert_eq!(Symbol::BatonMagique, OldSymbol::BatonMagique.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::BatonMagique,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::BatonMagique.into(),
            );

            assert_eq!(Symbol::Providence, OldSymbol::Providence.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Providence,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Providence.into(),
            );

            assert_eq!(Symbol::MagieDemoniaque, OldSymbol::MagieDemoniaque.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::MagieDemoniaque,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::MagieDemoniaque.into(),
            );

            assert_eq!(
                Symbol::MagieIncantatoire,
                OldSymbol::MagieIncantatoire.into(),
            );
            assert_eq!(
                SymbolField {
                    symbol: Symbol::MagieIncantatoire,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::MagieIncantatoire.into(),
            );

            assert_eq!(Symbol::Devastator, OldSymbol::Devastator.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Devastator,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Devastator.into(),
            );

            assert_eq!(Symbol::FusilElite, OldSymbol::FusilElite.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::FusilElite,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::FusilElite.into(),
            );

            assert_eq!(
                Symbol::MagieNecromantique,
                OldSymbol::MagieNecromantique.into(),
            );
            assert_eq!(
                SymbolField {
                    symbol: Symbol::MagieNecromantique,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::MagieNecromantique.into(),
            );

            assert_eq!(Symbol::Paix, OldSymbol::Paix.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Paix,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Paix.into(),
            );

            assert_eq!(Symbol::Portail, OldSymbol::Portail.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Portail,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Portail.into(),
            );

            assert_eq!(Symbol::Prescience, OldSymbol::Prescience(10.into()).into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Prescience,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Prescience(10.into()).into(),
            );

            assert_eq!(Symbol::Sablier, OldSymbol::Sablier.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Sablier,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Sablier.into(),
            );

            assert_eq!(Symbol::Blaster, OldSymbol::Blaster.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Blaster,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Blaster.into(),
            );

            assert_eq!(Symbol::Paralyseur, OldSymbol::Paralyseur.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Paralyseur,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Paralyseur.into(),
            );

            assert_eq!(Symbol::SabreBleu, OldSymbol::SabreBleu.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::SabreBleu,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::SabreBleu.into(),
            );

            assert_eq!(Symbol::SabreRouge, OldSymbol::SabreRouge.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::SabreRouge,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::SabreRouge.into(),
            );

            assert_eq!(Symbol::SabreViolet, OldSymbol::SabreViolet.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::SabreViolet,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::SabreViolet.into(),
            );

            assert_eq!(Symbol::SabreVert, OldSymbol::SabreVert.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::SabreVert,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::SabreVert.into(),
            );

            assert_eq!(Symbol::DragonBall1, OldSymbol::DragonBall1.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::DragonBall1,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::DragonBall1.into(),
            );

            assert_eq!(Symbol::DragonBall2, OldSymbol::DragonBall2.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::DragonBall2,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::DragonBall2.into(),
            );

            assert_eq!(Symbol::DragonBall3, OldSymbol::DragonBall3.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::DragonBall3,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::DragonBall3.into(),
            );

            assert_eq!(Symbol::DragonBall4, OldSymbol::DragonBall4.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::DragonBall4,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::DragonBall4.into(),
            );

            assert_eq!(
                Symbol::ForceLumineuse,
                OldSymbol::ForceLumineuse(10.into()).into(),
            );
            assert_eq!(
                SymbolField {
                    symbol: Symbol::ForceLumineuse,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::ForceLumineuse(10.into()).into(),
            );

            assert_eq!(
                Symbol::ForceObscure,
                OldSymbol::ForceObscure(10.into()).into(),
            );
            assert_eq!(
                SymbolField {
                    symbol: Symbol::ForceObscure,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::ForceObscure(10.into()).into(),
            );

            assert_eq!(Symbol::Wakfu, OldSymbol::Wakfu(10.into()).into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Wakfu,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Wakfu(10.into()).into(),
            );

            assert_eq!(
                Symbol::Sorcellerie,
                OldSymbol::Sorcellerie(10.into()).into(),
            );
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Sorcellerie,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Sorcellerie(10.into()).into(),
            );

            assert_eq!(Symbol::Credits, OldSymbol::Credits(10.into()).into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Credits,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Credits(10.into()).into(),
            );

            assert_eq!(Symbol::Bouclier, OldSymbol::Bouclier(10.into()).into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Bouclier,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Bouclier(10.into()).into(),
            );

            assert_eq!(Symbol::Bloque, OldSymbol::Bloque.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Bloque,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Bloque.into(),
            );

            assert_eq!(Symbol::Retourne, OldSymbol::Retourne.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Retourne,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Retourne.into(),
            );

            assert_eq!(Symbol::FlopFlop, OldSymbol::FlopFlop.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::FlopFlop,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::FlopFlop.into(),
            );

            assert_eq!(Symbol::Zizanie, OldSymbol::Zizanie.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Zizanie,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Zizanie.into(),
            );

            assert_eq!(Symbol::Traitre, OldSymbol::Traitre.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Traitre,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Traitre.into(),
            );

            assert_eq!(Symbol::AttaqueSournoise, OldSymbol::AttaqueSournoise.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::AttaqueSournoise,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::AttaqueSournoise.into(),
            );

            assert_eq!(Symbol::Pitie, OldSymbol::Pitie.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Pitie,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Pitie.into(),
            );

            assert_eq!(Symbol::Null, OldSymbol::Null.into(),);
            assert_eq!(
                SymbolField {
                    symbol: Symbol::Null,
                    value: CharacteristicValue::default(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                },
                OldSymbol::Null.into(),
            );
        }
    }
}
