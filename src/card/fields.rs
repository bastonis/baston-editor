// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the fields of a card
//!
//! This module contains :
//! * The name
//! * The title
//! * The type
//! * The number
//! * The power
//! * The characteristics
//! * The gender
//! * The era
//! * The archetype

use super::logos;
use super::symbol::{Symbol, SymbolField};
use crate::color::Color;
use crate::error::{Error, LogError};
use crate::render::RenderedCard;
use crate::render::{config::Config, images::Image, Group, Layer, Render};
use crate::types::CharacteristicValue;

use std::collections::{btree_map::Entry, BTreeMap, HashMap};

use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use strum_macros::{Display, EnumIter};
use usvg::{
    DominantBaseline, Text, TextAnchor, TextChunk, TextFlow, TextRendering, TextSpan, WritingMode,
};
use usvg::{Node, Transform};

/// Represents the Name of a Card
pub type NameField = BaseField;
/// Represents the Title of a Card
pub type TitleField = BaseField;
/// Represents the Type of a Card
pub type TypeField = BaseField;
/// Represents the Number of a Card,
/// This does not exist on Card anymore (used for backward compatibility)
/// It was replaced by [PowerField]
pub type NumberField = BaseField;

/// Represents a Power of a Card
pub type PowerField = PositionedField;

/// Represents the type of field for either a [NameField], [TypeField], or [TitleField]
#[derive(Display)]
pub enum FieldType {
    /// The type of a [NameField]
    Name,
    /// The type of a [TypeField]
    Type,
    /// The type of a [TitleField]
    Title,
}

/// Represents the basic fields of a Card
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct BaseField {
    /// Represents the value of the field (the text shown)
    #[serde(default)]
    pub value: String,
    /// Represents the size of the font that will be used to render it
    #[serde(default)]
    pub size: u8,
    /// Represents the color of the font that will be used to render it
    #[serde(default)]
    pub color: Color,
}

/// Represents a positioned field on a Card (like Number and Power)
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct PositionedField {
    /// Represents the value of the field (the text shown)
    #[serde(default)]
    pub value: String,
    /// Represents the size of the font that will be used to render it
    #[serde(default)]
    pub size: u8,
    /// Represents the color of the font that will be used to render it
    #[serde(default)]
    pub color: Color,
    /// Represents the position where it will be rendered
    #[serde(default)]
    pub position: Position,
}

/// Represents the position of a positioned field
#[derive(
    Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq, Hash, Display, EnumIter, Default,
)]
pub enum Position {
    /// The field will be rendered at the top left of the card
    #[serde(alias = "TOP_LEFT")]
    TopLeft,
    /// The field will be rendered at the top right of the card
    #[serde(alias = "TOP_RIGHT")]
    TopRight,
    /// The field will be rendered at the left of the name
    #[serde(alias = "LEFT_OF_NAME")]
    LeftOfName,
    /// The field will be rendered at the left of the type
    #[serde(alias = "LEFT_OF_TYPE")]
    LeftOfType,
    /// The field will be rendered at the left of the title
    #[serde(alias = "LEFT_OF_NICK_NAME")]
    LeftOfTitle,
    /// The field will be rendered at the right of the name
    #[serde(alias = "RIGHT_OF_NAME")]
    RightOfName,
    /// The field will be rendered at the right of the type
    #[serde(alias = "RIGHT_OF_TYPE")]
    RightOfType,
    /// The field will be rendered at the right of the title
    #[serde(alias = "RIGHT_OF_NICK_NAME")]
    RightOfTitle,
    /// The field will be rendered at the left of the pictograms
    #[serde(alias = "LEFT_OF_PICTO")]
    #[serde(alias = "TopOfStat")]
    LeftOfPictograms,

    /// The default position should only be used to be replaced by another position, not as a position by itself
    #[serde(other)]
    #[default]
    Default,
}

/// Represents the Side on which a text can be aligned
#[derive(Debug, PartialEq)]
pub enum Side {
    /// The text will be aligned on the left side
    Left,
    /// The text will be aligned on the right side
    Right,
    /// The text will be centered
    Middle,
}

/// Represents the characteristics of a card (WS, ST, SP, BR, IN, TO, LU, CH)
/// Each value is represented by a CharacteristicValue and as such hold a value between 0 and 127 incremented by 0.5 or a dice throw
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct CharacteristicsField {
    /// The values of the stats
    #[serde(default)]
    stats: BTreeMap<CharacteristicName, CharacteristicValue>,
    /// Represents the size of the font that will be used to render them
    #[serde(default)]
    pub size: u8,
    /// Represents the color of the font that will be used to render them
    #[serde(default)]
    pub color: Color,
    /// Represents whenever the compact fractions will be used to render them
    #[serde(alias = "compact fraction", default)]
    pub compact_fraction: bool,
    /// Represents the color of the name of each characteristic
    #[serde(default)]
    pub name_color: Color,
}

/// Represents the name of the characteristics of a card (WS, ST, SP, BR, IN, TO, LU, CH)
#[derive(
    Serialize,
    Deserialize,
    Debug,
    Clone,
    PartialEq,
    Ord,
    Eq,
    PartialOrd,
    Hash,
    Copy,
    Display,
    EnumIter,
)]
pub enum CharacteristicName {
    /// The Weapon Skill, its value is supposed to be between 0 and 15
    #[serde(alias = "Weapon Skill")]
    #[strum(serialize = "Weapon Skill")]
    WeaponSkill,

    /// The Strength, its value is supposed to be between 0 and 15
    Strength,

    /// The Speed, its value is supposed to be between 0 and 14
    Speed,

    /// The Bravery, its value is supposed to be between 0 and 13
    Bravery,

    /// The Intelligence, its value is supposed to be between 0 and 12
    Intelligence,

    /// The Toughness, its value is supposed to be between 0 and 15
    Toughness,

    /// The Luck, its value is supposed to be between 0 and 15
    Luck,

    /// The Charisma, its value is supposed to be between 0 and 15
    #[serde(alias = "Charisme")]
    Charisma,
}

/// Represents a Field which is a Logo
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct LogoField<L: logos::SvgLogo> {
    /// The logo
    #[serde(default)]
    #[serde(alias = "era")]
    #[serde(alias = "archetype")]
    #[serde(alias = "gender")]
    pub logo: L,
    /// The color of the logo
    #[serde(default)]
    pub color: Color,
}

/// Represents the Gender
pub type GenderField = LogoField<logos::Gender>;

/// Represents the Era
pub type EraField = LogoField<logos::Era>;

/// Represents the Archetype
pub type ArchetypeField = LogoField<logos::Archetype>;

impl BaseField {
    /// Returns true if the value of the field is empty
    pub fn is_empty(&self) -> bool {
        self.value.is_empty()
    }

    /// Returns the default value of the field
    ///
    /// Like [Default] would do, but it needs a [FieldType] to know which default value should be returned
    pub fn default(r#type: FieldType) -> Self {
        match r#type {
            FieldType::Name => super::DEFAULT_CARD.name.clone(),
            FieldType::Type => super::DEFAULT_CARD.r#type.clone(),
            FieldType::Title => super::DEFAULT_CARD.title.clone(),
        }
    }
}

impl super::Updatable for BaseField {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.value.is_empty() {
            log::warn!(
                "The value of a base field is empty, using default value : {}",
                default.value
            );
            res = !default.value.is_empty();
            self.value.clone_from(&default.value);
        }
        if self.size == 0 {
            log::warn!(
                "The size of a base field is not defined, using default value : {}",
                default.size
            );
            res = true;
            self.size = default.size;
        }
        if self.color == Color::default() {
            log::warn!(
                "The color of a base field is not defined, using default value : {}",
                default.color
            );
            res = true;
            self.color = default.color;
        }
        let closest_color = self.color.closest();
        if self.color != closest_color {
            log::warn!(
                "The color of a base field {} was too close to {}, replacing",
                self.color,
                closest_color
            );
            res = true;
            self.color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Self {
            value: "".to_owned(),
            size: 0,
            color: Color::default(),
        }
    }
}

impl Render for BaseField {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        Ok(usvg::Node::Text(Box::new(RenderedCard::create_text(
            layer,
            &self.value,
            self.color,
            self.size,
            &config.get_font(layer)?,
            config.get_position(layer)?,
            TextAnchor::Middle,
        ))))
    }
}

impl<L: logos::SvgLogo> Render for LogoField<L> {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let gender = self.logo.get_image().set_fill_color(self.color);
        let position = config.get_position(layer)?;
        let scale = config.get_scale(layer);
        Ok(gender.to_scale_node(layer, position, scale).into())
    }
}

impl Render for CharacteristicsField {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let name_font = config.get_font(&Layer::CharacteristicsName)?;
        let value_font = config.get_font(&Layer::CharacteristicsValue)?;
        let mut group = Group::new(layer, Transform::identity());
        let mut names = Group::new(
            Layer::CharacteristicsName,
            config.get_position(&Layer::CharacteristicsBand)?,
        );
        let mut values = Group::new(
            Layer::CharacteristicsValue,
            config.get_position(&Layer::CharacteristicsBand)?,
        );
        for (characteristic, characteristic_config) in config.get_characteristics().clone().iter() {
            if characteristic_config.name_size > 0. {
                let name = characteristic.to_string();
                if name.contains(' ') {
                    let words = name.split(' ');
                    let mut position = characteristic_config.name_position;
                    let size = characteristic_config.name_size;
                    let count: f32 = words.clone().count() as f32;
                    position.y -= ((count - 1.0) * size) / 2.0;
                    for word in words {
                        names.append(Node::Text(Box::new(RenderedCard::create_text(
                            &format!("{}-{}", Layer::CharacteristicsName, word),
                            &word.to_string(),
                            self.name_color,
                            size,
                            &name_font,
                            position,
                            TextAnchor::Middle,
                        ))));
                        position.y += size;
                    }
                } else {
                    names.append(Node::Text(Box::new(RenderedCard::create_text(
                        &format!("{}-{}", Layer::CharacteristicsName, name),
                        &name,
                        self.name_color,
                        characteristic_config.name_size,
                        &name_font,
                        characteristic_config.name_position,
                        TextAnchor::Middle,
                    ))))
                }
                let value = self.get(characteristic);
                if value != CharacteristicValue::Empty {
                    values.append(Node::Text(Box::new(RenderedCard::create_text(
                        &format!("{}-{}", Layer::CharacteristicsValue, name),
                        &value.to_nice_string(config.is_compact_fraction()),
                        self.color,
                        self.size,
                        &value_font,
                        characteristic_config.position,
                        TextAnchor::Middle,
                    ))))
                }
            } else {
                // The Luck and Charm are not linked to the characteristic band but directly to the card
                if self.get(characteristic) != CharacteristicValue::Empty {
                    group.append(Node::Text(Box::new(RenderedCard::create_text(
                        &format!("{}Value", characteristic),
                        &self
                            .get(characteristic)
                            .to_nice_string(config.is_compact_fraction()),
                        self.color,
                        self.size,
                        &value_font,
                        characteristic_config.position,
                        TextAnchor::Middle,
                    ))))
                }
            }
        }
        group.append(names);
        group.append(values);
        Ok(group.into())
    }
}

impl Render for Vec<PowerField> {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        card: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let mut node = Group::new(layer, Transform::identity());

        let font = config.get_font(layer)?;

        let mut spans: HashMap<Position, (Vec<TextSpan>, String)> = HashMap::new();

        for power in self {
            let value = RenderedCard::string_format(&power.value);
            if value.is_empty() {
                continue;
            }
            match spans.get_mut(&power.position) {
                Some((spans, text)) => {
                    let span = RenderedCard::create_text_span(
                        &format!("{}{}", Layer::Powers, value),
                        text.len(),
                        text.len() + value.len() + 1,
                        power.color,
                        power.size.into(),
                        &font,
                        power.position.into(),
                    );
                    spans.push(span);
                    *text = format!("{} {}", text, value)
                }
                None => {
                    let span = RenderedCard::create_text_span(
                        &format!("{}{}", Layer::Powers, value),
                        0,
                        value.len(),
                        power.color,
                        power.size.into(),
                        &font,
                        power.position.into(),
                    );
                    spans.insert(power.position, (vec![span], value));
                }
            }
        }

        for position in [
            Position::TopLeft,
            Position::TopRight,
            Position::LeftOfPictograms,
        ] {
            if let Some((spans, texts)) = spans.get(&position) {
                let coordinate = config.get_position(&position.into())?;
                let chunk = TextChunk {
                    x: Some(coordinate.x),
                    y: Some(coordinate.y),
                    anchor: position.into(),
                    spans: spans.to_vec(),
                    text_flow: TextFlow::Linear,
                    text: texts.to_string(),
                };
                let text = Text {
                    id: position.to_string(),
                    rendering_mode: TextRendering::OptimizeLegibility,
                    dx: vec![],
                    dy: vec![],
                    rotate: vec![0.0],
                    writing_mode: WritingMode::LeftToRight,
                    chunks: vec![chunk],
                    bounding_box: None,
                    flattened: None,
                    abs_transform: Transform::default(),
                    stroke_bounding_box: None,
                };
                node.append(Node::Text(Box::new(text)));
            }
        }
        for position in [
            Position::LeftOfName,
            Position::LeftOfType,
            Position::LeftOfTitle,
            Position::RightOfName,
            Position::RightOfType,
            Position::RightOfTitle,
        ] {
            if let Some((spans, texts)) = spans.get(&position) {
                let layer = card
                    .critical(
                        "card was not provided when we tried to add the powers, should not happen",
                    )
                    .node_by_id(Layer::from(position).to_string());

                let bounding_box = match layer {
                    Some(layer) => match layer.bounding_box() {
                        Some(bounding_box) => bounding_box,
                        None => {
                            log::error!("The layer {} does not have a bounding box (ie is empty) but a powers was added next to it", Layer::from(position).to_string());
                            let position = config.get_position(&Layer::from(position))?;
                            usvg::Rect::from_xywh(position.x, position.y, 0.0, 0.0)
                                .critical("Should never fail")
                        }
                    },
                    None => {
                        log::error!("The layer {} does not have a bounding box (ie is empty) but a powers was added next to it", Layer::from(position).to_string());
                        let position = config.get_position(&Layer::from(position))?;
                        usvg::Rect::from_xywh(position.x, position.y, 0.0, 0.0)
                            .critical("Should never fail")
                    }
                };
                let y = Some((bounding_box.top() + bounding_box.bottom()) / 2.0);
                let x = Some(match Side::from(position) {
                    Side::Left => bounding_box.right() + 30.0,
                    Side::Right => bounding_box.left() - 30.0,
                    _ => unreachable!(),
                });
                let chunk = TextChunk {
                    x,
                    y,
                    anchor: position.into(),
                    spans: spans.to_vec(),
                    text_flow: TextFlow::Linear,
                    text: texts.to_string(),
                };
                let text = Text {
                    id: position.to_string(),
                    rendering_mode: TextRendering::OptimizeLegibility,
                    dx: vec![],
                    dy: vec![],
                    rotate: vec![0.0],
                    writing_mode: WritingMode::LeftToRight,
                    chunks: vec![chunk],
                    bounding_box: None,
                    flattened: None,
                    abs_transform: Transform::default(),
                    stroke_bounding_box: None,
                };
                node.append(Node::Text(Box::new(text)));
            }
        }
        Ok(node.into())
    }
}

impl From<NumberField> for PowerField {
    fn from(number: NumberField) -> Self {
        Self {
            value: number.value,
            size: number.size,
            color: number.color,
            position: Position::default(),
        }
    }
}

impl TryFrom<PowerField> for SymbolField {
    type Error = Error;
    fn try_from(power: PowerField) -> Result<Self, Self::Error> {
        if power.value.is_empty() {
            return Err(Error::ConversionError);
        }

        match power.value.to_lowercase().as_str() {
            "00" => Ok(Symbol::Bloque.into()),
            "01" => Ok(Symbol::Traitre.into()),
            "02" => Ok(Symbol::Zizanie.into()),
            "03" => Ok(Symbol::Retourne.into()),
            "04" => Ok(Symbol::Pitie.into()),
            "05" => Ok(Symbol::FlopFlop.into()),
            "06" => Ok(Symbol::AttaqueSournoise.into()),
            "pax" | "paix" => Ok(Symbol::Paix.into()),
            value => match &value[..1] {
                "c" => {
                    if let Ok(value) = CharacteristicValue::try_from(&power.value[1..]) {
                        Ok(Self {
                            symbol: Symbol::Credits,
                            value,
                            text_color: Color::Default,
                            border_color: Color::Default,
                        })
                    } else {
                        Err(Error::ConversionError)
                    }
                }
                "f" => {
                    if let Ok(value) = CharacteristicValue::try_from(&power.value[1..]) {
                        Ok(Self {
                            symbol: Symbol::ForceObscure,
                            value,
                            text_color: Color::Default,
                            border_color: Color::Default,
                        })
                    } else {
                        Err(Error::ConversionError)
                    }
                }
                "b" => {
                    if let Ok(value) = CharacteristicValue::try_from(&power.value[1..]) {
                        Ok(Self {
                            symbol: Symbol::Bouclier,
                            value,
                            text_color: Color::Default,
                            border_color: Color::Default,
                        })
                    } else {
                        Err(Error::ConversionError)
                    }
                }
                _ => Err(Error::ConversionError),
            },
        }
    }
}

impl super::Updatable for PowerField {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.value.is_empty() {
            log::warn!(
                "The value of a power is empty, using default value : {}",
                default.value
            );
            res = !default.value.is_empty();
            self.value.clone_from(&default.value);
        }
        if self.size == 0 {
            log::warn!(
                "The size of a power is not defined, using default value : {}",
                default.size
            );
            res = true;
            self.size = default.size;
        }
        if self.color == Color::default() {
            log::warn!(
                "The color of a power is not defined, using default value : {}",
                default.color
            );
            res = true;
            self.color = default.color;
        }
        let closest_color = self.color.closest();
        if self.color != closest_color {
            log::warn!(
                "The color of a power {} was too close to {}, replacing",
                self.color,
                closest_color
            );
            res = true;
            self.color = closest_color;
        }
        if self.position == Position::default() {
            log::warn!(
                "The position of a power is not defined, using default value : {}",
                default.position
            );
            res = true;
            self.position = default.position;
        }
        res
    }

    fn get_default() -> Self {
        Self {
            value: "".to_owned(),
            size: 0,
            color: Color::default(),
            position: Position::default(),
        }
    }
}

impl super::Updatable for GenderField {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.color == Color::default() {
            log::warn!(
                "The color of the gender is not defined, using default value : {}",
                default.color
            );
            self.color = default.color;
            res = true
        }
        let closest_color = self.color.closest();
        if self.color != closest_color {
            log::warn!(
                "The color of the gender {} was too close to {}, replacing",
                self.color,
                closest_color
            );
            res = true;
            self.color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Self {
            logo: logos::Gender::default(),
            color: Color::default(),
        }
    }
}

impl super::Updatable for EraField {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.color == Color::default() {
            log::warn!(
                "The color of the era is not defined, using default value : {}",
                default.color
            );
            self.color = default.color;
            res = true
        }
        let closest_color = self.color.closest();
        if self.color != closest_color {
            log::warn!(
                "The color of the era {} was too close to {}, replacing",
                self.color,
                closest_color
            );
            res = true;
            self.color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Self {
            logo: logos::Era::default(),
            color: Color::default(),
        }
    }
}

impl super::Updatable for ArchetypeField {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.color == Color::default() {
            log::warn!(
                "The color of the archetype is not defined, using default value : {}",
                default.color
            );
            self.color = default.color;
            res = true
        }
        let closest_color = self.color.closest();
        if self.color != closest_color {
            log::warn!(
                "The color of the archetype {} was too close to {}, replacing",
                self.color,
                closest_color
            );
            res = true;
            self.color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Self {
            logo: logos::Archetype::default(),
            color: Color::default(),
        }
    }
}

impl super::Updatable for CharacteristicsField {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        for characteristic in CharacteristicName::iter() {
            if let Entry::Vacant(e) = self.stats.entry(characteristic) {
                let default = default.get(&characteristic);
                log::warn!(
                    "The characteristic {} is not defined, using default value : {}",
                    characteristic,
                    default
                );
                e.insert(default);
                res = true;
            }
        }
        if self.color == Color::default() {
            log::warn!(
                "The color of the characteristics is not defined, using default value : {}",
                default.color
            );
            self.color = default.color;
            res = true;
        }
        let closest_color = self.color.closest();
        if self.color != closest_color {
            log::warn!(
                "The color of the characteristics {} was too close to {}, replacing",
                self.color,
                closest_color
            );
            res = true;
            self.color = closest_color;
        }
        if self.size == 0 {
            log::warn!(
                "The size of the characteristics is not defined, using default value : {}",
                default.size
            );
            self.size = default.size;
            res = true;
        }
        if self.name_color == Color::default() {
            log::warn!("The color of the name of the characteristics is not defined, using default value : {}", default.name_color);
            self.name_color = default.name_color;
            res = true;
        }
        let closest_color = self.name_color.closest();
        if self.name_color != closest_color {
            log::warn!(
                "The color of the name of the characteristics {} was too close to {}, replacing",
                self.name_color,
                closest_color
            );
            res = true;
            self.name_color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Self {
            stats: BTreeMap::new(),
            size: 0,
            color: Color::default(),
            compact_fraction: false,
            name_color: Color::default(),
        }
    }
}

impl Default for PowerField {
    fn default() -> Self {
        super::DEFAULT_CARD.powers[0].clone()
    }
}

impl Default for EraField {
    fn default() -> Self {
        super::DEFAULT_CARD.era.clone()
    }
}

impl Default for GenderField {
    fn default() -> Self {
        super::DEFAULT_CARD.gender.clone()
    }
}

impl Default for ArchetypeField {
    fn default() -> Self {
        super::DEFAULT_CARD.archetype.clone()
    }
}

impl Default for CharacteristicsField {
    fn default() -> Self {
        super::DEFAULT_CARD.characteristics.clone()
    }
}

impl CharacteristicsField {
    /// Gets the value of the given characteristic
    pub fn get(&self, stat: &CharacteristicName) -> CharacteristicValue {
        match self.stats.get(stat) {
            Some(value) => *value,
            None => CharacteristicValue::default(),
        }
    }

    /// Sets the value of the given characteristic
    pub fn set(&mut self, stat: &CharacteristicName, value: CharacteristicValue) {
        self.stats.insert(*stat, value);
    }
}

impl From<Position> for Side {
    fn from(position: Position) -> Self {
        match position {
            Position::LeftOfName
            | Position::LeftOfType
            | Position::LeftOfTitle
            | Position::TopRight => Self::Right,
            Position::RightOfName
            | Position::RightOfType
            | Position::RightOfTitle
            | Position::TopLeft => Self::Left,
            Position::LeftOfPictograms => Self::Middle,
            Position::Default => panic!(
                "{}",
                Error::VariantDefaultForbiddenError("position".to_owned()).to_string()
            ),
        }
    }
}

impl From<Position> for TextAnchor {
    fn from(position: Position) -> Self {
        let side: Side = position.into();
        side.into()
    }
}

impl From<Side> for TextAnchor {
    fn from(side: Side) -> Self {
        match side {
            Side::Left => TextAnchor::Start,
            Side::Right => TextAnchor::End,
            Side::Middle => TextAnchor::Middle,
        }
    }
}

impl From<Position> for DominantBaseline {
    fn from(position: Position) -> Self {
        match position {
            Position::LeftOfName
            | Position::LeftOfType
            | Position::LeftOfTitle
            | Position::RightOfName
            | Position::RightOfType
            | Position::RightOfTitle
            | Position::LeftOfPictograms => Self::Central,
            Position::TopLeft | Position::TopRight => Self::Hanging,
            Position::Default => panic!(
                "{}",
                Error::VariantDefaultForbiddenError("position".to_owned()).to_string()
            ),
        }
    }
}

#[cfg(test)]
mod test_module_fields {

    #[cfg(test)]
    mod test_enum_position {

        use super::super::Position;

        use super::super::Side;

        use std::hash::Hash;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_position() {
            assert_eq!(
                serde_yaml::from_str::<Position>("LEFT_OF_NAME").expect("A test should never fail"),
                Position::LeftOfName
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("LeftOfName").expect("A test should never fail"),
                Position::LeftOfName
            );
            assert_eq!(
                serde_yaml::to_string(&Position::LeftOfName).expect("A test should never fail"),
                "LeftOfName\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("LEFT_OF_TYPE").expect("A test should never fail"),
                Position::LeftOfType
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("LeftOfType").expect("A test should never fail"),
                Position::LeftOfType
            );
            assert_eq!(
                serde_yaml::to_string(&Position::LeftOfType).expect("A test should never fail"),
                "LeftOfType\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("LEFT_OF_NICK_NAME")
                    .expect("A test should never fail"),
                Position::LeftOfTitle
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("LeftOfTitle").expect("A test should never fail"),
                Position::LeftOfTitle
            );
            assert_eq!(
                serde_yaml::to_string(&Position::LeftOfTitle).expect("A test should never fail"),
                "LeftOfTitle\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("RIGHT_OF_NAME")
                    .expect("A test should never fail"),
                Position::RightOfName
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("RightOfName").expect("A test should never fail"),
                Position::RightOfName
            );
            assert_eq!(
                serde_yaml::to_string(&Position::RightOfName).expect("A test should never fail"),
                "RightOfName\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("RIGHT_OF_TYPE")
                    .expect("A test should never fail"),
                Position::RightOfType
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("RightOfType").expect("A test should never fail"),
                Position::RightOfType
            );
            assert_eq!(
                serde_yaml::to_string(&Position::RightOfType).expect("A test should never fail"),
                "RightOfType\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("RIGHT_OF_NICK_NAME")
                    .expect("A test should never fail"),
                Position::RightOfTitle
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("RightOfTitle").expect("A test should never fail"),
                Position::RightOfTitle
            );
            assert_eq!(
                serde_yaml::to_string(&Position::RightOfTitle).expect("A test should never fail"),
                "RightOfTitle\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("TOP_RIGHT").expect("A test should never fail"),
                Position::TopRight
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("TopRight").expect("A test should never fail"),
                Position::TopRight
            );
            assert_eq!(
                serde_yaml::to_string(&Position::TopRight).expect("A test should never fail"),
                "TopRight\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("TOP_LEFT").expect("A test should never fail"),
                Position::TopLeft
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("TopLeft").expect("A test should never fail"),
                Position::TopLeft
            );
            assert_eq!(
                serde_yaml::to_string(&Position::TopLeft).expect("A test should never fail"),
                "TopLeft\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("LEFT_OF_PICTO")
                    .expect("A test should never fail"),
                Position::LeftOfPictograms
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("LeftOfPictograms")
                    .expect("A test should never fail"),
                Position::LeftOfPictograms
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("TopOfStat").expect("A test should never fail"),
                Position::LeftOfPictograms
            );
            assert_eq!(
                serde_yaml::to_string(&Position::LeftOfPictograms)
                    .expect("A test should never fail"),
                "LeftOfPictograms\n"
            );

            assert_eq!(
                serde_yaml::from_str::<Position>("DEFAULT").expect("A test should never fail"),
                Position::default()
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("Default").expect("A test should never fail"),
                Position::default()
            );
            assert_eq!(
                serde_yaml::from_str::<Position>("Anything else")
                    .expect("A test should never fail"),
                Position::default()
            );
            assert_eq!(
                serde_yaml::to_string(&Position::default()).expect("A test should never fail"),
                "Default\n"
            );
        }

        #[test]
        fn test_clone_position() {
            assert_eq!(Position::LeftOfName.clone(), Position::LeftOfName);
            assert_eq!(Position::LeftOfType.clone(), Position::LeftOfType);
            assert_eq!(Position::LeftOfTitle.clone(), Position::LeftOfTitle);
            assert_eq!(Position::RightOfName.clone(), Position::RightOfName);
            assert_eq!(Position::RightOfType.clone(), Position::RightOfType);
            assert_eq!(Position::RightOfTitle.clone(), Position::RightOfTitle);
            assert_eq!(Position::TopRight.clone(), Position::TopRight);
            assert_eq!(Position::TopLeft.clone(), Position::TopLeft);
            assert_eq!(
                Position::LeftOfPictograms.clone(),
                Position::LeftOfPictograms
            );
            assert_eq!(Position::default().clone(), Position::default());
        }

        #[test]
        fn test_display_position() {
            assert_eq!(format!("{}", Position::LeftOfName), "LeftOfName");
            assert_eq!(format!("{}", Position::LeftOfType), "LeftOfType");
            assert_eq!(format!("{}", Position::LeftOfTitle), "LeftOfTitle");
            assert_eq!(format!("{}", Position::RightOfName), "RightOfName");
            assert_eq!(format!("{}", Position::RightOfType), "RightOfType");
            assert_eq!(format!("{}", Position::RightOfTitle), "RightOfTitle");
            assert_eq!(format!("{}", Position::TopRight), "TopRight");
            assert_eq!(format!("{}", Position::TopLeft), "TopLeft");
            assert_eq!(
                format!("{}", Position::LeftOfPictograms),
                "LeftOfPictograms"
            );
            assert_eq!(format!("{}", Position::default()), "Default");

            assert_eq!(format!("{:?}", Position::LeftOfName), "LeftOfName");
            assert_eq!(format!("{:?}", Position::LeftOfType), "LeftOfType");
            assert_eq!(format!("{:?}", Position::LeftOfTitle), "LeftOfTitle");
            assert_eq!(format!("{:?}", Position::RightOfName), "RightOfName");
            assert_eq!(format!("{:?}", Position::RightOfType), "RightOfType");
            assert_eq!(format!("{:?}", Position::RightOfTitle), "RightOfTitle");
            assert_eq!(format!("{:?}", Position::TopRight), "TopRight");
            assert_eq!(format!("{:?}", Position::TopLeft), "TopLeft");
            assert_eq!(
                format!("{:?}", Position::LeftOfPictograms),
                "LeftOfPictograms"
            );
            assert_eq!(format!("{:?}", Position::default()), "Default");
        }

        #[test]
        fn test_hash_position() {
            for position in Position::iter() {
                position.hash(&mut std::collections::hash_map::DefaultHasher::new());
            }
        }

        #[test]
        fn test_into_side_position() {
            assert_eq!(Side::from(Position::LeftOfName), Side::Right);
            assert_eq!(Side::from(Position::LeftOfType), Side::Right);
            assert_eq!(Side::from(Position::LeftOfTitle), Side::Right);
            assert_eq!(Side::from(Position::RightOfName), Side::Left);
            assert_eq!(Side::from(Position::RightOfType), Side::Left);
            assert_eq!(Side::from(Position::RightOfTitle), Side::Left);
            assert_eq!(Side::from(Position::TopRight), Side::Right);
            assert_eq!(Side::from(Position::TopLeft), Side::Left);
            assert_eq!(Side::from(Position::LeftOfPictograms), Side::Middle);
        }

        #[test]
        #[should_panic(
            expected = "The default position should only be used to be replaced by another position, not as a position by itself"
        )]
        fn test_into_side_default_position() {
            let _ = Side::from(Position::default());
        }

        #[test]
        fn test_into_text_anchor_position() {
            assert_eq!(
                usvg::TextAnchor::from(Position::LeftOfName),
                usvg::TextAnchor::from(Side::from(Position::LeftOfName))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::LeftOfType),
                usvg::TextAnchor::from(Side::from(Position::LeftOfType))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::LeftOfTitle),
                usvg::TextAnchor::from(Side::from(Position::LeftOfTitle))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::RightOfName),
                usvg::TextAnchor::from(Side::from(Position::RightOfName))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::RightOfType),
                usvg::TextAnchor::from(Side::from(Position::RightOfType))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::RightOfTitle),
                usvg::TextAnchor::from(Side::from(Position::RightOfTitle))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::TopRight),
                usvg::TextAnchor::from(Side::from(Position::TopRight))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::TopLeft),
                usvg::TextAnchor::from(Side::from(Position::TopLeft))
            );
            assert_eq!(
                usvg::TextAnchor::from(Position::LeftOfPictograms),
                usvg::TextAnchor::from(Side::from(Position::LeftOfPictograms))
            );
        }

        #[test]
        #[should_panic(
            expected = "The default position should only be used to be replaced by another position, not as a position by itself"
        )]
        fn test_into_text_anchor_default_position() {
            let _ = usvg::TextAnchor::from(Position::default());
        }

        #[test]
        fn test_into_dominant_baseline_position() {
            assert_eq!(
                usvg::DominantBaseline::from(Position::LeftOfName),
                usvg::DominantBaseline::Central
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::LeftOfType),
                usvg::DominantBaseline::Central
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::LeftOfTitle),
                usvg::DominantBaseline::Central
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::RightOfName),
                usvg::DominantBaseline::Central
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::RightOfType),
                usvg::DominantBaseline::Central
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::RightOfTitle),
                usvg::DominantBaseline::Central
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::TopRight),
                usvg::DominantBaseline::Hanging
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::TopLeft),
                usvg::DominantBaseline::Hanging
            );
            assert_eq!(
                usvg::DominantBaseline::from(Position::LeftOfPictograms),
                usvg::DominantBaseline::Central
            );
        }

        #[test]
        #[should_panic(
            expected = "The default position should only be used to be replaced by another position, not as a position by itself"
        )]
        fn test_into_dominant_baseline_default_position() {
            let _ = usvg::DominantBaseline::from(Position::default());
        }
    }

    #[cfg(test)]
    mod test_enum_side {
        use super::super::Side;

        #[test]
        fn test_debug_side() {
            assert_eq!(format!("{:?}", Side::Left), "Left");
            assert_eq!(format!("{:?}", Side::Right), "Right");
            assert_eq!(format!("{:?}", Side::Middle), "Middle");
        }

        #[test]
        fn test_into_text_anchor_side() {
            assert_eq!(usvg::TextAnchor::from(Side::Left), usvg::TextAnchor::Start);
            assert_eq!(usvg::TextAnchor::from(Side::Right), usvg::TextAnchor::End);
            assert_eq!(
                usvg::TextAnchor::from(Side::Middle),
                usvg::TextAnchor::Middle
            );
        }
    }

    #[cfg(test)]
    mod test_enum_characteristic_name {
        use super::super::CharacteristicName;

        use std::hash::Hash;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_characteristic_name() {
            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("WeaponSkill")
                    .expect("A test should never fail"),
                CharacteristicName::WeaponSkill
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Weapon Skill")
                    .expect("A test should never fail"),
                CharacteristicName::WeaponSkill
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::WeaponSkill)
                    .expect("A test should never fail"),
                "WeaponSkill\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Strength")
                    .expect("A test should never fail"),
                CharacteristicName::Strength
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Strength)
                    .expect("A test should never fail"),
                "Strength\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Speed")
                    .expect("A test should never fail"),
                CharacteristicName::Speed
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Speed)
                    .expect("A test should never fail"),
                "Speed\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Bravery")
                    .expect("A test should never fail"),
                CharacteristicName::Bravery
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Bravery)
                    .expect("A test should never fail"),
                "Bravery\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Intelligence")
                    .expect("A test should never fail"),
                CharacteristicName::Intelligence
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Intelligence)
                    .expect("A test should never fail"),
                "Intelligence\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Toughness")
                    .expect("A test should never fail"),
                CharacteristicName::Toughness
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Toughness)
                    .expect("A test should never fail"),
                "Toughness\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Luck")
                    .expect("A test should never fail"),
                CharacteristicName::Luck
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Luck).expect("A test should never fail"),
                "Luck\n"
            );

            assert_eq!(
                serde_yaml::from_str::<CharacteristicName>("Charisma")
                    .expect("A test should never fail"),
                CharacteristicName::Charisma
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicName::Charisma)
                    .expect("A test should never fail"),
                "Charisma\n"
            );
        }

        #[test]
        fn test_display_characteristic_name() {
            assert_eq!(
                format!("{}", CharacteristicName::WeaponSkill),
                "Weapon Skill"
            );
            assert_eq!(format!("{}", CharacteristicName::Strength), "Strength");
            assert_eq!(format!("{}", CharacteristicName::Speed), "Speed");
            assert_eq!(format!("{}", CharacteristicName::Bravery), "Bravery");
            assert_eq!(
                format!("{}", CharacteristicName::Intelligence),
                "Intelligence"
            );
            assert_eq!(format!("{}", CharacteristicName::Toughness), "Toughness");
            assert_eq!(format!("{}", CharacteristicName::Luck), "Luck");
            assert_eq!(format!("{}", CharacteristicName::Charisma), "Charisma");

            assert_eq!(
                format!("{:?}", CharacteristicName::WeaponSkill),
                "WeaponSkill"
            );
            assert_eq!(format!("{:?}", CharacteristicName::Strength), "Strength");
            assert_eq!(format!("{:?}", CharacteristicName::Speed), "Speed");
            assert_eq!(format!("{:?}", CharacteristicName::Bravery), "Bravery");
            assert_eq!(
                format!("{:?}", CharacteristicName::Intelligence),
                "Intelligence"
            );
            assert_eq!(format!("{:?}", CharacteristicName::Toughness), "Toughness");
            assert_eq!(format!("{:?}", CharacteristicName::Luck), "Luck");
            assert_eq!(format!("{:?}", CharacteristicName::Charisma), "Charisma");
        }

        #[test]
        fn test_clone_characteristic_name() {
            assert_eq!(
                CharacteristicName::WeaponSkill.clone(),
                CharacteristicName::WeaponSkill
            );
            assert_eq!(
                CharacteristicName::Strength.clone(),
                CharacteristicName::Strength
            );
            assert_eq!(CharacteristicName::Speed.clone(), CharacteristicName::Speed);
            assert_eq!(
                CharacteristicName::Bravery.clone(),
                CharacteristicName::Bravery
            );
            assert_eq!(
                CharacteristicName::Intelligence.clone(),
                CharacteristicName::Intelligence
            );
            assert_eq!(
                CharacteristicName::Toughness.clone(),
                CharacteristicName::Toughness
            );
            assert_eq!(CharacteristicName::Luck.clone(), CharacteristicName::Luck);
            assert_eq!(
                CharacteristicName::Charisma.clone(),
                CharacteristicName::Charisma
            );
        }

        #[test]
        fn test_hash_characteristic_name() {
            let mut hasher = std::collections::hash_map::DefaultHasher::new();
            for characteristic in CharacteristicName::iter() {
                assert_eq!(
                    characteristic.hash(&mut hasher),
                    characteristic.hash(&mut hasher)
                )
            }
        }

        #[test]
        fn test_ord_characteristic_name() {
            let mut iter = CharacteristicName::iter();
            assert_eq!(iter.next(), Some(CharacteristicName::WeaponSkill));
            assert_eq!(iter.next(), Some(CharacteristicName::Strength));
            assert_eq!(iter.next(), Some(CharacteristicName::Speed));
            assert_eq!(iter.next(), Some(CharacteristicName::Bravery));
            assert_eq!(iter.next(), Some(CharacteristicName::Intelligence));
            assert_eq!(iter.next(), Some(CharacteristicName::Toughness));
            assert_eq!(iter.next(), Some(CharacteristicName::Luck));
            assert_eq!(iter.next(), Some(CharacteristicName::Charisma));
            assert_eq!(iter.next(), None);

            assert!(CharacteristicName::WeaponSkill < CharacteristicName::Strength);
            assert!(CharacteristicName::Strength < CharacteristicName::Speed);
            assert!(CharacteristicName::Speed < CharacteristicName::Bravery);
            assert!(CharacteristicName::Bravery < CharacteristicName::Intelligence);
            assert!(CharacteristicName::Intelligence < CharacteristicName::Toughness);
            assert!(CharacteristicName::Toughness < CharacteristicName::Luck);
            assert!(CharacteristicName::Luck < CharacteristicName::Charisma);

            assert_eq!(
                CharacteristicName::WeaponSkill.max(CharacteristicName::Strength),
                CharacteristicName::Strength
            );
            assert_eq!(
                CharacteristicName::Strength.max(CharacteristicName::Speed),
                CharacteristicName::Speed
            );
            assert_eq!(
                CharacteristicName::Speed.max(CharacteristicName::Bravery),
                CharacteristicName::Bravery
            );
            assert_eq!(
                CharacteristicName::Bravery.max(CharacteristicName::Intelligence),
                CharacteristicName::Intelligence
            );
            assert_eq!(
                CharacteristicName::Intelligence.max(CharacteristicName::Toughness),
                CharacteristicName::Toughness
            );
            assert_eq!(
                CharacteristicName::Toughness.max(CharacteristicName::Luck),
                CharacteristicName::Luck
            );
            assert_eq!(
                CharacteristicName::Luck.max(CharacteristicName::Charisma),
                CharacteristicName::Charisma
            );

            assert_eq!(
                CharacteristicName::WeaponSkill.min(CharacteristicName::Strength),
                CharacteristicName::WeaponSkill
            );
            assert_eq!(
                CharacteristicName::Strength.min(CharacteristicName::Speed),
                CharacteristicName::Strength
            );
            assert_eq!(
                CharacteristicName::Speed.min(CharacteristicName::Bravery),
                CharacteristicName::Speed
            );
            assert_eq!(
                CharacteristicName::Bravery.min(CharacteristicName::Intelligence),
                CharacteristicName::Bravery
            );
            assert_eq!(
                CharacteristicName::Intelligence.min(CharacteristicName::Toughness),
                CharacteristicName::Intelligence
            );
            assert_eq!(
                CharacteristicName::Toughness.min(CharacteristicName::Luck),
                CharacteristicName::Toughness
            );
            assert_eq!(
                CharacteristicName::Luck.min(CharacteristicName::Charisma),
                CharacteristicName::Luck
            );
        }
    }

    mod test_enum_field_type {
        use super::super::FieldType;

        #[test]
        fn test_display() {
            assert_eq!(format!("{}", FieldType::Name), "Name");
            assert_eq!(format!("{}", FieldType::Type), "Type");
            assert_eq!(format!("{}", FieldType::Title), "Title");
        }
    }

    #[cfg(test)]
    mod test_struct_power_field {
        use super::super::PowerField;

        use super::super::super::Updatable;
        use super::super::Position;
        use crate::card::symbol::{Symbol, SymbolField};
        use crate::color::Color;
        use crate::error::Error;

        #[test]
        fn test_convert_power_field() {
            let mut power = PowerField {
                value: "00".to_owned(),
                size: 0,
                position: Position::default(),
                color: Color::default(),
            };
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Bloque.into()
            );
            power.value = "01".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Traitre.into()
            );
            power.value = "02".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Zizanie.into()
            );
            power.value = "03".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Retourne.into()
            );
            power.value = "04".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Pitie.into()
            );
            power.value = "05".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::FlopFlop.into()
            );
            power.value = "06".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::AttaqueSournoise.into()
            );
            power.value = "PaX".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Paix.into()
            );
            power.value = "pAiX".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                Symbol::Paix.into()
            );

            power.value = "f10".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                SymbolField {
                    symbol: Symbol::ForceObscure,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                }
            );
            power.value = "FD10".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                SymbolField {
                    symbol: Symbol::ForceObscure,
                    value: "D10".try_into().expect("A test should never fail"),
                    text_color: Color::Default,
                    border_color: Color::Default,
                }
            );
            power.value = "Farm".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()),
                Err(Error::ConversionError)
            );

            power.value = "bD10".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                SymbolField {
                    symbol: Symbol::Bouclier,
                    value: "D10".try_into().expect("A test should never fail"),
                    text_color: Color::Default,
                    border_color: Color::Default,
                }
            );
            power.value = "B10".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                SymbolField {
                    symbol: Symbol::Bouclier,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                }
            );
            power.value = "Ball".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()),
                Err(Error::ConversionError)
            );

            power.value = "c10".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                SymbolField {
                    symbol: Symbol::Credits,
                    value: 10.into(),
                    text_color: Color::Default,
                    border_color: Color::Default,
                }
            );
            power.value = "CD10".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()).expect("A test should never fail"),
                SymbolField {
                    symbol: Symbol::Credits,
                    value: "D10".try_into().expect("A test should never fail"),
                    text_color: Color::Default,
                    border_color: Color::Default,
                }
            );
            power.value = "Chance".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()),
                Err(Error::ConversionError)
            );

            power.value = "A power".to_owned();
            assert_eq!(
                SymbolField::try_from(power.clone()),
                Err(Error::ConversionError)
            );
        }

        #[test]
        fn test_serde_power_field() {
            let power = PowerField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
                position: Position::TopRight,
            };

            let test = "value: test\ncolor: Purple\nsize: 123\nposition: TopRight";
            let mut test =
                serde_yaml::from_str::<PowerField>(test).expect("A test should never fail");
            assert!(!test.update(&power));
            assert_eq!(test, power);

            let test = "value: test\ncolor: Purple\nposition: TopRight";
            let mut test =
                serde_yaml::from_str::<PowerField>(test).expect("A test should never fail");
            assert!(test.update(&power));
            assert_eq!(test, power);

            let test = "value: test\nsize: 123\nposition: TopRight";
            let mut test =
                serde_yaml::from_str::<PowerField>(test).expect("A test should never fail");
            assert!(test.update(&power));
            assert_eq!(test, power);

            let test = "color: Purple\nsize: 123\nposition: TopRight";
            let mut test =
                serde_yaml::from_str::<PowerField>(test).expect("A test should never fail");
            assert!(test.update(&power));
            assert_eq!(test, power);

            let test = "value: test\ncolor: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<PowerField>(test).expect("A test should never fail");
            assert!(test.update(&power));
            assert_eq!(test, power);

            let test = "";
            let mut test =
                serde_yaml::from_str::<PowerField>(test).expect("A test should never fail");
            assert!(test.update(&power));
            assert_eq!(test, power);

            assert_eq!(
                serde_yaml::to_string(&power).expect("A test should never fail"),
                "value: test\nsize: 123\ncolor: Purple\nposition: TopRight\n"
            )
        }

        #[test]
        fn test_default_power_field() {
            let power = PowerField::default();
            assert_eq!(power, crate::card::Card::get_default().powers[0]);

            assert_eq!(
                PowerField::get_default(),
                PowerField {
                    value: "".to_owned(),
                    color: Color::default(),
                    size: 0,
                    position: Position::default(),
                }
            )
        }

        #[test]
        fn test_debug_power_field() {
            assert_eq!(
                format!("{:?}", PowerField::get_default()),
                "PositionedField { value: \"\", size: 0, color: Default, position: Default }"
            );
        }
    }

    #[cfg(test)]
    mod test_struct_vec_power_field {
        use super::super::PowerField;

        use super::super::Position;
        use crate::card::illustration::{Mode, Orientation};
        use crate::card::Card;
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_render_vec_power_field() {
            let mut powers = vec![];
            for position in Position::iter() {
                if position != Position::default() {
                    powers.push(PowerField {
                        value: "bla".to_owned(),
                        color: Color::Black,
                        size: 100,
                        position,
                    });
                    powers.push(PowerField {
                        value: "bla2".to_owned(),
                        color: Color::Black,
                        size: 100,
                        position,
                    });
                }
            }

            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);
                    let mut card = Card::default();
                    card.name.value = "Test".to_owned(); // Field with data
                    card.r#type.value = " ".to_owned(); // Field with data but without size
                    card.title.value = "".to_owned(); // Field without data
                    let card = card.render(false).expect("A test should never fail");

                    let render = powers
                        .render(&Layer::Powers, &config, Some(&card))
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::Powers));

                    assert!(std::panic::catch_unwind(|| powers.render(
                        &Layer::Powers,
                        &config,
                        None
                    ))
                    .is_err());
                }
            }
        }
    }

    #[cfg(test)]
    mod test_struct_base_field {
        use super::super::{BaseField, NameField, NumberField, TitleField, TypeField};

        use super::super::super::Updatable;
        use super::super::{FieldType, Position, PowerField};
        use crate::card::illustration::{Mode, Orientation};
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_name_field() {
            let name = NameField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };

            let test = "value: test\ncolor: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<NameField>(test).expect("A test should never fail");
            assert!(!test.update(&name));
            assert_eq!(test, name);

            let test = "value: test\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<NameField>(test).expect("A test should never fail");
            assert!(test.update(&name));
            assert_eq!(test, name);

            let test = "value: test\nsize: 123";
            let mut test =
                serde_yaml::from_str::<NameField>(test).expect("A test should never fail");
            assert!(test.update(&name));
            assert_eq!(test, name);

            let test = "color: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<NameField>(test).expect("A test should never fail");
            assert!(test.update(&name));
            assert_eq!(test, name);

            let test = "";
            let mut test =
                serde_yaml::from_str::<NameField>(test).expect("A test should never fail");
            assert!(test.update(&name));
            assert_eq!(test, name);

            assert_eq!(
                serde_yaml::to_string(&name).expect("A test should never fail"),
                "value: test\nsize: 123\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_serde_type_field() {
            let r#type = TypeField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };

            let test = "value: test\ncolor: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<TypeField>(test).expect("A test should never fail");
            assert!(!test.update(&r#type));
            assert_eq!(test, r#type);

            let test = "value: test\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<TypeField>(test).expect("A test should never fail");
            assert!(test.update(&r#type));
            assert_eq!(test, r#type);

            let test = "value: test\nsize: 123";
            let mut test =
                serde_yaml::from_str::<TypeField>(test).expect("A test should never fail");
            assert!(test.update(&r#type));
            assert_eq!(test, r#type);

            let test = "color: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<TypeField>(test).expect("A test should never fail");
            assert!(test.update(&r#type));
            assert_eq!(test, r#type);

            let test = "";
            let mut test =
                serde_yaml::from_str::<TypeField>(test).expect("A test should never fail");
            assert!(test.update(&r#type));
            assert_eq!(test, r#type);

            assert_eq!(
                serde_yaml::to_string(&r#type).expect("A test should never fail"),
                "value: test\nsize: 123\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_serde_title_field() {
            let title = TitleField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };

            let test = "value: test\ncolor: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<TitleField>(test).expect("A test should never fail");
            assert!(!test.update(&title));
            assert_eq!(test, title);

            let test = "value: test\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<TitleField>(test).expect("A test should never fail");
            assert!(test.update(&title));
            assert_eq!(test, title);

            let test = "value: test\nsize: 123";
            let mut test =
                serde_yaml::from_str::<TitleField>(test).expect("A test should never fail");
            assert!(test.update(&title));
            assert_eq!(test, title);

            let test = "color: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<TitleField>(test).expect("A test should never fail");
            assert!(test.update(&title));
            assert_eq!(test, title);

            let test = "";
            let mut test =
                serde_yaml::from_str::<TitleField>(test).expect("A test should never fail");
            assert!(test.update(&title));
            assert_eq!(test, title);

            assert_eq!(
                serde_yaml::to_string(&title).expect("A test should never fail"),
                "value: test\nsize: 123\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_serde_number_field() {
            let number = NumberField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };

            let test = "value: test\ncolor: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<NumberField>(test).expect("A test should never fail");
            assert!(!test.update(&number));
            assert_eq!(test, number);

            let test = "value: test\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<NumberField>(test).expect("A test should never fail");
            assert!(test.update(&number));
            assert_eq!(test, number);

            let test = "value: test\nsize: 123";
            let mut test =
                serde_yaml::from_str::<NumberField>(test).expect("A test should never fail");
            assert!(test.update(&number));
            assert_eq!(test, number);

            let test = "color: Purple\nsize: 123";
            let mut test =
                serde_yaml::from_str::<NumberField>(test).expect("A test should never fail");
            assert!(test.update(&number));
            assert_eq!(test, number);

            let test = "";
            let mut test =
                serde_yaml::from_str::<NumberField>(test).expect("A test should never fail");
            assert!(test.update(&number));
            assert_eq!(test, number);

            assert_eq!(
                serde_yaml::to_string(&number).expect("A test should never fail"),
                "value: test\nsize: 123\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_debug_base_field() {
            let base = BaseField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };
            assert_eq!(
                format!("{:?}", base),
                "BaseField { value: \"test\", size: 123, color: Purple }"
            );
        }

        #[test]
        fn test_clone_base_field() {
            let base = BaseField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };
            assert_eq!(base.clone(), base);
        }

        #[test]
        fn test_into_power_field_number_field() {
            let number = NumberField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };
            let power = PowerField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
                position: Position::default(),
            };
            assert_eq!(PowerField::from(number), power);
        }

        #[test]
        fn test_default_base_field() {
            let base = BaseField::get_default();
            assert_eq!(
                base,
                BaseField {
                    value: "".into(),
                    size: 0,
                    color: Color::default()
                }
            );

            assert_eq!(
                BaseField::default(FieldType::Name),
                crate::card::Card::get_default().name
            );
            assert_eq!(
                BaseField::default(FieldType::Type),
                crate::card::Card::get_default().r#type
            );
            assert_eq!(
                BaseField::default(FieldType::Title),
                crate::card::Card::get_default().title
            );
        }

        #[test]
        fn test_render_base_field() {
            let field = BaseField {
                value: "test".to_owned(),
                color: Color::Purple,
                size: 123,
            };
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    for layer in vec![Layer::Name, Layer::Title, Layer::Type] {
                        let render = field
                            .render(&layer, &config, None)
                            .expect("A test should never fail");

                        assert!(render.has_node(&layer));
                    }
                }
            }
        }
    }

    #[cfg(test)]
    mod test_struct_gender_field {
        use super::super::GenderField;

        use super::super::super::Updatable;
        use crate::card::illustration::{Mode, Orientation};
        use crate::card::logos::Gender;
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_gender_field() {
            let gender = GenderField {
                logo: Gender::Male,
                color: Color::Purple,
            };

            let test = "gender: Male\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<GenderField>(test).expect("A test should never fail");
            assert!(!test.update(&gender));
            assert_eq!(test, gender);

            let test = "gender: Male";
            let mut test =
                serde_yaml::from_str::<GenderField>(test).expect("A test should never fail");
            assert!(test.update(&gender));
            assert_eq!(test, gender);

            let test = "color: Purple";
            let mut test =
                serde_yaml::from_str::<GenderField>(test).expect("A test should never fail");
            assert!(!test.update(&gender));
            assert_eq!(
                test,
                GenderField {
                    logo: Gender::default(),
                    color: Color::Purple
                }
            );

            assert_eq!(
                serde_yaml::to_string(&gender).expect("A test should never fail"),
                "logo: Male\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_debug_gender_field() {
            let gender = GenderField {
                logo: Gender::Male,
                color: Color::Purple,
            };
            assert_eq!(
                format!("{:?}", gender),
                "LogoField { logo: Male, color: Purple }"
            );
        }

        #[test]
        fn test_clone_gender_field() {
            let gender = GenderField {
                logo: Gender::Male,
                color: Color::Purple,
            };
            assert_eq!(gender.clone(), gender);
        }

        #[test]
        fn test_default_gender_field() {
            let gender = GenderField::default();
            assert_eq!(gender, crate::card::Card::get_default().gender);

            assert_eq!(
                GenderField::get_default(),
                GenderField {
                    logo: Gender::default(),
                    color: Color::default()
                }
            );
        }

        #[test]
        fn test_render_gender_field() {
            let field = GenderField {
                logo: Gender::default(),
                color: Color::Purple,
            };
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let render = field
                        .render(&Layer::Gender, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::Gender));
                }
            }
        }
    }

    #[cfg(test)]
    mod test_struct_era_field {
        use super::super::EraField;

        use super::super::super::Updatable;
        use crate::card::illustration::{Mode, Orientation};
        use crate::card::logos::Era;
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_era_field() {
            let era = EraField {
                logo: Era::Future,
                color: Color::Purple,
            };

            let test = "era: Future\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<EraField>(test).expect("A test should never fail");
            assert!(!test.update(&era));
            assert_eq!(test, era);

            let test = "era: Future";
            let mut test =
                serde_yaml::from_str::<EraField>(test).expect("A test should never fail");
            assert!(test.update(&era));
            assert_eq!(test, era);

            let test = "color: Purple";
            let mut test =
                serde_yaml::from_str::<EraField>(test).expect("A test should never fail");
            assert!(!test.update(&era));
            assert_eq!(
                test,
                EraField {
                    logo: Era::default(),
                    color: Color::Purple
                }
            );

            assert_eq!(
                serde_yaml::to_string(&era).expect("A test should never fail"),
                "logo: Future\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_debug_era_field() {
            let era = EraField {
                logo: Era::All,
                color: Color::Purple,
            };
            assert_eq!(
                format!("{:?}", era),
                "LogoField { logo: All, color: Purple }"
            );
        }

        #[test]
        fn test_clone_era_field() {
            let era = EraField {
                logo: Era::All,
                color: Color::Purple,
            };
            assert_eq!(era.clone(), era);
        }

        #[test]
        fn test_default_era_field() {
            let era = EraField::default();
            assert_eq!(era, crate::card::Card::get_default().era);

            assert_eq!(
                EraField::get_default(),
                EraField {
                    logo: Era::default(),
                    color: Color::default()
                }
            );
        }

        #[test]
        fn test_render_era_field() {
            let field = EraField {
                logo: Era::default(),
                color: Color::Purple,
            };
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let render = field
                        .render(&Layer::Era, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::Era));
                }
            }
        }
    }

    #[cfg(test)]
    mod test_struct_archetype_field {
        use super::super::ArchetypeField;

        use super::super::super::Updatable;
        use crate::card::illustration::{Mode, Orientation};
        use crate::card::logos::Archetype;
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_archetype_field() {
            let archetype = ArchetypeField {
                logo: Archetype::Wanderer,
                color: Color::Purple,
            };

            let test = "archetype: Wanderer\ncolor: Purple";
            let mut test =
                serde_yaml::from_str::<ArchetypeField>(test).expect("A test should never fail");
            assert!(!test.update(&archetype));
            assert_eq!(test, archetype);

            let test = "archetype: Wanderer";
            let mut test =
                serde_yaml::from_str::<ArchetypeField>(test).expect("A test should never fail");
            assert!(test.update(&archetype));
            assert_eq!(test, archetype);

            let test = "color: Purple";
            let mut test =
                serde_yaml::from_str::<ArchetypeField>(test).expect("A test should never fail");
            assert!(!test.update(&archetype));
            assert_eq!(
                test,
                ArchetypeField {
                    logo: Archetype::default(),
                    color: Color::Purple
                }
            );

            assert_eq!(
                serde_yaml::to_string(&archetype).expect("A test should never fail"),
                "logo: Wanderer\ncolor: Purple\n"
            )
        }

        #[test]
        fn test_debug_archetype_field() {
            let archetype = ArchetypeField {
                logo: Archetype::Uncivilized,
                color: Color::Purple,
            };
            assert_eq!(
                format!("{:?}", archetype),
                "LogoField { logo: Uncivilized, color: Purple }"
            );
        }

        #[test]
        fn test_clone_archetype_field() {
            let archetype = ArchetypeField {
                logo: Archetype::Uncivilized,
                color: Color::Purple,
            };
            assert_eq!(archetype.clone(), archetype);
        }

        #[test]
        fn test_default_archetype_field() {
            let archetype = ArchetypeField::default();
            assert_eq!(archetype, crate::card::Card::get_default().archetype);

            assert_eq!(
                ArchetypeField::get_default(),
                ArchetypeField {
                    logo: Archetype::default(),
                    color: Color::default()
                }
            );
        }

        #[test]
        fn test_render_archetype_field() {
            let field = ArchetypeField {
                logo: Archetype::default(),
                color: Color::Purple,
            };
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let render = field
                        .render(&Layer::Archetype, &config, None)
                        .expect("A test should never fail");
                    assert!(render.has_node(&Layer::Archetype));
                }
            }
        }
    }

    #[cfg(test)]
    mod test_struct_characteristics_field {
        use super::super::CharacteristicsField;

        use super::super::super::Updatable;
        use super::super::CharacteristicName;
        use crate::card::illustration::{Mode, Orientation};
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};
        use crate::types::CharacteristicValue;

        use std::collections::BTreeMap;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_characteristics_field() {
            let characteristics = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(!test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact_fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(!test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: false\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(!test.update(&characteristics));
            let mut tmp = characteristics.clone();
            tmp.compact_fraction = false;
            assert_eq!(test, tmp);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(!test.update(&characteristics));
            let mut tmp = characteristics.clone();
            tmp.compact_fraction = false;
            assert_eq!(test, tmp);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            let mut tmp = characteristics.clone();
            tmp.compact_fraction = false;
            assert_eq!(test, tmp);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  WeaponSkill: 1\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "stats:\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            let test = "size: 120\ncolor: Purple\ncompact fraction: true\nname_color: Pink";
            let mut test = serde_yaml::from_str::<CharacteristicsField>(test)
                .expect("A test should never fail");
            assert!(test.update(&characteristics));
            assert_eq!(test, characteristics);

            assert_eq!(serde_yaml::to_string(&characteristics).expect("A test should never fail"), "stats:\n  WeaponSkill: 1\n  Strength: 2\n  Speed: 3\n  Bravery: 4\n  Intelligence: 5\n  Toughness: 6\n  Luck: 7\n  Charisma: 8\nsize: 120\ncolor: Purple\ncompact_fraction: true\nname_color: Pink\n");
        }

        #[test]
        fn test_get_characteristics_field() {
            let characteristics = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };
            let mut i = 1;
            for characteristic in CharacteristicName::iter() {
                assert_eq!(characteristics.get(&characteristic), i.into());
                i += 1;
            }

            let characteristics = CharacteristicsField {
                stats: BTreeMap::new(),
                size: 120,
                color: Color::Purple,
                compact_fraction: false,
                name_color: Color::Pink,
            };
            for characteristic in CharacteristicName::iter() {
                assert_eq!(
                    characteristics.get(&characteristic),
                    CharacteristicValue::default()
                );
            }
        }

        #[test]
        fn test_set_characteristics_field() {
            let mut characteristics = CharacteristicsField::default();
            for characteristic in CharacteristicName::iter() {
                characteristics.set(&characteristic, 1.into());
            }

            for characteristic in CharacteristicName::iter() {
                assert_eq!(characteristics.get(&characteristic), 1.into());
            }
        }

        #[test]
        fn test_default_characteristics_field() {
            let characteristics = CharacteristicsField::default();
            assert_eq!(
                characteristics,
                crate::card::Card::get_default().characteristics
            );

            assert_eq!(
                CharacteristicsField::get_default(),
                CharacteristicsField {
                    stats: BTreeMap::new(),
                    size: 0,
                    color: Color::default(),
                    compact_fraction: false,
                    name_color: Color::default()
                }
            )
        }

        #[test]
        fn test_debug_characteristics_field() {
            let characteristics = CharacteristicsField::get_default();
            assert_eq!(format!("{:?}", characteristics), "CharacteristicsField { stats: {}, size: 0, color: Default, compact_fraction: false, name_color: Default }");
        }

        #[test]
        fn test_render_characteristics_field() {
            let mut characteristics = CharacteristicsField::default();
            characteristics.set(&CharacteristicName::WeaponSkill, CharacteristicValue::Empty);
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let render = characteristics
                        .render(&Layer::Characteristics, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::CharacteristicsValue));
                    assert!(render.has_node(&Layer::CharacteristicsName));
                    assert!(render.has_node(&Layer::LuckValue));
                    assert!(render.has_node(&Layer::CharismaValue));
                }
            }

            let mut characteristics = CharacteristicsField::default();
            for characteristic in CharacteristicName::iter() {
                characteristics.set(&characteristic, CharacteristicValue::Empty);
            }
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let render = characteristics
                        .render(&Layer::Characteristics, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::CharacteristicsValue));
                    assert!(render.has_node(&Layer::CharacteristicsName));
                    assert!(!render.has_node(&Layer::LuckValue));
                    assert!(!render.has_node(&Layer::CharismaValue));
                }
            }
        }
    }

    mod test_log_updatable {
        use super::super::super::Updatable;
        use super::super::{
            ArchetypeField, BaseField, CharacteristicsField, EraField, GenderField, PowerField,
        };
        use super::super::{CharacteristicName, Position};
        use crate::card::logos::{Archetype, Era, Gender};
        use crate::color::Color;

        use log::Level;
        use log_tester::LogTester;
        use std::collections::BTreeMap;

        #[test]
        fn test_value_base_field() {
            LogTester::start();
            let mut field = BaseField {
                value: "".to_owned(),
                size: 100,
                color: Color::Black,
            };

            let default = BaseField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "Something");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Black);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The value of a base field is empty, using default value : Something"
            ));
        }

        #[test]
        fn test_size_base_field() {
            LogTester::start();
            let mut field = BaseField {
                value: "test".to_owned(),
                size: 0,
                color: Color::Black,
            };

            let default = BaseField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 10);
            assert_eq!(field.color, Color::Black);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The size of a base field is not defined, using default value : 10"
            ));
        }

        #[test]
        fn test_color_base_field() {
            LogTester::start();
            let mut field = BaseField {
                value: "test".to_owned(),
                size: 100,
                color: Color::Default,
            };

            let default = BaseField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Purple);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of a base field is not defined, using default value : Purple"
            ));
        }

        #[test]
        fn test_closest_color_base_field() {
            LogTester::start();
            let mut field = BaseField {
                value: "test".to_owned(),
                size: 100,
                color: Color::Custom(0, 0, 0, 0),
            };

            let default = BaseField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Transparent);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(Level::Warn, "The color of a base field Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_value_power_field() {
            LogTester::start();
            let mut field = PowerField {
                value: "".to_owned(),
                size: 100,
                color: Color::Black,
                position: Position::TopLeft,
            };

            let default = PowerField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
                position: Position::TopRight,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "Something");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.position, Position::TopLeft);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The value of a power is empty, using default value : Something"
            ));
        }

        #[test]
        fn test_size_power_field() {
            LogTester::start();
            let mut field = PowerField {
                value: "test".to_owned(),
                size: 0,
                color: Color::Black,
                position: Position::TopLeft,
            };

            let default = PowerField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
                position: Position::TopRight,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 10);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.position, Position::TopLeft);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The size of a power is not defined, using default value : 10"
            ));
        }

        #[test]
        fn test_color_power_field() {
            LogTester::start();
            let mut field = PowerField {
                value: "test".to_owned(),
                size: 100,
                color: Color::Default,
                position: Position::TopLeft,
            };

            let default = PowerField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
                position: Position::TopRight,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Purple);
            assert_eq!(field.position, Position::TopLeft);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of a power is not defined, using default value : Purple"
            ));
        }

        #[test]
        fn test_closest_color_power_field() {
            LogTester::start();
            let mut field = PowerField {
                value: "test".to_owned(),
                size: 100,
                color: Color::Custom(0, 0, 0, 0),
                position: Position::TopLeft,
            };

            let default = PowerField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
                position: Position::TopRight,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Transparent);
            assert_eq!(field.position, Position::TopLeft);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of a power Custom(0, 0, 0, 0) was too close to Transparent, replacing"
            ));
        }

        #[test]
        fn test_position_power_field() {
            LogTester::start();
            let mut field = PowerField {
                value: "test".to_owned(),
                size: 100,
                color: Color::Black,
                position: Position::default(),
            };

            let default = PowerField {
                value: "Something".to_owned(),
                size: 10,
                color: Color::Purple,
                position: Position::TopRight,
            };

            assert!(field.update(&default));
            assert_eq!(field.value, "test");
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.position, Position::TopRight);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The position of a power is not defined, using default value : TopRight"
            ));
        }

        #[test]
        fn test_gender_gender_field() {
            LogTester::start();
            let mut field = GenderField {
                logo: Gender::default(),
                color: Color::Black,
            };

            let default = GenderField {
                logo: Gender::Male,
                color: Color::Purple,
            };

            assert!(!field.update(&default));
            assert_eq!(field.logo, Gender::default());
            assert_eq!(field.color, Color::Black);

            assert!(LogTester::len() == 0);
        }

        #[test]
        fn test_color_gender_field() {
            LogTester::start();
            let mut field = GenderField {
                logo: Gender::Female,
                color: Color::Default,
            };

            let default = GenderField {
                logo: Gender::Male,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.logo, Gender::Female);
            assert_eq!(field.color, Color::Purple);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the gender is not defined, using default value : Purple"
            ));
        }

        #[test]
        fn test_closest_color_gender_field() {
            LogTester::start();
            let mut field = GenderField {
                logo: Gender::Female,
                color: Color::Custom(0, 0, 0, 0),
            };

            let default = GenderField {
                logo: Gender::Male,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.logo, Gender::Female);
            assert_eq!(field.color, Color::Transparent);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(Level::Warn, "The color of the gender Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_era_era_field() {
            LogTester::start();
            let mut field = EraField {
                logo: Era::default(),
                color: Color::Black,
            };

            let default = EraField {
                logo: Era::Past,
                color: Color::Purple,
            };

            assert!(!field.update(&default));
            assert_eq!(field.logo, Era::default());
            assert_eq!(field.color, Color::Black);

            assert!(LogTester::len() == 0);
        }

        #[test]
        fn test_color_era_field() {
            LogTester::start();
            let mut field = EraField {
                logo: Era::Future,
                color: Color::Default,
            };

            let default = EraField {
                logo: Era::Past,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.logo, Era::Future);
            assert_eq!(field.color, Color::Purple);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the era is not defined, using default value : Purple"
            ));
        }

        #[test]
        fn test_closest_color_era_field() {
            LogTester::start();
            let mut field = EraField {
                logo: Era::Future,
                color: Color::Custom(0, 0, 0, 0),
            };

            let default = EraField {
                logo: Era::Past,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.logo, Era::Future);
            assert_eq!(field.color, Color::Transparent);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the era Custom(0, 0, 0, 0) was too close to Transparent, replacing"
            ));
        }

        #[test]
        fn test_archetype_archetype_field() {
            LogTester::start();
            let mut field = ArchetypeField {
                logo: Archetype::default(),
                color: Color::Black,
            };

            let default = ArchetypeField {
                logo: Archetype::Religious,
                color: Color::Purple,
            };

            assert!(!field.update(&default));
            assert_eq!(field.logo, Archetype::default());
            assert_eq!(field.color, Color::Black);

            assert!(LogTester::len() == 0);
        }

        #[test]
        fn test_color_archetype_field() {
            LogTester::start();
            let mut field = ArchetypeField {
                logo: Archetype::Magic,
                color: Color::Default,
            };

            let default = ArchetypeField {
                logo: Archetype::Religious,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.logo, Archetype::Magic);
            assert_eq!(field.color, Color::Purple);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the archetype is not defined, using default value : Purple"
            ));
        }

        #[test]
        fn test_closest_color_archetype_field() {
            LogTester::start();
            let mut field = ArchetypeField {
                logo: Archetype::Magic,
                color: Color::Custom(0, 0, 0, 0),
            };

            let default = ArchetypeField {
                logo: Archetype::Religious,
                color: Color::Purple,
            };

            assert!(field.update(&default));
            assert_eq!(field.logo, Archetype::Magic);
            assert_eq!(field.color, Color::Transparent);

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(Level::Warn, "The color of the archetype Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_value_characteristics_field() {
            LogTester::start();
            let mut field = CharacteristicsField {
                stats: BTreeMap::new(),
                size: 100,
                color: Color::Black,
                compact_fraction: false,
                name_color: Color::White,
            };

            let default = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            assert!(field.update(&default));
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.compact_fraction, false);
            assert_eq!(field.name_color, Color::White);
            assert_eq!(field.get(&CharacteristicName::WeaponSkill), 1.into());
            assert_eq!(field.get(&CharacteristicName::Strength), 2.into());
            assert_eq!(field.get(&CharacteristicName::Speed), 3.into());
            assert_eq!(field.get(&CharacteristicName::Bravery), 4.into());
            assert_eq!(field.get(&CharacteristicName::Intelligence), 5.into());
            assert_eq!(field.get(&CharacteristicName::Toughness), 6.into());
            assert_eq!(field.get(&CharacteristicName::Luck), 7.into());
            assert_eq!(field.get(&CharacteristicName::Charisma), 8.into());

            assert!(LogTester::len() == 8);
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Weapon Skill is not defined, using default value : 1"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Strength is not defined, using default value : 2"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Speed is not defined, using default value : 3"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Bravery is not defined, using default value : 4"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Intelligence is not defined, using default value : 5"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Toughness is not defined, using default value : 6"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Luck is not defined, using default value : 7"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "The characteristic Charisma is not defined, using default value : 8"
            ));
        }

        #[test]
        fn test_size_characteristics_field() {
            LogTester::start();
            let mut field = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 11.into()),
                    (CharacteristicName::Strength, 22.into()),
                    (CharacteristicName::Speed, 33.into()),
                    (CharacteristicName::Bravery, 44.into()),
                    (CharacteristicName::Intelligence, 55.into()),
                    (CharacteristicName::Toughness, 66.into()),
                    (CharacteristicName::Luck, 77.into()),
                    (CharacteristicName::Charisma, 88.into()),
                ]),
                size: 0,
                color: Color::Black,
                compact_fraction: false,
                name_color: Color::White,
            };

            let default = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            assert!(field.update(&default));
            assert_eq!(field.size, 120);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.compact_fraction, false);
            assert_eq!(field.name_color, Color::White);
            assert_eq!(
                field.stats.get(&CharacteristicName::WeaponSkill),
                Some(&11.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Strength),
                Some(&22.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Speed),
                Some(&33.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Bravery),
                Some(&44.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Intelligence),
                Some(&55.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Toughness),
                Some(&66.into())
            );
            assert_eq!(field.stats.get(&CharacteristicName::Luck), Some(&77.into()));
            assert_eq!(
                field.stats.get(&CharacteristicName::Charisma),
                Some(&88.into())
            );

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The size of the characteristics is not defined, using default value : 120"
            ));
        }

        #[test]
        fn test_color_characteristics_field() {
            LogTester::start();
            let mut field = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 11.into()),
                    (CharacteristicName::Strength, 22.into()),
                    (CharacteristicName::Speed, 33.into()),
                    (CharacteristicName::Bravery, 44.into()),
                    (CharacteristicName::Intelligence, 55.into()),
                    (CharacteristicName::Toughness, 66.into()),
                    (CharacteristicName::Luck, 77.into()),
                    (CharacteristicName::Charisma, 88.into()),
                ]),
                size: 100,
                color: Color::Default,
                compact_fraction: false,
                name_color: Color::White,
            };

            let default = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            assert!(field.update(&default));
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Purple);
            assert_eq!(field.compact_fraction, false);
            assert_eq!(field.name_color, Color::White);
            assert_eq!(
                field.stats.get(&CharacteristicName::WeaponSkill),
                Some(&11.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Strength),
                Some(&22.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Speed),
                Some(&33.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Bravery),
                Some(&44.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Intelligence),
                Some(&55.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Toughness),
                Some(&66.into())
            );
            assert_eq!(field.stats.get(&CharacteristicName::Luck), Some(&77.into()));
            assert_eq!(
                field.stats.get(&CharacteristicName::Charisma),
                Some(&88.into())
            );

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the characteristics is not defined, using default value : Purple"
            ));
        }

        #[test]
        fn test_closest_color_characteristics_field() {
            LogTester::start();
            let mut field = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 11.into()),
                    (CharacteristicName::Strength, 22.into()),
                    (CharacteristicName::Speed, 33.into()),
                    (CharacteristicName::Bravery, 44.into()),
                    (CharacteristicName::Intelligence, 55.into()),
                    (CharacteristicName::Toughness, 66.into()),
                    (CharacteristicName::Luck, 77.into()),
                    (CharacteristicName::Charisma, 88.into()),
                ]),
                size: 100,
                color: Color::Custom(0, 0, 0, 0),
                compact_fraction: false,
                name_color: Color::White,
            };

            let default = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            assert!(field.update(&default));
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Transparent);
            assert_eq!(field.compact_fraction, false);
            assert_eq!(field.name_color, Color::White);
            assert_eq!(
                field.stats.get(&CharacteristicName::WeaponSkill),
                Some(&11.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Strength),
                Some(&22.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Speed),
                Some(&33.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Bravery),
                Some(&44.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Intelligence),
                Some(&55.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Toughness),
                Some(&66.into())
            );
            assert_eq!(field.stats.get(&CharacteristicName::Luck), Some(&77.into()));
            assert_eq!(
                field.stats.get(&CharacteristicName::Charisma),
                Some(&88.into())
            );

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(Level::Warn, "The color of the characteristics Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_name_color_characteristics_field() {
            LogTester::start();
            let mut field = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 11.into()),
                    (CharacteristicName::Strength, 22.into()),
                    (CharacteristicName::Speed, 33.into()),
                    (CharacteristicName::Bravery, 44.into()),
                    (CharacteristicName::Intelligence, 55.into()),
                    (CharacteristicName::Toughness, 66.into()),
                    (CharacteristicName::Luck, 77.into()),
                    (CharacteristicName::Charisma, 88.into()),
                ]),
                size: 100,
                color: Color::Black,
                compact_fraction: false,
                name_color: Color::Default,
            };

            let default = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            assert!(field.update(&default));
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.compact_fraction, false);
            assert_eq!(field.name_color, Color::Pink);
            assert_eq!(
                field.stats.get(&CharacteristicName::WeaponSkill),
                Some(&11.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Strength),
                Some(&22.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Speed),
                Some(&33.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Bravery),
                Some(&44.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Intelligence),
                Some(&55.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Toughness),
                Some(&66.into())
            );
            assert_eq!(field.stats.get(&CharacteristicName::Luck), Some(&77.into()));
            assert_eq!(
                field.stats.get(&CharacteristicName::Charisma),
                Some(&88.into())
            );

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(Level::Warn, "The color of the name of the characteristics is not defined, using default value : Pink"));
        }

        #[test]
        fn test_closest_name_color_characteristics_field() {
            LogTester::start();
            let mut field = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 11.into()),
                    (CharacteristicName::Strength, 22.into()),
                    (CharacteristicName::Speed, 33.into()),
                    (CharacteristicName::Bravery, 44.into()),
                    (CharacteristicName::Intelligence, 55.into()),
                    (CharacteristicName::Toughness, 66.into()),
                    (CharacteristicName::Luck, 77.into()),
                    (CharacteristicName::Charisma, 88.into()),
                ]),
                size: 100,
                color: Color::Black,
                compact_fraction: false,
                name_color: Color::Custom(0, 0, 0, 0),
            };

            let default = CharacteristicsField {
                stats: BTreeMap::from([
                    (CharacteristicName::WeaponSkill, 1.into()),
                    (CharacteristicName::Strength, 2.into()),
                    (CharacteristicName::Speed, 3.into()),
                    (CharacteristicName::Bravery, 4.into()),
                    (CharacteristicName::Intelligence, 5.into()),
                    (CharacteristicName::Toughness, 6.into()),
                    (CharacteristicName::Luck, 7.into()),
                    (CharacteristicName::Charisma, 8.into()),
                ]),
                size: 120,
                color: Color::Purple,
                compact_fraction: true,
                name_color: Color::Pink,
            };

            assert!(field.update(&default));
            assert_eq!(field.size, 100);
            assert_eq!(field.color, Color::Black);
            assert_eq!(field.compact_fraction, false);
            assert_eq!(field.name_color, Color::Transparent);
            assert_eq!(
                field.stats.get(&CharacteristicName::WeaponSkill),
                Some(&11.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Strength),
                Some(&22.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Speed),
                Some(&33.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Bravery),
                Some(&44.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Intelligence),
                Some(&55.into())
            );
            assert_eq!(
                field.stats.get(&CharacteristicName::Toughness),
                Some(&66.into())
            );
            assert_eq!(field.stats.get(&CharacteristicName::Luck), Some(&77.into()));
            assert_eq!(
                field.stats.get(&CharacteristicName::Charisma),
                Some(&88.into())
            );

            assert!(LogTester::len() == 1);
            assert!(LogTester::contains(Level::Warn, "The color of the name of the characteristics Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }
    }
}
