// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the symbols

use super::logos::Logo;
use crate::color::Color;
use crate::error::{Error, LogError};
use crate::render::{
    config::Config,
    images::{Image, RasterImage},
    Group, Layer, Render, RenderedCard,
};
use crate::types::CharacteristicValue;

use image::ImageFormat;
use serde::{de::Error as SerdeError, Deserialize, Serialize};
use serde_yaml::value::Value;
use serde_yaml::Mapping;
use strum_macros::{Display, EnumIter};
use usvg::Transform;

pub mod old_symbols;
pub mod symbols_config;

use symbols_config::{Config as SymbolsConfig, SymbolConfig};

/// Represents a Symbol
#[derive(
    Debug, Clone, Copy, PartialEq, EnumIter, Default, Serialize, Deserialize, Display, Eq, Hash,
)]
pub enum Symbol {
    Arc,
    Casque,
    Couronne,
    Serviteur,
    Crocs,
    Epee,
    Poing,
    Etendard,
    Flamme,
    Hurlante,
    Medic,
    Oeil,
    Collision,
    Ecrasement,
    Fanatique,
    Fuite,
    Gel,
    Killer,
    Necromancie,
    Poison,
    RazDeMaree,
    Tornade,
    Eblouissement,
    Tenebre,
    Incendie,
    BatonMagique,
    Providence,
    MagieDemoniaque,
    MagieIncantatoire,
    Devastator,
    FusilElite,
    MagieNecromantique,
    Paix,
    Portail,
    Prescience,
    Sablier,
    Blaster,
    Paralyseur,
    SabreBleu,
    SabreRouge,
    SabreViolet,
    SabreVert,
    DragonBall1,
    DragonBall2,
    DragonBall3,
    DragonBall4,
    ForceLumineuse,
    ForceObscure,
    Wakfu,
    Sorcellerie,
    Credits,
    Bouclier,
    Bloque,
    Retourne,
    FlopFlop,
    Zizanie,
    Traitre,
    AttaqueSournoise,
    Pitie,
    Empty,
    #[default]
    Null,
}

/// Represents a Symbol with its value
#[derive(Debug, Clone, Copy, PartialEq, Default, Serialize)]
pub struct SymbolField {
    /// The Symbol
    pub symbol: Symbol,
    /// The value of the Symbol
    #[serde(default)]
    pub value: CharacteristicValue,
    /// The color of the text
    ///
    /// If not defined or equal to [Color::Default] a default color corresponding to the symbol will be used
    #[serde(default)]
    pub text_color: Color,
    /// The color of the border
    ///
    /// Currently unused
    #[serde(default)]
    pub border_color: Color,
}

impl SymbolField {
    /// Renders this symbol in accordance to the symbol config provided
    pub fn render(
        &self,
        config: &Config,
        symbol_config: &SymbolConfig,
        font: &usvg::Font,
        width: f32,
    ) -> Group {
        let image = self
            .symbol
            .get_image()
            .critical("Symbol's image are loaded from embedded. This should always be some");
        let ratio = match symbol_config.diameter {
            Some(diameter) => diameter as f32,
            None => image.size().width,
        } / width;
        let size = image.size().width / ratio;
        let mut image = image.to_width_node(
            self.to_string(),
            -(symbol_config.center / ratio),
            Some(size),
        );
        if self.value != CharacteristicValue::Empty {
            let text_color = if self.text_color == Color::Default {
                if symbol_config.text_color == Color::Default {
                    Color::RedStat
                } else {
                    symbol_config.text_color
                }
            } else {
                self.text_color
            };
            image.append(usvg::Node::Text(Box::new(RenderedCard::create_text(
                &format!("{}_text", self),
                &self.value.to_nice_string(config.is_compact_fraction()),
                text_color,
                match symbol_config.text_size {
                    Some(size) => size / ratio,
                    None => 150.0,
                },
                font,
                match symbol_config.text_position {
                    Some(position) => position,
                    None => symbol_config.center,
                } / ratio,
                usvg::TextAnchor::Middle,
            ))));
        }

        image
    }
}

impl Logo for Symbol {
    fn get_image(&self) -> Option<impl Image> {
        Some(match self {
            Self::Null => RasterImage::from_bytes(include_bytes!("../logos/symbols/Null.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Null.png, this file is embed in the code so it should not fail"),
            Self::Empty => RasterImage::from_bytes(include_bytes!("../logos/symbols/Empty.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Empty.png, this file is embed in the code so it should not fail"),
            Self::Arc => RasterImage::from_bytes(include_bytes!("../logos/symbols/Arc.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Arc.png, this file is embed in the code so it should not fail"),
            Self::Casque => RasterImage::from_bytes(include_bytes!("../logos/symbols/Casque.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Casque.png, this file is embed in the code so it should not fail"),
            Self::Couronne => RasterImage::from_bytes(include_bytes!("../logos/symbols/Couronne.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Couronne.png, this file is embed in the code so it should not fail"),
            Self::Serviteur => RasterImage::from_bytes(include_bytes!("../logos/symbols/Serviteur.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Serviteur.png, this file is embed in the code so it should not fail"),
            Self::Crocs => RasterImage::from_bytes(include_bytes!("../logos/symbols/Crocs.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Crocs.png, this file is embed in the code so it should not fail"),
            Self::Epee => RasterImage::from_bytes(include_bytes!("../logos/symbols/Epee.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Epee.png, this file is embed in the code so it should not fail"),
            Self::Poing => RasterImage::from_bytes(include_bytes!("../logos/symbols/Poing.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Poing.png, this file is embed in the code so it should not fail"),
            Self::Etendard => RasterImage::from_bytes(include_bytes!("../logos/symbols/Etendard.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Etendard.png, this file is embed in the code so it should not fail"),
            Self::Flamme => RasterImage::from_bytes(include_bytes!("../logos/symbols/Flamme.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Flamme.png, this file is embed in the code so it should not fail"),
            Self::Hurlante => RasterImage::from_bytes(include_bytes!("../logos/symbols/Hurlante.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Hurlante.png, this file is embed in the code so it should not fail"),
            Self::Medic => RasterImage::from_bytes(include_bytes!("../logos/symbols/Medic.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Medic.png, this file is embed in the code so it should not fail"),
            Self::Oeil => RasterImage::from_bytes(include_bytes!("../logos/symbols/Oeil.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Oeil.png, this file is embed in the code so it should not fail"),
            Self::Collision => RasterImage::from_bytes(include_bytes!("../logos/symbols/Collision.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Collision.png, this file is embed in the code so it should not fail"),
            Self::Ecrasement => RasterImage::from_bytes(include_bytes!("../logos/symbols/Ecrasement.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Ecrasement.png, this file is embed in the code so it should not fail"),
            Self::Fanatique => RasterImage::from_bytes(include_bytes!("../logos/symbols/Fanatique.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Fanatique.png, this file is embed in the code so it should not fail"),
            Self::Fuite => RasterImage::from_bytes(include_bytes!("../logos/symbols/Fuite.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Fuite.png, this file is embed in the code so it should not fail"),
            Self::Gel => RasterImage::from_bytes(include_bytes!("../logos/symbols/Gel.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Gel.png, this file is embed in the code so it should not fail"),
            Self::Killer => RasterImage::from_bytes(include_bytes!("../logos/symbols/Killer.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Killer.png, this file is embed in the code so it should not fail"),
            Self::Necromancie => RasterImage::from_bytes(include_bytes!("../logos/symbols/Necromancie.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Necromancie.png, this file is embed in the code so it should not fail"),
            Self::Poison => RasterImage::from_bytes(include_bytes!("../logos/symbols/Poison.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Poison.png, this file is embed in the code so it should not fail"),
            Self::RazDeMaree => RasterImage::from_bytes(include_bytes!("../logos/symbols/RazDeMaree.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/RazDeMaree.png, this file is embed in the code so it should not fail"),
            Self::Tornade => RasterImage::from_bytes(include_bytes!("../logos/symbols/Tornade.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Tornade.png, this file is embed in the code so it should not fail"),
            Self::Incendie => RasterImage::from_bytes(include_bytes!("../logos/symbols/Incendie.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Incendie.png, this file is embed in the code so it should not fail"),
            Self::Eblouissement => RasterImage::from_bytes(include_bytes!("../logos/symbols/Eblouissement.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Eblouissement.png, this file is embed in the code so it should not fail"),
            Self::Tenebre => RasterImage::from_bytes(include_bytes!("../logos/symbols/Tenebre.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Tenebre.png, this file is embed in the code so it should not fail"),
            Self::BatonMagique => RasterImage::from_bytes(include_bytes!("../logos/symbols/BatonMagique.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/BatonMagique.png, this file is embed in the code so it should not fail"),
            Self::Providence => RasterImage::from_bytes(include_bytes!("../logos/symbols/Providence.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Providence.png, this file is embed in the code so it should not fail"),
            Self::MagieDemoniaque => RasterImage::from_bytes(include_bytes!("../logos/symbols/MagieDemoniaque.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/MagieDemoniaque.png, this file is embed in the code so it should not fail"),
            Self::MagieIncantatoire => RasterImage::from_bytes(include_bytes!("../logos/symbols/MagieIncantatoire.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/MagieIncantatoire.png, this file is embed in the code so it should not fail"),
            Self::Devastator => RasterImage::from_bytes(include_bytes!("../logos/symbols/Devastator.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Devastator.png, this file is embed in the code so it should not fail"),
            Self::FusilElite => RasterImage::from_bytes(include_bytes!("../logos/symbols/FusilElite.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/FusilElite.png, this file is embed in the code so it should not fail"),
            Self::MagieNecromantique => RasterImage::from_bytes(include_bytes!("../logos/symbols/MagieNecromantique.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/MagieNecromantique.png, this file is embed in the code so it should not fail"),
            Self::Paix => RasterImage::from_bytes(include_bytes!("../logos/symbols/Paix.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Paix.png, this file is embed in the code so it should not fail"),
            Self::Portail => RasterImage::from_bytes(include_bytes!("../logos/symbols/Portail.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Portail.png, this file is embed in the code so it should not fail"),
            Self::Prescience => RasterImage::from_bytes(include_bytes!("../logos/symbols/Prescience.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Prescience.png, this file is embed in the code so it should not fail"),
            Self::Sablier => RasterImage::from_bytes(include_bytes!("../logos/symbols/Sablier.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Sablier.png, this file is embed in the code so it should not fail"),
            Self::Blaster => RasterImage::from_bytes(include_bytes!("../logos/symbols/Blaster.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Blaster.png, this file is embed in the code so it should not fail"),
            Self::Paralyseur => RasterImage::from_bytes(include_bytes!("../logos/symbols/Paralyseur.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Paralyseur.png, this file is embed in the code so it should not fail"),
            Self::SabreBleu => RasterImage::from_bytes(include_bytes!("../logos/symbols/SabreBleu.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/SabreBleu.png, this file is embed in the code so it should not fail"),
            Self::SabreRouge => RasterImage::from_bytes(include_bytes!("../logos/symbols/SabreRouge.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/SabreRouge.png, this file is embed in the code so it should not fail"),
            Self::SabreViolet => RasterImage::from_bytes(include_bytes!("../logos/symbols/SabreViolet.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/SabreViolet.png, this file is embed in the code so it should not fail"),
            Self::SabreVert => RasterImage::from_bytes(include_bytes!("../logos/symbols/SabreVert.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/SabreVert.png, this file is embed in the code so it should not fail"),
            Self::DragonBall1 => RasterImage::from_bytes(include_bytes!("../logos/symbols/DragonBall1.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/DragonBall1.png, this file is embed in the code so it should not fail"),
            Self::DragonBall2 => RasterImage::from_bytes(include_bytes!("../logos/symbols/DragonBall2.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/DragonBall2.png, this file is embed in the code so it should not fail"),
            Self::DragonBall3 => RasterImage::from_bytes(include_bytes!("../logos/symbols/DragonBall3.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/DragonBall3.png, this file is embed in the code so it should not fail"),
            Self::DragonBall4 => RasterImage::from_bytes(include_bytes!("../logos/symbols/DragonBall4.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/DragonBall4.png, this file is embed in the code so it should not fail"),
            Self::ForceLumineuse => RasterImage::from_bytes(include_bytes!("../logos/symbols/ForceLumineuse.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/ForceLumineuse.png, this file is embed in the code so it should not fail"),
            Self::ForceObscure => RasterImage::from_bytes(include_bytes!("../logos/symbols/ForceObscure.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/ForceObscure.png, this file is embed in the code so it should not fail"),
            Self::Wakfu => RasterImage::from_bytes(include_bytes!("../logos/symbols/Wakfu.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Wakfu.png, this file is embed in the code so it should not fail"),
            Self::Sorcellerie => RasterImage::from_bytes(include_bytes!("../logos/symbols/Sorcellerie.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Sorcellerie.png, this file is embed in the code so it should not fail"),
            Self::Credits => RasterImage::from_bytes(include_bytes!("../logos/symbols/Credits.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Credits.png, this file is embed in the code so it should not fail"),
            Self::Bouclier => RasterImage::from_bytes(include_bytes!("../logos/symbols/Bouclier.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Bouclier.png, this file is embed in the code so it should not fail"),
            Self::Bloque => RasterImage::from_bytes(include_bytes!("../logos/symbols/Bloque.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Bloque.png, this file is embed in the code so it should not fail"),
            Self::Retourne => RasterImage::from_bytes(include_bytes!("../logos/symbols/Retourne.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Retourne.png, this file is embed in the code so it should not fail"),
            Self::FlopFlop => RasterImage::from_bytes(include_bytes!("../logos/symbols/FlopFlop.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/FlopFlop.png, this file is embed in the code so it should not fail"),
            Self::Zizanie => RasterImage::from_bytes(include_bytes!("../logos/symbols/Zizanie.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Zizanie.png, this file is embed in the code so it should not fail"),
            Self::Traitre => RasterImage::from_bytes(include_bytes!("../logos/symbols/Traitre.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Traitre.png, this file is embed in the code so it should not fail"),
            Self::AttaqueSournoise => RasterImage::from_bytes(include_bytes!("../logos/symbols/AttaqueSournoise.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/AttaqueSournoise.png, this file is embed in the code so it should not fail"),
            Self::Pitie => RasterImage::from_bytes(include_bytes!("../logos/symbols/Pitie.png"), Some(ImageFormat::Png)).critical("Error while loading src/logos/symbols/Pitie.png, this file is embed in the code so it should not fail"),
        })
    }
}

impl<'de> Deserialize<'de> for SymbolField {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let value = Value::deserialize(deserializer)?;

        if let Ok(symbol) = serde_yaml::from_value::<old_symbols::OldSymbol>(value.clone()) {
            return Ok(symbol.into());
        }

        if let Value::Mapping(map) = value {
            map.try_into().map_err(D::Error::custom)
        } else {
            Err(D::Error::custom(Error::ValueError {
                expected: "a symbol".to_owned(),
                current: format!("{:?}", value),
            }))
        }
    }
}

impl TryFrom<Mapping> for SymbolField {
    type Error = Error;

    fn try_from(map: Mapping) -> Result<Self, Self::Error> {
        if map.len() > 4 {
            return Err(Error::LengthError {
                expected_size: 4,
                current_size: map.len(),
            });
        }
        Ok(Self {
            symbol: serde_yaml::from_value::<Symbol>(
                map.get("symbol")
                    .ok_or(Error::MissingValueError("symbol".to_owned()))?
                    .clone(),
            )?,
            value: match map.get("value") {
                Some(value) => serde_yaml::from_value::<CharacteristicValue>(value.clone())?,
                None => CharacteristicValue::default(),
            },
            text_color: match map.get("text_color") {
                Some(Value::Null) => Color::Default,
                Some(text_color) => serde_yaml::from_value(text_color.clone())?,
                None => Color::Default,
            },
            border_color: match map.get("border_color") {
                Some(Value::Null) => Color::Default,
                Some(border_color) => serde_yaml::from_value(border_color.clone())?,
                None => Color::Default,
            },
        })
    }
}

impl Render for Vec<SymbolField> {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let symbols = if self.len() > config.get_max_symbols() {
            log::error!(
                "Too many symbols in the card, only the first {} will be rendered",
                config.get_max_symbols()
            );
            self[0..config.get_max_symbols()].to_vec()
        } else {
            self.to_vec()
        };
        let symbols_config = SymbolsConfig::default();
        let positions = config.get_symbols_positions(
            symbols
                .len()
                .try_into()
                .critical("More than 255 symbols is not handle by the software"),
        )?;
        let font = &config.get_font(layer)?;
        let width = config.get_width(layer)?;
        let mut node = Group::new(layer, Transform::identity());
        for i in 0..symbols.len() {
            let symbol_config = symbols_config.get_symbol(&symbols[i].symbol);
            let mut group = Group::new(format!("{}{}", layer, i), positions[i]);
            let image = symbols[i].render(config, symbol_config, font, width);
            group.append(image);
            node.append(group)
        }
        Ok(node.into())
    }
}

impl From<Symbol> for SymbolField {
    fn from(symbol: Symbol) -> Self {
        Self {
            symbol,
            value: CharacteristicValue::default(),
            text_color: Color::Default,
            border_color: Color::Default,
        }
    }
}

impl std::fmt::Display for SymbolField {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}{}",
            self.symbol,
            match &self.value {
                CharacteristicValue::Empty => "".to_owned(),
                _ => format!("({})", &self.value),
            }
        )
    }
}

impl super::Updatable for SymbolField {
    fn update(&mut self, _default: &Self) -> bool {
        let mut res = false;
        if self.text_color != Color::Default {
            let close = self.text_color.closest();
            if close != self.text_color {
                log::warn!(
                    "The color of the text of a symbol {} was too close to {}, replacing",
                    self.text_color,
                    close,
                );
                self.text_color = close;
                res = true;
            }
        }
        if self.border_color != Color::Default {
            let close = self.border_color.closest();
            if close != self.border_color {
                log::warn!(
                    "The color of the border of a symbol {} was too close to {}, replacing",
                    self.border_color,
                    close,
                );
                self.border_color = close;
                res = true;
            }
        }
        res
    }

    fn get_default() -> Self {
        Self::default()
    }
}

#[cfg(test)]
mod test_module_symbol {

    #[cfg(test)]
    mod test_enum_symbol {

        use crate::card::logos::Logo;

        use super::super::Symbol;

        use strum::IntoEnumIterator;

        #[test]
        fn test_default_symbol() {
            assert_eq!(Symbol::default(), Symbol::Null);
        }

        #[test]
        fn test_display_symbol() {
            assert_eq!(Symbol::Arc.to_string(), "Arc");
            assert_eq!(Symbol::Casque.to_string(), "Casque");
            assert_eq!(Symbol::Couronne.to_string(), "Couronne");
            assert_eq!(Symbol::Crocs.to_string(), "Crocs");
            assert_eq!(Symbol::Epee.to_string(), "Epee");
            assert_eq!(Symbol::Poing.to_string(), "Poing");
            assert_eq!(Symbol::Etendard.to_string(), "Etendard");
            assert_eq!(Symbol::Flamme.to_string(), "Flamme");
            assert_eq!(Symbol::Hurlante.to_string(), "Hurlante");
            assert_eq!(Symbol::Medic.to_string(), "Medic");
            assert_eq!(Symbol::Oeil.to_string(), "Oeil");
            assert_eq!(Symbol::Collision.to_string(), "Collision");
            assert_eq!(Symbol::Ecrasement.to_string(), "Ecrasement");
            assert_eq!(Symbol::Fanatique.to_string(), "Fanatique");
            assert_eq!(Symbol::Fuite.to_string(), "Fuite");
            assert_eq!(Symbol::Gel.to_string(), "Gel");
            assert_eq!(Symbol::Killer.to_string(), "Killer");
            assert_eq!(Symbol::Necromancie.to_string(), "Necromancie");
            assert_eq!(Symbol::Poison.to_string(), "Poison");
            assert_eq!(Symbol::RazDeMaree.to_string(), "RazDeMaree");
            assert_eq!(Symbol::Tornade.to_string(), "Tornade");
            assert_eq!(Symbol::Eblouissement.to_string(), "Eblouissement");
            assert_eq!(Symbol::Tenebre.to_string(), "Tenebre");
            assert_eq!(Symbol::BatonMagique.to_string(), "BatonMagique");
            assert_eq!(Symbol::Providence.to_string(), "Providence");
            assert_eq!(Symbol::MagieDemoniaque.to_string(), "MagieDemoniaque");
            assert_eq!(Symbol::MagieIncantatoire.to_string(), "MagieIncantatoire");
            assert_eq!(Symbol::Devastator.to_string(), "Devastator");
            assert_eq!(Symbol::FusilElite.to_string(), "FusilElite");
            assert_eq!(Symbol::MagieNecromantique.to_string(), "MagieNecromantique");
            assert_eq!(Symbol::Paix.to_string(), "Paix");
            assert_eq!(Symbol::Portail.to_string(), "Portail");
            assert_eq!(Symbol::Prescience.to_string(), "Prescience");
            assert_eq!(Symbol::Sablier.to_string(), "Sablier");
            assert_eq!(Symbol::Blaster.to_string(), "Blaster");
            assert_eq!(Symbol::Paralyseur.to_string(), "Paralyseur");
            assert_eq!(Symbol::SabreBleu.to_string(), "SabreBleu");
            assert_eq!(Symbol::SabreRouge.to_string(), "SabreRouge");
            assert_eq!(Symbol::SabreViolet.to_string(), "SabreViolet");
            assert_eq!(Symbol::SabreVert.to_string(), "SabreVert");
            assert_eq!(Symbol::DragonBall1.to_string(), "DragonBall1");
            assert_eq!(Symbol::DragonBall2.to_string(), "DragonBall2");
            assert_eq!(Symbol::DragonBall3.to_string(), "DragonBall3");
            assert_eq!(Symbol::DragonBall4.to_string(), "DragonBall4");
            assert_eq!(Symbol::ForceLumineuse.to_string(), "ForceLumineuse");
            assert_eq!(Symbol::ForceObscure.to_string(), "ForceObscure");
            assert_eq!(Symbol::Wakfu.to_string(), "Wakfu");
            assert_eq!(Symbol::Sorcellerie.to_string(), "Sorcellerie");
            assert_eq!(Symbol::Credits.to_string(), "Credits");
            assert_eq!(Symbol::Bouclier.to_string(), "Bouclier");
            assert_eq!(Symbol::Bloque.to_string(), "Bloque");
            assert_eq!(Symbol::Retourne.to_string(), "Retourne");
            assert_eq!(Symbol::FlopFlop.to_string(), "FlopFlop");
            assert_eq!(Symbol::Zizanie.to_string(), "Zizanie");
            assert_eq!(Symbol::Traitre.to_string(), "Traitre");
            assert_eq!(Symbol::AttaqueSournoise.to_string(), "AttaqueSournoise");
            assert_eq!(Symbol::Pitie.to_string(), "Pitie");
            assert_eq!(Symbol::Null.to_string(), "Null");
        }

        #[test]
        fn test_debug_symbol() {
            assert_eq!(format!("{:?}", Symbol::Arc), "Arc");
            assert_eq!(format!("{:?}", Symbol::Casque), "Casque");
            assert_eq!(format!("{:?}", Symbol::Couronne), "Couronne");
            assert_eq!(format!("{:?}", Symbol::Crocs), "Crocs");
            assert_eq!(format!("{:?}", Symbol::Epee), "Epee");
            assert_eq!(format!("{:?}", Symbol::Poing), "Poing");
            assert_eq!(format!("{:?}", Symbol::Etendard), "Etendard");
            assert_eq!(format!("{:?}", Symbol::Flamme), "Flamme");
            assert_eq!(format!("{:?}", Symbol::Hurlante), "Hurlante");
            assert_eq!(format!("{:?}", Symbol::Medic), "Medic");
            assert_eq!(format!("{:?}", Symbol::Oeil), "Oeil");
            assert_eq!(format!("{:?}", Symbol::Collision), "Collision");
            assert_eq!(format!("{:?}", Symbol::Ecrasement), "Ecrasement");
            assert_eq!(format!("{:?}", Symbol::Fanatique), "Fanatique");
            assert_eq!(format!("{:?}", Symbol::Fuite), "Fuite");
            assert_eq!(format!("{:?}", Symbol::Gel), "Gel");
            assert_eq!(format!("{:?}", Symbol::Killer), "Killer");
            assert_eq!(format!("{:?}", Symbol::Necromancie), "Necromancie");
            assert_eq!(format!("{:?}", Symbol::Poison), "Poison");
            assert_eq!(format!("{:?}", Symbol::RazDeMaree), "RazDeMaree");
            assert_eq!(format!("{:?}", Symbol::Tornade), "Tornade");
            assert_eq!(format!("{:?}", Symbol::Eblouissement), "Eblouissement");
            assert_eq!(format!("{:?}", Symbol::Tenebre), "Tenebre");
            assert_eq!(format!("{:?}", Symbol::BatonMagique), "BatonMagique");
            assert_eq!(format!("{:?}", Symbol::Providence), "Providence");
            assert_eq!(format!("{:?}", Symbol::MagieDemoniaque), "MagieDemoniaque");
            assert_eq!(
                format!("{:?}", Symbol::MagieIncantatoire),
                "MagieIncantatoire"
            );
            assert_eq!(format!("{:?}", Symbol::Devastator), "Devastator");
            assert_eq!(format!("{:?}", Symbol::FusilElite), "FusilElite");
            assert_eq!(
                format!("{:?}", Symbol::MagieNecromantique),
                "MagieNecromantique"
            );
            assert_eq!(format!("{:?}", Symbol::Paix), "Paix");
            assert_eq!(format!("{:?}", Symbol::Portail), "Portail");
            assert_eq!(format!("{:?}", Symbol::Prescience), "Prescience");
            assert_eq!(format!("{:?}", Symbol::Sablier), "Sablier");
            assert_eq!(format!("{:?}", Symbol::Blaster), "Blaster");
            assert_eq!(format!("{:?}", Symbol::Paralyseur), "Paralyseur");
            assert_eq!(format!("{:?}", Symbol::SabreBleu), "SabreBleu");
            assert_eq!(format!("{:?}", Symbol::SabreRouge), "SabreRouge");
            assert_eq!(format!("{:?}", Symbol::SabreViolet), "SabreViolet");
            assert_eq!(format!("{:?}", Symbol::SabreVert), "SabreVert");
            assert_eq!(format!("{:?}", Symbol::DragonBall1), "DragonBall1");
            assert_eq!(format!("{:?}", Symbol::DragonBall2), "DragonBall2");
            assert_eq!(format!("{:?}", Symbol::DragonBall3), "DragonBall3");
            assert_eq!(format!("{:?}", Symbol::DragonBall4), "DragonBall4");
            assert_eq!(format!("{:?}", Symbol::ForceLumineuse), "ForceLumineuse");
            assert_eq!(format!("{:?}", Symbol::ForceObscure), "ForceObscure");
            assert_eq!(format!("{:?}", Symbol::Wakfu), "Wakfu");
            assert_eq!(format!("{:?}", Symbol::Sorcellerie), "Sorcellerie");
            assert_eq!(format!("{:?}", Symbol::Credits), "Credits");
            assert_eq!(format!("{:?}", Symbol::Bouclier), "Bouclier");
            assert_eq!(format!("{:?}", Symbol::Bloque), "Bloque");
            assert_eq!(format!("{:?}", Symbol::Retourne), "Retourne");
            assert_eq!(format!("{:?}", Symbol::FlopFlop), "FlopFlop");
            assert_eq!(format!("{:?}", Symbol::Zizanie), "Zizanie");
            assert_eq!(format!("{:?}", Symbol::Traitre), "Traitre");
            assert_eq!(
                format!("{:?}", Symbol::AttaqueSournoise),
                "AttaqueSournoise"
            );
            assert_eq!(format!("{:?}", Symbol::Pitie), "Pitie");
            assert_eq!(format!("{:?}", Symbol::Null), "Null");
        }

        #[test]
        fn test_serde_symbol() {
            assert_eq!(
                serde_yaml::to_string(&Symbol::Arc).expect("A test should never fail"),
                "Arc\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Arc").expect("A test should never fail"),
                Symbol::Arc
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Casque).expect("A test should never fail"),
                "Casque\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Casque").expect("A test should never fail"),
                Symbol::Casque
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Couronne).expect("A test should never fail"),
                "Couronne\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Couronne").expect("A test should never fail"),
                Symbol::Couronne
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Crocs).expect("A test should never fail"),
                "Crocs\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Crocs").expect("A test should never fail"),
                Symbol::Crocs
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Epee).expect("A test should never fail"),
                "Epee\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Epee").expect("A test should never fail"),
                Symbol::Epee
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Poing).expect("A test should never fail"),
                "Poing\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Poing").expect("A test should never fail"),
                Symbol::Poing
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Etendard).expect("A test should never fail"),
                "Etendard\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Etendard").expect("A test should never fail"),
                Symbol::Etendard
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Flamme).expect("A test should never fail"),
                "Flamme\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Flamme").expect("A test should never fail"),
                Symbol::Flamme
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Hurlante).expect("A test should never fail"),
                "Hurlante\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Hurlante").expect("A test should never fail"),
                Symbol::Hurlante
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Medic).expect("A test should never fail"),
                "Medic\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Medic").expect("A test should never fail"),
                Symbol::Medic
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Oeil).expect("A test should never fail"),
                "Oeil\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Oeil").expect("A test should never fail"),
                Symbol::Oeil
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Collision).expect("A test should never fail"),
                "Collision\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Collision").expect("A test should never fail"),
                Symbol::Collision
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Ecrasement).expect("A test should never fail"),
                "Ecrasement\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Ecrasement").expect("A test should never fail"),
                Symbol::Ecrasement
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Fanatique).expect("A test should never fail"),
                "Fanatique\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Fanatique").expect("A test should never fail"),
                Symbol::Fanatique
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Fuite).expect("A test should never fail"),
                "Fuite\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Fuite").expect("A test should never fail"),
                Symbol::Fuite
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Gel).expect("A test should never fail"),
                "Gel\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Gel").expect("A test should never fail"),
                Symbol::Gel
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Killer).expect("A test should never fail"),
                "Killer\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Killer").expect("A test should never fail"),
                Symbol::Killer
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Necromancie).expect("A test should never fail"),
                "Necromancie\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Necromancie").expect("A test should never fail"),
                Symbol::Necromancie
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Poison).expect("A test should never fail"),
                "Poison\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Poison").expect("A test should never fail"),
                Symbol::Poison
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::RazDeMaree).expect("A test should never fail"),
                "RazDeMaree\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("RazDeMaree").expect("A test should never fail"),
                Symbol::RazDeMaree
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Tornade).expect("A test should never fail"),
                "Tornade\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Tornade").expect("A test should never fail"),
                Symbol::Tornade
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Eblouissement).expect("A test should never fail"),
                "Eblouissement\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Eblouissement").expect("A test should never fail"),
                Symbol::Eblouissement
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Tenebre).expect("A test should never fail"),
                "Tenebre\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Tenebre").expect("A test should never fail"),
                Symbol::Tenebre
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::BatonMagique).expect("A test should never fail"),
                "BatonMagique\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("BatonMagique").expect("A test should never fail"),
                Symbol::BatonMagique
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Providence).expect("A test should never fail"),
                "Providence\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Providence").expect("A test should never fail"),
                Symbol::Providence
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::MagieDemoniaque).expect("A test should never fail"),
                "MagieDemoniaque\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("MagieDemoniaque")
                    .expect("A test should never fail"),
                Symbol::MagieDemoniaque
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::MagieIncantatoire)
                    .expect("A test should never fail"),
                "MagieIncantatoire\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("MagieIncantatoire")
                    .expect("A test should never fail"),
                Symbol::MagieIncantatoire
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Devastator).expect("A test should never fail"),
                "Devastator\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Devastator").expect("A test should never fail"),
                Symbol::Devastator
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::FusilElite).expect("A test should never fail"),
                "FusilElite\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("FusilElite").expect("A test should never fail"),
                Symbol::FusilElite
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::MagieNecromantique)
                    .expect("A test should never fail"),
                "MagieNecromantique\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("MagieNecromantique")
                    .expect("A test should never fail"),
                Symbol::MagieNecromantique
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::MagieNecromantique)
                    .expect("A test should never fail"),
                "MagieNecromantique\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("MagieNecromantique")
                    .expect("A test should never fail"),
                Symbol::MagieNecromantique
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Paix).expect("A test should never fail"),
                "Paix\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Paix").expect("A test should never fail"),
                Symbol::Paix
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Portail).expect("A test should never fail"),
                "Portail\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Portail").expect("A test should never fail"),
                Symbol::Portail
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Prescience).expect("A test should never fail"),
                "Prescience\n"
            );
            assert_eq!(
                serde_yaml::from_str::<Symbol>("Prescience").expect("A test should never fail"),
                Symbol::Prescience
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Sablier).expect("A test should never fail"),
                "Sablier\n"
            );

            assert_eq!(
                serde_yaml::to_string(&Symbol::Blaster).expect("A test should never fail"),
                "Blaster\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Paralyseur).expect("A test should never fail"),
                "Paralyseur\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::SabreBleu).expect("A test should never fail"),
                "SabreBleu\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::SabreRouge).expect("A test should never fail"),
                "SabreRouge\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::SabreViolet).expect("A test should never fail"),
                "SabreViolet\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::SabreVert).expect("A test should never fail"),
                "SabreVert\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::DragonBall1).expect("A test should never fail"),
                "DragonBall1\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::DragonBall2).expect("A test should never fail"),
                "DragonBall2\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::DragonBall3).expect("A test should never fail"),
                "DragonBall3\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::DragonBall4).expect("A test should never fail"),
                "DragonBall4\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::ForceLumineuse).expect("A test should never fail"),
                "ForceLumineuse\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::ForceObscure).expect("A test should never fail"),
                "ForceObscure\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Wakfu).expect("A test should never fail"),
                "Wakfu\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Sorcellerie).expect("A test should never fail"),
                "Sorcellerie\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Credits).expect("A test should never fail"),
                "Credits\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Bouclier).expect("A test should never fail"),
                "Bouclier\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Bloque).expect("A test should never fail"),
                "Bloque\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Retourne).expect("A test should never fail"),
                "Retourne\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::FlopFlop).expect("A test should never fail"),
                "FlopFlop\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Zizanie).expect("A test should never fail"),
                "Zizanie\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Traitre).expect("A test should never fail"),
                "Traitre\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::AttaqueSournoise).expect("A test should never fail"),
                "AttaqueSournoise\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Pitie).expect("A test should never fail"),
                "Pitie\n"
            );
            assert_eq!(
                serde_yaml::to_string(&Symbol::Null).expect("A test should never fail"),
                "'Null'\n"
            );
        }

        #[test]
        fn test_clone_symbol() {
            for symbol in Symbol::iter() {
                assert_eq!(symbol.clone(), symbol);
            }
        }

        #[test]
        fn test_get_image() {
            for symbol in Symbol::iter() {
                symbol.get_image();
            }
        }
    }

    #[cfg(test)]
    mod test_struct_vec_symbol_field {
        use super::super::{Symbol, SymbolField};

        use crate::card::illustration::{Mode, Orientation};
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};
        use crate::types::CharacteristicValue;

        use log_tester::LogTester;
        use strum::IntoEnumIterator;

        #[test]
        fn test_render_vec_symbols() {
            let mut config = Config::default();

            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    for i in 1..config.get_max_symbols() + 2 {
                        let mut symbols: Vec<SymbolField> = Symbol::iter()
                            .cycle()
                            .take(i)
                            .map(|elt| elt.into())
                            .collect();
                        symbols[0].value = CharacteristicValue::Empty;
                        symbols[0].text_color = Color::Black;
                        let render = symbols.render(&Layer::Symbols, &config, None);
                        assert!(render.is_ok());
                        assert!(render
                            .expect("A test should never fail")
                            .has_node(&Layer::Symbols));
                    }
                }
            }
        }

        #[test]
        fn test_log_vec_symbols() {
            LogTester::start();
            let config = Config::default();
            let symbols: Vec<SymbolField> = Symbol::iter()
                .cycle()
                .take(config.get_max_symbols() + 1)
                .map(|elt| elt.into())
                .collect();
            let render = symbols.render(&Layer::Symbols, &config, None);
            assert!(render.is_ok());

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                log::Level::Error,
                &format!(
                    "Too many symbols in the card, only the first {} will be rendered",
                    config.get_max_symbols()
                )
            ),);
        }
    }

    #[cfg(test)]
    mod test_struct_symbol_field {
        use crate::color::Color;
        use crate::render::config::Config;
        use crate::render::Layer;
        use crate::types::CharacteristicValue;

        use super::super::old_symbols::OldSymbol;
        use super::super::symbols_config::Config as SymbolsConfig;
        use super::super::{Symbol, SymbolField};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_symbol_field() {
            for symbol in Symbol::iter() {
                let symbol: SymbolField = symbol.into();
                let str = format!(
                    "symbol: {}\nvalue: ''\ntext_color: Default\nborder_color: Default\n",
                    if symbol.symbol != Symbol::Null {
                        format!("{}", symbol.symbol)
                    } else {
                        "'Null'".to_owned()
                    }
                );
                assert_eq!(
                    serde_yaml::to_string(&symbol).expect("A test should never fail"),
                    str
                );
                println!("{}", str);
                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );

                let symbol = SymbolField {
                    value: 10.into(),
                    ..symbol
                };
                let str = format!(
                    "symbol: {}\nvalue: 10\ntext_color: Default\nborder_color: Default\n",
                    if symbol.symbol != Symbol::Null {
                        format!("{}", symbol.symbol)
                    } else {
                        "'Null'".to_owned()
                    }
                );
                assert_eq!(
                    serde_yaml::to_string(&symbol).expect("A test should never fail"),
                    str
                );
                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );

                let str = format!(
                    "symbol: {}\ntext_color: Default\nborder_color: Default\n",
                    if symbol.symbol != Symbol::Null {
                        format!("{}", symbol.symbol)
                    } else {
                        "'Null'".to_owned()
                    }
                );

                let symbol = SymbolField {
                    value: CharacteristicValue::Empty,
                    ..symbol
                };

                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );

                let str = format!(
                    "symbol: {}\nvalue: null\nborder_color: Default\n",
                    if symbol.symbol != Symbol::Null {
                        format!("{}", symbol.symbol)
                    } else {
                        "'Null'".to_owned()
                    }
                );

                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );

                let str = format!(
                    "symbol: {}\nvalue: null\ntext_color: Default\n",
                    if symbol.symbol != Symbol::Null {
                        format!("{}", symbol.symbol)
                    } else {
                        "'Null'".to_owned()
                    }
                );

                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );
            }

            assert!(serde_yaml::from_str::<SymbolField>(
                "value: null\ntext_color: Default\nborder_color: Default\n"
            )
            .is_err());

            assert!(serde_yaml::from_str::<SymbolField>(
                "symbol: Wrong\nvalue: null\ntext_color: Default\nborder_color: Default\n"
            )
            .is_err());

            assert!(
                serde_yaml::from_str::<SymbolField>(
                    "symbol: Null\nvalue: null\ntext_color: Default\nborder_color: Default\nsomething: too much\n"
                ).is_err()
            );

            assert!(serde_yaml::from_str::<SymbolField>("10").is_err());
        }

        #[test]
        fn test_deserialize_symbol_field_with_old_symbols() {
            for old_symbol in OldSymbol::iter() {
                let mut symbol: SymbolField = old_symbol.clone().into();
                if old_symbol.get_text().is_some() {
                    symbol.value = 10.into();
                }
                let str = format!(
                    "{}{}\n",
                    match symbol.symbol {
                        Symbol::Null => "'Null'".to_owned(),
                        _ => format!("{}", symbol.symbol),
                    },
                    match &symbol.value {
                        CharacteristicValue::Empty => "".to_owned(),
                        value => format!(": {}", value),
                    }
                );
                println!("{}", str);
                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );
                let str = format!(
                    "{}{}\n",
                    match symbol.symbol {
                        Symbol::Null => "'Null'".to_owned(),
                        _ => format!("{}", symbol.symbol),
                    },
                    match &symbol.value {
                        CharacteristicValue::Empty => "".to_owned(),
                        value => format!("({})", value),
                    }
                );
                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );
                let str = serde_yaml::to_string(&symbol).expect("A test should never fail");
                println!("{}", str);
                assert_eq!(
                    serde_yaml::from_str::<SymbolField>(&str).expect("A test should never fail"),
                    symbol
                );
            }
        }

        #[test]
        fn test_display() {
            for symbol in Symbol::iter() {
                let symbol: SymbolField = symbol.into();
                assert_eq!(symbol.to_string(), symbol.symbol.to_string());
                let symbol = SymbolField {
                    value: 10.into(),
                    ..symbol
                };
                assert_eq!(symbol.to_string(), format!("{}(10)", symbol.symbol));
            }
        }

        #[test]
        fn test_render() {
            let symbols_config = SymbolsConfig::default();
            let config = Config::default();
            let font = &config
                .get_font(&Layer::Symbols)
                .expect("A test should never fail");
            let width = config
                .get_width(&Layer::Symbols)
                .expect("A test should never fail");

            let symbol = SymbolField {
                symbol: Symbol::Null,
                value: 10.into(),
                text_color: Color::Default,
                border_color: Color::Default,
            };
            let symbol_config = symbols_config.get_symbol(&Symbol::Null);

            let _ = symbol.render(&config, &symbol_config, font, width);

            let symbol = SymbolField {
                symbol: Symbol::Arc,
                value: CharacteristicValue::Empty,
                text_color: Color::Red,
                border_color: Color::Purple,
            };
            let symbol_config = symbols_config.get_symbol(&Symbol::Arc);

            let _ = symbol.render(&config, &symbol_config, font, width);

            let symbol = SymbolField {
                symbol: Symbol::Couronne,
                value: 10.into(),
                text_color: Color::Red,
                border_color: Color::Purple,
            };
            let symbol_config = symbols_config.get_symbol(&Symbol::Couronne);

            let _ = symbol.render(&config, &symbol_config, font, width);
        }
    }
}
