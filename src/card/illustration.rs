// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for parts of a card related to illustration

use crate::color::Color;
use crate::error::Error;
use crate::render::images::{Image, RasterImage, Svg};
use crate::render::{config::Config, Group, Layer, Point, Render, RenderedCard};

use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};
use strum_macros::{Display, EnumIter};
use usvg::Transform;

/// Size of a card
static SIZE: Point = Point { x: 1500., y: 2300. };

/// Represents the illustration of a Card
///
/// <span>&#9888;</span> Currently the position is not optional during deserialization
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Illustration {
    /// If present and exists on the system, will be used during updating to replace the image
    #[serde(alias = "illustration", default, skip_serializing)]
    path: Option<String>,
    /// Position of the image, is currently not optional during deserialization
    #[serde(flatten, default)]
    pub position: Point,
    /// Zoom of the image, in pourcent
    #[serde(default)]
    pub zoom: u16,
    /// Mode of the illustration
    #[serde(default)]
    pub mode: Mode,
    /// Allow saves to carry the image within themselves
    #[serde(default)]
    image: Option<RasterImage>,
}

/// Represents the mode of the illustration
#[derive(
    Serialize, Deserialize, Debug, Clone, Copy, PartialEq, Eq, Hash, EnumIter, Default, Display,
)]
pub enum Mode {
    /// The illustration overflows on its border
    #[serde(alias = "OVER_BORDER")]
    OverBorder,
    /// The illustration overflows on its border and the background of the card
    #[serde(alias = "OVER_BACK")]
    OverBack,
    /// The illustration overflows on its border, the background of the card and the characteristics band but not the text
    #[serde(alias = "OVER_BAND")]
    OverBand,
    /// There is no illustration border nor main background
    #[serde(alias = "FULL_ART")]
    FullArt,
    /// There is no illustration border, main background nor background of the characteristics, but the characteristics strips are visible
    #[serde(alias = "BELLOW_STRIP")]
    #[serde(alias = "BellowStrip")]
    FullBellowStrip,
    /// The is no illustration border, main background, background of the characteristics nor strips of the characteristics
    #[serde(alias = "BELLOW_TEXT")]
    #[serde(alias = "BellowText")]
    FullBellowText,
    /// The normal mode
    #[serde(alias = "NORMAL", other)]
    #[default]
    Classic,
}

/// Represents the template of the Card
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Template {
    /// Whether the card is horizontal or vertical
    #[serde(alias = "template", default)]
    pub orientation: Orientation,
    /// Color of the main background of the card
    #[serde(alias = "main color", default)]
    pub main_color: Color,
    /// Color of the strip around the luck and the charisma
    ///
    /// If not presents, will be replaced by `characteristic_strip_color` during update
    #[serde(alias = "lu ch strip color", default)]
    pub luck_and_charm_strip_color: Color,
    /// Color of the strip around the illustration
    #[serde(alias = "border color", default)]
    pub border_color: Color,
    /// Color of the background of the image
    #[serde(alias = "image background color", default)]
    pub image_background_color: Color,
    /// Color of the background of the Characteristics
    #[serde(alias = "stat color", default)]
    pub characteristic_color: Color,
    /// Color of the strip around the Characteristics
    #[serde(alias = "stats strip color", default)]
    #[serde(alias = "strip color")]
    pub characteristic_strip_color: Color,
    /// Color of the name of the Characteristics
    ///
    /// It is deprecated and is replaced by `name_color` in [CharacteristicsField](crate::card::fields::CharacteristicsField)
    #[serde(alias = "stat name color", default, skip_serializing)]
    pub characteristic_name_color: Color,
    /// Color of the text above the pictograms
    ///
    /// It is deprecated and is replaced by `text_color` in [Pictogram](crate::card::pictogram::Pictogram)
    #[serde(alias = "picto num color", default, skip_serializing)]
    pub pictogram_number_color: Color,
}

/// Represents the orientation of the Card
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, EnumIter, Default, Display)]
pub enum Orientation {
    /// The characteristics are displayed horizontally
    #[serde(alias = "HORIZONTAL")]
    Horizontal,
    /// The characteristics are displayed vertically
    #[serde(alias = "VERTICAL", other)]
    #[default]
    Vertical,
}

impl Illustration {
    /// Move the illustration from the given translation
    ///
    /// `translation` is proportional to the size of the card, meaning if a translation is (1.0, 0.0) the illustration will be moved to the right by the width of the card
    pub fn move_image<P: Into<Point>>(&mut self, translation: P) {
        let translation: Point = translation.into() * SIZE;
        self.position = self.position + translation
    }

    /// Zoom in the illustration
    ///
    /// `cursor` is the position of the cursor relative to the illustration its coordinate should be between 0 and 1
    pub fn zoom_in(&mut self, cursor: Option<Point>) {
        if let Some(cursor) = cursor {
            let cursor = cursor * SIZE;
            self.position = cursor + (self.position - cursor) * 1.1;
        }
        let new: u16 = self.zoom * 11 / 10;
        self.zoom = if new >= u16::MAX / 11 {
            u16::MAX / 11
        } else if new > self.zoom {
            new
        } else {
            new + 1
        }
    }

    /// Zoom out the illustration
    ///
    /// `cursor` is the position of the cursor relative to the illustration its coordinate should be between 0 and 1
    pub fn zoom_out(&mut self, cursor: Option<Point>) {
        if let Some(cursor) = cursor {
            let cursor = cursor * SIZE;
            self.position = cursor + (self.position - cursor) / 1.1;
        }
        let new: u16 = self.zoom * 10 / 11;

        self.zoom = if new == 0 { 1 } else { new }
    }

    /// Sets the zoom of the illustration
    pub fn set_zoom(&mut self, zoom: u16) {
        if zoom == 0 {
            self.zoom = 1
        } else if zoom > u16::MAX / 11 {
            self.zoom = u16::MAX / 11
        } else {
            self.zoom = zoom
        }
    }

    /// Reset the illustration to its default values
    pub fn reset(&mut self) {
        self.zoom = 100;
        self.position = Point::default();
        self.mode = Mode::Classic;
    }

    /// Load the illustration from the given path
    pub fn load(&mut self, path: &PathBuf) -> Result<(), Error> {
        self.image = match RasterImage::open(path, None) {
            Ok(image) => Some(image),
            Err(err) => {
                log::warn!("Error while opening the file {:?}: {err}", path);
                return Err(err);
            }
        };
        Ok(())
    }

    /// Get the path of the illustration if any
    pub fn path(&self) -> Option<PathBuf> {
        if let Some(path) = &self.path {
            let path = Path::new(&path);
            Some(path.to_path_buf())
        } else {
            None
        }
    }
}

impl super::Updatable for Template {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if self.main_color == Color::default() {
            log::warn!(
                "The main color is not set, using default value : {}",
                default.main_color
            );
            res = true;
            self.main_color = default.main_color
        }
        let closest_color = self.main_color.closest();
        if self.main_color != closest_color {
            log::warn!(
                "The main color {} was too close to {}, replacing",
                self.main_color,
                closest_color
            );
            res = true;
            self.main_color = closest_color;
        }
        if self.border_color == Color::default() {
            log::warn!(
                "The color of the illustration's border is not set, using default value : {}",
                default.border_color
            );
            res = true;
            self.border_color = default.border_color
        }
        let closest_color = self.border_color.closest();
        if self.border_color != closest_color {
            log::warn!(
                "The color of the illustration's border {} was too close to {}, replacing",
                self.border_color,
                closest_color
            );
            res = true;
            self.border_color = closest_color;
        }
        if self.characteristic_color == Color::default() {
            log::warn!(
                "The color of the Characteristics band is not set, using default value : {}",
                default.characteristic_color
            );
            res = true;
            self.characteristic_color = default.characteristic_color
        }
        let closest_color = self.characteristic_color.closest();
        if self.characteristic_color != closest_color {
            log::warn!(
                "The color of the Characteristics band {} was too close to {}, replacing",
                self.characteristic_color,
                closest_color
            );
            res = true;
            self.characteristic_color = closest_color;
        }
        if self.characteristic_strip_color == Color::default() {
            log::warn!(
                "The color of the Characteristics strip is not set, using default value : {}",
                default.characteristic_strip_color
            );
            res = true;
            self.characteristic_strip_color = default.characteristic_strip_color
        }
        let closest_color = self.characteristic_strip_color.closest();
        if self.characteristic_strip_color != closest_color {
            log::warn!(
                "The color of the Characteristics strip {} was too close to {}, replacing",
                self.characteristic_strip_color,
                closest_color
            );
            res = true;
            self.characteristic_strip_color = closest_color;
        }
        if self.luck_and_charm_strip_color == Color::default() {
            log::warn!("The color of the luck and the charm strip is not set, using the characteristic strip value : {}", self.characteristic_strip_color);
            res = true;
            self.luck_and_charm_strip_color = self.characteristic_strip_color
        }
        let closest_color = self.luck_and_charm_strip_color.closest();
        if self.luck_and_charm_strip_color != closest_color {
            log::warn!(
                "The color of the luck and the charm strip {} was too close to {}, replacing",
                self.luck_and_charm_strip_color,
                closest_color
            );
            res = true;
            self.luck_and_charm_strip_color = closest_color;
        }
        if self.image_background_color == Color::default() {
            log::warn!(
                "The color of the image background is not set, using default value : {}",
                default.image_background_color
            );
            res = true;
            self.image_background_color = default.image_background_color
        }
        let closest_color = self.image_background_color.closest();
        if self.image_background_color != closest_color {
            log::warn!(
                "The color of the image background {} was too close to {}, replacing",
                self.image_background_color,
                closest_color
            );
            res = true;
            self.image_background_color = closest_color;
        }
        res
    }

    fn get_default() -> Self {
        Template {
            orientation: Orientation::default(),
            main_color: Color::default(),
            luck_and_charm_strip_color: Color::default(),
            border_color: Color::default(),
            image_background_color: Color::default(),
            characteristic_color: Color::default(),
            characteristic_strip_color: Color::default(),
            characteristic_name_color: Color::default(),
            pictogram_number_color: Color::default(),
        }
    }
}

impl super::Updatable for Illustration {
    fn update(&mut self, default: &Self) -> bool {
        let mut res = false;
        if let Some(path) = &self.path {
            if std::path::Path::new(&path).exists() {
                log::info!("Loading file {} as the picture", path);
                self.image = match RasterImage::open(path, None) {
                    Ok(image) => Some(image),
                    Err(err) => {
                        log::warn!("Error while opening the file {}: {err}", path);
                        None
                    }
                };
                res = self.image.is_some();
            } else {
                log::warn!("File {} does not exist", path);
            }
        }
        if self.zoom == 0 {
            res = true;
            self.zoom = default.zoom;
            log::warn!("The zoom has been updated to {}", self.zoom);
        }
        if self.path.is_none() && self.image.is_none() {
            log::warn!("No picture was provided, the default one will be used");
        }
        res
    }

    fn get_default() -> Self {
        Self {
            path: None,
            zoom: 0,
            position: Point { x: -100., y: 420. },
            mode: Mode::default(),
            image: None,
        }
    }
}

impl Render for Illustration {
    fn render(
        &self,
        layer: &Layer,
        _: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let image = match &self.image {
            Some(image) => image.clone(),
            None => RasterImage::from_bytes(include_bytes!("../Alakir.png"), None)?,
        };
        Ok(image
            .to_scale_node(layer, self.position, self.zoom as f32 / 100.)
            .into())
    }
}

impl Render for Template {
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        _: Option<&RenderedCard>,
    ) -> Result<usvg::Node, Error> {
        let mut group = Group::new(layer, Transform::identity());
        group.append(
            Svg::from_str(
                include_str!("../templates/card_background.svg"),
                &Layer::Background,
            )
            .set_fill_color(self.image_background_color),
        );
        group.append(
            Svg::from_str(
                match self.orientation {
                    Orientation::Vertical => include_str!("../templates/illus_border_V.svg"),
                    Orientation::Horizontal => include_str!("../templates/illus_border_H.svg"),
                },
                &Layer::IllustrationBorder,
            )
            .set_fill_color(self.border_color)
            .to_scale_node(
                &Layer::IllustrationBorder,
                config.get_position(&Layer::IllustrationBorder)?,
                config.get_scale(&Layer::IllustrationBorder),
            ),
        );
        group.append(
            Svg::from_str(
                match self.orientation {
                    Orientation::Vertical => include_str!("../templates/main_V.svg"),
                    Orientation::Horizontal => include_str!("../templates/main_H.svg"),
                },
                &Layer::Main,
            )
            .set_fill_color(self.main_color)
            .to_scale_node(
                &Layer::Main,
                config.get_position(&Layer::Main)?,
                config.get_scale(&Layer::Main),
            ),
        );
        group.append(
            Svg::from_str(
                match self.orientation {
                    Orientation::Vertical => include_str!("../templates/stat_V.svg"),
                    Orientation::Horizontal => include_str!("../templates/stat_H.svg"),
                },
                &Layer::CharacteristicsBand,
            )
            .set_fill_color(self.characteristic_color)
            .to_scale_node(
                &Layer::CharacteristicsBand,
                config.get_position(&Layer::CharacteristicsBand)?,
                config.get_scale(&Layer::CharacteristicsBand),
            ),
        );
        group.append(
            Svg::from_str(include_str!("../templates/luck.svg"), &Layer::Luck)
                .set_fill_color(self.characteristic_color)
                .set_stroke_color(Color::Transparent)
                .to_scale_node(
                    &Layer::Luck,
                    config.get_position(&Layer::Luck)?,
                    config.get_scale(&Layer::Luck),
                ),
        );
        group.append(
            Svg::from_str(include_str!("../templates/charism.svg"), &Layer::Charisma)
                .set_fill_color(self.characteristic_color)
                .set_stroke_color(Color::Transparent)
                .to_scale_node(
                    &Layer::Charisma,
                    config.get_position(&Layer::Charisma)?,
                    config.get_scale(&Layer::Charisma),
                ),
        );
        group.append(
            Svg::from_str(
                match self.orientation {
                    Orientation::Vertical => {
                        include_str!("../templates/characteristic_border_V.svg")
                    }
                    Orientation::Horizontal => {
                        include_str!("../templates/characteristic_border_H.svg")
                    }
                },
                &Layer::CharacteristicsBorder,
            )
            .set_fill_color(self.characteristic_strip_color)
            .to_scale_node(
                &Layer::CharacteristicsBorder,
                config.get_position(&Layer::CharacteristicsBorder)?,
                config.get_scale(&Layer::CharacteristicsBorder),
            ),
        );
        group.append(
            Svg::from_str(include_str!("../templates/luck.svg"), &Layer::Luck)
                .set_stroke_color(self.luck_and_charm_strip_color)
                .set_fill_color(Color::Transparent)
                .to_scale_node(
                    &Layer::LuckBorder,
                    config.get_position(&Layer::LuckBorder)?,
                    config.get_scale(&Layer::LuckBorder),
                ),
        );
        group.append(
            Svg::from_str(include_str!("../templates/charism.svg"), &Layer::Charisma)
                .set_stroke_color(self.luck_and_charm_strip_color)
                .set_fill_color(Color::Transparent)
                .to_scale_node(
                    &Layer::CharismaBorder,
                    config.get_position(&Layer::CharismaBorder)?,
                    config.get_scale(&Layer::CharismaBorder),
                ),
        );
        Ok(group.into())
    }
}

impl Default for Template {
    fn default() -> Self {
        super::DEFAULT_CARD.template.clone()
    }
}

impl Default for Illustration {
    fn default() -> Self {
        super::DEFAULT_CARD.illustration.clone()
    }
}

#[cfg(test)]
mod test_module_illustration {

    #[cfg(test)]
    mod test_enum_mode {
        use super::super::Mode;

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_mode() {
            assert_eq!(
                Mode::OverBorder,
                serde_yaml::from_str("OverBorder").expect("A test should never fail")
            );
            assert_eq!(
                Mode::OverBorder,
                serde_yaml::from_str("OVER_BORDER").expect("A test should never fail")
            );
            assert_eq!(
                "OverBorder\n",
                serde_yaml::to_string(&Mode::OverBorder).expect("A test should never fail")
            );

            assert_eq!(
                Mode::OverBack,
                serde_yaml::from_str("OverBack").expect("A test should never fail")
            );
            assert_eq!(
                Mode::OverBack,
                serde_yaml::from_str("OVER_BACK").expect("A test should never fail")
            );
            assert_eq!(
                "OverBack\n",
                serde_yaml::to_string(&Mode::OverBack).expect("A test should never fail")
            );

            assert_eq!(
                Mode::OverBand,
                serde_yaml::from_str("OverBand").expect("A test should never fail")
            );
            assert_eq!(
                Mode::OverBand,
                serde_yaml::from_str("OVER_BAND").expect("A test should never fail")
            );
            assert_eq!(
                "OverBand\n",
                serde_yaml::to_string(&Mode::OverBand).expect("A test should never fail")
            );

            assert_eq!(
                Mode::FullArt,
                serde_yaml::from_str("FullArt").expect("A test should never fail")
            );
            assert_eq!(
                Mode::FullArt,
                serde_yaml::from_str("FULL_ART").expect("A test should never fail")
            );
            assert_eq!(
                "FullArt\n",
                serde_yaml::to_string(&Mode::FullArt).expect("A test should never fail")
            );

            assert_eq!(
                Mode::FullBellowStrip,
                serde_yaml::from_str("FullBellowStrip").expect("A test should never fail")
            );
            assert_eq!(
                Mode::FullBellowStrip,
                serde_yaml::from_str("BELLOW_STRIP").expect("A test should never fail")
            );
            assert_eq!(
                Mode::FullBellowStrip,
                serde_yaml::from_str("BellowStrip").expect("A test should never fail")
            );
            assert_eq!(
                "FullBellowStrip\n",
                serde_yaml::to_string(&Mode::FullBellowStrip).expect("A test should never fail")
            );

            assert_eq!(
                Mode::FullBellowText,
                serde_yaml::from_str("FullBellowText").expect("A test should never fail")
            );
            assert_eq!(
                Mode::FullBellowText,
                serde_yaml::from_str("BELLOW_TEXT").expect("A test should never fail")
            );
            assert_eq!(
                Mode::FullBellowText,
                serde_yaml::from_str("BellowText").expect("A test should never fail")
            );
            assert_eq!(
                "FullBellowText\n",
                serde_yaml::to_string(&Mode::FullBellowText).expect("A test should never fail")
            );

            assert_eq!(
                Mode::Classic,
                serde_yaml::from_str("Classic").expect("A test should never fail")
            );
            assert_eq!(
                Mode::Classic,
                serde_yaml::from_str("NORMAL").expect("A test should never fail")
            );
            assert_eq!(
                Mode::Classic,
                serde_yaml::from_str("Anything else").expect("A test should never fail")
            );
            assert_eq!(
                "Classic\n",
                serde_yaml::to_string(&Mode::Classic).expect("A test should never fail")
            );
        }

        #[test]
        fn test_debug_mode() {
            assert_eq!("OverBorder", format!("{:?}", Mode::OverBorder));
            assert_eq!("OverBack", format!("{:?}", Mode::OverBack));
            assert_eq!("OverBand", format!("{:?}", Mode::OverBand));
            assert_eq!("FullArt", format!("{:?}", Mode::FullArt));
            assert_eq!("FullBellowStrip", format!("{:?}", Mode::FullBellowStrip));
            assert_eq!("FullBellowText", format!("{:?}", Mode::FullBellowText));
            assert_eq!("Classic", format!("{:?}", Mode::Classic));
        }

        #[test]
        fn test_default_mode() {
            assert_eq!(Mode::default(), Mode::Classic);
        }

        #[test]
        fn test_clone_mode() {
            for mode in Mode::iter() {
                assert_eq!(mode, mode.clone());
            }
        }
    }

    #[cfg(test)]
    mod test_enum_orientation {
        use super::super::Orientation;

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_orientation() {
            assert_eq!(
                Orientation::Horizontal,
                serde_yaml::from_str("Horizontal").expect("A test should never fail")
            );
            assert_eq!(
                Orientation::Horizontal,
                serde_yaml::from_str("HORIZONTAL").expect("A test should never fail")
            );
            assert_eq!(
                "Horizontal\n",
                serde_yaml::to_string(&Orientation::Horizontal).expect("A test should never fail")
            );

            assert_eq!(
                Orientation::Vertical,
                serde_yaml::from_str("Vertical").expect("A test should never fail")
            );
            assert_eq!(
                Orientation::Vertical,
                serde_yaml::from_str("VERTICAL").expect("A test should never fail")
            );
            assert_eq!(
                Orientation::Vertical,
                serde_yaml::from_str("Anything else").expect("A test should never fail")
            );
            assert_eq!(
                "Vertical\n",
                serde_yaml::to_string(&Orientation::Vertical).expect("A test should never fail")
            );
        }

        #[test]
        fn test_debug_orientation() {
            assert_eq!("Horizontal", format!("{:?}", Orientation::Horizontal));
            assert_eq!("Vertical", format!("{:?}", Orientation::Vertical));
        }

        #[test]
        fn test_default_orientation() {
            assert_eq!(Orientation::default(), Orientation::Vertical);
        }

        #[test]
        fn test_clone_orientation() {
            for orientation in Orientation::iter() {
                assert_eq!(orientation, orientation.clone());
            }
        }
    }

    #[cfg(test)]
    mod test_struct_template {
        use super::super::Template;

        use super::super::super::Updatable;
        use super::super::{Mode, Orientation};
        use crate::color::Color;
        use crate::render::{config::Config, HasNode, Layer, Render};

        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_template() {
            let template = Template {
                orientation: Orientation::Vertical,
                main_color: Color::Black,
                luck_and_charm_strip_color: Color::Blue,
                border_color: Color::Green,
                image_background_color: Color::Purple,
                characteristic_color: Color::Pink,
                characteristic_strip_color: Color::Red,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(!test.update(&template));
            assert_eq!(template, test);

            // Characteristic name color has been moved to characteristics field, hence is not serialized anymore
            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\n";
            assert_eq!(
                s,
                serde_yaml::to_string(&template).expect("A test should never fail")
            );

            let s = "orientation: Vertical\nmain color: Black\nlu ch strip color: Blue\nborder color: Green\nimage background color: Purple\nstat color: Pink\nstats strip color: Red\nstat name color: Yellow\npicto num color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(!test.update(&template));
            assert_eq!(template, test);

            let s = "main_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(!test.update(&template));
            assert_eq!(template, test);

            let s = "orientation: Vertical\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(test.update(&template));
            assert_eq!(template, test);

            let s = "orientation: Vertical\nmain_color: Black\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(test.update(&template));
            assert_eq!(
                Template {
                    luck_and_charm_strip_color: Color::Red,
                    ..template.clone()
                },
                test
            );

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(test.update(&template));
            assert_eq!(template, test);

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(test.update(&template));
            assert_eq!(template, test);

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(test.update(&template));
            assert_eq!(template, test);

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_name_color: Yellow\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(test.update(&template));
            assert_eq!(template, test);

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\npictogram_number_color: Lime\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(!test.update(&template));
            assert_eq!(
                Template {
                    characteristic_name_color: Color::default(),
                    ..template.clone()
                },
                test
            );

            let s = "orientation: Vertical\nmain_color: Black\nluck_and_charm_strip_color: Blue\nborder_color: Green\nimage_background_color: Purple\ncharacteristic_color: Pink\ncharacteristic_strip_color: Red\ncharacteristic_name_color: Yellow\n";
            let mut test = serde_yaml::from_str::<Template>(&s).expect("A test should never fail");
            assert!(!test.update(&template));
            assert_eq!(
                Template {
                    pictogram_number_color: Color::default(),
                    ..template.clone()
                },
                test
            );
        }

        #[test]
        fn test_debug_template() {
            let template = Template {
                orientation: Orientation::Vertical,
                main_color: Color::Black,
                luck_and_charm_strip_color: Color::Blue,
                border_color: Color::Green,
                image_background_color: Color::Purple,
                characteristic_color: Color::Pink,
                characteristic_strip_color: Color::Red,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };
            let s = "Template { orientation: Vertical, main_color: Black, luck_and_charm_strip_color: Blue, border_color: Green, image_background_color: Purple, characteristic_color: Pink, characteristic_strip_color: Red, characteristic_name_color: Yellow, pictogram_number_color: Lime }";
            assert_eq!(s, format!("{:?}", template));
        }

        #[test]
        fn test_default_template() {
            assert_eq!(
                Template::default(),
                crate::card::Card::get_default().template
            );

            assert_eq!(
                Template::get_default(),
                Template {
                    orientation: Orientation::default(),
                    main_color: Color::default(),
                    luck_and_charm_strip_color: Color::default(),
                    border_color: Color::default(),
                    image_background_color: Color::default(),
                    characteristic_color: Color::default(),
                    characteristic_strip_color: Color::default(),
                    characteristic_name_color: Color::default(),
                    pictogram_number_color: Color::default(),
                }
            );
        }

        #[test]
        fn test_render_template() {
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let template = Template {
                        orientation,
                        ..Template::default()
                    };

                    let render = template
                        .render(&Layer::Template, &config, None)
                        .expect("A test should never fail");

                    assert!(render.has_node(&Layer::Background));
                    assert!(render.has_node(&Layer::IllustrationBorder));
                    assert!(render.has_node(&Layer::Main));
                    assert!(render.has_node(&Layer::CharacteristicsBand));
                    assert!(render.has_node(&Layer::Luck));
                    assert!(render.has_node(&Layer::Charisma));
                    assert!(render.has_node(&Layer::CharacteristicsBorder));
                    assert!(render.has_node(&Layer::LuckBorder));
                    assert!(render.has_node(&Layer::CharismaBorder));
                }
            }
        }
    }

    #[cfg(test)]
    mod test_struct_illustration {
        use super::super::Illustration;

        use super::super::super::Updatable;
        use crate::card::illustration::{Mode, Orientation};
        use crate::render::{config::Config, HasNode, Layer, Point, Render};
        use std::path::PathBuf;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_illustration() {
            let illustration = Illustration {
                path: None,
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };

            assert_eq!(
                "x: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\nimage: null\n",
                serde_yaml::to_string(&illustration).expect("A test should never fail")
            );

            let s = "x: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(!test.update(&illustration));
            assert_eq!(illustration, test);

            let s = "path: wrong\nx: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(!test.update(&illustration));
            assert_eq!(
                Illustration {
                    path: Some("wrong".to_owned()),
                    ..illustration.clone()
                },
                test
            );

            let s = "illustration: wrong\nx: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(!test.update(&illustration));
            assert_eq!(
                Illustration {
                    path: Some("wrong".to_owned()),
                    ..illustration.clone()
                },
                test
            );

            let s = "path: tests/images/Alakir.png\nx: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(test.update(&illustration));
            assert_ne!(illustration, test);

            let s = "path: tests/images/Alakir.svg\nx: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(!test.update(&illustration));
            assert_eq!(
                Illustration {
                    path: Some("tests/images/Alakir.svg".to_owned()),
                    ..illustration.clone()
                },
                test
            );

            // let s = "zoom: 100\nmode: Classic\nimage: null\n";
            // let mut test = serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            // assert!(!test.update(&illustration));
            // assert_eq!(illustration, test);

            let s = "x: 0.0\ny: 0.0\nmode: Classic\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(test.update(&illustration));
            assert_eq!(illustration, test);

            let s = "x: 0.0\ny: 0.0\nzoom: 100\nimage: null\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(!test.update(&illustration));
            assert_eq!(illustration, test);

            let s = "x: 0.0\ny: 0.0\nzoom: 100\nmode: Classic\n";
            let mut test =
                serde_yaml::from_str::<Illustration>(&s).expect("A test should never fail");
            assert!(!test.update(&illustration));
            assert_eq!(illustration, test);
        }

        #[test]
        fn test_debug_illustration() {
            let s = "Illustration { path: None, position: Point { x: 0.0, y: 0.0 }, zoom: 100, mode: Classic, image: None }";
            let illustration = Illustration {
                path: None,
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };
            assert_eq!(s, format!("{:?}", illustration));
        }

        #[test]
        fn test_default_illustration() {
            assert_eq!(
                Illustration::default(),
                crate::card::Card::get_default().illustration
            );

            let illustration = Illustration {
                path: None,
                position: Point { x: -100., y: 420. },
                zoom: 0,
                mode: Mode::Classic,
                image: None,
            };
            assert_eq!(Illustration::get_default(), illustration)
        }

        #[test]
        fn test_render_illustration() {
            for orientation in Orientation::iter() {
                for mode in Mode::iter() {
                    let mut config = Config::default();
                    config.set_orientation(orientation);
                    config.set_mode(mode);

                    let illustration = Illustration {
                        path: None,
                        position: Point { x: 0., y: 0. },
                        zoom: 100,
                        mode: Mode::Classic,
                        image: None,
                    };

                    assert!(illustration
                        .render(&Layer::Illustration, &config, None)
                        .expect("A test should never fail")
                        .has_node(&Layer::Illustration));

                    let mut illustration = Illustration {
                        path: Some("tests/images/Alakir.png".to_owned()),
                        position: Point { x: 0., y: 0. },
                        zoom: 100,
                        mode: Mode::Classic,
                        image: None,
                    };
                    assert!(illustration.update(&Illustration::get_default()));
                    assert!(illustration
                        .render(&Layer::Illustration, &config, None)
                        .expect("A test should never fail")
                        .has_node(&Layer::Illustration));
                }
            }
        }

        #[test]
        fn test_move_illustration() {
            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };
            illustration.move_image(Point { x: 1., y: 1. });

            assert_eq!(illustration.position, super::super::SIZE);
        }

        #[test]
        fn test_zoom_illustration() {
            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };

            illustration.zoom_in(None);
            assert_eq!(illustration.zoom, 110);
            assert_eq!(illustration.position, Point { x: 0., y: 0. });

            illustration.zoom_out(None);
            assert_eq!(illustration.zoom, 100);
            assert_eq!(illustration.position, Point { x: 0., y: 0. });

            illustration.zoom_in(Some(Point { x: 0., y: 0. }));
            assert_eq!(illustration.zoom, 110);
            assert_eq!(illustration.position, Point { x: 0., y: 0. });

            illustration.zoom_out(Some(Point { x: 0., y: 0. }));
            assert_eq!(illustration.zoom, 100);
            assert_eq!(illustration.position, Point { x: 0., y: 0. });

            illustration.zoom_in(Some(Point { x: 1., y: 1. }));
            assert_eq!(illustration.zoom, 110);
            let expected = -super::super::SIZE * 0.1;
            assert_eq!(illustration.position, expected);

            illustration.zoom_out(Some(Point { x: 1., y: 1. }));
            assert_eq!(illustration.zoom, 100);
            assert_eq!(illustration.position, Point { x: 0., y: 0. });

            illustration.zoom_in(Some(Point { x: 0.5, y: 0.5 }));
            assert_eq!(illustration.zoom, 110);
            let expected = -super::super::SIZE * 0.05;
            assert_eq!(illustration.position, expected);

            illustration.zoom_out(Some(Point { x: 0.5, y: 0.5 }));
            assert_eq!(illustration.zoom, 100);
            assert_eq!(illustration.position, Point { x: 0., y: 0. });
        }

        #[test]
        fn test_bounded_zoom_illustration() {
            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: u16::MAX / 11 - 10,
                mode: Mode::Classic,
                image: None,
            };
            illustration.zoom_in(None);
            assert_eq!(illustration.zoom, u16::MAX / 11);

            illustration.zoom_in(None);
            assert_eq!(illustration.zoom, u16::MAX / 11);

            illustration.set_zoom(u16::MAX);
            assert_eq!(illustration.zoom, u16::MAX / 11);

            illustration.set_zoom(0);
            assert_eq!(illustration.zoom, 1);

            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 1,
                mode: Mode::Classic,
                image: None,
            };
            illustration.zoom_in(None);
            assert_eq!(illustration.zoom, 2);
            illustration.zoom_in(None);
            assert_eq!(illustration.zoom, 3);

            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 2,
                mode: Mode::Classic,
                image: None,
            };
            illustration.zoom_out(None);
            assert_eq!(illustration.zoom, 1);
            illustration.zoom_out(None);
            assert_eq!(illustration.zoom, 1);
        }

        #[test]
        fn test_reset_illustration() {
            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 123., y: 122. },
                zoom: 120,
                mode: Mode::FullArt,
                image: None,
            };
            illustration.reset();
            assert_eq!(
                illustration.path,
                Some("tests/images/Alakir.png".to_owned())
            );
            assert_eq!(illustration.position, Point::default());
            assert_eq!(illustration.zoom, 100);
            assert_eq!(illustration.mode, Mode::Classic);
            assert_eq!(illustration.image, None);
        }

        #[test]
        fn test_load_illustration() {
            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };

            assert!(illustration
                .load(&PathBuf::from("tests/Alakir.png"))
                .is_err());
            assert!(illustration.image.is_none());

            assert!(illustration
                .load(&PathBuf::from("tests/images/Alakir.png"))
                .is_ok());
            assert!(illustration.image.is_some());

            assert!(illustration
                .load(&PathBuf::from("tests/Alakir.png"))
                .is_err());
            assert!(illustration.image.is_some());
        }
    }

    mod test_log_updatable {
        use log::Level;
        use log_tester::LogTester;

        use super::super::super::Updatable;
        use super::super::{Illustration, Template};
        use super::super::{Mode, Orientation};
        use crate::color::Color;
        use crate::render::Point;

        #[test]
        fn test_no_path_illustration() {
            LogTester::start();
            let mut illustration = Illustration {
                path: None,
                position: Point { x: 1., y: 2. },
                zoom: 111,
                mode: Mode::FullArt,
                image: None,
            };

            let default = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };

            assert!(!illustration.update(&default));
            assert_eq!(illustration.path, None);
            assert_eq!(illustration.position, Point { x: 1., y: 2. });
            assert_eq!(illustration.zoom, 111);
            assert_eq!(illustration.mode, Mode::FullArt);
            assert!(illustration.image.is_none());

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "No picture was provided, the default one will be used"
            ));
        }

        #[test]
        fn test_fake_path_illustration() {
            LogTester::start();
            let mut illustration = Illustration {
                path: Some("tests/Alakir.png".to_owned()),
                position: Point { x: 1., y: 2. },
                zoom: 111,
                mode: Mode::FullArt,
                image: None,
            };

            let default = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };

            assert!(!illustration.update(&default));
            assert_eq!(illustration.path, Some("tests/Alakir.png".to_owned()));
            assert_eq!(illustration.position, Point { x: 1., y: 2. });
            assert_eq!(illustration.zoom, 111);
            assert_eq!(illustration.mode, Mode::FullArt);
            assert!(illustration.image.is_none());

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "File tests/Alakir.png does not exist"
            ));
        }

        #[test]
        fn test_wrong_file_illustration() {
            LogTester::start();
            let mut illustration = Illustration {
                path: Some("tests/cli.rs".to_owned()),
                position: Point { x: 1., y: 2. },
                zoom: 111,
                mode: Mode::FullArt,
                image: None,
            };

            let default = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 100,
                mode: Mode::Classic,
                image: None,
            };

            assert!(!illustration.update(&default));
            assert_eq!(illustration.path, Some("tests/cli.rs".to_owned()));
            assert_eq!(illustration.position, Point { x: 1., y: 2. });
            assert_eq!(illustration.zoom, 111);
            assert_eq!(illustration.mode, Mode::FullArt);
            assert!(illustration.image.is_none());

            assert_eq!(LogTester::len(), 2);
            assert!(LogTester::contains(
                Level::Warn,
                "Error while opening the file tests/cli.rs: "
            ));
            assert!(LogTester::contains(
                Level::Info,
                "Loading file tests/cli.rs as the picture"
            ));
        }

        #[test]
        fn test_right_file_illustration() {
            LogTester::start();
            let mut illustration = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 1., y: 2. },
                zoom: 111,
                mode: Mode::FullArt,
                image: None,
            };

            let default = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 222,
                mode: Mode::Classic,
                image: None,
            };

            assert!(illustration.update(&default));
            assert_eq!(
                illustration.path,
                Some("tests/images/Alakir.png".to_owned())
            );
            assert_eq!(illustration.position, Point { x: 1., y: 2. });
            assert_eq!(illustration.zoom, 111);
            assert_eq!(illustration.mode, Mode::FullArt);
            assert!(illustration.image.is_some());

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Info,
                "Loading file tests/images/Alakir.png as the picture"
            ));
        }

        #[test]
        fn test_zoom_illustration() {
            LogTester::start();
            let mut illustration = Illustration {
                path: None,
                position: Point { x: 1., y: 2. },
                zoom: 0,
                mode: Mode::FullArt,
                image: None,
            };

            let default = Illustration {
                path: Some("tests/images/Alakir.png".to_owned()),
                position: Point { x: 0., y: 0. },
                zoom: 111,
                mode: Mode::Classic,
                image: None,
            };

            assert!(illustration.update(&default));
            assert!(illustration.path.is_none());
            assert_eq!(illustration.zoom, 111);
            assert_eq!(illustration.position, Point { x: 1., y: 2. });
            assert_eq!(illustration.mode, Mode::FullArt);
            assert!(illustration.image.is_none());

            assert_eq!(LogTester::len(), 2);
            assert!(LogTester::contains(
                Level::Warn,
                "The zoom has been updated to 111"
            ));
            assert!(LogTester::contains(
                Level::Warn,
                "No picture was provided, the default one will be used"
            ));
        }

        static DEFAULT_TEMPLATE: Template = Template {
            orientation: Orientation::Vertical,
            main_color: Color::Purple,
            luck_and_charm_strip_color: Color::Pink,
            border_color: Color::Orange,
            image_background_color: Color::Arrancar,
            characteristic_color: Color::Turquoise,
            characteristic_strip_color: Color::MonsterYellow,
            characteristic_name_color: Color::HumanBrown,
            pictogram_number_color: Color::HumanPurple,
        };

        #[test]
        fn test_main_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::default(),
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Purple);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The main color is not set, using default value : Purple"
            ));
        }

        #[test]
        fn test_close_main_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Custom(0, 0, 0, 0),
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Transparent);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The main color Custom(0, 0, 0, 0) was too close to Transparent, replacing"
            ));
        }

        #[test]
        fn test_luck_and_charm_strip_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Black,
                luck_and_charm_strip_color: Color::default(),
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Black);
            assert_eq!(template.luck_and_charm_strip_color, Color::Blue);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the luck and the charm strip is not set, using the characteristic strip value : Blue"));
        }

        #[test]
        fn test_close_luck_and_charm_strip_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Black,
                luck_and_charm_strip_color: Color::Custom(0, 0, 0, 0),
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Black);
            assert_eq!(template.luck_and_charm_strip_color, Color::Transparent);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the luck and the charm strip Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_border_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::White,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::default(),
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::White);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::Orange);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the illustration's border is not set, using default value : Orange"
            ));
        }

        #[test]
        fn test_close_border_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::White,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::Custom(0, 0, 0, 0),
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::White);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::Transparent);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the illustration's border Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_image_background_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Red,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::default(),
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Red);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Arrancar);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the image background is not set, using default value : Arranc"
            ));
        }

        #[test]
        fn test_close_image_background_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Red,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Custom(0, 0, 0, 0),
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Red);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Transparent);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the image background Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_characteristic_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Green,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::default(),
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Green);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Turquoise);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(
                Level::Warn,
                "The color of the Characteristics band is not set, using default value : Turquoise"
            ));
        }

        #[test]
        fn test_close_characteristic_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Green,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Custom(0, 0, 0, 0),
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Green);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Transparent);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the Characteristics band Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_characteristic_strip_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Blue,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::default(),
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Blue);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::MonsterYellow);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the Characteristics strip is not set, using default value : MonsterYellow"));
        }

        #[test]
        fn test_strip_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Black,
                luck_and_charm_strip_color: Color::default(),
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::default(),
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Black);
            assert_eq!(template.luck_and_charm_strip_color, Color::MonsterYellow);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::MonsterYellow);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 2);
            assert!(LogTester::contains(Level::Warn, "The color of the Characteristics strip is not set, using default value : MonsterYellow"));
            assert!(LogTester::contains(Level::Warn, "The color of the luck and the charm strip is not set, using the characteristic strip value : MonsterYellow"));
        }

        #[test]
        fn test_close_characteristic_strip_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Blue,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Custom(0, 0, 0, 0),
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Lime,
            };

            assert!(template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Blue);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Transparent);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 1);
            assert!(LogTester::contains(Level::Warn, "The color of the Characteristics strip Custom(0, 0, 0, 0) was too close to Transparent, replacing"));
        }

        #[test]
        fn test_characteristic_name_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Yellow,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::default(),
                pictogram_number_color: Color::Lime,
            };

            assert!(!template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Yellow);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::default());
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 0);
        }

        #[test]
        fn test_close_characteristic_name_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Yellow,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Custom(0, 0, 0, 0),
                pictogram_number_color: Color::Lime,
            };

            assert!(!template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Yellow);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(
                template.characteristic_name_color,
                Color::Custom(0, 0, 0, 0)
            );
            assert_eq!(template.pictogram_number_color, Color::Lime);

            assert_eq!(LogTester::len(), 0);
        }

        #[test]
        fn test_pictogram_number_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Lime,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::default(),
            };

            assert!(!template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Lime);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::default());

            assert_eq!(LogTester::len(), 0);
        }

        #[test]
        fn test_close_pictogram_number_color_template() {
            LogTester::start();
            let mut template = Template {
                orientation: Orientation::Horizontal,
                main_color: Color::Lime,
                luck_and_charm_strip_color: Color::Black,
                border_color: Color::White,
                image_background_color: Color::Red,
                characteristic_color: Color::Green,
                characteristic_strip_color: Color::Blue,
                characteristic_name_color: Color::Yellow,
                pictogram_number_color: Color::Custom(0, 0, 0, 0),
            };

            assert!(!template.update(&DEFAULT_TEMPLATE));
            assert_eq!(template.orientation, Orientation::Horizontal);
            assert_eq!(template.main_color, Color::Lime);
            assert_eq!(template.luck_and_charm_strip_color, Color::Black);
            assert_eq!(template.border_color, Color::White);
            assert_eq!(template.image_background_color, Color::Red);
            assert_eq!(template.characteristic_color, Color::Green);
            assert_eq!(template.characteristic_strip_color, Color::Blue);
            assert_eq!(template.characteristic_name_color, Color::Yellow);
            assert_eq!(template.pictogram_number_color, Color::Custom(0, 0, 0, 0));

            assert_eq!(LogTester::len(), 0);
        }
    }
}
