// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the [Pictogram](crate::card::pictogram::Pictogram) part of the GUI of the Baston Editor.

use crate::card::pictogram::{Pictogram, Reference, ReferenceType};
use crate::error::LogError;

use super::card_part_ui::pictogram_ui::Message;
use super::{GetUI, Gui, GuiMessage, TabId, PADDING, SPACING};

use std::collections::{hash_map::Entry, HashMap};

use iced::widget::{column, row, Button, Column, Text, TextInput};
use iced::{Alignment, Element, Length};
use iced_aw::menu::{Item, Menu, MenuBar};
use iced_aw::{Badge, TypedInput};
use iced_fonts::{
    bootstrap::{icon_to_string, Bootstrap},
    BOOTSTRAP_FONT,
};
use itertools::Itertools;

/// Creates the tab for the [Pictogram](crate::card::pictogram::Pictogram) modification
pub fn pictogram_tab<'a, Theme, Renderer>(gui: &Gui) -> Column<'a, GuiMessage, Theme, Renderer>
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::container::Catalog
        + iced::widget::text::Catalog
        + iced::widget::pick_list::Catalog
        + iced_aw::card::Catalog
        + iced::widget::text_input::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + iced_aw::style::menu_bar::Catalog
        + iced::widget::button::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
{
    if gui.current_tab != TabId::PictogramTab {
        return column![];
    }
    let card = gui
        .card
        .read()
        .critical("Unable to get the read lock on the card in order to create the pictogram tab");
    let field_ref: Vec<&str> = card
        .pictograms
        .iter()
        .filter_map(|p| {
            if p.reference.r#type == ReferenceType::Field {
                Some(p.reference.value.as_str())
            } else {
                None
            }
        })
        .unique()
        .collect();

    let mut pictogram_field: HashMap<&str, Vec<(usize, &Pictogram)>> = HashMap::new();
    for (index, pictogram) in card.pictograms.iter().enumerate() {
        if pictogram.reference.r#type == ReferenceType::Field {
            match pictogram_field.entry(pictogram.reference.value.as_str()) {
                Entry::Occupied(mut e) => {
                    e.get_mut().push((index, pictogram));
                }
                Entry::Vacant(e) => {
                    e.insert(vec![(index, pictogram)]);
                }
            }
        }
    }

    let mut content = column![];

    for field in field_ref.iter() {
        let field = (*field).to_owned();
        let reference = Reference {
            r#type: ReferenceType::Field,
            value: field.clone(),
        };
        content = content.push(
            Element::from(
                pictogram_field
                    .get(field.as_str())
                    .critical("This element should be there")
                    .get_ui(
                        TextInput::new("Reference", &field)
                            .on_input(move |value| Message::ChangeRefName(field.clone(), value)),
                        (&card, reference),
                    ),
            )
            .map(GuiMessage::Pictogram),
        );
    }

    if field_ref.is_empty() {
        content = content.push(
            Button::new(
                Text::new(icon_to_string(Bootstrap::PlusSquareDotted)).font(BOOTSTRAP_FONT),
            )
            .on_press(GuiMessage::Pictogram(Message::New(Reference::default()))),
        )
    }

    content
        .spacing(SPACING)
        .padding(PADDING)
        .align_x(Alignment::Center)
        .width(Length::Fill)
}

pub fn reference_menu<'a, Theme, Renderer>(
    card: &crate::card::Card,
) -> MenuBar<'a, Reference, Theme, Renderer>
where
    Theme: iced_aw::widgets::menu::Catalog
        + iced::widget::button::Catalog
        + iced::widget::text::Catalog
        + iced_aw::style::menu_bar::Catalog
        + iced::widget::text_input::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
    Renderer: iced::advanced::text::Renderer<Font = iced::Font> + 'a,
{
    let button = |label: &str, type_: ReferenceType| {
        Item::new(
            Button::new(Text::new(label.to_owned()))
                .on_press(Reference {
                    r#type: type_,
                    value: label.to_owned(),
                })
                .width(Length::Fill),
        )
    };

    let menu_button = |label: &str| {
        Badge::new(
            row![
                Text::new(label.to_owned())
                    .width(Length::Fill)
                    .align_y(iced::alignment::Vertical::Center),
                Text::new(iced_fonts::bootstrap::icon_to_string(
                    Bootstrap::CaretRightFill
                ))
                .font(BOOTSTRAP_FONT)
                .width(Length::Shrink)
                .align_y(iced::alignment::Vertical::Center),
            ]
            .align_y(iced::Alignment::Center),
        )
        .width(Length::Fill)
    };

    let menu = |name: &str, content| {
        Item::with_menu(
            menu_button(name),
            Menu::new(content).max_width(180.0).offset(0.0).spacing(5.0),
        )
    };

    let powers: Vec<_> = card
        .powers
        .iter()
        .map(|power| power.value.as_str())
        .map(|label| button(label, ReferenceType::Power))
        .collect();
    let mut field: Vec<_> = card
        .pictograms
        .iter()
        .filter_map(|pictogram| {
            if pictogram.reference.r#type == ReferenceType::Field {
                Some(pictogram.reference.value.as_str())
            } else {
                None
            }
        })
        .unique()
        .map(|label| button(label, ReferenceType::Field))
        .collect();
    field.push(Item::new(
        TypedInput::new("Other", &"".to_owned())
            .on_submit(move |value| Reference {
                r#type: ReferenceType::Field,
                value: value.critical("A string is always a string"),
            })
            .width(Length::Fill),
    ));

    let no_powers = powers.is_empty();
    let powers = menu("Powers", powers);
    let field = menu("Field", field);

    let menus = if no_powers {
        vec![field]
    } else {
        vec![powers, field]
    };

    let menu = Item::with_menu(
        Badge::new(
            row![
                Text::new("Change Reference")
                    .width(Length::Fill)
                    .align_y(iced::alignment::Vertical::Center),
                Text::new(iced_fonts::bootstrap::icon_to_string(
                    Bootstrap::CaretDownFill
                ))
                .font(BOOTSTRAP_FONT)
                .width(Length::Shrink)
                .align_y(iced::alignment::Vertical::Center),
            ]
            .align_y(iced::Alignment::Center),
        )
        .width(Length::Shrink),
        Menu::new(menus).max_width(180.0).offset(15.0).spacing(5.0),
    );

    MenuBar::new(vec![menu])
}

#[cfg(test)]
mod test_module_pictogram_tab {
    use super::pictogram_tab;

    use crate::card::Card;
    use crate::gui::{Gui, TabId};

    use strum::IntoEnumIterator;

    #[test]
    fn test_pictogram_tab() {
        let mut card = Card::new();
        for pictograms in [vec![], vec![crate::card::pictogram::Pictogram::default()]] {
            card.pictograms = pictograms;
            let mut gui = Gui::new(card.clone());
            for tab in TabId::iter() {
                gui.current_tab = tab;
                pictogram_tab::<iced::Theme, iced::Renderer>(&gui);
            }
        }
    }
}
