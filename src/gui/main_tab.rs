// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the main part of the GUI of the Baston Editor.

use super::{GetUI, Gui, GuiMessage, TabId, PADDING, SPACING};
use crate::card::fields::{BaseField, CharacteristicsField, FieldType};

use iced::widget::{column, row, Row};
use iced::{Element, Length};

/// Creates the tab with the UI for most of the modification
pub fn main_tab<'a, Theme, Renderer>(gui: &Gui) -> Row<'a, GuiMessage, Theme, Renderer>
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::container::Catalog
        + iced::widget::button::Catalog
        + iced::widget::checkbox::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + iced::widget::text::Catalog
        + 'a,
{
    if gui.current_tab != TabId::MainTab {
        return row![];
    }
    let card = gui
        .card
        .read()
        .expect("Error while getting the read lock on the card");
    let name = Element::from(
        card.name
            .get_ui("Name", &BaseField::default(FieldType::Name)),
    )
    .map(GuiMessage::Name);
    let title = Element::from(
        card.title
            .get_ui("Title", &BaseField::default(FieldType::Title)),
    )
    .map(GuiMessage::Title);
    let r#type = Element::from(
        card.r#type
            .get_ui("Type", &BaseField::default(FieldType::Type)),
    )
    .map(GuiMessage::Type);
    let characteristics = Element::from(
        card.characteristics
            .get_ui("Characteristics", &CharacteristicsField::default()),
    )
    .map(GuiMessage::Characteristics);
    let template = Element::from(card.template.get_ui("Template", ())).map(GuiMessage::Template);
    let illustration =
        Element::from(card.illustration.get_ui("Illustration", ())).map(GuiMessage::Illustration);

    let extra_deck =
        Element::from(card.extra_deck.get_ui("Extra Deck", ())).map(GuiMessage::ExtraDeck);
    let save = Element::from(super::card_part_ui::save_ui::save_ui()).map(GuiMessage::Save);
    let faction = Element::from(card.faction.get_ui("Faction", ())).map(GuiMessage::Faction);

    let gender = Element::from(card.gender.get_ui("Gender", ())).map(GuiMessage::Gender);
    let era = Element::from(card.era.get_ui("Era", ())).map(GuiMessage::Era);
    let archetype =
        Element::from(card.archetype.get_ui("Archetype", ())).map(GuiMessage::Archetype);

    let main = row![
        column![name, template, extra_deck, save]
            .spacing(SPACING)
            .height(Length::Shrink),
        column![r#type, characteristics, faction]
            .spacing(SPACING)
            .height(Length::Shrink),
        column![title, gender, archetype, era, illustration]
            .spacing(SPACING)
            .height(Length::Shrink),
    ]
    .spacing(SPACING)
    .padding(PADDING)
    .height(Length::Shrink);

    main
}

#[cfg(test)]
mod test_module_main_tab {
    use super::main_tab;

    use crate::card::Card;
    use crate::gui::{Gui, TabId};

    use strum::IntoEnumIterator;

    #[test]
    fn test_main_tab() {
        let mut gui = Gui::new(Card::new());
        for tab in TabId::iter() {
            gui.current_tab = tab;
            main_tab::<iced::Theme, iced::Renderer>(&gui);
        }
    }
}
