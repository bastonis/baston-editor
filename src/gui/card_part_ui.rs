// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the GUI of part of a [Card](crate::card::Card).

pub mod characteristics_ui;
pub mod fields_ui;
pub mod illustration_ui;
pub mod logo_field_ui;
pub mod logo_ui;
pub mod pictogram_ui;
pub mod power_ui;
pub mod save_ui;
pub mod symbol_ui;
pub mod template_ui;
