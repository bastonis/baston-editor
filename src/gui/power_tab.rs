// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the power part of the GUI of the Baston Editor.

use super::card_part_ui::power_ui::Message;
use super::{GetUI, Gui, GuiMessage, TabId, PADDING, SPACING};
use crate::card::pictogram::{Pictogram, ReferenceType};
use crate::error::LogError;

use std::collections::HashMap;

use iced::widget::{column, Button, Column, Row, Text};
use iced::{Alignment, Element, Length};
use iced_fonts::{
    bootstrap::{icon_to_string, Bootstrap},
    BOOTSTRAP_FONT,
};

/// Creates the tab for the [Power](crate::card::fields::PowerField) modification
pub fn power_tab<'a, Theme, Renderer>(gui: &Gui) -> Column<'a, GuiMessage, Theme, Renderer>
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::container::Catalog
        + iced::widget::text::Catalog
        + iced::widget::pick_list::Catalog
        + iced_aw::card::Catalog
        + iced::widget::text_input::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + iced_aw::style::menu_bar::Catalog
        + iced::widget::button::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
{
    if gui.current_tab != TabId::PowerTab {
        return column![];
    }
    let card = gui
        .card
        .read()
        .critical("Unable to get the read lock on the card in order to create the pictogram tab");

    let mut pictograms: HashMap<&str, Vec<(usize, &Pictogram)>> = card
        .powers
        .iter()
        .map(|p| (p.value.as_str(), vec![]))
        .collect();

    for (index, pictogram) in card.pictograms.iter().enumerate() {
        if pictogram.reference.r#type == ReferenceType::Power {
            pictograms
                .get_mut(pictogram.reference.value.as_str())
                .critical("A pictogram cannot refer to a power that does not exist")
                .push((index, pictogram));
        }
    }

    let mut content = column![];

    for (index, power) in card.powers.iter().enumerate() {
        let pictograms = pictograms
            .get(power.value.as_str())
            .critical("At worst a power is referred by an empty list of pictograms");
        let name = Row::with_children([
            Text::new(power.value.clone()).width(Length::Fill).into(),
            Button::new(Text::new(icon_to_string(Bootstrap::CaretUp)).font(BOOTSTRAP_FONT))
                .on_press_maybe(if index != 0 {
                    Some(Message::Up(index))
                } else {
                    None
                })
                .into(),
            Button::new(Text::new(icon_to_string(Bootstrap::CaretDown)).font(BOOTSTRAP_FONT))
                .on_press_maybe(if index + 1 < card.powers.len() {
                    Some(Message::Down(index))
                } else {
                    None
                })
                .into(),
            Button::new(Text::new(icon_to_string(Bootstrap::Trash)).font(BOOTSTRAP_FONT))
                .on_press(Message::Delete(index))
                .into(),
        ])
        .spacing(SPACING)
        .align_y(Alignment::Center)
        .width(Length::Fill);
        content = content.push(
            Element::from(power.get_ui(name, (index, pictograms, &card))).map(GuiMessage::Power),
        );
    }

    let content = content.push(
        Element::from(
            Button::new(
                Text::new(icon_to_string(Bootstrap::PlusSquareDotted)).font(BOOTSTRAP_FONT),
            )
            .on_press(super::card_part_ui::power_ui::Message::New),
        )
        .map(GuiMessage::Power),
    );

    content
        .spacing(SPACING)
        .padding(PADDING)
        .align_x(Alignment::Center)
        .width(Length::Fill)
}

#[cfg(test)]
mod test_module_power_tab {
    use super::power_tab;

    use crate::card::fields::PowerField;
    use crate::card::Card;
    use crate::gui::{Gui, TabId};

    use strum::IntoEnumIterator;

    #[test]
    fn test_power_tab() {
        let mut card = Card::new();
        for powers in [vec![], vec![PowerField::default()]] {
            card.powers = powers;
            let mut gui = Gui::new(card.clone());
            for tab in TabId::iter() {
                gui.current_tab = tab;
                power_tab::<iced::Theme, iced::Renderer>(&gui);
            }
        }
    }
}
