// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the [SymbolField](crate::card::symbol::SymbolField) part of the GUI of the Baston Editor.

use super::{GetUI, Gui, TabId};

use iced::widget::column;

/// Creates the tab for modification of the list of [SymbolField]
pub fn symbol_tab<'a, Theme, Renderer>(
    gui: &Gui,
) -> iced::Element<'a, crate::gui::GuiMessage, Theme, Renderer>
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::container::Catalog
        + iced::widget::text::Catalog
        + iced::widget::text_input::Catalog
        + iced::widget::button::Catalog
        + iced_aw::widgets::card::Catalog
        + iced::widget::pick_list::Catalog
        + iced_aw::widgets::menu::Catalog
        + iced_aw::style::menu_bar::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
{
    if gui.current_tab != TabId::SymbolTab {
        return column![].into();
    }
    let card = gui.card.read().expect("Failed to read card");

    iced::Element::from(card.symbols.get_ui("", ())).map(super::GuiMessage::Symbol)
}

#[cfg(test)]
mod test_module_symbol_tab {
    use super::symbol_tab;

    use crate::card::symbol::SymbolField;
    use crate::card::Card;
    use crate::gui::{Gui, TabId};

    use strum::IntoEnumIterator;

    #[test]
    fn test_symbol_tab() {
        let mut card = Card::new();
        for symbols in [vec![], vec![SymbolField::default()]] {
            card.symbols = symbols;
            let mut gui = Gui::new(card.clone());
            for tab in TabId::iter() {
                gui.current_tab = tab;
                symbol_tab::<iced::Theme, iced::Renderer>(&gui);
            }
        }
    }
}
