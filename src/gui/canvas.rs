// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for [Canvas], a widget representing a SVG render of a [Card]

use crate::card::Card;
use crate::error::{Error, LogError};
use crate::render::Point;

use std::sync::{Arc, RwLock};

use iced::advanced::graphics::core::{keyboard, SmolStr};
use iced::advanced::renderer;
use iced::advanced::renderer::Style;
use iced::advanced::widget::{tree::State, Tree};
use iced::advanced::Clipboard;
use iced::advanced::Layout;
use iced::advanced::Shell;
use iced::advanced::Widget;
use iced::event::{Event, Status};
use iced::mouse::{self, Cursor, ScrollDelta};
use iced::widget::svg::Handle;
use iced::widget::Svg;
use iced::{Element, Length, Rectangle, Size};

/// Widget representing a [Card] rendering
pub struct Canvas<'a> {
    /// The [Card] to render
    card: Arc<RwLock<Card>>,
    /// Some cache to not redraw the card when nothing has changed
    cache: Option<Svg<'a>>,
}

impl Canvas<'_> {
    /// Creates a new [Canvas] from the given [Card]
    pub fn new(card: Arc<RwLock<Card>>) -> Result<Self, Error> {
        let mut new = Self { card, cache: None };

        new.update_cache()?;
        Ok(new)
    }

    /// Update the cache
    ///
    /// Recreate the [Svg] widget embedded in this one
    fn update_cache(&mut self) -> Result<(), Error> {
        let card = self
            .card
            .read()
            .critical("Error while trying to read the card");
        let mut render = card.render(true)?;
        render.add_overlay(175);
        let string: String = render.try_into()?;
        self.cache = Some(
            Svg::new(Handle::from_memory(std::borrow::Cow::Owned(
                string.as_bytes().to_vec(),
            )))
            .width(Length::Shrink)
            .height(Length::Shrink),
        );
        Ok(())
    }
}

impl<'a, Message, Theme, Renderer> Widget<Message, Theme, Renderer> for Canvas<'a>
where
    Renderer: renderer::Renderer + iced::advanced::svg::Renderer,
    Theme: iced::widget::svg::Catalog,
    Svg<'a>: Widget<Message, Theme, Renderer>,
{
    fn size(&self) -> Size<Length> {
        let svg: &Svg = self
            .cache
            .as_ref()
            .critical("The cache should have been updated");
        <Svg as Widget<Message, Theme, Renderer>>::size(svg)
    }

    fn layout(
        &self,
        tree: &mut Tree,
        renderer: &Renderer,
        limits: &iced::advanced::layout::Limits,
    ) -> iced::advanced::layout::Node {
        let svg: &Svg = self
            .cache
            .as_ref()
            .critical("The cache should have been updated");
        <Svg as Widget<Message, Theme, Renderer>>::layout(svg, tree, renderer, limits)
    }

    fn draw(
        &self,
        tree: &Tree,
        renderer: &mut Renderer,
        theme: &Theme,
        style: &Style,
        layout: Layout<'_>,
        cursor: Cursor,
        viewport: &Rectangle,
    ) {
        let svg: &Svg = self
            .cache
            .as_ref()
            .critical("The cache should have been updated");
        <Svg as Widget<Message, Theme, Renderer>>::draw(
            svg, tree, renderer, theme, style, layout, cursor, viewport,
        )
    }

    fn on_event(
        &mut self,
        state: &mut Tree,
        event: Event,
        layout: Layout<'_>,
        cursor: Cursor,
        _renderer: &Renderer,
        _clipboard: &mut dyn Clipboard,
        shell: &mut Shell<'_, Message>,
        _bounds: &Rectangle,
    ) -> Status {
        let bounds = layout.bounds();
        let Some(cursor_position) = cursor.position_in(bounds) else {
            return Status::Ignored;
        };

        let cursor_position: Point = cursor_position.into();
        let bounds: Point = bounds.into();

        match event {
            Event::Mouse(mouse_event) => match mouse_event {
                mouse::Event::ButtonPressed(mouse::Button::Left) => {
                    state.state = State::Some(Box::new(cursor_position));
                    Status::Captured
                }
                mouse::Event::ButtonReleased(mouse::Button::Left) => {
                    state.state = State::None;
                    Status::Captured
                }
                mouse::Event::CursorMoved { .. } => {
                    if let State::Some(old_position) = &state.state {
                        if let Some(old_position) = old_position.downcast_ref::<Point>() {
                            let old_position: Point = *old_position;
                            let diff = cursor_position - old_position;
                            if diff.x.abs() >= 2. || diff.y.abs() >= 2. {
                                state.state = State::Some(Box::new(cursor_position));
                                let mut card = self
                                    .card
                                    .write()
                                    .critical("Error while trying to modify the card");
                                card.illustration.move_image(diff / bounds);
                                drop(card);
                                self.update_cache()
                                    .critical("Error while updating the canvas cache");
                                shell.invalidate_widgets()
                            }
                            Status::Captured
                        } else {
                            Status::Ignored
                        }
                    } else {
                        Status::Ignored
                    }
                }
                mouse::Event::WheelScrolled { delta } => match delta {
                    ScrollDelta::Lines { x: _, y } | ScrollDelta::Pixels { x: _, y }
                        if y.abs() >= 1. =>
                    {
                        let mut card = self
                            .card
                            .write()
                            .critical("Error while trying to modify the card");
                        if y < 0. {
                            card.illustration.zoom_out(Some(cursor_position / bounds));
                        } else {
                            card.illustration.zoom_in(Some(cursor_position / bounds));
                        }
                        drop(card);
                        self.update_cache()
                            .critical("Error while updating the canvas cache");
                        shell.invalidate_widgets();
                        Status::Captured
                    }
                    _ => Status::Ignored,
                },
                _ => Status::Ignored,
            },
            Event::Keyboard(keyboard_event) => match keyboard_event {
                keyboard::Event::KeyPressed { key, .. }
                    if key == keyboard::Key::Character(SmolStr::new_static("+"))
                        || key == keyboard::Key::Named(keyboard::key::Named::ZoomIn) =>
                {
                    let mut card = self
                        .card
                        .write()
                        .critical("Error while trying to modify the card");
                    card.illustration.zoom_in(None);
                    drop(card);
                    self.update_cache()
                        .critical("Error while updating the canvas cache");
                    shell.invalidate_widgets();
                    Status::Captured
                }
                keyboard::Event::KeyPressed { key, .. }
                    if key == keyboard::Key::Character(SmolStr::new_static("-"))
                        || key == keyboard::Key::Named(keyboard::key::Named::ZoomOut) =>
                {
                    let mut card = self
                        .card
                        .write()
                        .critical("Error while trying to modify the card");
                    card.illustration.zoom_out(None);
                    drop(card);
                    self.update_cache()
                        .critical("Error while updating the canvas cache");
                    shell.invalidate_widgets();
                    Status::Captured
                }
                keyboard::Event::KeyPressed {
                    key: keyboard::Key::Named(keyboard::key::Named::ArrowUp),
                    ..
                } => {
                    let mut card = self
                        .card
                        .write()
                        .critical("Error while trying to modify the card");
                    card.illustration.position.y -= 1.;
                    drop(card);
                    self.update_cache()
                        .critical("Error while updating the canvas cache");
                    shell.invalidate_widgets();
                    Status::Captured
                }
                keyboard::Event::KeyPressed {
                    key: keyboard::Key::Named(keyboard::key::Named::ArrowDown),
                    ..
                } => {
                    let mut card = self
                        .card
                        .write()
                        .critical("Error while trying to modify the card");
                    card.illustration.position.y += 1.;
                    drop(card);
                    self.update_cache()
                        .critical("Error while updating the canvas cache");
                    shell.invalidate_widgets();
                    Status::Captured
                }
                keyboard::Event::KeyPressed {
                    key: keyboard::Key::Named(keyboard::key::Named::ArrowLeft),
                    ..
                } => {
                    let mut card = self
                        .card
                        .write()
                        .critical("Error while trying to modify the card");
                    card.illustration.position.x -= 1.;
                    drop(card);
                    self.update_cache()
                        .critical("Error while updating the canvas cache");
                    shell.invalidate_widgets();
                    Status::Captured
                }
                keyboard::Event::KeyPressed {
                    key: keyboard::Key::Named(keyboard::key::Named::ArrowRight),
                    ..
                } => {
                    let mut card = self
                        .card
                        .write()
                        .critical("Error while trying to modify the card");
                    card.illustration.position.x += 1.;
                    drop(card);
                    self.update_cache()
                        .critical("Error while updating the canvas cache");
                    shell.invalidate_widgets();
                    Status::Captured
                }
                _ => Status::Ignored,
            },
            _ => Status::Ignored,
        }
    }
}

impl<'a, Message, Theme, Renderer> From<Canvas<'a>> for Element<'a, Message, Theme, Renderer>
where
    Renderer: renderer::Renderer + iced::advanced::svg::Renderer,
    Theme: iced::widget::svg::Catalog,
    Svg<'a>: Widget<Message, Theme, Renderer>,
{
    fn from(canvas: Canvas<'a>) -> Self {
        Self::new(canvas)
    }
}

#[cfg(test)]
mod test_module_canvas {
    use super::Canvas;

    use crate::card::Card;

    use std::ops::Deref;
    use std::sync::{Arc, RwLock};

    use iced::advanced::Widget;
    use iced::{Element, Renderer, Theme};

    #[test]
    fn test_new() {
        let card = Arc::new(RwLock::new(Card::new()));
        let canvas = Canvas::new(card.clone()).expect("A test should never fail");
        assert!(canvas.cache.is_some());
        assert_eq!(
            card.read().expect("A test should never fail").deref(),
            canvas
                .card
                .read()
                .expect("A test should never fail")
                .deref()
        );
    }

    #[test]
    fn test_size() {
        let canvas =
            Canvas::new(Arc::new(RwLock::new(Card::new()))).expect("A test should never fail");

        <Canvas as Widget<(), Theme, Renderer>>::size(&canvas);
    }

    #[test]
    fn test_element() {
        let canvas =
            Canvas::new(Arc::new(RwLock::new(Card::new()))).expect("A test should never fail");

        let _elt: Element<(), Theme, Renderer> = canvas.into();
    }
}
