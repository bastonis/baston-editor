// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the GUI for the [CharacteristicsField]

use crate::card::fields::{CharacteristicName, CharacteristicsField};
use crate::color::Color;
use crate::gui::GetUI;
use crate::types::CharacteristicValue;

use iced::advanced::renderer;
use iced::widget::{column, row, Button, Checkbox, Column, Text};
use iced::{Alignment, Length};
use iced_aw::widgets::{typed_input::TypedInput, Card, NumberInput};
use strum::IntoEnumIterator;

/// The messages that can be sent to modify the [CharacteristicsField]
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    /// Message to set the value of the [CharacteristicName]
    Value(CharacteristicName, CharacteristicValue),
    /// Message to set the color of the characteristics
    Color(Color),
    /// Message to set the size of the characteristics
    Size(u8),
    /// Message to set the color of the name of the characteristics
    NameColor(Color),
    /// Message to set whether or not to use compact fractions
    Fraction(bool),
}

impl<'a, Theme, Renderer, Name>
    GetUI<'a, Card<'a, Message, Theme, Renderer>, Message, Theme, Renderer, &Self, Name>
    for CharacteristicsField
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::checkbox::Catalog
        + iced::widget::button::Catalog
        + iced::widget::text_input::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(&self, name: Name, default: &Self) -> Card<'a, Message, Theme, Renderer> {
        let mut grid = Column::new().align_x(Alignment::Center).spacing(3);
        for characteristic in CharacteristicName::iter() {
            let name = Text::new(format!("{characteristic}:")).width(Length::FillPortion(1));

            let id = iced::widget::text_input::Id::unique();
            // println!("{id:?}");
            let input = TypedInput::new(&characteristic.to_string(), &self.get(&characteristic))
                .on_input(move |value: CharacteristicValue| Message::Value(characteristic, value))
                .width(Length::FillPortion(1))
                .id(id);

            grid = grid.push(row![name, input].align_y(Alignment::Center).spacing(3));
        }

        let color = Color::pick_list(self.color, Message::Color).width(Length::FillPortion(1));
        let grid = grid.push(
            row![Text::new("Color:").width(Length::FillPortion(1)), color]
                .align_y(Alignment::Center)
                .spacing(3),
        );

        let name_color =
            Color::pick_list(self.name_color, Message::NameColor).width(Length::FillPortion(1));
        let grid = grid.push(
            row![
                Text::new("Name Color:").width(Length::FillPortion(1)),
                name_color
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let size = NumberInput::new(&self.size, 1..=u8::MAX, Message::Size)
            .width(Length::Fill)
            .step(1);

        Card::new(
            name,
            column![
                grid,
                row![
                    "Size",
                    size.width(Length::Fill),
                    Button::new("Reset")
                        .on_press(Message::Size(default.size))
                        .width(Length::Shrink)
                ]
                .align_y(Alignment::Center)
                .spacing(3),
                Checkbox::new("Compact Fraction", self.compact_fraction)
                    .on_toggle(Message::Fraction),
            ]
            .align_x(Alignment::Center)
            .spacing(3),
        )
    }
}

impl crate::gui::Message<CharacteristicsField, ()> for Message {
    fn apply(&self, field: &mut CharacteristicsField) {
        match self {
            Self::Value(characteristic, value) => field.set(characteristic, *value),
            Self::Size(size) if *size > 0 => field.size = *size,
            Self::Size(_) => (),
            Self::Color(color) => field.color = color.process_custom(field.color),
            Self::NameColor(color) => field.name_color = color.process_custom(field.name_color),
            Self::Fraction(fraction) => field.compact_fraction = *fraction,
        }
    }
}

#[cfg(test)]
mod test_module_characteristics_ui {
    use super::Message;

    use crate::card::fields::{CharacteristicName, CharacteristicsField};
    use crate::color::Color;
    use crate::gui::{GetUI, Message as MessageTrait};

    use strum::IntoEnumIterator;

    #[test]
    fn test_characteristics_ui() {
        let field = CharacteristicsField::default();
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> =
            field.get_ui("Test", &field);
    }

    #[test]
    fn test_apply() {
        let mut default = CharacteristicsField::default();
        default.size = 1;
        default.color = Color::Purple;
        default.name_color = Color::Pink;
        default.compact_fraction = false;
        let mut i = 0;
        for characteristic in CharacteristicName::iter() {
            default.set(&characteristic, i.into());
            i += 1;
        }

        let default = default;
        let mut field = default.clone();

        for characteristic in CharacteristicName::iter() {
            let message = Message::Value(characteristic, 20.into());
            message.apply(&mut field);

            assert_eq!(field.get(&characteristic), 20.into());
            for charact in CharacteristicName::iter() {
                if charact != characteristic {
                    assert_eq!(field.get(&charact), default.get(&charact));
                }
            }
            assert_eq!(field.size, default.size);
            assert_eq!(field.color, default.color);
            assert_eq!(field.name_color, default.name_color);
            assert_eq!(field.compact_fraction, default.compact_fraction);

            field.set(&characteristic, default.get(&characteristic));
        }

        Message::Size(20).apply(&mut field);

        assert_eq!(field.size, 20);
        assert_eq!(field.color, default.color);
        assert_eq!(field.name_color, default.name_color);
        assert_eq!(field.compact_fraction, default.compact_fraction);
        for characteristic in CharacteristicName::iter() {
            assert_eq!(field.get(&characteristic), default.get(&characteristic));
        }

        field.size = default.size;

        Message::Size(0).apply(&mut field);
        assert_eq!(field, default);

        Message::Color(Color::Red).apply(&mut field);

        assert_eq!(field.size, default.size);
        assert_eq!(field.color, Color::Red);
        assert_eq!(field.name_color, default.name_color);
        assert_eq!(field.compact_fraction, default.compact_fraction);
        for characteristic in CharacteristicName::iter() {
            assert_eq!(field.get(&characteristic), default.get(&characteristic));
        }

        field.color = default.color;

        Message::NameColor(Color::Blue).apply(&mut field);

        assert_eq!(field.size, default.size);
        assert_eq!(field.color, default.color);
        assert_eq!(field.name_color, Color::Blue);
        assert_eq!(field.compact_fraction, default.compact_fraction);
        for characteristic in CharacteristicName::iter() {
            assert_eq!(field.get(&characteristic), default.get(&characteristic));
        }

        field.name_color = default.name_color;

        Message::Fraction(true).apply(&mut field);

        assert_eq!(field.size, default.size);
        assert_eq!(field.color, default.color);
        assert_eq!(field.name_color, default.name_color);
        assert_eq!(field.compact_fraction, true);
        for characteristic in CharacteristicName::iter() {
            assert_eq!(field.get(&characteristic), default.get(&characteristic));
        }
    }
}
