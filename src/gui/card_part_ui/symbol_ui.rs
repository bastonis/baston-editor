// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of the [SymbolField]

use crate::card::symbol::{Symbol, SymbolField};
use crate::color::Color;
use crate::error::LogError;
use crate::gui::{GetUI, PADDING, SPACING};
use crate::types::CharacteristicValue;

use std::collections::HashMap;

use iced::alignment::Horizontal;
use iced::widget::{row, Button, Column, PickList, Row, Text};
use iced::{alignment, Length};
use iced_aw::widgets::menu::{Item, Menu, MenuBar};
use iced_aw::widgets::{Badge, Card, TypedInput};
use iced_fonts::{
    bootstrap::{icon_to_string, Bootstrap},
    BOOTSTRAP_FONT,
};
use lazy_static::lazy_static;
use serde::Deserialize;
use strum_macros::Display;

lazy_static! {
    /// Classification of Symbols
    static ref SYMBOL_CLASSIFICATION: HashMap<SymbolCategory, Vec<Symbol>> = serde_yaml::from_str(include_str!("../../config/symbol_classification.yml")).critical("The file src/config/symbol_classification.yml is embedded in the code, there should be no error");
}

/// Message for modification of a [SymbolField] or the list of symbols
///
/// The index of the symbol currently modified is passed by the parent message (see [Message](crate::gui::GuiMessage::Symbol)).
#[derive(Debug, Clone, PartialEq)]
pub enum Message {
    /// Moves the symbol up in the list if possible
    Up(usize),
    /// Moves the symbol down in the list if possible
    Down(usize),
    /// Deletes the symbol
    Delete(usize),
    /// Creates a new symbol
    New,
    /// Changes the [Symbol] of the [SymbolField] to the given one
    Symbol(usize, Symbol),
    /// Changes the value
    Value(usize, CharacteristicValue),
    /// Changes the color of the value
    TextColor(usize, Color),
}

/// Possible category of symbols
#[derive(Deserialize, Display, Debug, PartialEq, Eq, Hash)]
pub enum SymbolCategory {
    Defense,
    Degat,
    Elementaire,
    Guerrier,
    Intrinseque,
    Malice,
    Esprit,
    Pouvoir,
    Resurection,
    Univers,
    Zone,
    Autres,
}

impl<'a, Theme, Renderer, Name>
    GetUI<'a, Column<'a, Message, Theme, Renderer>, Message, Theme, Renderer, (), Name>
    for Vec<SymbolField>
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::container::Catalog
        + iced::widget::text::Catalog
        + iced::widget::text_input::Catalog
        + iced::widget::button::Catalog
        + iced_aw::widgets::card::Catalog
        + iced::widget::pick_list::Catalog
        + iced_aw::widgets::menu::Catalog
        + iced_aw::style::menu_bar::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(&self, _name: Name, _: ()) -> Column<'a, Message, Theme, Renderer> {
        let mut content = Vec::new();
        for (index, symbol) in self.iter().enumerate() {
            content.push(
                symbol
                    .get_ui(Text::new(symbol.symbol.to_string()), (index, self.len()))
                    .into(),
            );
        }

        content.push(
            Button::new(
                Text::new(icon_to_string(Bootstrap::PlusSquareDotted)).font(BOOTSTRAP_FONT),
            )
            .on_press(Message::New)
            .into(),
        );

        let main = Column::from_vec(content)
            .spacing(SPACING)
            .padding(PADDING)
            .width(Length::Fill)
            .align_x(Horizontal::Center);
        main
    }
}

impl<'a, Theme, Renderer, Name>
    GetUI<'a, Card<'a, Message, Theme, Renderer>, Message, Theme, Renderer, (usize, usize), Name>
    for SymbolField
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::container::Catalog
        + iced::widget::text::Catalog
        + iced::widget::text_input::Catalog
        + iced::widget::button::Catalog
        + iced::widget::pick_list::Catalog
        + iced_aw::widgets::card::Catalog
        + iced_aw::widgets::menu::Catalog
        + iced_aw::style::menu_bar::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(
        &self,
        name: Name,
        (index, max): (usize, usize),
    ) -> Card<'a, Message, Theme, Renderer> {
        let mut colors = Color::list();
        colors.push(Color::Default);
        let content = Row::new()
            .spacing(SPACING)
            .width(Length::Fill)
            .push(
                iced::Element::from(Symbol::menu(self.symbol).width(Length::Fill))
                    .map(move |symbol| Message::Symbol(index, symbol)),
            )
            .push(
                TypedInput::new("Value", &self.value)
                    .on_input(move |value| Message::Value(index, value))
                    .width(Length::Fill),
            )
            .push(
                PickList::new(colors, Some(self.text_color), move |color: Color| {
                    Message::TextColor(index, color)
                })
                .width(Length::Fill),
            )
            .push(
                Button::new(Text::new(icon_to_string(Bootstrap::CaretUp)).font(BOOTSTRAP_FONT))
                    .on_press_maybe(if index > 0 {
                        Some(Message::Up(index))
                    } else {
                        None
                    }),
            )
            .push(
                Button::new(Text::new(icon_to_string(Bootstrap::CaretDown)).font(BOOTSTRAP_FONT))
                    .on_press_maybe(if index < max - 1 {
                        Some(Message::Down(index))
                    } else {
                        None
                    }),
            )
            .push(
                Button::new(Text::new(icon_to_string(Bootstrap::Trash)).font(BOOTSTRAP_FONT))
                    .width(Length::Shrink)
                    .on_press(Message::Delete(index)),
            );

        Card::new(name, content)
    }
}

impl crate::gui::Message<Vec<SymbolField>, ()> for Message {
    /// Apply the message on the symbol field specified by the index
    ///
    /// Applying to an index outside the range of the symbols will do nothing
    fn apply(&self, symbols: &mut Vec<SymbolField>) {
        match self {
            Self::Up(index) => {
                if *index > 0 && *index < symbols.len() {
                    symbols.swap(*index, index - 1)
                }
            }
            Self::Down(index) => {
                if *index < symbols.len() - 1 {
                    symbols.swap(*index, index + 1)
                }
            }
            Self::Delete(index) => {
                if *index < symbols.len() {
                    symbols.remove(*index);
                }
            }
            Self::New => symbols.push(SymbolField::default()),
            Self::Symbol(index, symbol) => {
                if *index < symbols.len() {
                    symbols[*index].symbol = *symbol
                }
            }
            Self::Value(index, value) => {
                if *index < symbols.len() {
                    symbols[*index].value = *value
                }
            }
            Self::TextColor(index, color) => {
                if *index < symbols.len() {
                    symbols[*index].text_color = if *color == Color::Default {
                        Color::Default
                    } else {
                        color.process_custom(symbols[*index].text_color)
                    }
                }
            }
        }
    }
}

impl Symbol {
    /// Create a menu to choose a symbol from
    pub fn menu<'a, Theme, Renderer>(current: Self) -> MenuBar<'a, Self, Theme, Renderer>
    where
        Theme: iced_aw::widgets::menu::Catalog
            + iced::widget::button::Catalog
            + iced::widget::text::Catalog
            + iced_aw::style::menu_bar::Catalog
            + iced_aw::widget::badge::Catalog
            + 'a,
        Renderer: iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    {
        let button = |symbol: &Self| {
            Item::new(
                Button::new(Text::new(symbol.to_string()))
                    .on_press(*symbol)
                    .width(Length::Fill),
            )
        };
        let menu_button = |label: &str| {
            Badge::new(
                row![
                    Text::new(label.to_owned())
                        .width(Length::Fill)
                        .align_y(alignment::Vertical::Center),
                    Text::new(iced_fonts::bootstrap::icon_to_string(
                        Bootstrap::CaretRightFill
                    ))
                    .font(BOOTSTRAP_FONT)
                    .width(Length::Shrink)
                    .align_y(alignment::Vertical::Center),
                ]
                .align_y(iced::Alignment::Center),
            )
            .width(Length::Fill)
        };
        let menu = |name: &str, content| {
            Item::with_menu(
                menu_button(name),
                Menu::new(content).max_width(180.0).offset(0.0).spacing(5.0),
            )
        };

        let mut menus = Vec::new();

        for (category, symbols) in SYMBOL_CLASSIFICATION.iter() {
            let symbols = symbols.iter().map(button).collect();
            let cat = menu(&category.to_string(), symbols);
            menus.push(cat);
        }

        let menu = Item::with_menu(
            Badge::new(
                row![
                    Text::new(current.to_string())
                        .width(Length::Fill)
                        .align_y(alignment::Vertical::Center),
                    Text::new(iced_fonts::bootstrap::icon_to_string(
                        Bootstrap::CaretDownFill
                    ))
                    .font(BOOTSTRAP_FONT)
                    .width(Length::Shrink)
                    .align_y(alignment::Vertical::Center),
                ]
                .align_y(iced::Alignment::Center),
            )
            .width(Length::Shrink),
            Menu::new(menus).max_width(180.0).offset(15.0).spacing(5.0),
        );

        MenuBar::new(vec![menu])
    }
}

#[cfg(test)]
mod test_module_symbol_ui {
    use super::Message;

    use crate::card::symbol::{Symbol, SymbolField};
    use crate::color::Color;
    use crate::gui::{GetUI, Message as MessageTrait};

    use strum::IntoEnumIterator;

    #[test]
    fn test_vec_get_ui() {
        let symbol = SymbolField::default();
        let mut symbols = vec![symbol];
        let _: iced::widget::Column<_, iced::Theme, iced::Renderer> = symbols.get_ui("", ());

        symbols.clear();
        let _: iced::widget::Column<_, iced::Theme, iced::Renderer> = symbols.get_ui("", ());
    }

    #[test]
    fn test_symbol_get_ui() {
        let symbol = SymbolField::default();

        for i in 0..2 {
            let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> =
                symbol.get_ui("", (i, 3));
        }
    }

    #[test]
    fn test_apply() {
        let symbol0 = SymbolField {
            symbol: Symbol::Arc,
            value: 0.into(),
            text_color: Color::White,
            border_color: Color::Blue,
        };

        let symbol1 = SymbolField {
            symbol: Symbol::Epee,
            value: 1.into(),
            text_color: Color::Black,
            border_color: Color::Red,
        };

        let symbol2 = SymbolField {
            symbol: Symbol::Couronne,
            value: 2.into(),
            text_color: Color::Gray,
            border_color: Color::Green,
        };

        let mut symbols = vec![symbol0, symbol1, symbol2];

        Message::Up(0).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1, symbol2]);

        Message::Up(1).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol0, symbol2]);

        Message::Up(2).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol2, symbol0]);

        Message::Up(3).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol2, symbol0]);

        Message::Down(3).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol2, symbol0]);

        Message::Down(2).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol2, symbol0]);

        Message::Down(1).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol0, symbol2]);

        Message::Down(0).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1, symbol2]);

        Message::Delete(0).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol1, symbol2]);
        symbols.insert(0, symbol0);

        Message::Delete(1).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol2]);
        symbols.insert(1, symbol1);

        Message::Delete(2).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1]);
        symbols.insert(2, symbol2);

        Message::Delete(3).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1, symbol2]);

        Message::New.apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![symbol0, symbol1, symbol2, SymbolField::default()]
        );
        symbols.pop();

        Message::Symbol(0, Symbol::Blaster).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                SymbolField {
                    symbol: Symbol::Blaster,
                    ..symbol0
                },
                symbol1,
                symbol2
            ]
        );
        symbols[0] = symbol0;

        Message::Symbol(1, Symbol::Blaster).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                symbol0,
                SymbolField {
                    symbol: Symbol::Blaster,
                    ..symbol1
                },
                symbol2
            ]
        );
        symbols[1] = symbol1;

        Message::Symbol(2, Symbol::Blaster).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                symbol0,
                symbol1,
                SymbolField {
                    symbol: Symbol::Blaster,
                    ..symbol2
                }
            ]
        );
        symbols[2] = symbol2;

        Message::Symbol(3, Symbol::Blaster).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1, symbol2]);

        Message::Value(0, 10.into()).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                SymbolField {
                    value: 10.into(),
                    ..symbol0
                },
                symbol1,
                symbol2
            ]
        );
        symbols[0] = symbol0;

        Message::Value(1, 10.into()).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                symbol0,
                SymbolField {
                    value: 10.into(),
                    ..symbol1
                },
                symbol2
            ]
        );
        symbols[1] = symbol1;

        Message::Value(2, 10.into()).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                symbol0,
                symbol1,
                SymbolField {
                    value: 10.into(),
                    ..symbol2
                }
            ]
        );
        symbols[2] = symbol2;

        Message::Value(3, 10.into()).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1, symbol2]);

        Message::TextColor(0, Color::Red).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                SymbolField {
                    text_color: Color::Red,
                    ..symbol0
                },
                symbol1,
                symbol2
            ]
        );
        symbols[0] = symbol0;

        Message::TextColor(1, Color::Default).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                symbol0,
                SymbolField {
                    text_color: Color::Default,
                    ..symbol1
                },
                symbol2
            ]
        );
        symbols[1] = symbol1;

        Message::TextColor(2, Color::Red).apply(&mut symbols);
        assert_eq!(
            symbols,
            vec![
                symbol0,
                symbol1,
                SymbolField {
                    text_color: Color::Red,
                    ..symbol2
                }
            ]
        );
        symbols[2] = symbol2;

        Message::TextColor(3, Color::Red).apply(&mut symbols);
        assert_eq!(symbols, vec![symbol0, symbol1, symbol2]);
    }

    #[test]
    fn test_symbol_menu() {
        let _ = Symbol::menu::<iced::Theme, iced::Renderer>(Symbol::default());
    }

    #[test]
    fn test_symbol_classification() {
        for symbol in Symbol::iter() {
            let mut contained = false;
            for (_, symbols) in super::SYMBOL_CLASSIFICATION.iter() {
                contained = contained || symbols.contains(&symbol);
            }
            println!("{}", symbol);
            assert!(contained);
        }
    }
}
