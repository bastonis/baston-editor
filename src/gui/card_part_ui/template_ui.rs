// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of the [Template]

use crate::card::illustration::{Orientation, Template};
use crate::color::Color;
use crate::gui::{AsPickList, GetUI};

use iced::advanced::renderer;
use iced::widget::{row, Column, Text};
use iced::{Alignment, Length};
use iced_aw::widgets::Card;

/// Message for the [Template]
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    /// Message sent to change the orientation
    Orientation(Orientation),
    /// Message sent to change the main color
    MainColor(Color),
    /// Message sent to change the border color
    BorderColor(Color),
    /// Message sent to change the color of the strip of the characteristic
    CharacteristicStripColor(Color),
    /// Message sent to change the color of the characteristic band
    CharacteristicBandColor(Color),
    /// Message sent to change the color of the strip of luck and charm
    LuckCharmStripColor(Color),
    /// Message sent to change the color of the background
    ImageBackgroundColor(Color),
}

impl<'a, Theme, Renderer, Name>
    GetUI<'a, Card<'a, Message, Theme, Renderer>, Message, Theme, Renderer, (), Name> for Template
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::container::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(&self, name: Name, _: ()) -> Card<'a, Message, Theme, Renderer> {
        let grid = Column::new().align_x(Alignment::Center).spacing(3);

        let grid = grid.push(
            row![
                Text::new("Orientation:").width(Length::FillPortion(1)),
                Orientation::pick_list(self.orientation, Message::Orientation)
                    .width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let grid = grid.push(
            row![
                Text::new("Main Color:").width(Length::FillPortion(1)),
                Color::pick_list(self.main_color, Message::MainColor).width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let grid = grid.push(
            row![
                Text::new("Border Color:").width(Length::FillPortion(1)),
                Color::pick_list(self.border_color, Message::BorderColor)
                    .width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let grid = grid.push(
            row![
                Text::new("Background Color:").width(Length::FillPortion(1)),
                Color::pick_list(self.image_background_color, Message::ImageBackgroundColor)
                    .width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let grid = grid.push(
            row![
                Text::new("Characteristic Band Color:").width(Length::FillPortion(1)),
                Color::pick_list(self.characteristic_color, Message::CharacteristicBandColor)
                    .width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let grid = grid.push(
            row![
                Text::new("Characteristic Strip Color:").width(Length::FillPortion(1)),
                Color::pick_list(
                    self.characteristic_strip_color,
                    Message::CharacteristicStripColor
                )
                .width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        let grid = grid.push(
            row![
                Text::new("Luck-Charm Color:").width(Length::FillPortion(1)),
                Color::pick_list(
                    self.luck_and_charm_strip_color,
                    Message::LuckCharmStripColor
                )
                .width(Length::FillPortion(1))
            ]
            .align_y(Alignment::Center)
            .spacing(3),
        );

        Card::new(name, grid)
    }
}

impl crate::gui::Message<Template, ()> for Message {
    fn apply(&self, template: &mut Template) {
        match self {
            Self::Orientation(orientation) => template.orientation = *orientation,
            Self::MainColor(color) => {
                template.main_color = color.process_custom(template.main_color)
            }
            Self::BorderColor(color) => {
                template.border_color = color.process_custom(template.border_color)
            }
            Self::ImageBackgroundColor(color) => {
                template.image_background_color =
                    color.process_custom(template.image_background_color)
            }
            Self::CharacteristicBandColor(color) => {
                template.characteristic_color = color.process_custom(template.characteristic_color)
            }
            Self::CharacteristicStripColor(color) => {
                template.characteristic_strip_color =
                    color.process_custom(template.characteristic_strip_color)
            }
            Self::LuckCharmStripColor(color) => {
                template.luck_and_charm_strip_color =
                    color.process_custom(template.luck_and_charm_strip_color)
            }
        }
    }
}

#[cfg(test)]
mod test_module_template_ui {
    use super::Message;

    use crate::card::illustration::{Orientation, Template};
    use crate::color::Color;
    use crate::gui::{GetUI, Message as MessageTrait};

    #[test]
    fn test_get_ui() {
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> =
            Template::default().get_ui("name", ());
    }

    #[test]
    fn test_apply() {
        let default = Template {
            orientation: Orientation::Horizontal,
            main_color: Color::Red,
            border_color: Color::Green,
            image_background_color: Color::Purple,
            characteristic_color: Color::Pink,
            characteristic_strip_color: Color::Blue,
            luck_and_charm_strip_color: Color::Yellow,
            characteristic_name_color: Color::Lime,
            pictogram_number_color: Color::Turquoise,
        };

        let mut template = default.clone();

        Message::Orientation(Orientation::Vertical).apply(&mut template);
        assert_eq!(
            template,
            Template {
                orientation: Orientation::Vertical,
                ..default
            }
        );
        template.orientation = default.orientation;

        Message::MainColor(Color::Orange).apply(&mut template);
        assert_eq!(
            template,
            Template {
                main_color: Color::Orange,
                ..default
            }
        );
        template.main_color = default.main_color;

        Message::BorderColor(Color::Orange).apply(&mut template);
        assert_eq!(
            template,
            Template {
                border_color: Color::Orange,
                ..default
            }
        );
        template.border_color = default.border_color;

        Message::ImageBackgroundColor(Color::Orange).apply(&mut template);
        assert_eq!(
            template,
            Template {
                image_background_color: Color::Orange,
                ..default
            }
        );
        template.image_background_color = default.image_background_color;

        Message::CharacteristicBandColor(Color::Orange).apply(&mut template);
        assert_eq!(
            template,
            Template {
                characteristic_color: Color::Orange,
                ..default
            }
        );
        template.characteristic_color = default.characteristic_color;

        Message::CharacteristicStripColor(Color::Orange).apply(&mut template);
        assert_eq!(
            template,
            Template {
                characteristic_strip_color: Color::Orange,
                ..default
            }
        );
        template.characteristic_strip_color = default.characteristic_strip_color;

        Message::LuckCharmStripColor(Color::Orange).apply(&mut template);
        assert_eq!(
            template,
            Template {
                luck_and_charm_strip_color: Color::Orange,
                ..default
            }
        );
        template.luck_and_charm_strip_color = default.luck_and_charm_strip_color;
    }
}
