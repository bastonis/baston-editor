// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of the [Illustration]

use crate::card::illustration::{Illustration, Mode};
use crate::error::LogError;
use crate::gui::{AsPickList, GetUI};

use iced::advanced::renderer;
use iced::widget::{column, row, Button, Text};
use iced::{Alignment, Length};
use iced_aw::widgets::{Card, NumberInput};

/// The messages that can be sent to modify the [Illustration]
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    /// Message sent to change the mode of the [Illustration]
    Mode(Mode),
    /// Message sent to change the zoom of the [Illustration]
    Zoom(u16),
    /// Message sent to reset the [Illustration], ie revert to default values
    Reset,
    /// Message sent to open a image for the [Illustration]
    NewFile,
}

impl<'a, Theme, Renderer, Name>
    GetUI<'a, Card<'a, Message, Theme, Renderer>, Message, Theme, Renderer, (), Name>
    for Illustration
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::button::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(&self, name: Name, _: ()) -> Card<'a, Message, Theme, Renderer> {
        let zoom = NumberInput::new(&self.zoom, 1..=u16::MAX, Message::Zoom)
            .width(Length::Fill)
            .step(1);

        Card::new(
            name,
            column![
                row!["Zoom:", zoom].align_y(Alignment::Center).spacing(3),
                row![
                    Text::new("Mode:"),
                    Mode::pick_list(self.mode, Message::Mode)
                ]
                .align_y(Alignment::Center)
                .spacing(3),
                Button::new(Text::new("Illustration Reset").width(Length::Fill).center())
                    .on_press(Message::Reset)
                    .width(Length::Fill),
                Button::new(
                    Text::new("Illustration Import")
                        .width(Length::Fill)
                        .center()
                )
                .on_press(Message::NewFile)
                .width(Length::Fill),
            ]
            .align_x(Alignment::Center)
            .spacing(3),
        )
    }
}

/// Opens a file dialog to select an image and load it in the [Illustration]
fn open_illustration(illustration: &mut Illustration) {
    let result = super::save_ui::file_dialog(illustration.path(), None)
        .add_filter("Images", &["jpg", "jpeg", "png", "gif", "bmp", "webp"])
        .pick_file();

    if let Some(filename) = result {
        let _ = illustration
            .load(&filename)
            .error("An error occurred while loading the illustration");
    }
}

impl crate::gui::Message<Illustration, ()> for Message {
    fn apply(&self, illustration: &mut Illustration) {
        match self {
            Self::Mode(mode) => illustration.mode = *mode,
            Self::Zoom(zoom) => illustration.set_zoom(*zoom),
            Self::Reset => illustration.reset(),
            Self::NewFile => open_illustration(illustration),
        }
    }
}

#[cfg(test)]
mod test_module_illustration_ui {
    use super::Message;

    use crate::card::illustration::{Illustration, Mode};
    use crate::gui::{GetUI, Message as MessageTrait};

    #[test]
    fn test_get_ui() {
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> =
            Illustration::default().get_ui("name", ());
    }

    #[test]
    fn test_apply() {
        let mut illustration = Illustration::default();

        Message::Mode(Mode::Classic).apply(&mut illustration);
        assert_eq!(illustration.mode, Mode::Classic);

        Message::Mode(Mode::FullArt).apply(&mut illustration);
        assert_eq!(illustration.mode, Mode::FullArt);

        Message::Zoom(111).apply(&mut illustration);
        assert_eq!(illustration.zoom, 111);

        Message::Zoom(1).apply(&mut illustration);
        assert_eq!(illustration.zoom, 1);

        Message::Zoom(0).apply(&mut illustration);
        assert_eq!(illustration.zoom, 1);

        Message::Reset.apply(&mut illustration);
        let mut reset = Illustration::default();
        reset.reset();
        assert_eq!(illustration, reset);
    }
}
