// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of [Pictogram](crate::card::pictogram::Pictogram)

use crate::card::pictogram::{Effect, Pictogram, Reference, ReferenceType, Type};
use crate::color::Color;
use crate::gui::{AsPickList, GetUI, SPACING};

use iced::advanced::renderer;
use iced::widget::scrollable::{Direction, Scrollbar};
use iced::widget::{Button, Row, Scrollable, Text, TextInput};
use iced::{Alignment, Element, Length};
use iced_aw::widgets::{Card, Grid, GridRow, NumberInput};
use iced_fonts::{
    bootstrap::{icon_to_string, Bootstrap},
    BOOTSTRAP_FONT,
};
use itertools::{Itertools, Position};

/// Message for the [Pictogram]
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    /// Message sent to change the effect
    Effect(usize, Effect),
    /// Message sent to change the type
    Type(usize, Type),
    /// Message sent to change the value
    Value(usize, String),
    /// Message sent to change the number of stars
    Stars(usize, u8),
    /// Message sent to change the color of the text
    TextColor(usize, Color),
    /// Message sent to change the color of the border
    BorderColor(usize, Color),
    /// Message sent to change the reference
    Reference(usize, Reference),
    /// Message sent to change the name of a field reference
    ChangeRefName(String, String),
    /// Message sent to add a new pictogram
    New(Reference),
    /// Message sent to remove a pictogram
    Delete(usize),
    /// Message sent to move a pictogram up
    Up(usize),
    /// Message sent to move a pictogram down
    Down(usize),
}

impl<'a, Theme, Renderer, Name>
    GetUI<
        'a,
        Card<'a, Message, Theme, Renderer>,
        Message,
        Theme,
        Renderer,
        (&crate::card::Card, Reference),
        Name,
    > for Vec<(usize, &Pictogram)>
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::container::Catalog
        + iced::widget::text_input::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + iced::widget::button::Catalog
        + iced_aw::style::menu_bar::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(
        &self,
        name: Name,
        (card, default_ref): (&crate::card::Card, Reference),
    ) -> Card<'a, Message, Theme, Renderer> {
        let content = Row::with_children(self.iter().with_position().map(
            |(position, (index, pictogram)): (Position, &(usize, &Pictogram))| {
                let pictogram: Pictogram = (*pictogram).clone();
                let index = *index;
                let name = Row::with_children([
                    Text::new(format!("Pictogram {}", index + 1)).into(),
                    Button::new(
                        Text::new(icon_to_string(Bootstrap::CaretLeft)).font(BOOTSTRAP_FONT),
                    )
                    .on_press_maybe(
                        if position != Position::First && position != Position::Only {
                            Some(Message::Up(index))
                        } else {
                            None
                        },
                    )
                    .into(),
                    Button::new(Text::new(icon_to_string(Bootstrap::Trash)).font(BOOTSTRAP_FONT))
                        .on_press(Message::Delete(index))
                        .into(),
                    Button::new(
                        Text::new(icon_to_string(Bootstrap::CaretRight)).font(BOOTSTRAP_FONT),
                    )
                    .on_press_maybe(
                        if position != Position::Last && position != Position::Only {
                            Some(Message::Down(index))
                        } else {
                            None
                        },
                    )
                    .into(),
                ])
                .spacing(SPACING)
                .align_y(Alignment::Center);
                Element::from(
                    pictogram.get_ui(
                        name,
                        (
                            index,
                            Element::from(super::super::pictogram_tab::reference_menu(card))
                                .map(move |reference| Message::Reference(index, reference)),
                        ),
                    ),
                )
            },
        ));

        let content = content.push(
            Button::new(
                Text::new(icon_to_string(Bootstrap::PlusSquareDotted)).font(BOOTSTRAP_FONT),
            )
            .on_press(Message::New(default_ref)),
        );

        let scrollbar = Scrollbar::new();
        let content = Scrollable::new(content.align_y(Alignment::Center).spacing(SPACING))
            .direction(Direction::Horizontal(scrollbar))
            .width(Length::Fill)
            .anchor_top()
            .spacing(SPACING);
        Card::new(name, content)
    }
}

impl<'a, Theme, Renderer, Name>
    GetUI<
        'a,
        Card<'a, Message, Theme, Renderer>,
        Message,
        Theme,
        Renderer,
        (usize, Element<'a, Message, Theme, Renderer>),
        Name,
    > for Pictogram
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::container::Catalog
        + iced::widget::text_input::Catalog
        + iced::widget::button::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + iced_aw::style::menu_bar::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(
        &self,
        name: Name,
        (index, reference): (usize, Element<'a, Message, Theme, Renderer>),
    ) -> Card<'a, Message, Theme, Renderer> {
        let effect = Effect::pick_list(self.effect, move |effect: Effect| {
            Message::Effect(index, effect)
        });

        let type_ = Type::pick_list(self.r#type, move |type_: Type| Message::Type(index, type_));

        let value = TextInput::new("Value", &self.value)
            .on_input(move |value| Message::Value(index, value));

        let stars = Row::with_children([
            Element::from(Text::new("Stars: ")),
            Element::from(
                NumberInput::new(&self.stars, 0..=3, move |stars| {
                    Message::Stars(index, stars)
                })
                .width(Length::Fill),
                // TypedInput::new("Stars", &self.stars)
                //     .on_input(move |stars: u8| Message::Stars(index, stars))
                //     .width(Length::Fill),
            ),
        ])
        .align_y(Alignment::Center)
        .spacing(SPACING);

        let value_color = GridRow::with_elements(vec![
            Element::from("Value's color: "),
            Element::from(Color::pick_list(self.text_color, move |color| {
                Message::TextColor(index, color)
            })),
        ]);

        let border_color = GridRow::with_elements(vec![
            Element::from("Border's color: "),
            Element::from(Color::pick_list(self.border, move |color| {
                Message::BorderColor(index, color)
            })),
        ]);

        let colors = Grid::with_rows(vec![value_color, border_color]).spacing(SPACING as f32);

        Card::new(
            name,
            Grid::with_rows(vec![
                GridRow::with_elements(vec![effect.width(Length::Fill)]),
                GridRow::with_elements(vec![type_.width(Length::Fill)]),
                GridRow::with_elements(vec![value.width(Length::Fill)]),
                GridRow::with_elements(vec![stars.width(Length::Fill)]),
                GridRow::with_elements(vec![colors]),
                GridRow::with_elements(vec![reference]),
            ])
            .spacing(SPACING as f32)
            // .padding(PADDING)
            .width(Length::Shrink),
        )
        .width(Length::Shrink)
    }
}

impl crate::gui::Message<crate::card::Card, ()> for Message {
    fn apply(&self, card: &mut crate::card::Card) {
        match self {
            Self::Effect(index, effect) => card.pictograms[*index].effect = *effect,
            Self::Type(index, type_) => card.pictograms[*index].r#type = *type_,
            Self::Value(index, value) => card.pictograms[*index].value = value.clone(),
            Self::Stars(index, stars) => {
                if *stars < 4 {
                    card.pictograms[*index].stars = *stars
                }
            }
            Self::TextColor(index, color) => card.pictograms[*index].text_color = *color,
            Self::BorderColor(index, color) => card.pictograms[*index].border = *color,
            Self::Reference(index, reference) => {
                card.pictograms[*index].reference = reference.clone()
            }
            Self::ChangeRefName(old, new) => {
                for pictogram in &mut card.pictograms {
                    if pictogram.reference.r#type == ReferenceType::Field
                        && pictogram.reference.value == *old
                    {
                        pictogram.reference.value = new.clone();
                    }
                }
            }
            Self::New(reference) => card.add_pictogram(Pictogram {
                reference: reference.clone(),
                ..Pictogram::default()
            }),
            Self::Delete(index) => {
                card.pictograms.remove(*index);
            }
            Self::Up(index) => {
                let mut index = *index;
                while index > 0
                    && card.pictograms[index - 1].reference != card.pictograms[index].reference
                {
                    card.pictograms.swap(index, index - 1);
                    index -= 1;
                }
                if index > 0 {
                    card.pictograms.swap(index, index - 1);
                }
            }

            Self::Down(index) => {
                let mut index = *index;
                while index < card.pictograms.len() - 1
                    && card.pictograms[index + 1].reference != card.pictograms[index].reference
                {
                    card.pictograms.swap(index, index + 1);
                    index += 1;
                }
                if index < card.pictograms.len() - 1 {
                    card.pictograms.swap(index, index + 1)
                }
            }
        }
    }
}
