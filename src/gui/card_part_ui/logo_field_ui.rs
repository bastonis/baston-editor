// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of the [LogoField]

use crate::card::fields::LogoField;
use crate::card::logos::SvgLogo;
use crate::color::Color;
use crate::gui::{AsPickList, GetUI, SPACING};

use iced::advanced::renderer;
use iced::widget::column;
use iced::{Alignment, Length};
use iced_aw::widgets::Card;

/// Message set for modification of [LogoField]
#[derive(Clone, Debug, PartialEq)]
pub enum Message<L> {
    /// Message sent to change the logo
    Logo(L),
    /// Message sent to change the color
    Color(Color),
}

impl<'a, Theme, Renderer, Name, L>
    GetUI<'a, Card<'a, Message<L>, Theme, Renderer>, Message<L>, Theme, Renderer, (), Name>
    for LogoField<L>
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::container::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message<L>, Theme, Renderer>>,
    L: SvgLogo + AsPickList + Copy + 'static,
{
    fn get_ui(&self, name: Name, _: ()) -> Card<'a, Message<L>, Theme, Renderer> {
        Card::new(
            name,
            column![
                L::pick_list(self.logo, Message::Logo).width(Length::Fill),
                Color::pick_list(self.color, Message::Color).width(Length::Fill),
            ]
            .align_x(Alignment::Center)
            .spacing(SPACING),
        )
    }
}

impl<L: SvgLogo + Copy> crate::gui::Message<LogoField<L>, ()> for Message<L> {
    fn apply(&self, logo: &mut LogoField<L>) {
        match self {
            Self::Logo(new_logo) => logo.logo = *new_logo,
            Self::Color(color) => logo.color = color.process_custom(logo.color),
        }
    }
}

#[cfg(test)]
mod test_module_logo_field_ui {
    use super::Message;

    use crate::card::fields::EraField;
    use crate::card::logos::Era;
    use crate::color::Color;
    use crate::gui::{GetUI, Message as MessageTrait};

    #[test]
    fn test_get_ui() {
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> =
            EraField::default().get_ui("name", ());
    }

    #[test]
    fn test_apply() {
        let mut logo = EraField {
            logo: Era::All,
            color: Color::Purple,
        };

        Message::Logo(Era::Future).apply(&mut logo);
        assert_eq!(logo.logo, Era::Future);
        assert_eq!(logo.color, Color::Purple);

        Message::Color(Color::Red).apply(&mut logo);
        assert_eq!(logo.color, Color::Red);
        assert_eq!(logo.logo, Era::Future);
    }
}
