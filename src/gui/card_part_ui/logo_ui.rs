// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of [logos](crate::card::logo::Logo)

use crate::gui::{AsPickList, GetUI};

use iced::advanced::renderer;
use iced::Length;
use iced_aw::widgets::Card;

/// Message for the [Logo](crate::card::logo::Logo)
#[derive(Clone, Debug, PartialEq)]
pub enum Message<L> {
    /// Message sent to change the logo
    Logo(L),
}

impl<'a, Theme, Renderer, Name, L>
    GetUI<'a, Card<'a, Message<L>, Theme, Renderer>, Message<L>, Theme, Renderer, (), Name> for L
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::container::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message<L>, Theme, Renderer>>,
    L: AsPickList + Copy + 'static,
{
    fn get_ui(&self, name: Name, _: ()) -> Card<'a, Message<L>, Theme, Renderer> {
        Card::new(name, L::pick_list(*self, Message::Logo).width(Length::Fill))
    }
}

impl<L> crate::gui::Message<L, ()> for Message<L>
where
    L: Copy,
{
    fn apply(&self, logo: &mut L) {
        match self {
            Self::Logo(new_logo) => *logo = *new_logo,
        }
    }
}

#[cfg(test)]
mod test_module_logo_ui {
    use super::Message;

    use crate::card::logos::Era;
    use crate::gui::{GetUI, Message as MessageTrait};

    #[test]
    fn test_get_ui() {
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> = Era::All.get_ui("name", ());
    }

    #[test]
    fn test_apply() {
        let mut logo = Era::All;

        Message::Logo(Era::Future).apply(&mut logo);
        assert_eq!(logo, Era::Future);
    }
}
