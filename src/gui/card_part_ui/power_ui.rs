// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the GUI of the [Power](crate::card::fields::PowerField) of a [Card](crate::card::Card)

use crate::card::fields::{Position, PowerField};
use crate::card::pictogram::{Pictogram, Reference, ReferenceType};
use crate::color::Color;
use crate::gui::{GetUI, SPACING};

use iced::widget::{row, Button, Column, PickList, TextInput};
use iced::{Alignment, Element, Length};
use iced_aw::widgets::{Card, NumberInput};
use strum::IntoEnumIterator;

/// Message sent to handle modification of a [Power](crate::card::fields::PowerField)
#[derive(Debug, Clone)]
pub enum Message {
    /// Message sent to create a new power
    New,
    /// Message sent to delete the power
    Delete(usize),
    /// Message sent to move the power up
    Up(usize),
    /// Message sent to move the power down
    Down(usize),
    /// Message sent to change the value
    Value(usize, String),
    /// Message sent to change the color
    Color(usize, Color),
    /// Message sent to change the size
    Size(usize, u8),
    /// Message sent to change the position
    Position(usize, Position),
    /// Message sent to change the pictogram
    Pictogram(super::pictogram_ui::Message),
}

impl Position {
    /// Gets the list of possible values
    fn list() -> Vec<Self> {
        Self::iter()
            .filter(|position: &Position| *position != Position::Default)
            .collect()
    }

    /// Creates a new pick list
    ///
    /// The possible values are the ones from [list](Self::list)
    ///
    /// # Arguments
    /// * `current` - The current value
    /// * `messaging` - The function to send messages
    fn pick_list<'a, Message, Messaging, Theme, Renderer>(
        current: Self,
        on_select: Messaging,
    ) -> PickList<'a, Self, Vec<Self>, Self, Message, Theme, Renderer>
    where
        Message: Clone,
        Messaging: Fn(Self) -> Message + Copy + 'static,
        Theme: iced::widget::pick_list::Catalog,
        Renderer: iced::advanced::text::Renderer,
    {
        PickList::new(Self::list(), Some(current), move |value: Self| {
            on_select(value)
        })
    }
}

impl<'a, Theme, Renderer, Name>
    GetUI<
        'a,
        Card<'a, Message, Theme, Renderer>,
        Message,
        Theme,
        Renderer,
        (usize, &Vec<(usize, &Pictogram)>, &crate::card::Card),
        Name,
    > for PowerField
where
    Renderer:
        iced::advanced::renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::container::Catalog
        + iced::widget::text_input::Catalog
        + iced::widget::button::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + iced_aw::style::menu_bar::Catalog
        + iced_aw::widget::badge::Catalog
        + 'a,
    Name: Into<iced::Element<'a, Message, Theme, Renderer>>,
{
    fn get_ui(
        &self,
        name: Name,
        (index, pictograms, card): (usize, &Vec<(usize, &Pictogram)>, &crate::card::Card),
    ) -> Card<'a, Message, Theme, Renderer> {
        let value = TextInput::new("Value", self.value.as_str())
            .on_input(move |value| Message::Value(index, value));

        let position = Position::pick_list(self.position, move |position: Position| {
            Message::Position(index, position)
        });

        let size = NumberInput::new(&self.size, 1..=u8::MAX, move |size: u8| {
            Message::Size(index, size)
        })
        .width(Length::Fill)
        .step(1);

        let size = row![
            "Size",
            size.width(Length::Fill),
            Button::new("Reset")
                .on_press(Message::Size(index, PowerField::default().size))
                .width(Length::Shrink)
        ]
        .align_y(Alignment::Center)
        .spacing(3);

        let color = Color::pick_list(self.color, move |color: Color| Message::Color(index, color));

        let content = Column::with_children([
            row![
                Element::from(value.width(Length::FillPortion(1))),
                Element::from(position.width(Length::FillPortion(1)))
            ]
            .spacing(SPACING)
            .into(),
            row![
                Element::from(size.width(Length::FillPortion(1))),
                Element::from(color.width(Length::FillPortion(1)))
            ]
            .spacing(SPACING)
            .into(),
            Element::from(pictograms.get_ui(
                "Pictogram",
                (
                    card,
                    Reference {
                        r#type: ReferenceType::Power,
                        value: self.value.clone(),
                    },
                ),
            ))
            .map(Message::Pictogram),
        ])
        .spacing(SPACING);

        Card::new(name, content)
    }
}

impl crate::gui::Message<crate::card::Card, ()> for Message {
    fn apply(&self, card: &mut crate::card::Card) {
        match self {
            Message::New => card.powers.push(PowerField::default()),
            Message::Delete(index) => {
                let power = card.powers[*index].value.as_str();
                let mut i = 0;
                while i < card.pictograms.len() {
                    if card.pictograms[i].reference.r#type == ReferenceType::Power
                        && card.pictograms[i].reference.value == power
                    {
                        card.pictograms.remove(i);
                    } else {
                        i += 1
                    }
                }
                card.powers.remove(*index);
            }
            Message::Up(index) => {
                if *index > 0 {
                    card.powers.swap(*index, index - 1);
                }
            }
            Message::Down(index) => {
                if *index < card.powers.len() - 1 {
                    card.powers.swap(*index, index + 1);
                }
            }
            Message::Value(index, value) => {
                let old = card.powers[*index].value.as_str();
                for pictogram in card.pictograms.iter_mut() {
                    if pictogram.reference.r#type == ReferenceType::Power
                        && pictogram.reference.value == old
                    {
                        pictogram.reference.value = value.clone();
                    }
                }

                card.powers[*index].value = value.clone();
            }
            Message::Color(index, color) => card.powers[*index].color = *color,
            Message::Size(index, size) => card.powers[*index].size = *size,
            Message::Position(index, position) => card.powers[*index].position = *position,
            Message::Pictogram(message) => message.apply(card),
        }
    }
}
