// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI to save, export, load, show or reset a card

use crate::error::LogError;

use iced::advanced::renderer;
use iced::widget::{column, Button, Text};
use iced::{Alignment, Length};
use iced_aw::widgets::Card;
use rfd::FileDialog;

/// Message corresponding to each action
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    /// Reset the card to the default one
    Reset,
    /// Save the card to a file
    Save,
    /// Show the card in a temporary file
    Show,
    /// Export the card to a file
    Export,
    /// Open a card from a file
    Open,
}

/// Create the UI to save, export, load , show or reset a card
pub fn save_ui<'a, Theme, Renderer>() -> Card<'a, Message, Theme, Renderer>
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced_aw::style::card::Catalog
        + iced::widget::text::Catalog
        + iced::widget::button::Catalog
        + 'a,
{
    Card::new(
        "Save",
        column![
            Button::new(Text::new("X Reset Card X").width(Length::Fill).center())
                .on_press(Message::Reset)
                .width(Length::Fill),
            Button::new(Text::new("Show Card").width(Length::Fill).center())
                .on_press(Message::Show)
                .width(Length::Fill),
            Button::new(Text::new("Export Card").width(Length::Fill).center())
                .on_press(Message::Export)
                .width(Length::Fill),
            Button::new(Text::new("Save Card").width(Length::Fill).center())
                .on_press(Message::Save)
                .width(Length::Fill),
            Button::new(Text::new("Open Card").width(Length::Fill).center())
                .on_press(Message::Open)
                .width(Length::Fill),
        ]
        .align_x(Alignment::Center)
        .spacing(3),
    )
}

impl crate::gui::Message<crate::card::Card, ()> for Message {
    fn apply(&self, card: &mut crate::card::Card) {
        match self {
            Self::Show => {
                let _ = card
                    .show(false, true)
                    .error("An error occurred while rendering the card");
            }
            Self::Export => export_card(card),
            Self::Save => save_card(card),
            Self::Open => open_card(card),
            Self::Reset => card.reset(),
        }
    }
}

/// Create a file dialog
///
/// If a path is given, the first existing parent will be used as the directory where the dialog will open
///
/// If an extension and a path is given and the path point to a file with an extension, the extension will be replaced and the dialog will suggest this filename
pub(super) fn file_dialog(path: Option<std::path::PathBuf>, extension: Option<&str>) -> FileDialog {
    let mut file_dialog = FileDialog::new();
    if let Some(mut path) = path {
        println!("Path: {}", path.display());
        if let Some(extension) = extension {
            if path.set_extension(extension) {
                if let Some(filename) = path.file_name() {
                    println!(
                        "Filename: {}",
                        filename
                            .to_str()
                            .critical("Unable to translate filename to a string")
                    );
                    file_dialog = file_dialog.set_file_name(
                        filename
                            .to_str()
                            .critical("Unable to translate filename to a string"),
                    );
                }
            }
        }

        let mut parent = path.parent();
        while parent.is_some() && !parent.unwrap().exists() {
            println!("Parent: {}", parent.unwrap().display());
            parent = parent.unwrap().parent();
        }

        if let Some(parent) = parent {
            println!("Parent: {}", parent.display());
            if let Ok(parent) = parent.canonicalize().error("Unable to canonicalize path") {
                println!("Parent: {}", parent.display());
                file_dialog = file_dialog.set_directory(parent);
            }
        }
    }

    file_dialog
}

/// Opens a file dialog to select a file and then export the rendering of the [Card](crate::card::Card) in it
fn export_card(card: &crate::card::Card) {
    let file_dialog = file_dialog(card.path().cloned(), Some("png"))
        .add_filter("Rasterized images", &["png"])
        .add_filter("Vectorial images", &["svg", "pdf"]);
    let result = file_dialog.save_file();
    if let Some(filename) = result {
        let render = card
            .render(false)
            .error("An error occurred while rendering the card");
        let Ok(mut render) = render else {
            return;
        };
        let _ = render.save(&filename).error(&format!(
            "An error occurred while saving the rendered card to {}",
            &filename.display()
        ));
    }
}

/// Opens a file dialog to select a file and then save the [Card](crate::card::Card) in it
fn save_card(card: &crate::card::Card) {
    let file_dialog =
        file_dialog(card.path().cloned(), Some("yml")).add_filter("YAML", &["yml", "yaml"]);
    let result = file_dialog.save_file();
    if let Some(filename) = result {
        let _ = card.save(&filename).error(&format!(
            "An error occurred while saving the card to {}",
            &filename.display()
        ));
    }
}

/// Opens a file dialog to select a file and then load the [Card](crate::card::Card) in it
fn open_card(card: &mut crate::card::Card) {
    let result = file_dialog(card.path().cloned(), None)
        .add_filter("YAML", &["yml", "yaml"])
        .pick_file();
    if let Some(filename) = result {
        let _ = card.load(&filename).error(&format!(
            "An error occurred while loading the card from {}",
            &filename.display()
        ));
    }
}

#[cfg(test)]
mod test_module_save_ui {
    use super::{save_ui, Message};

    use crate::card::Card;
    use crate::gui::Message as MessageTrait;

    #[test]
    fn test_get_ui() {
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> = save_ui();
    }

    #[test]
    fn test_apply() {
        let mut card = Card::open("tests/input.yml").expect("A test should never fail");

        Message::Reset.apply(&mut card);
        assert_eq!(card, Card::new());
    }
}
