// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the UI of the [BaseField]

use crate::card::fields::BaseField;
use crate::color::Color;
use crate::gui::GetUI;

use iced::advanced::renderer;
use iced::widget::TextInput;
use iced::widget::{column, row, Button};
use iced::{Alignment, Length};
use iced_aw::widgets::{Card, NumberInput};

/// Message set for modification of [BaseField]
#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    /// Message sent to change the value of the field
    Value(String),
    /// Message sent to change the size of the field
    Size(u8),
    /// Message sent to change the color of the field
    Color(Color),
}

impl<'a, Theme, Renderer>
    GetUI<'a, Card<'a, Message, Theme, Renderer>, Message, Theme, Renderer, &Self, &'a str>
    for BaseField
where
    Renderer: renderer::Renderer + iced::advanced::text::Renderer<Font = iced::Font> + 'a,
    Theme: iced::widget::pick_list::Catalog
        + iced_aw::style::card::Catalog
        + iced::widget::button::Catalog
        + iced::widget::text::Catalog
        + iced::widget::text_input::Catalog
        + iced_aw::style::number_input::ExtendedCatalog
        + 'a,
{
    fn get_ui(&self, name: &'a str, default: &Self) -> Card<'a, Message, Theme, Renderer> {
        let value = TextInput::new(name, if self.value == " " { "" } else { &self.value })
            .on_input(Message::Value)
            .id(iced::widget::text_input::Id::unique());
        let size = NumberInput::new(&self.size, 1..=u8::MAX, Message::Size)
            .width(Length::Fill)
            .step(1);

        let color = Color::pick_list(self.color, Message::Color);

        Card::new(
            name,
            column![
                value,
                row![
                    "Size",
                    size.width(Length::Fill),
                    Button::new("Reset")
                        .on_press(Message::Size(default.size))
                        .width(Length::Shrink)
                ]
                .align_y(Alignment::Center)
                .spacing(3),
                color,
            ]
            .align_x(Alignment::Center)
            .spacing(3),
        )
    }
}

impl crate::gui::Message<BaseField, ()> for Message {
    fn apply(&self, field: &mut BaseField) {
        match self {
            Message::Value(value) => {
                if value.is_empty() {
                    field.value = " ".to_owned()
                } else {
                    field.value = value.to_string()
                }
            }
            Message::Size(size) if *size > 0 => field.size = *size,
            Message::Size(_) => (),
            Message::Color(color) => field.color = color.process_custom(field.color),
        }
    }
}

#[cfg(test)]
mod test_module_fields_ui {
    use super::Message;

    use crate::card::fields::BaseField;
    use crate::color::Color;
    use crate::gui::{GetUI, Message as MessageTrait};

    #[test]
    fn test_get_ui() {
        let field = BaseField {
            value: "value".to_owned(),
            size: 1,
            color: Color::Purple,
        };
        let _: iced_aw::widgets::Card<_, iced::Theme, iced::Renderer> = field.get_ui(
            "name",
            &BaseField::default(crate::card::fields::FieldType::Name),
        );
    }

    #[test]
    fn test_apply() {
        let mut field = BaseField {
            value: "value".to_owned(),
            size: 1,
            color: Color::Purple,
        };

        Message::Value("".to_owned()).apply(&mut field);
        assert_eq!(field.value, " ".to_owned());
        assert_eq!(field.size, 1);
        assert_eq!(field.color, Color::Purple);

        Message::Value("value".to_owned()).apply(&mut field);
        assert_eq!(field.value, "value".to_owned());
        assert_eq!(field.size, 1);
        assert_eq!(field.color, Color::Purple);

        Message::Size(2).apply(&mut field);
        assert_eq!(field.value, "value".to_owned());
        assert_eq!(field.size, 2);
        assert_eq!(field.color, Color::Purple);

        Message::Size(0).apply(&mut field);
        assert_eq!(field.value, "value".to_owned());
        assert_eq!(field.size, 2);
        assert_eq!(field.color, Color::Purple);

        Message::Color(Color::Pink).apply(&mut field);
        assert_eq!(field.value, "value".to_owned());
        assert_eq!(field.size, 2);
        assert_eq!(field.color, Color::Pink);
    }
}
