// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Represents a "Baston" card

use crate::color::Color;
use crate::error::{Error, LogError};
use crate::render::RenderedCard;

use std::convert::AsRef;
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use std::thread::sleep;
use std::time::Duration;

use lazy_static::lazy_static;
use log::warn;
use serde::{Deserialize, Serialize};
use tempfile::{Builder, NamedTempFile};

pub mod fields;
pub mod illustration;
pub mod logos;
pub mod pictogram;
pub mod symbol;

lazy_static! {
    /// Default Card, loaded at startup, retrieved from the config file
    static ref DEFAULT_CARD: Card = Card::get_default();
}

/// Represents a "Baston" card
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Card {
    /// Path to the yaml file if the card is loaded from a yaml file
    #[serde(skip)]
    path: Option<PathBuf>,

    /// Version of the software used to generate the card
    #[serde(default)]
    version: crate::auto_update::Version,

    /// Name of the card
    #[serde(default = "fields::BaseField::get_default")]
    pub name: fields::NameField,

    /// Title of the card
    #[serde(default = "fields::BaseField::get_default")]
    pub title: fields::TitleField,

    /// Type of the card
    #[serde(default = "fields::BaseField::get_default")]
    pub r#type: fields::TypeField,

    /// Old number of the card, here for backward compatibility
    ///
    /// Replaced by powers
    #[serde(skip_serializing)]
    number: Option<fields::NumberField>,

    /// Old power of the card, here for backward compatibility
    ///
    /// Replaced by powers
    #[serde(skip_serializing)]
    power: Option<fields::PowerField>,

    /// Powers of the card
    #[serde(default)]
    pub powers: Vec<fields::PowerField>,

    /// Template of the card
    #[serde(default = "illustration::Template::get_default")]
    pub template: illustration::Template,

    /// Faction of the card
    #[serde(default)]
    pub faction: logos::Faction,

    /// Gender of the card
    #[serde(default = "fields::GenderField::get_default")]
    pub gender: fields::GenderField,

    /// Era of the card
    #[serde(default = "fields::EraField::get_default")]
    pub era: fields::EraField,

    /// Archetype of the card
    #[serde(default = "fields::ArchetypeField::get_default")]
    pub archetype: fields::ArchetypeField,

    /// Characteristics of the card
    #[serde(alias = "characteristic")]
    #[serde(alias = "caract")]
    #[serde(default = "fields::CharacteristicsField::get_default")]
    pub characteristics: fields::CharacteristicsField,

    /// Symbols of the card
    #[serde(default)]
    pub symbols: Vec<symbol::SymbolField>,

    /// Pictograms of the card
    #[serde(default)]
    pub pictograms: Vec<pictogram::Pictogram>,

    /// Whether the card is an extra deck
    #[serde(alias = "extra deck", default)]
    pub extra_deck: logos::ExtraDeck,

    /// Illustration of the card
    #[serde(default = "illustration::Illustration::get_default")]
    pub illustration: illustration::Illustration,
}

/// Trait for part of a card that can be updated, meaning that missing fields can be replaced by the one from the config
trait Updatable {
    /// Update an old version of the object to a new one and return if the object was modified <br>
    /// <span>&#9888;</span> Should not be used excepted while loading a card
    fn update(&mut self, _default: &Self) -> bool;

    /// Get the default version of the object, unlike [Default], load the default from an hardcoded value
    fn get_default() -> Self;
}

impl Card {
    /// Creates a new Card
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    ///
    /// Card::new();
    /// ```
    pub fn new() -> Self {
        lazy_static::initialize(&DEFAULT_CARD);
        let mut card = DEFAULT_CARD.clone();
        card.powers = vec![];
        card.pictograms = vec![];
        card.symbols = vec![];
        card
    }

    /// Get the default card from the config file
    ///
    /// Unlike [`default`](Card::default), which load the default from an hardcoded file
    fn get_default() -> Self {
        let config_lock = Self::get_lock(600);
        let mut card: Self = confy::load("baston_editor", "default_card")
            .critical("A issue occurred while loading the default card configuration");

        if card.update(&Self::default(), true) {
            confy::store("baston_editor", "default_card", card.clone())
                .critical("A issue occurred while saving the default card configuration")
        }
        config_lock
            .close()
            .critical("A issue occurred while removing the lock on the default card configuration");
        card
    }

    /// Get a lock on the config file, prevent concurrent access (especially during testing or running several instances)
    fn get_lock(ttl: u32) -> NamedTempFile {
        if ttl == 0 {
            log::error!("Could not get the lock in 60 sec, failing");
            panic!("Could not get the lock in 60 sec, failing");
        }
        match Builder::new()
            .prefix("baston_editor_config")
            .suffix(".lock")
            .rand_bytes(0)
            .tempfile()
        {
            Ok(f) => f,
            Err(_) => {
                log::warn!("baston_editor.lock already exists");
                sleep(Duration::from_millis(100));
                Self::get_lock(ttl - 1)
            }
        }
    }

    /// Opens the yaml file representing a card specified and returns the Card
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    ///
    /// Card::open("tests/input.yml").unwrap();
    /// ```
    pub fn open<P: AsRef<OsStr> + AsRef<Path>>(filename: P) -> Result<Self, Error> {
        log::info!(
            "Opening card {}",
            <P as AsRef<Path>>::as_ref(&filename).display()
        );
        let filename = Path::new(&filename);
        let mut card: Self = confy::load_path(filename)?;
        card.path = Some(filename.to_path_buf());
        card.full_update(&DEFAULT_CARD);
        Ok(card)
    }

    /// Loads the card in the yaml file specified by the filename argument
    ///
    /// This function replace the content of the card by the content of the yaml file, unlike [`open`](Card::open) which creates a new card
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    ///
    /// let mut card = Card::new();
    /// card.load("tests/input.yml").unwrap();
    /// ```
    pub fn load<P: AsRef<OsStr> + AsRef<Path>>(&mut self, filename: P) -> Result<(), Error> {
        *self = Self::open(filename)?;
        Ok(())
    }

    /// Resets the card to its default value
    ///
    /// This function reset the content of the card by the content of the yaml file, similar to [`new`](Card::new) but without creating a new card
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    ///
    /// let mut card = Card::open("tests/input.yml").unwrap();
    /// card.reset();
    /// ```
    pub fn reset(&mut self) {
        *self = Self::new();
    }

    /// Saves the card in the yaml file specified by the filename argument
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    ///
    /// Card::new().save("tests/output.yml").unwrap();
    /// ```
    pub fn save<P: AsRef<OsStr> + AsRef<Path>>(&self, filename: P) -> Result<(), Error> {
        let filename = Path::new(&filename);
        if let Some(extension) = filename.extension() {
            match extension.to_ascii_lowercase().to_str() {
                Some("yml") | Some("yaml") => Ok(confy::store_path(filename, self)?),
                Some(extension) => Err(Error::UnsupportedFormatError(extension.to_string())),
                _ => Err(Error::UnknownFormatError),
            }
        } else {
            Err(Error::UnknownFormatError)
        }
    }

    /// Renders the card and saves it at the given path
    ///
    /// The boolean `no_border` is used to specify whether or not the limits of the card should be rendered
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    ///
    /// Card::new().print("tests/output.svg", true).unwrap();
    /// ```
    pub fn print<P: AsRef<OsStr> + AsRef<Path>>(
        &self,
        filename: P,
        no_border: bool,
    ) -> Result<(), Error> {
        let filename = Path::new(&filename);
        if let Some(extension) = filename.extension() {
            match extension.to_ascii_lowercase().to_str() {
                Some("svg") | Some("pdf") => self.render(no_border)?.save(filename),
                Some("png") => self.render(no_border)?.rasterize(filename, no_border),
                Some(extension) => Err(Error::UnsupportedFormatError(extension.to_string())),
                _ => Err(Error::UnknownFormatError),
            }
        } else {
            Err(Error::UnknownFormatError)
        }
    }

    /// Renders the card in a tempfile and opens it
    ///
    /// The boolean `no_border` is used to specify whether or not the limits of the card should be rendered
    ///
    pub fn show(&self, no_border: bool, open_output: bool) -> Result<(), Error> {
        let tempfile = Builder::new().suffix(".svg").tempfile()?;
        self.print(tempfile.path(), no_border)?;
        let (_, path) = tempfile.keep()?;
        if open_output {
            Ok(open::that(path)?)
        } else {
            Ok(())
        }
    }

    /// Returns the path of the card if the card was loaded from a yaml file,
    /// returns None otherwise
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::card::Card;
    /// use std::path::Path;
    ///
    /// let card = Card::new();
    /// assert_eq!(card.path(), None);
    ///
    /// let card = Card::open("tests/input.yml").unwrap();
    /// assert!(card.path().is_some());
    /// ```
    pub fn path(&self) -> Option<&PathBuf> {
        self.path.as_ref()
    }

    /// Creates a rendering of the Card
    ///
    /// The boolean `no_border` is used to specify whether or not the limits of the card should be rendered
    ///
    pub fn render(&self, no_border: bool) -> Result<RenderedCard, Error> {
        RenderedCard::new(self, no_border)
    }

    /// Updates the content of the card with the content of the default card
    ///
    /// Replaces th missing fields of the card with the ones of the default card
    fn full_update(&mut self, default: &Card) -> bool {
        self.update(default, false)
    }

    /// Updates the content of the card with the content of the default card
    ///
    /// Replaces th missing fields of the card with the ones of the default card
    ///
    /// If `ignore_empty` is true, the function will not remove empty powers
    fn update(&mut self, default: &Card, ignore_empty: bool) -> bool {
        let mut res = false;
        let current_version = crate::auto_update::current_version();
        if self.version < current_version {
            log::info!("The card is using a former version {}, updating it to the current version {current_version}", self.version);
            self.version = current_version;
            res = true;
        }
        log::info!("Processing the name");
        res = self.name.update(&default.name) || res;
        log::info!("Processing the title");
        res = self.title.update(&default.title) || res;
        log::info!("Processing the type");
        res = self.r#type.update(&default.r#type) || res;
        log::info!("Processing the illustration");
        res = self.illustration.update(&default.illustration) || res;
        log::info!("Processing the gender");
        res = self.gender.update(&default.gender) || res;
        log::info!("Processing the era");
        res = self.era.update(&default.era) || res;
        log::info!("Processing the archetype");
        res = self.archetype.update(&default.archetype) || res;
        log::info!("Processing the characteristics");
        if self.template.characteristic_name_color != Color::default()
            && self.characteristics.name_color == Color::default()
        {
            log::info!(
                "The color of the name of the characteristic is at the old place, updating it"
            );
            res = true;
            self.characteristics.name_color = self.template.characteristic_name_color;
        }
        res = self.characteristics.update(&default.characteristics) || res;

        log::info!("Processing the template");
        res = self.template.update(&default.template) || res;

        log::info!("Processing the powers");
        if let Some(power) = self.power.clone() {
            log::info!("A power is at the old place, updating it");
            res = true;
            self._add_power(power, ignore_empty);
            self.power = None
        }
        if let Some(number) = self.number.clone() {
            log::info!("A number is at the old place, updating it");
            res = true;
            self._add_power(number.into(), ignore_empty);
            self.number = None
        }
        res = self.split_power() || res;

        res = self.convert_power_to_symbol() || res;

        if !ignore_empty {
            res = self.remove_empty_power() || res;
        }

        let default_power = if !default.powers.is_empty() {
            default.powers[0].clone()
        } else {
            log::warn!("The default file does not have a power");
            fields::PowerField::default()
        };
        let mut powers = vec![];
        for power in &mut self.powers {
            res = power.update(&default_power) || res;
            if powers.contains(&power.value.as_str()) {
                warn!(
                    "The card contains several times the same power ({}).",
                    power.value
                );
            } else {
                powers.push(power.value.as_str());
            }
        }

        log::info!("Processing the symbols");
        let default_symbol = if !default.symbols.is_empty() {
            default.symbols[0]
        } else {
            log::warn!("The default file does not have a symbol");
            symbol::SymbolField::default()
        };

        for symbol in &mut self.symbols {
            res = symbol.update(&default_symbol) || res;
        }

        log::info!("Processing the pictograms");
        let default_pictogram = if !default.pictograms.is_empty() {
            default.pictograms[0].clone()
        } else {
            log::warn!("The default file does not have a pictogram");
            pictogram::Pictogram::default()
        };
        let default_pictogram = if self.template.pictogram_number_color != Color::default() {
            log::info!("The color of the pictogram's text is at the old place, updating it");
            pictogram::Pictogram {
                text_color: self.template.pictogram_number_color,
                ..default_pictogram
            }
        } else {
            default_pictogram
        };

        for pictogram in &mut self.pictograms {
            res = pictogram.update(&default_pictogram) || res;
            res = pictogram.check_ref(&self.powers) || res;
        }
        res
    }

    /// Returns the Mode
    pub fn get_mode(&self) -> illustration::Mode {
        self.illustration.mode
    }

    /// Sets the Mode
    pub fn set_mode(&mut self, mode: illustration::Mode) {
        self.illustration.mode = mode
    }

    /// Returns the Orientation
    pub fn get_orientation(&self) -> illustration::Orientation {
        self.template.orientation
    }

    /// Sets the Orientation
    pub fn set_orientation(&mut self, orientation: illustration::Orientation) {
        self.template.orientation = orientation
    }

    /// Adds a Power
    pub fn add_power(&mut self, power: fields::PowerField) {
        self._add_power(power, false)
    }

    /// Adds a Power
    ///
    /// If `ignore_empty` is true, the function will not remove empty powers
    fn _add_power(&mut self, power: fields::PowerField, ignore_empty: bool) {
        if ignore_empty || !power.value.is_empty() {
            self.powers.push(power)
        }
    }

    /// Removes powers which value is empty
    pub fn remove_empty_power(&mut self) -> bool {
        let mut i = 0;

        let mut res = false;

        while i < self.powers.len() {
            if self.powers[i].value.is_empty() {
                self.powers.remove(i);
                res = true
            } else {
                i += 1
            }
        }

        res
    }

    /// Splits the powers of the card, if they contain whitespace
    fn split_power(&mut self) -> bool {
        let mut i = 0;

        let mut res = false;

        while i < self.powers.len() {
            if !self.powers[i].value.is_empty() {
                let value = self.powers[i].value.clone();
                let values = value.split(' ').collect::<Vec<&str>>();
                if values.len() > 1 {
                    res = true;
                    for value in values {
                        let mut power = self.powers[i].clone();
                        power.value = value.to_string();
                        self.powers.push(power);
                    }
                    self.powers.remove(i);
                } else {
                    i += 1
                }
            } else {
                i += 1
            }
        }

        res
    }

    /// Converts old powers that are now symbols
    fn convert_power_to_symbol(&mut self) -> bool {
        let mut res = false;

        let mut i = 0;

        while i < self.powers.len() {
            if let Ok(symbol) = symbol::SymbolField::try_from(self.powers[i].clone()) {
                self.add_symbol(symbol);
                self.powers.remove(i);
                res = true;
            } else {
                i += 1;
            }
        }
        res
    }

    /// Adds a Symbol
    pub fn add_symbol(&mut self, symbol: symbol::SymbolField) {
        self.symbols.push(symbol)
    }

    /// Adds a Pictogram
    pub fn add_pictogram(&mut self, pictogram: pictogram::Pictogram) {
        self.pictograms.push(pictogram)
    }
}

impl Default for Card {
    /// <span>&#9888;</span> Should not be used because it load a hardcoded default version
    ///
    /// [`new`](Card::new) should be used instead
    fn default() -> Self {
        serde_yaml::from_str::<Card>(include_str!("config/default_card.yml"))
            .critical("This should never fail as it is an embedded file")
    }
}

#[cfg(test)]
mod test_module_card {
    use super::Card;

    use super::fields::PowerField;
    use super::illustration::{Mode, Orientation};
    use super::pictogram::{Effect, Pictogram, Reference, Type};
    use super::symbol::{Symbol, SymbolField};
    use crate::color::Color;
    use crate::error::Error;

    use log_tester::LogTester;
    use std::ffi::OsStr;
    use std::panic;
    use std::path::Path;
    use std::{fs::File, io::Read};
    use strum::IntoEnumIterator;
    use tempfile::Builder;

    #[test]
    fn test_new_card() {
        Card::new();
    }

    #[test]
    fn test_open_card() {
        Card::open("tests/input.yml").expect("A test should never fail");

        Card::open("tests/input_ugly.yml").expect("A test should never fail");
    }

    #[test]
    fn test_load_default_card() {
        Card::default();
    }

    #[test]
    fn test_load_card() {
        let mut card = Card::new();

        assert!(card.load("tests/input.yml").is_ok());
        assert_eq!(
            card,
            Card::open("tests/input.yml").expect("A test should never fail")
        );
    }

    #[test]
    fn test_reset_card() {
        let mut card = Card::open("tests/input.yml").expect("A test should never fail");
        card.reset();
        assert_eq!(card, Card::new());
    }

    #[test]
    fn test_save_card() {
        let tmp = Builder::new()
            .suffix(".yml")
            .tempfile()
            .expect("A test should never fail");
        Card::new()
            .save(tmp.path().to_str().expect("A test should never fail"))
            .expect("A test should never fail");

        let err = Card::new()
            .save("tests/output")
            .expect_err("A test should never fail");

        assert_eq!(err.to_string(), Error::UnknownFormatError.to_string());

        let filename: &OsStr =
            unsafe { OsStr::from_encoded_bytes_unchecked(b"tests/output.\xc3\x28") };
        let err = Card::new()
            .save(filename)
            .expect_err("A test should never fail");
        assert_eq!(err.to_string(), Error::UnknownFormatError.to_string());
    }

    #[test]
    fn test_lock_card() {
        let lock = Card::get_lock(600);
        let thread = std::thread::spawn(|| Card::get_lock(600));
        std::thread::sleep(std::time::Duration::from_millis(1000));
        lock.close().expect("A test should never fail");
        thread
            .join()
            .expect("A test should never fail")
            .close()
            .expect("A test should never fail")
    }

    #[test]
    fn test_missing_name() {
        test_missing_field("name");
    }

    #[test]
    fn test_missing_title() {
        test_missing_field("title");
    }

    #[test]
    fn test_missing_type() {
        test_missing_field("type");
    }

    #[test]
    fn test_missing_powers() {
        test_missing_field("powers");
    }

    #[test]
    fn test_missing_illustration() {
        test_missing_field("illustration");
    }

    #[test]
    fn test_missing_template() {
        test_missing_field("template");
    }

    #[test]
    fn test_missing_faction() {
        test_missing_field("faction");
    }

    #[test]
    fn test_missing_characteristics() {
        test_missing_field("characteristics");
    }

    #[test]
    fn test_missing_symbols() {
        test_missing_field("symbols");
    }

    #[test]
    fn test_missing_pictograms() {
        test_missing_field("pictograms");
    }

    #[test]
    fn test_missing_extra_deck() {
        test_missing_field("extra_deck");
    }

    #[test]
    fn test_missing_gender() {
        test_missing_field("gender");
    }

    #[test]
    fn test_missing_era() {
        test_missing_field("era");
    }

    #[test]
    fn test_missing_archetype() {
        test_missing_field("title");
    }

    #[test]
    fn test_missing_fields() {
        for field in [
            "name",
            "title",
            "type",
            "powers",
            "illustration",
            "template",
            "faction",
            "characteristics",
            "symbols",
            "pictograms",
            "extra_deck",
            "gender",
            "era",
            "archetype",
        ] {
            test_missing_field(field);
        }
    }
    fn test_missing_field(field: &str) {
        let _ = Card::open(format!("tests/input_field_missing/{}.yml", field))
            .expect("A test should never fail");
    }

    #[test]
    #[should_panic(expected = "Could not get the lock in 60 sec, failing")]
    fn test_lock_fail_card() {
        let lock = Card::get_lock(600);
        let thread = std::thread::spawn(|| Card::get_lock(60));
        std::thread::sleep(std::time::Duration::from_millis(10000));
        lock.close().expect("A test should never fail");
        let e = thread.join().expect_err("A test should never fail");
        panic::resume_unwind(e)
    }

    #[test]
    fn test_update_card() {
        let mut default = Card::default();
        let tmp = default.clone();
        default.full_update(&tmp);
        let comp = Card {
            pictograms: vec![],
            ..default.clone()
        };
        for field in [
            "name",
            "title",
            "type",
            "powers",
            "illustration",
            "template",
            "faction",
            "characteristics",
            "symbols",
            "pictograms",
            "extra_deck",
            "gender",
            "era",
            "archetype",
        ] {
            let mut file = File::open(format!("tests/input_field_missing/{}.yml", field))
                .expect("A test should never fail");
            let mut data = String::new();
            file.read_to_string(&mut data)
                .expect("A test should never fail");
            let mut card = serde_yaml::from_str::<Card>(&data).expect("A test should never fail");
            card.full_update(&default);
            assert_eq!(card, comp);
        }

        let mut card = Card::open("tests/input_ugly.yml").expect("A test should never fail");
        assert!(!card.full_update(&card.clone()));
    }

    #[test]
    fn test_update_card_with_missing_in_default() {
        let mut card = Card::new();
        let default = Card::new();

        LogTester::start();
        assert!(!card.full_update(&default));

        assert!(LogTester::contains(
            log::Level::Warn,
            "The default file does not have a power"
        ));
        assert!(LogTester::contains(
            log::Level::Warn,
            "The default file does not have a pictogram"
        ))
    }

    #[test]
    fn test_split_power_card() {
        let mut card = Card::new();
        card.powers = vec![PowerField {
            value: "01 124 Test Bouclier B12".to_owned(),
            ..PowerField::default()
        }];
        card.split_power();
        assert_eq!(card.powers.len(), 5);
        assert_eq!(card.powers[0].value, "01");
        assert_eq!(card.powers[1].value, "124");
        assert_eq!(card.powers[2].value, "Test");
        assert_eq!(card.powers[3].value, "Bouclier");
        assert_eq!(card.powers[4].value, "B12");
    }

    #[test]
    fn test_convert_power_to_symbol_card() {
        let mut card = Card::new();
        card.powers = vec![
            PowerField {
                value: "01".to_owned(),
                ..PowerField::default()
            },
            PowerField {
                value: "124".to_owned(),
                ..PowerField::default()
            },
            PowerField {
                value: "Test".to_owned(),
                ..PowerField::default()
            },
            PowerField {
                value: "Bouclier".to_owned(),
                ..PowerField::default()
            },
            PowerField {
                value: "B12".to_owned(),
                ..PowerField::default()
            },
        ];
        card.symbols = vec![];
        card.convert_power_to_symbol();
        assert_eq!(card.powers.len(), 3);
        assert_eq!(card.powers[0].value, "124");
        assert_eq!(card.powers[1].value, "Test");
        assert_eq!(card.powers[2].value, "Bouclier");

        assert_eq!(card.symbols.len(), 2);
        assert_eq!(card.symbols[0], Symbol::Traitre.into());
        assert_eq!(
            card.symbols[1],
            SymbolField {
                value: 12.into(),
                ..Symbol::Bouclier.into()
            }
        );
    }

    #[test]
    fn test_add_symbol_card() {
        let mut card = Card::new();
        card.symbols = vec![];
        card.add_symbol(Symbol::Bloque.into());
        card.add_symbol(SymbolField {
            value: 12.into(),
            ..Symbol::Bouclier.into()
        });
        assert_eq!(card.symbols.len(), 2);
        assert_eq!(card.symbols[0], Symbol::Bloque.into());
        assert_eq!(
            card.symbols[1],
            SymbolField {
                value: 12.into(),
                ..Symbol::Bouclier.into()
            }
        );
    }

    #[test]
    fn test_add_power_card() {
        let mut card = Card::new();
        card.powers = vec![];
        card.add_power(PowerField {
            value: "01".to_owned(),
            ..PowerField::default()
        });
        card.add_power(PowerField {
            value: "124".to_owned(),
            ..PowerField::default()
        });
        card.add_power(PowerField {
            value: "".to_owned(),
            ..PowerField::default()
        });
        assert_eq!(card.powers.len(), 2);
        assert_eq!(card.powers[0].value, "01");
        assert_eq!(card.powers[1].value, "124");
    }

    #[test]
    fn test_add_pictogram_card() {
        let mut card = Card::new();
        card.pictograms = vec![];
        card.add_pictogram(Pictogram {
            effect: Effect::Negative,
            r#type: Type::Attack,
            value: "".to_owned(),
            stars: 0,
            border: Color::Transparent,
            text_color: Color::Black,
            reference: Reference::default(),
        });
        card.add_pictogram(Pictogram {
            effect: Effect::Mandatory,
            r#type: Type::Defense,
            value: "".to_owned(),
            stars: 0,
            border: Color::Transparent,
            text_color: Color::Black,
            reference: Reference::default(),
        });
        assert_eq!(card.pictograms.len(), 2);
        assert_eq!(
            card.pictograms[0],
            Pictogram {
                effect: Effect::Negative,
                r#type: Type::Attack,
                value: "".to_owned(),
                stars: 0,
                border: Color::Transparent,
                text_color: Color::Black,
                reference: Reference::default(),
            }
        );
        assert_eq!(
            card.pictograms[1],
            Pictogram {
                effect: Effect::Mandatory,
                r#type: Type::Defense,
                value: "".to_owned(),
                stars: 0,
                border: Color::Transparent,
                text_color: Color::Black,
                reference: Reference::default(),
            }
        );
    }

    #[test]
    fn test_render_card() {
        assert!(Card::new().render(false).is_ok());
    }

    #[test]
    fn test_print_card() {
        let tempfile = Builder::new()
            .suffix(".svg")
            .tempfile()
            .expect("A test should never fail");
        Card::new()
            .print(
                tempfile.path().to_str().expect("A test should never fail"),
                false,
            )
            .expect("A test should never fail");

        let tempfile = Builder::new()
            .suffix(".pdf")
            .tempfile()
            .expect("A test should never fail");
        Card::new()
            .print(
                tempfile.path().to_str().expect("A test should never fail"),
                false,
            )
            .expect("A test should never fail");

        let tempfile = Builder::new()
            .suffix(".png")
            .tempfile()
            .expect("A test should never fail");
        Card::new()
            .print(
                tempfile.path().to_str().expect("A test should never fail"),
                false,
            )
            .expect("A test should never fail");

        let tempfile = Builder::new()
            .suffix(".jpg")
            .tempfile()
            .expect("A test should never fail");
        assert_eq!(
            Card::new().print(
                tempfile.path().to_str().expect("A test should never fail"),
                false
            ),
            Err(Error::UnsupportedFormatError("jpg".to_owned()))
        );
        let tempfile = Builder::new()
            .suffix("")
            .tempfile()
            .expect("A test should never fail");
        assert_eq!(
            Card::new().print(
                tempfile.path().to_str().expect("A test should never fail"),
                false
            ),
            Err(Error::UnknownFormatError)
        );

        let filename: &OsStr =
            unsafe { OsStr::from_encoded_bytes_unchecked(b"tests/output.\xc3\x28") };
        let err = Card::new()
            .print(filename, false)
            .expect_err("A test should never fail");
        assert_eq!(err.to_string(), Error::UnknownFormatError.to_string());
    }

    #[test]
    fn test_show_card() {
        Card::new()
            .show(true, false)
            .expect("A test should never fail");
    }

    #[test]
    fn test_path_card() {
        let card = Card::new();
        let path = card.path();
        assert!(path.is_none());

        let card = Card::open("tests/input.yml").expect("A test should never fail");
        let path = card.path().expect("A test should never fail");
        assert_eq!(path, Path::new("tests/input.yml"));
    }

    #[test]
    fn test_mode_card() {
        let mut card = Card::new();
        for mode in Mode::iter() {
            card.set_mode(mode);
            assert_eq!(card.get_mode(), mode);
        }
    }

    #[test]
    fn test_orientation_card() {
        let mut card = Card::new();
        for orientation in Orientation::iter() {
            card.set_orientation(orientation);
            assert_eq!(card.get_orientation(), orientation);
        }
    }

    #[test]
    fn test_debug_card() {
        let card = Card::new();
        assert_eq!(format!("{:?}", card),
            format!("Card {{ path: {:?}, version: {:?}, name: {:?}, title: {:?}, type: {:?}, number: {:?}, power: {:?}, powers: {:?}, template: {:?}, faction: {:?}, gender: {:?}, era: {:?}, archetype: {:?}, characteristics: {:?}, symbols: {:?}, pictograms: {:?}, extra_deck: {:?}, illustration: {:?} }}", card.path, card.version, card.name, card.title, card.r#type, card.number, card.power, card.powers, card.template, card.faction, card.gender, card.era, card.archetype, card.characteristics, card.symbols, card.pictograms, card.extra_deck, card.illustration));
    }
}

#[cfg(test)]
mod test_show {
    use strum::IntoEnumIterator;

    use super::illustration::{Mode, Orientation};
    use super::Card;

    #[test]
    #[ignore]
    fn test_show() {
        let mut card = Card::open("tests/input_ugly.yml").expect("A test should never fail");
        for orientation in Orientation::iter() {
            card.set_orientation(orientation);
            for mode in Mode::iter() {
                card.set_mode(mode);
                assert_eq!(
                    card.show(false, true).expect("A test should never fail"),
                    ()
                );
            }
        }
        let card = Card::open("tests/input_ugly_char.yml").expect("A test should never fail");
        assert_eq!(
            card.show(false, true).expect("A test should never fail"),
            ()
        );
    }
}
