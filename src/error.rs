// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for error handling

#[cfg(feature = "gui")]
use crate::cli::GUI;
// use crate::gui::WINDOW;

use std::fmt;

#[cfg(feature = "gui")]
use rfd::{MessageButtons, MessageDialog, MessageLevel};
use strum_macros::Display;

/// Enum used to represent or box any Error that the library could throw
#[derive(Debug)]
pub enum Error {
    /// Error raised when a object is not of the expected size
    LengthError {
        /// The expected size
        expected_size: usize,
        /// The size that was found
        current_size: usize,
    },

    /// Error raised when a value is not the one expected
    ValueError {
        /// The expected value
        expected: String,
        /// The value found
        current: String,
    },

    /// Error raised when a type is not the one expected
    VariantMismatchError {
        /// The expected variant
        expected: ErrorKind,
        /// The variant found
        current: ErrorKind,
    },

    /// Error raised by trying to read a file of unknown format
    UnknownFormatError,

    /// Error raised when the format of a file in not supported
    UnsupportedFormatError(String),

    /// Error thrown when a conversion
    ConversionError,

    /// Error thrown when using a default enum when not allowed
    VariantDefaultForbiddenError(String),

    /// Error thrown when accessing a value that is nor here
    MissingValueError(String),

    /// Error thrown when an IO error happens
    IoError(std::io::Error),

    /// Error thrown when a Utf8 error happens
    Utf8Error(std::str::Utf8Error),

    /// Error thrown by [usvg]
    UsvgError(usvg::Error),

    /// Error thrown by [image]
    ImageError(image::ImageError),

    /// Error thrown by [png]
    PngError(png::EncodingError),

    /// Error thrown by [serde_yaml]
    SerdeYamlError(serde_yaml::Error),

    /// Error thrown by [tempfile]
    PersistError(tempfile::PersistError),

    /// Error thrown by [confy]
    ConfyError(confy::ConfyError),

    /// Error thrown by [svg2pdf]
    Svg2PdfError(svg2pdf::usvg::Error),

    /// Error thrown by [iced]
    #[cfg(feature = "gui")]
    IcedError(iced::Error),

    /// Error thrown by [iced::advanced::graphics::core::window::icon]
    #[cfg(feature = "gui")]
    IconError(iced::advanced::graphics::core::window::icon::Error),

    /// Error thrown by [self_update]
    #[cfg(feature = "auto_update")]
    SelfUpdateError(self_update::errors::Error),

    /// Error thrown by [std::num]
    ParseIntError(std::num::ParseIntError),

    /// Error thrown by [svg2pdf]
    Svg2PdfConversionError(svg2pdf::ConversionError),
}

/// Enums over the different variant of [Error]
#[derive(Debug, Display, PartialEq)]
pub enum ErrorKind {
    /// Kind of [Error::LengthError]
    LengthError,
    /// Kind of [Error::ValueError]
    ValueError,
    /// Kind of [Error::VariantMismatchError]
    VariantMismatchError,
    /// Kind of [Error::UnknownFormatError]
    UnknownFormatError,
    /// Kind of [Error::UnsupportedFormatError]
    UnsupportedFormatError,
    /// Kind of [Error::ConversionError]
    ConversionError,
    /// Kind of [Error::VariantDefaultForbiddenError]
    VariantDefaultForbiddenError,
    /// Kind of [Error::MissingValueError]
    MissingValueError,
    /// Kind of [Error::IoError]
    IoError,
    /// Kind of [Error::Utf8Error]
    Utf8Error,
    /// Kind of [Error::UsvgError]
    UsvgError,
    /// Kind of [Error::ImageError]
    ImageError,
    /// Kind of [Error::PngError]
    PngError,
    /// Kind of [Error::SerdeYamlError]
    SerdeYamlError,
    /// Kind of [Error::PersistError]
    PersistError,
    /// Kind of [Error::ConfyError]
    ConfyError,
    /// Kind of [Error::Svg2PdfError]
    Svg2PdfError,
    /// Kind of [Error::IcedError]
    #[cfg(feature = "gui")]
    IcedError,
    /// Kind of [Error::IconError]
    #[cfg(feature = "gui")]
    IconError,
    /// Kind of [Error::SelfUpdateError]
    #[cfg(feature = "auto_update")]
    SelfUpdateError,
    /// Kind of [Error::ParseIntError]
    ParseIntError,
    /// Kind of [Error::Svg2PdfConversionError]
    Svg2PdfConversionError,
}

/// Trait to log errors
pub trait LogError<T> {
    /// Log an error and panic if there is an error
    fn critical(self, message: &str) -> T;

    /// Log an error and continue if there is an error
    fn error(self, message: &str) -> Self;

    /// Log a warning and continue if there is an error
    fn warn(self, message: &str) -> Self;

    /// Log an info and continue if there is an error
    fn info(self, message: &str) -> Self;

    /// Log a debug and continue if there is an error
    fn debug(self, message: &str) -> Self;

    /// Log a trace and continue if there is an error
    fn trace(self, message: &str) -> Self;
}

impl std::error::Error for Error {}

impl Error {
    /// Created a new error which merge two value error by combining the expected value <br>
    /// <span>&#9888;</span> Check if the value that was provided (current field) is the same for both error, will return a ValueError if they are not equal
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::error::Error;
    ///
    /// let err1 = Error::ValueError{expected: "expected1".to_owned(), current: "current".to_owned()};
    /// let err2 = Error::ValueError{expected: "expected2".to_owned(), current: "current".to_owned()};
    ///
    /// err1.merge(err2);
    /// ```
    pub fn merge(self, other: Self) -> Self {
        match self {
            Self::ValueError {
                expected: expected1,
                current: current1,
            } => match other {
                Self::ValueError {
                    expected: expected2,
                    current: current2,
                } => {
                    if current1 == current2 {
                        Self::ValueError {
                            expected: expected1 + " or " + &expected2,
                            current: current1,
                        }
                    } else {
                        Self::ValueError {
                            expected: current1,
                            current: current2,
                        }
                    }
                }
                variant => Self::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: variant.kind(),
                },
            },
            variant => Self::VariantMismatchError {
                expected: ErrorKind::ValueError,
                current: variant.kind(),
            },
        }
    }

    /// Returns the type of the error
    pub fn kind(&self) -> ErrorKind {
        match self {
            Self::LengthError { .. } => ErrorKind::LengthError,
            Self::ValueError { .. } => ErrorKind::ValueError,
            Self::VariantMismatchError { .. } => ErrorKind::VariantMismatchError,
            Self::UnknownFormatError => ErrorKind::UnknownFormatError,
            Self::UnsupportedFormatError(_) => ErrorKind::UnsupportedFormatError,
            Self::ConversionError => ErrorKind::ConversionError,
            Self::VariantDefaultForbiddenError(_) => ErrorKind::VariantDefaultForbiddenError,
            Self::MissingValueError(_) => ErrorKind::MissingValueError,
            Self::IoError(_) => ErrorKind::IoError,
            Self::Utf8Error(_) => ErrorKind::Utf8Error,
            Self::UsvgError(_) => ErrorKind::UsvgError,
            Self::ImageError(_) => ErrorKind::ImageError,
            Self::PngError(_) => ErrorKind::PngError,
            Self::SerdeYamlError(_) => ErrorKind::SerdeYamlError,
            Self::PersistError(_) => ErrorKind::PersistError,
            Self::ConfyError(_) => ErrorKind::ConfyError,
            Self::Svg2PdfError(_) => ErrorKind::Svg2PdfError,
            #[cfg(feature = "gui")]
            Self::IcedError(_) => ErrorKind::IcedError,
            #[cfg(feature = "gui")]
            Self::IconError(_) => ErrorKind::IconError,
            #[cfg(feature = "auto_update")]
            Self::SelfUpdateError(_) => ErrorKind::SelfUpdateError,
            Self::ParseIntError(_) => ErrorKind::ParseIntError,
            Self::Svg2PdfConversionError(_) => ErrorKind::Svg2PdfConversionError,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::LengthError {
                expected_size,
                current_size,
            } => {
                if expected_size > current_size {
                    write!(
                        f,
                        "The object is too short: {} elements expected, {} found",
                        expected_size, current_size
                    )
                } else {
                    write!(
                        f,
                        "The object is too long: {} elements expected, {} found",
                        expected_size, current_size
                    )
                }
            }
            Self::ValueError { expected, current } => {
                write!(
                    f,
                    "The value provided was not expected: {} expected, {} provided",
                    expected, current
                )
            }
            Self::VariantMismatchError { expected, current } => {
                write!(f, "The variant provided was not expected: {} variant expected, {} variant provided", expected, current)
            }
            Self::UnknownFormatError => {
                write!(f, "The format of the data could not be determined")
            }
            Self::UnsupportedFormatError(format) => {
                write!(f, "The format {format} is not supported for this action")
            }
            Self::ConversionError => {
                write!(f, "An error happened during a conversion")
            }
            Self::VariantDefaultForbiddenError(structure) => write!(f, "The default {structure} should only be used to be replaced by another {structure}, not as a {structure} by itself"),
            Self::MissingValueError(value) => write!(f, "The value for {value} was not provided"),
            Self::IoError(err) => err.fmt(f),
            Self::Utf8Error(err) => err.fmt(f),
            Self::UsvgError(err) => err.fmt(f),
            Self::ImageError(err) => err.fmt(f),
            Self::PngError(err) => err.fmt(f),
            Self::SerdeYamlError(err) => err.fmt(f),
            Self::PersistError(err) => err.fmt(f),
            Self::ConfyError(err) => err.fmt(f),
            Self::Svg2PdfError(err) => err.fmt(f),
            #[cfg(feature="gui")]
            Self::IcedError(err) => err.fmt(f),
            #[cfg(feature="gui")]
            Self::IconError(err) => err.fmt(f),
            #[cfg(feature="auto_update")]
            Self::SelfUpdateError(err) => err.fmt(f),
            Self::ParseIntError(err) => err.fmt(f),
            Self::Svg2PdfConversionError(err) => err.fmt(f),
        }
    }
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        self.to_string() == other.to_string()
    }
}

impl From<std::str::Utf8Error> for Error {
    fn from(error: std::str::Utf8Error) -> Self {
        Self::Utf8Error(error)
    }
}

impl From<usvg::Error> for Error {
    fn from(error: usvg::Error) -> Self {
        Self::UsvgError(error)
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Self::IoError(error)
    }
}

impl From<image::ImageError> for Error {
    fn from(error: image::ImageError) -> Self {
        Self::ImageError(error)
    }
}

impl From<png::EncodingError> for Error {
    fn from(error: png::EncodingError) -> Self {
        Self::PngError(error)
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(error: serde_yaml::Error) -> Self {
        Self::SerdeYamlError(error)
    }
}

impl From<tempfile::PersistError> for Error {
    fn from(error: tempfile::PersistError) -> Self {
        Self::PersistError(error)
    }
}

impl From<confy::ConfyError> for Error {
    fn from(error: confy::ConfyError) -> Self {
        Self::ConfyError(error)
    }
}

impl From<svg2pdf::usvg::Error> for Error {
    fn from(error: svg2pdf::usvg::Error) -> Self {
        Self::Svg2PdfError(error)
    }
}

#[cfg(feature = "gui")]
impl From<iced::Error> for Error {
    fn from(error: iced::Error) -> Self {
        Self::IcedError(error)
    }
}

#[cfg(feature = "auto_update")]
impl From<self_update::errors::Error> for Error {
    fn from(error: self_update::errors::Error) -> Self {
        Self::SelfUpdateError(error)
    }
}

impl From<std::num::ParseIntError> for Error {
    fn from(error: std::num::ParseIntError) -> Self {
        Self::ParseIntError(error)
    }
}

#[cfg(feature = "gui")]
impl From<iced::advanced::graphics::core::window::icon::Error> for Error {
    fn from(error: iced::advanced::graphics::core::window::icon::Error) -> Self {
        Self::IconError(error)
    }
}

impl From<svg2pdf::ConversionError> for Error {
    fn from(error: svg2pdf::ConversionError) -> Self {
        Self::Svg2PdfConversionError(error)
    }
}

impl<T, E: std::fmt::Display + std::fmt::Debug> LogError<T> for Result<T, E> {
    fn critical(self, message: &str) -> T {
        if let Err(ref err) = self {
            log::error!("{}: {}", message, err);
            #[cfg(feature = "gui")]
            if *GUI.read().expect("Failed to get read lock on GUI flag") {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Error")
                    .set_description(format!("{}: {}", message, err))
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
        }
        self.expect(message)
    }

    fn error(self, message: &str) -> Self {
        if let Err(ref err) = self {
            log::error!("{}: {}", message, err);
            #[cfg(feature = "gui")]
            if *GUI.read().expect("Failed to get read lock on GUI flag") {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Error")
                    .set_description(format!("{}: {}", message, err))
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
        }
        self
    }

    fn warn(self, message: &str) -> Self {
        if let Err(ref err) = self {
            log::warn!("{}: {}", message, err);
        }
        self
    }

    fn info(self, message: &str) -> Self {
        if let Err(ref err) = self {
            log::info!("{}: {}", message, err);
        }
        self
    }

    fn debug(self, message: &str) -> Self {
        if let Err(ref err) = self {
            log::debug!("{}: {}", message, err);
        }
        self
    }

    fn trace(self, message: &str) -> Self {
        if let Err(ref err) = self {
            log::trace!("{}: {}", message, err);
        }
        self
    }
}

impl LogError<()> for Error {
    fn critical(self, message: &str) {
        log::error!("{}: {}", message, self);
        #[cfg(feature = "gui")]
        if *GUI.read().expect("Failed to get read lock on GUI flag") {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Error")
                .set_description(format!("{}: {}", message, self))
                .set_buttons(MessageButtons::Ok)
                .show();
        }
        panic!("{}: {:?}", message, self);
    }

    fn error(self, message: &str) -> Self {
        log::error!("{}: {}", message, self);
        #[cfg(feature = "gui")]
        if *GUI.read().expect("Failed to get read lock on GUI flag") {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Error")
                .set_description(format!("{}: {}", message, self))
                .set_buttons(MessageButtons::Ok)
                .show();
        }
        self
    }

    fn warn(self, message: &str) -> Self {
        log::warn!("{}: {}", message, self);
        self
    }

    fn info(self, message: &str) -> Self {
        log::info!("{}: {}", message, self);
        self
    }

    fn debug(self, message: &str) -> Self {
        log::debug!("{}: {}", message, self);
        self
    }

    fn trace(self, message: &str) -> Self {
        log::trace!("{}: {}", message, self);
        self
    }
}

impl<T> LogError<T> for Option<T> {
    fn critical(self, message: &str) -> T {
        if let Some(val) = self {
            val
        } else {
            log::error!("{}", message);
            #[cfg(feature = "gui")]
            if *GUI.read().expect("Failed to get read lock on GUI flag") {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Error")
                    .set_description(message)
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
            panic!("{}", message);
        }
    }

    fn error(self, message: &str) -> Self {
        if let Some(val) = self {
            Some(val)
        } else {
            log::error!("{}", message);
            #[cfg(feature = "gui")]
            if *GUI.read().expect("Failed to get read lock on GUI flag") {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Error")
                    .set_description(message)
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
            None
        }
    }

    fn warn(self, message: &str) -> Self {
        if let Some(val) = self {
            Some(val)
        } else {
            log::warn!("{}", message);
            None
        }
    }

    fn info(self, message: &str) -> Self {
        if let Some(val) = self {
            Some(val)
        } else {
            log::info!("{}", message);
            None
        }
    }

    fn debug(self, message: &str) -> Self {
        if let Some(val) = self {
            Some(val)
        } else {
            log::debug!("{}", message);
            None
        }
    }

    fn trace(self, message: &str) -> Self {
        if let Some(val) = self {
            Some(val)
        } else {
            log::trace!("{}", message);
            None
        }
    }
}

#[cfg(test)]
mod test_module_error {
    mod test_enum_error {

        use super::super::{Error, ErrorKind};

        use serde::de::Error as SerdeError;

        #[test]
        fn test_length_error() {
            assert_eq!(
                Error::LengthError {
                    expected_size: 5,
                    current_size: 2
                }
                .to_string(),
                "The object is too short: 5 elements expected, 2 found"
            );
            assert_eq!(
                Error::LengthError {
                    expected_size: 5,
                    current_size: 10
                }
                .to_string(),
                "The object is too long: 5 elements expected, 10 found"
            )
        }

        #[test]
        fn test_value_error() {
            assert_eq!(
                Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned()
                }
                .to_string(),
                "The value provided was not expected: bla expected, blabla provided"
            )
        }

        #[test]
        fn test_variant_mismatch_error() {
            assert_eq!(
                Error::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: ErrorKind::VariantMismatchError
                }
                .to_string(),
                format!(
                "The variant provided was not expected: {} variant expected, {} variant provided",
                ErrorKind::ValueError,
                ErrorKind::VariantMismatchError
            )
            )
        }

        #[test]
        fn test_value_error_merge() {
            assert_eq!(
                Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned()
                }
                .merge(Error::ValueError {
                    expected: "blabl".to_owned(),
                    current: "blabla".to_owned()
                })
                .to_string(),
                "The value provided was not expected: bla or blabl expected, blabla provided"
            );
            assert_eq!(
                Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabl".to_owned()
                }
                .merge(Error::ValueError {
                    expected: "blabl".to_owned(),
                    current: "blabla".to_owned()
                })
                .to_string(),
                "The value provided was not expected: blabl expected, blabla provided"
            );
            assert_eq!(
                Error::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: ErrorKind::UsvgError
                }
                .merge(Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned()
                })
                .to_string(),
                Error::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: ErrorKind::VariantMismatchError
                }
                .to_string()
            );
            assert_eq!(
                Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned()
                }
                .merge(Error::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: ErrorKind::VariantMismatchError
                })
                .to_string(),
                Error::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: ErrorKind::VariantMismatchError
                }
                .to_string()
            );
        }

        #[test]
        fn test_debug_trait() {
            assert_eq!(
                format!(
                    "{:?}",
                    Error::LengthError {
                        expected_size: 5,
                        current_size: 2,
                    }
                ),
                "LengthError { expected_size: 5, current_size: 2 }"
            );

            assert_eq!(
                format!(
                    "{:?}",
                    Error::ValueError {
                        expected: "expected_value".to_owned(),
                        current: "current_value".to_owned(),
                    }
                ),
                "ValueError { expected: \"expected_value\", current: \"current_value\" }"
            );

            assert_eq!(
                format!(
                    "{:?}",
                    Error::VariantMismatchError {
                        expected: ErrorKind::ValueError,
                        current: ErrorKind::UnknownFormatError,
                    }
                ),
                "VariantMismatchError { expected: ValueError, current: UnknownFormatError }"
            );

            assert_eq!(
                format!("{:?}", Error::UnknownFormatError),
                "UnknownFormatError"
            );

            assert_eq!(format!("{:?}", Error::ConversionError), "ConversionError");

            assert_eq!(
                format!(
                    "{:?}",
                    Error::VariantDefaultForbiddenError("color".to_owned())
                ),
                "VariantDefaultForbiddenError(\"color\")"
            );

            assert_eq!(
                format!("{:?}", Error::MissingValueError("color".to_owned())),
                "MissingValueError(\"color\")"
            );

            assert_eq!(
                format!(
                    "{:?}",
                    Error::IoError(std::io::Error::new(std::io::ErrorKind::Other, "TestError"))
                ),
                "IoError(Custom { kind: Other, error: \"TestError\" })"
            );

            #[allow(invalid_from_utf8)]
            let error = std::str::from_utf8(b"\xc3\x28").expect_err("A test should never fail");
            assert_eq!(
                format!("{:?}", error),
                "Utf8Error { valid_up_to: 0, error_len: Some(1) }"
            );

            assert_eq!(
                format!("{:?}", Error::UsvgError(usvg::Error::NotAnUtf8Str)),
                "UsvgError(NotAnUtf8Str)"
            );

            assert_eq!(
                format!(
                    "{:?}",
                    Error::Svg2PdfError(svg2pdf::usvg::Error::NotAnUtf8Str)
                ),
                "Svg2PdfError(NotAnUtf8Str)"
            );

            assert_eq!(
                format!(
                    "{:?}",
                    Error::ImageError(image::ImageError::Decoding(
                        image::error::DecodingError::from_format_hint(
                            image::error::ImageFormatHint::Unknown
                        ),
                    ))
                ),
                "ImageError(Decoding(DecodingError { format: Unknown, underlying: None }))"
            );

            assert_eq!(
                format!("{:?}", Error::PngError(png::EncodingError::LimitsExceeded)),
                "PngError(LimitsExceeded)"
            );

            let error = serde_yaml::Error::custom("Test");
            assert_eq!(
                format!("{:?}", Error::SerdeYamlError(error)),
                "SerdeYamlError(Error(\"Test\"))"
            );

            let error = tempfile::PersistError {
                error: std::io::Error::new(std::io::ErrorKind::Other, "Test"),
                file: tempfile::NamedTempFile::new().expect("A test should never fail"),
            };
            let error_copy = tempfile::PersistError {
                error: std::io::Error::new(std::io::ErrorKind::Other, "Test"),
                file: tempfile::NamedTempFile::new().expect("A test should never fail"),
            };
            assert_eq!(
                format!("{:?}", Error::PersistError(error)),
                format!("PersistError({:?})", error_copy),
            );

            assert_eq!(
                format!(
                    "{:?}",
                    Error::ConfyError(confy::ConfyError::DirectoryCreationFailed(
                        std::io::Error::new(std::io::ErrorKind::Other, "TestError")
                    ))
                ),
                format!(
                    "ConfyError({:?})",
                    confy::ConfyError::DirectoryCreationFailed(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "TestError"
                    ))
                )
            );

            #[cfg(feature = "gui")]
            assert_eq!(
                format!(
                    "{:?}",
                    Error::IcedError(iced::Error::ExecutorCreationFailed(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "TestError"
                    )))
                ),
                format!(
                    "IcedError({:?})",
                    iced::Error::ExecutorCreationFailed(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "TestError"
                    ))
                )
            );

            #[cfg(feature = "gui")]
            let error =
                iced::advanced::graphics::core::window::icon::Error::ByteCountNotDivisibleBy4 {
                    byte_count: 0,
                };
            #[cfg(feature = "gui")]
            let error_copy =
                iced::advanced::graphics::core::window::icon::Error::ByteCountNotDivisibleBy4 {
                    byte_count: 0,
                };
            #[cfg(feature = "gui")]
            assert_eq!(
                format!("{:?}", Error::IconError(error)),
                format!("IconError({:?})", error_copy)
            );

            #[cfg(feature = "auto_update")]
            assert_eq!(
                format!(
                    "{:?}",
                    Error::SelfUpdateError(self_update::errors::Error::Update("test".to_owned()))
                ),
                format!(
                    "SelfUpdateError({:?})",
                    self_update::errors::Error::Update("test".to_owned())
                )
            );

            let error = i32::from_str_radix("a12", 10).expect_err("A test should never fail");
            assert_eq!(
                format!("{:?}", Error::ParseIntError(error.clone())),
                format!("ParseIntError({:?})", error)
            );

            let error = svg2pdf::ConversionError::InvalidImage;
            assert_eq! {
                format!("{:?}", Error::Svg2PdfConversionError(error.clone())),
                format!("Svg2PdfConversionError({:?})", error)
            }
        }

        #[test]
        fn test_display() {
            assert_eq!(
                Error::UnknownFormatError.to_string(),
                "The format of the data could not be determined"
            );
            assert_eq!(
                Error::ConversionError.to_string(),
                "An error happened during a conversion"
            );

            assert_eq!(Error::VariantDefaultForbiddenError("color".to_owned()).to_string(), "The default color should only be used to be replaced by another color, not as a color by itself");

            assert_eq!(
                Error::MissingValueError("color".to_owned()).to_string(),
                "The value for color was not provided"
            );

            assert_eq!(
                Error::UnsupportedFormatError("test".to_owned()).to_string(),
                "The format test is not supported for this action"
            );

            #[allow(invalid_from_utf8)]
            let error = match std::str::from_utf8(b"\xc3\x28") {
                Ok(_) => panic!("An error was supposed to be thrown, but did not"),
                Err(err) => err,
            };
            assert_eq!(Error::from(error).to_string(), error.to_string());
            assert_eq!(
                Error::from(std::io::Error::new(std::io::ErrorKind::Other, "TestError"))
                    .to_string(),
                "TestError"
            );
            assert_eq!(
                Error::from(usvg::Error::NotAnUtf8Str).to_string(),
                usvg::Error::NotAnUtf8Str.to_string()
            );
            assert_eq!(
                Error::from(svg2pdf::usvg::Error::NotAnUtf8Str).to_string(),
                svg2pdf::usvg::Error::NotAnUtf8Str.to_string()
            );
            assert_eq!(
                Error::from(image::error::ImageError::Decoding(
                    image::error::DecodingError::from_format_hint(
                        image::error::ImageFormatHint::Unknown
                    )
                ))
                .to_string(),
                image::error::ImageError::Decoding(image::error::DecodingError::from_format_hint(
                    image::error::ImageFormatHint::Unknown
                ))
                .to_string()
            );
            assert_eq!(
                Error::from(png::EncodingError::LimitsExceeded).to_string(),
                png::EncodingError::LimitsExceeded.to_string()
            );

            let error = serde_yaml::Error::custom("Test");
            assert_eq!(
                Error::from(error).to_string(),
                serde_yaml::Error::custom("Test").to_string()
            );

            let error = tempfile::PersistError {
                error: std::io::Error::new(std::io::ErrorKind::Other, "Test"),
                file: tempfile::NamedTempFile::new().expect("A test should never fail"),
            };
            let error_copy = tempfile::PersistError {
                error: std::io::Error::new(std::io::ErrorKind::Other, "Test"),
                file: tempfile::NamedTempFile::new().expect("A test should never fail"),
            };
            assert_eq!(Error::from(error).to_string(), error_copy.to_string());

            let error = confy::ConfyError::DirectoryCreationFailed(std::io::Error::new(
                std::io::ErrorKind::Other,
                "TestError",
            ));
            let error_copy = confy::ConfyError::DirectoryCreationFailed(std::io::Error::new(
                std::io::ErrorKind::Other,
                "TestError",
            ));
            assert_eq!(Error::from(error).to_string(), error_copy.to_string());

            #[cfg(feature = "gui")]
            assert_eq!(
                Error::from(iced::Error::ExecutorCreationFailed(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "TestError"
                )))
                .to_string(),
                iced::Error::ExecutorCreationFailed(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "TestError"
                ))
                .to_string()
            );

            #[cfg(feature = "gui")]
            let error =
                iced::advanced::graphics::core::window::icon::Error::ByteCountNotDivisibleBy4 {
                    byte_count: 0,
                };
            #[cfg(feature = "gui")]
            let error_copy =
                iced::advanced::graphics::core::window::icon::Error::ByteCountNotDivisibleBy4 {
                    byte_count: 0,
                };
            #[cfg(feature = "gui")]
            assert_eq!(Error::from(error).to_string(), error_copy.to_string());

            #[cfg(feature = "auto_update")]
            assert_eq!(
                Error::from(self_update::errors::Error::Update("test".to_owned())).to_string(),
                Error::SelfUpdateError(self_update::errors::Error::Update("test".to_owned()))
                    .to_string()
            );

            let error = i32::from_str_radix("a12", 10).expect_err("A test should never fail");
            assert_eq!(Error::from(error.clone()).to_string(), error.to_string());

            let error = svg2pdf::ConversionError::InvalidImage;
            assert_eq!(Error::from(error.clone()).to_string(), error.to_string());
        }

        #[test]
        fn test_kind() {
            assert_eq!(
                Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned()
                }
                .kind(),
                ErrorKind::ValueError
            );
            assert_eq!(
                Error::VariantMismatchError {
                    expected: ErrorKind::ValueError,
                    current: ErrorKind::LengthError
                }
                .kind(),
                ErrorKind::VariantMismatchError
            );
            assert_eq!(
                Error::LengthError {
                    expected_size: 0,
                    current_size: 0
                }
                .kind(),
                ErrorKind::LengthError
            );
            assert_eq!(
                Error::UnknownFormatError.kind(),
                ErrorKind::UnknownFormatError
            );
            assert_eq!(
                Error::UnsupportedFormatError("test".to_owned()).kind(),
                ErrorKind::UnsupportedFormatError
            );
            assert_eq!(Error::ConversionError.kind(), ErrorKind::ConversionError);
            assert_eq!(
                Error::VariantDefaultForbiddenError("Color".to_owned()).kind(),
                ErrorKind::VariantDefaultForbiddenError
            );
            assert_eq!(
                Error::MissingValueError("test".to_owned()).kind(),
                ErrorKind::MissingValueError
            );
            #[allow(invalid_from_utf8)]
            let error = match std::str::from_utf8(b"\xc3\x28") {
                Ok(_) => panic!("An error was supposed to be thrown, but did not"),
                Err(err) => err,
            };
            assert_eq!(Error::from(error).kind(), ErrorKind::Utf8Error);
            assert_eq!(
                Error::from(std::io::Error::new(std::io::ErrorKind::Other, "TestError")).kind(),
                ErrorKind::IoError
            );
            assert_eq!(
                Error::from(usvg::Error::NotAnUtf8Str).kind(),
                ErrorKind::UsvgError
            );
            assert_eq!(
                Error::from(svg2pdf::usvg::Error::NotAnUtf8Str).kind(),
                ErrorKind::Svg2PdfError
            );
            assert_eq!(
                Error::from(image::error::ImageError::Decoding(
                    image::error::DecodingError::from_format_hint(
                        image::error::ImageFormatHint::Unknown
                    )
                ))
                .kind(),
                ErrorKind::ImageError
            );
            assert_eq!(
                Error::from(png::EncodingError::LimitsExceeded).kind(),
                ErrorKind::PngError
            );

            let error = serde_yaml::Error::custom("Test");
            assert_eq!(Error::from(error).kind(), ErrorKind::SerdeYamlError);

            let error = tempfile::PersistError {
                error: std::io::Error::new(std::io::ErrorKind::Other, "Test"),
                file: tempfile::NamedTempFile::new().expect("A test should never fail"),
            };
            assert_eq!(Error::from(error).kind(), ErrorKind::PersistError);

            let error = confy::ConfyError::DirectoryCreationFailed(std::io::Error::new(
                std::io::ErrorKind::Other,
                "TestError",
            ));
            assert_eq!(Error::from(error).kind(), ErrorKind::ConfyError);

            #[cfg(feature = "gui")]
            assert_eq!(
                Error::from(iced::Error::ExecutorCreationFailed(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "TestError"
                )))
                .kind(),
                ErrorKind::IcedError
            );

            #[cfg(feature = "gui")]
            let error =
                iced::advanced::graphics::core::window::icon::Error::ByteCountNotDivisibleBy4 {
                    byte_count: 0,
                };
            #[cfg(feature = "gui")]
            assert_eq!(Error::from(error).kind(), ErrorKind::IconError);

            #[cfg(feature = "auto_update")]
            assert_eq!(
                Error::from(self_update::errors::Error::Update("test".to_owned())).kind(),
                ErrorKind::SelfUpdateError
            );

            let error = i32::from_str_radix("a12", 10).expect_err("A test should never fail");
            assert_eq!(Error::from(error).kind(), ErrorKind::ParseIntError);

            let error = svg2pdf::ConversionError::InvalidImage;
            assert_eq!(Error::from(error).kind(), ErrorKind::Svg2PdfConversionError);
        }
    }

    mod test_trait_log_error {
        use super::super::{Error, LogError};

        use log::Level;
        use log_tester::LogTester;

        #[test]
        fn test_critical_error() {
            LogTester::start();

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let binding = std::panic::catch_unwind(|| {
                let error2 = Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned(),
                };
                error2.critical("test")
            })
            .expect_err("A test should never fail");

            let res: &String = binding
                .as_ref()
                .downcast_ref()
                .expect("A test should never fail");

            assert_eq!(*res, format!("test: {:?}", error));

            assert!(LogTester::contains(
                Level::Error,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_error_error() {
            LogTester::start();

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2 = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            assert_eq!(error2.error("test"), error);

            assert!(LogTester::contains(
                Level::Error,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_warn_error() {
            LogTester::start();

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2 = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            assert_eq!(error2.warn("test"), error);

            assert!(LogTester::contains(
                Level::Warn,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_info_error() {
            LogTester::start();

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2 = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            assert_eq!(error2.info("test"), error);

            assert!(LogTester::contains(
                Level::Info,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_debug_error() {
            LogTester::start();

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2 = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            assert_eq!(error2.debug("test"), error);

            assert!(LogTester::contains(
                Level::Debug,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_trace_error() {
            LogTester::start();

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2 = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            assert_eq!(error2.trace("test"), error);

            assert!(LogTester::contains(
                Level::Trace,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_critical_result() {
            LogTester::start();

            let res: Result<(), Error> = Ok(());
            assert_eq!(res.critical("test"), ());

            assert!(LogTester::len() == 0);

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let binding = std::panic::catch_unwind(|| {
                let error2: Result<(), Error> = Err(Error::ValueError {
                    expected: "bla".to_owned(),
                    current: "blabla".to_owned(),
                });
                error2.critical("test")
            })
            .expect_err("A test should never fail");

            let res: &String = binding
                .as_ref()
                .downcast_ref()
                .expect("A test should never fail");

            assert_eq!(*res, format!("test: {:?}", error));

            assert!(LogTester::contains(
                Level::Error,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_error_result() {
            LogTester::start();

            let res: Result<(), Error> = Ok(());

            assert_eq!(res.error("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2: Result<(), Error> = Err(Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            });

            assert_eq!(
                error2.error("test").expect_err("A test should never fail"),
                error
            );

            assert!(LogTester::contains(
                Level::Error,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_warn_result() {
            LogTester::start();

            let res: Result<(), Error> = Ok(());

            assert_eq!(res.warn("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2: Result<(), Error> = Err(Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            });

            assert_eq!(
                error2.warn("test").expect_err("A test should never fail"),
                error
            );

            assert!(LogTester::contains(
                Level::Warn,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_info_result() {
            LogTester::start();

            let res: Result<(), Error> = Ok(());

            assert_eq!(res.info("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2: Result<(), Error> = Err(Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            });

            assert_eq!(
                error2.info("test").expect_err("A test should never fail"),
                error
            );

            assert!(LogTester::contains(
                Level::Info,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_debug_result() {
            LogTester::start();

            let res: Result<(), Error> = Ok(());

            assert_eq!(res.debug("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2: Result<(), Error> = Err(Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            });

            assert_eq!(
                error2.debug("test").expect_err("A test should never fail"),
                error
            );

            assert!(LogTester::contains(
                Level::Debug,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_trace_result() {
            LogTester::start();

            let res: Result<(), Error> = Ok(());

            assert_eq!(res.trace("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let error = Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            };

            let error2: Result<(), Error> = Err(Error::ValueError {
                expected: "bla".to_owned(),
                current: "blabla".to_owned(),
            });

            assert_eq!(
                error2.trace("test").expect_err("A test should never fail"),
                error
            );

            assert!(LogTester::contains(
                Level::Trace,
                &format!("test: {}", error)
            ))
        }

        #[test]
        fn test_critical_option() {
            LogTester::start();

            let res = Some(());
            assert_eq!(res.critical("test"), ());

            assert!(LogTester::len() == 0);

            let none: Option<()> = None;

            let binding = std::panic::catch_unwind(|| none.critical("test"))
                .expect_err("A test should never fail");

            let res: &String = binding
                .as_ref()
                .downcast_ref()
                .expect("A test should never fail");

            assert_eq!(res.as_str(), "test");

            assert!(LogTester::contains(Level::Error, "test"))
        }

        #[test]
        fn test_error_option() {
            LogTester::start();

            let res = Some(());

            assert_eq!(res.error("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let none: Option<()> = None;

            assert_eq!(none.error("test"), none);

            assert!(LogTester::contains(Level::Error, "test"))
        }

        #[test]
        fn test_warn_option() {
            LogTester::start();

            let res = Some(());

            assert_eq!(res.warn("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let none: Option<()> = None;

            assert_eq!(none.warn("test"), none);

            assert!(LogTester::contains(Level::Warn, "test"))
        }

        #[test]
        fn test_info_option() {
            LogTester::start();

            let res = Some(());

            assert_eq!(res.info("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let none: Option<()> = None;

            assert_eq!(none.info("test"), none);

            assert!(LogTester::contains(Level::Info, "test"))
        }

        #[test]
        fn test_debug_option() {
            LogTester::start();

            let res = Some(());

            assert_eq!(res.debug("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let none: Option<()> = None;

            assert_eq!(none.debug("test"), none);

            assert!(LogTester::contains(Level::Debug, "test"))
        }

        #[test]
        fn test_trace_option() {
            LogTester::start();

            let res = Some(());

            assert_eq!(res.trace("test").expect("A test should never fail"), ());
            assert!(LogTester::len() == 0);

            let none: Option<()> = None;

            assert_eq!(none.trace("test"), none);

            assert!(LogTester::contains(Level::Trace, "test"))
        }
    }
}
