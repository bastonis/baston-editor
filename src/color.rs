// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Colors used for the cards

#[cfg(feature = "gui")]
use crate::color_picker;
use crate::error::{Error, LogError};

use std::cmp::{Ord, Ordering};
use std::sync::RwLock;

#[cfg(feature = "gui")]
use iced::widget::PickList;
use lazy_static::lazy_static;
use regex::RegexBuilder;
use serde::{de::Error as SerdeError, Deserialize, Serialize};
use serde_yaml::Value;
use strum::IntoEnumIterator;
use strum_macros::{Display, EnumIter};

lazy_static! {
    /// List of available colors and custom ones already used in the card
    pub static ref COLORS: RwLock<Vec<Color>> =
        RwLock::new({
            let mut res = Color::iter()
                .filter(|color| color != &Color::Default)
                .collect::<Vec<Color>>();
            res.sort();
            res
        });
}

/// Represents a color
///
/// During deserialization, names in french are translated for backward compatibility
#[derive(Debug, Serialize, Deserialize, Copy, Clone, PartialEq, EnumIter, Display, Hash, Eq)]
pub enum Color {
    /// The color white (#ffffffff)
    #[serde(alias = "Blanc")]
    #[serde(alias = "BLANC")]
    White,
    /// The color gray (#c0c0c0ff)
    #[serde(alias = "Gris")]
    #[serde(alias = "GRIS")]
    Gray,
    /// The color black (#000000ff)
    #[serde(alias = "Noir")]
    #[serde(alias = "NOIR")]
    Black,
    /// The color blue (#0000ffff)
    #[serde(alias = "Bleu")]
    #[serde(alias = "BLEU")]
    Blue,
    /// The color light blue (#a8d0feff)
    #[serde(alias = "Bleu Clair")]
    #[serde(alias = "BLEU_CLAIR")]
    LightBlue,
    /// The color space blue (#08a8dcff)
    #[serde(alias = "Bleu Space")]
    #[serde(alias = "BLEU_SPACE")]
    SpaceBlue,
    /// The color blue band (#2a6cb7ff)
    #[serde(alias = "Bleu Band")]
    #[serde(alias = "BLEU_BAND")]
    BlueBand,
    /// The color dwarf blue (#0e2e9dff)
    #[serde(alias = "Bleu Nain")]
    #[serde(alias = "BLEU_NAIN")]
    DwarfBlue,
    /// The color red (#ff0000ff)
    #[serde(alias = "Rouge")]
    #[serde(alias = "ROUGE")]
    Red,
    /// The color red stat (#da4630ff)
    #[serde(alias = "Rouge Stat")]
    #[serde(alias = "ROUGE_STAT")]
    RedStat,
    /// The color red chaos (#ba3030ff)
    #[serde(alias = "Rouge Chaos")]
    #[serde(alias = "ROUGE_CHAOS")]
    ChaosRed,
    /// The color red band (#e26e4cff)
    #[serde(alias = "Rouge Band")]
    #[serde(alias = "ROUGE_BAND")]
    RedBand,
    /// The color lime (#00ff00ff)
    #[serde(alias = "Vert")]
    #[serde(alias = "VERT")]
    Lime,
    /// The color green (#008000ff)
    #[serde(alias = "Vert Ecr")]
    #[serde(alias = "VERT_ECR")]
    Green,
    /// The color goblin green (#1e741dff)
    #[serde(alias = "Vert Gb")]
    #[serde(alias = "VERT_GB")]
    GoblinGreen,
    /// The color forest green (#596101ff)
    #[serde(alias = "Vert Forest")]
    #[serde(alias = "VERT_FOREST")]
    ForestGreen,
    /// The color elf green (#81e907ff)
    #[serde(alias = "Vert Elfe")]
    #[serde(alias = "VERT_ELFE")]
    ElfGreen,
    /// The color arrancar (#18698aff)
    #[serde(alias = "ARRANC")]
    #[serde(alias = "Arranc")]
    Arrancar,
    /// The color brown (#5b2f0fff)
    #[serde(alias = "Brun")]
    #[serde(alias = "BRUN")]
    Brown,
    /// The color human brown (#cd8669ff)
    #[serde(alias = "Brun Homm")]
    #[serde(alias = "BRUN_HOMM")]
    HumanBrown,
    /// The color yellow (#ffff00ff)
    #[serde(alias = "Jaune")]
    #[serde(alias = "JAUNE")]
    Yellow,
    /// The color monster yellow (#ffdc00ff)
    #[serde(alias = "Jaune Mons")]
    #[serde(alias = "JAUNE_MONS")]
    MonsterYellow,
    /// The color skaven beige (#efc495ff)
    #[serde(alias = "Beige Skaven")]
    #[serde(alias = "BEIGE_SKAVEN")]
    SkavenBeige,
    /// The color imperial beige (#ffddd4ff)
    #[serde(alias = "Beige Imp")]
    #[serde(alias = "BEIGE_IMP")]
    ImperialBeige,
    /// The color purple (#8a35a5ff)
    #[serde(alias = "Violet")]
    #[serde(alias = "VIOLET")]
    Purple,
    /// The color human purple (#3b256dff)
    #[serde(alias = "Violet Homm")]
    #[serde(alias = "VIOLET_HOMM")]
    HumanPurple,
    /// The color turquoise (#3ae2b5ff)
    #[serde(alias = "Turquoise Manga")]
    #[serde(alias = "TURQUOISE_MANGA")]
    Turquoise,
    /// The color orange (#f2822cff)
    #[serde(alias = "ORANGE")]
    Orange,
    /// The color beige stat (#f1ecdaff)
    #[serde(alias = "Orange Stat")]
    #[serde(alias = "ORANGE_STAT")]
    #[serde(alias = "OrangeStat")]
    BeigeStat,
    /// The color pink (#ff5bd8ff)
    #[serde(alias = "Rose")]
    #[serde(alias = "ROSE")]
    Pink,
    /// A fully transparent color (#00000000)
    Transparent,
    /// Any color that can be represented by a rgba tuple
    #[serde(deserialize_with = "Color::custom_deserialize")]
    #[strum(to_string = "Custom({0}, {1}, {2}, {3})")]
    Custom(u8, u8, u8, u8),

    /// "The default color should only be used to be replaced by another color, not as a color by itself"
    #[serde(other)]
    Default,
}

impl Color {
    /// Gets the RGBA tuple representing the color
    ///
    /// <span>&#9888;</span> Calling this function on the default color will fail
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::color::Color;
    ///
    /// Color::Black.color();
    /// ```
    pub fn color(&self) -> (u8, u8, u8, u8) {
        match self {
            Self::Arrancar => (0x18, 0x69, 0x8a, 0xff),
            Self::ImperialBeige => (0xff, 0xdd, 0xd4, 0xff),
            Self::SkavenBeige => (0xef, 0xc4, 0x95, 0xff),
            Self::White => (0xff, 0xff, 0xff, 0xff),
            Self::Blue => (0x00, 0x00, 0xff, 0xff),
            Self::BlueBand => (0x2a, 0x6c, 0xb7, 0xff),
            Self::LightBlue => (0xa8, 0xd0, 0xfe, 0xff),
            Self::DwarfBlue => (0x0e, 0x2e, 0x9d, 0xff),
            Self::SpaceBlue => (0x08, 0xa8, 0xdc, 0xff),
            Self::Brown => (0x5b, 0x2f, 0x0f, 0xff),
            Self::HumanBrown => (0xcd, 0x86, 0x69, 0xff),
            Self::Gray => (0xc0, 0xc0, 0xc0, 0xff),
            Self::Yellow => (0xff, 0xff, 0x00, 0xff),
            Self::MonsterYellow => (0xff, 0xdc, 0x00, 0xff),
            Self::Black => (0x00, 0x00, 0x00, 0xff),
            Self::Orange => (0xf2, 0x80, 0x2c, 0xff),
            Self::BeigeStat => (0xf1, 0xec, 0xda, 0xff),
            Self::Pink => (0xff, 0x5b, 0xd8, 0xff),
            Self::Red => (0xff, 0x00, 0x00, 0xff),
            Self::RedBand => (0xe2, 0x6e, 0x4c, 0xff),
            Self::ChaosRed => (0xba, 0x30, 0x30, 0xff),
            Self::RedStat => (0xda, 0x46, 0x30, 0xff),
            Self::Transparent => (0x00, 0x00, 0x00, 0x00),
            Self::Turquoise => (0x3a, 0xe2, 0xb5, 0xff),
            Self::Lime => (0x00, 0xff, 0x00, 0xff),
            Self::Green => (0x00, 0x80, 0x00, 0xff),
            Self::ElfGreen => (0x81, 0xe9, 0x07, 0xff),
            Self::ForestGreen => (0x59, 0x61, 0x01, 0xff),
            Self::GoblinGreen => (0x1e, 0x74, 0x1d, 0xff),
            Self::Purple => (0x8a, 0x39, 0xa5, 0xff),
            Self::HumanPurple => (0x3b, 0x25, 0x6d, 0xff),
            Self::Custom(r, g, b, a) => (*r, *g, *b, *a),
            Self::Default => panic!(
                "{}",
                Error::VariantDefaultForbiddenError("color".to_owned()).to_string()
            ),
        }
    }

    /// Return the HSV representation of the color
    ///
    /// <span>&#9888;</span> Calling this function on the default color will fail
    ///
    /// # Examples
    ///
    /// ```
    /// use baston_editor::color::Color;
    ///
    /// Color::Black.get_hsv();
    /// ```
    ///
    pub fn get_hsv(&self) -> (f32, f32, f32) {
        let (r, g, b, _) = self.color();
        let (r, g, b) = (r as f32 / 255., g as f32 / 255., b as f32 / 255.);
        let c_max = if r > g {
            if r > b {
                r
            } else {
                b
            }
        } else if b > g {
            b
        } else {
            g
        };
        let c_min = if r < g {
            if r < b {
                r
            } else {
                b
            }
        } else if b < g {
            b
        } else {
            g
        };
        let hue = if c_min == c_max {
            0.
        } else if c_max == r {
            let tmp = 60. * ((g - b) / (c_max - c_min));
            if tmp < 0. {
                tmp + 360.
            } else {
                tmp
            }
        } else if c_max == g {
            60. * ((b - r) / (c_max - c_min) + 2.)
        } else {
            60. * ((r - b) / (c_max - c_min) + 4.)
        };

        let saturation = if c_max == 0. {
            0.
        } else {
            (c_max - c_min) / c_max
        };

        let value = c_max;

        (hue, saturation, value)
    }

    /// Returns the HTML code of the color, missing the opacity
    ///
    /// <span>&#9888;</span> Calling this function on the default color will fail
    ///
    /// # Example
    ///
    /// ```
    /// use baston_editor::color::Color;
    ///
    /// assert_eq!(Color::Red.get_html(), "#ff0000");
    /// ```
    pub fn get_html(&self) -> String {
        let (red, green, blue, _) = self.color();
        format!("#{:02x}{:02x}{:02x}", red, green, blue)
    }

    /// Returns the opacity of the color, in a normalized value (between 0 and 1)
    ///
    /// <span>&#9888;</span> Calling this function on the default color will fail
    ///
    /// # Example
    ///
    /// ```
    /// use baston_editor::color::Color;
    ///
    /// assert_eq!(Color::Red.get_opacity(), 1.);
    /// assert_eq!(Color::Transparent.get_opacity(), 0.);
    /// ```
    pub fn get_opacity(&self) -> f32 {
        let (_, _, _, opacity) = self.color();
        opacity as f32 / 255.0
    }

    /// Deserializes the content of a Custom Color
    ///
    /// This allow several format:
    /// * the HTML format in the format '\#rrggbbaa' with '\#' and 'aa' optional
    /// * the sequence of four u8 in the format RGBA with A optional
    ///
    /// # Exemples
    /// ```
    /// use baston_editor::color::Color;
    ///
    /// assert_eq!(
    ///     serde_yaml::from_str::<Color>("!Custom 0aff12").unwrap(),
    ///     Color::Custom(10, 255, 18, 255)
    /// );
    /// assert_eq!(
    ///     serde_yaml::from_str::<Color>("!Custom 0aff1221").unwrap(),
    ///     Color::Custom(10, 255, 18, 33)
    /// );
    /// assert_eq!(
    ///     serde_yaml::from_str::<Color>("!Custom\n- 0\n- 11\n- 22").unwrap(),
    ///     Color::Custom(0, 11, 22, 255)
    /// );
    /// assert_eq!(
    ///     serde_yaml::from_str::<Color>("!Custom\n- 0\n- 11\n- 22\n- 33").unwrap(),
    ///     Color::Custom(0, 11, 22, 33)
    /// );
    /// ```
    fn custom_deserialize<'de, D>(deserializer: D) -> Result<(u8, u8, u8, u8), D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        match Value::deserialize(deserializer)? {
            Value::String(data) => Ok(Color::try_from(&data).map_err(D::Error::custom)?.color()),
            Value::Number(data) => Ok(Color::try_from(&format!("{}", data))
                .map_err(D::Error::custom)?
                .color()),
            Value::Sequence(data) => match data.len() {
                4 => Ok((
                    u8::deserialize(data[0].clone()).map_err(D::Error::custom)?,
                    u8::deserialize(data[1].clone()).map_err(D::Error::custom)?,
                    u8::deserialize(data[2].clone()).map_err(D::Error::custom)?,
                    u8::deserialize(data[3].clone()).map_err(D::Error::custom)?,
                )),
                3 => Ok((
                    u8::deserialize(data[0].clone()).map_err(D::Error::custom)?,
                    u8::deserialize(data[1].clone()).map_err(D::Error::custom)?,
                    u8::deserialize(data[2].clone()).map_err(D::Error::custom)?,
                    255,
                )),
                _ => Err(D::Error::custom(Error::LengthError {
                    expected_size: 4,
                    current_size: data.len(),
                })),
            },
            Value::Mapping(data) => Ok((
                match data.get("r") {
                    Some(value) => u8::deserialize(value.clone()).map_err(D::Error::custom)?,
                    None => 0,
                },
                match data.get("g") {
                    Some(value) => u8::deserialize(value.clone()).map_err(D::Error::custom)?,
                    None => 0,
                },
                match data.get("b") {
                    Some(value) => u8::deserialize(value.clone()).map_err(D::Error::custom)?,
                    None => 0,
                },
                match data.get("a") {
                    Some(value) => u8::deserialize(value.clone()).map_err(D::Error::custom)?,
                    None => 255,
                },
            )),
            _ => Err(D::Error::custom(Error::ConversionError)),
        }
    }

    /// Creates a new color from RGBA
    pub fn from_rgba(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self::Custom(
            (r * 255.0) as u8,
            (g * 255.0) as u8,
            (b * 255.0) as u8,
            (a * 255.0) as u8,
        )
        .closest()
    }

    /// Calls to color picker if the color is `Custom(0, 0, 0, 0)`
    ///
    /// `default_color` is returned if color picker did not return a color
    #[cfg(feature = "gui")]
    pub fn process_custom(&self, default_color: Self) -> Self {
        let color = if let Self::Custom(0, 0, 0, 0) = self {
            if let Some(color) = color_picker::select_color(default_color) {
                color
            } else {
                default_color
            }
        } else {
            *self
        };
        color.closest()
    }
    #[cfg(not(feature = "gui"))]
    pub fn process_custom(&self, _default_color: Self) -> Self {
        self.closest()
    }

    /// Returns the closest color from ['COLORS'](static@COLORS) to the one given in argument
    ///
    /// If no colors are closed enough, the current color is added to ['COLORS'](static@COLORS) and returned
    pub fn closest(self) -> Self {
        let mut colors = COLORS
            .write()
            .critical("Failed to get write lock on COLORS");
        let color = colors
            .iter()
            .min_by_key(|other| self.distance(other))
            .critical("There should be at least one color");
        if self.distance(color) <= 250 {
            *color
        } else {
            colors.push(self);
            colors.sort();
            self
        }
    }

    /// Returns the distance between two colors
    ///
    /// The distance is the square of an euclidean distance on the rgba space
    pub fn distance(&self, other: &Self) -> u32 {
        let (r1, g1, b1, a1) = self.color();
        let (r2, g2, b2, a2) = other.color();
        let r = r1.abs_diff(r2) as u32;
        let g = g1.abs_diff(g2) as u32;
        let b = b1.abs_diff(b2) as u32;
        let a = a1.abs_diff(a2) as u32;
        r * r + g * g + b * b + a * a
    }

    /// Gets the list of possible colors
    ///
    /// This includes all the colors in ['COLORS'](static@COLORS)
    pub fn list() -> Vec<Self> {
        COLORS
            .read()
            .critical("Failed to get read lock on COLORS")
            .to_vec()
    }

    /// Creates a new pick list
    ///
    /// The possible values are the ones from [list](Self::list)
    ///
    /// # Arguments
    /// * `current` - The current value
    /// * `messaging` - The function to send messages
    #[cfg(feature = "gui")]
    pub fn pick_list<'a, Message, Messaging, Theme, Renderer>(
        current: Self,
        on_select: Messaging,
    ) -> PickList<'a, Self, Vec<Self>, Self, Message, Theme, Renderer>
    where
        Self: Sized + PartialEq + Clone + ToString,
        Message: Clone,
        Messaging: Fn(Self) -> Message + Copy + 'static,
        Theme: iced::widget::pick_list::Catalog,
        Renderer: iced::advanced::text::Renderer,
    {
        PickList::new(Self::list(), Some(current), move |value: Self| {
            on_select(value)
        })
    }
}

impl TryFrom<&String> for Color {
    type Error = Error;

    fn try_from(color: &String) -> Result<Self, Self::Error> {
        Self::try_from(color.as_str())
    }
}

impl TryFrom<&str> for Color {
    type Error = Error;

    fn try_from(color: &str) -> Result<Self, Self::Error> {
        let re = RegexBuilder::new(
            r"^\\?\#?(?<r>[0-9a-f]{2})(?<g>[0-9a-f]{2})(?<b>[0-9a-f]{2})(?<a>[0-9a-f]{2})?$",
        )
        .ignore_whitespace(true)
        .case_insensitive(true)
        .build()
        .critical("Error with the regex, should not happen");
        if let Some(capture) = re.captures(color) {
            Ok(Self::Custom(
                u8::from_str_radix(&capture["r"], 16).critical("This should never fail"),
                u8::from_str_radix(&capture["g"], 16).critical("This should never fail"),
                u8::from_str_radix(&capture["b"], 16).critical("This should never fail"),
                capture
                    .name("a")
                    .map_or(Ok(255), |v| u8::from_str_radix(v.as_str(), 16))
                    .critical("This should never fail"),
            ))
        } else {
            Err(Error::ConversionError)
        }
    }
}

impl Default for Color {
    fn default() -> Self {
        Self::Default
    }
}

impl Ord for Color {
    /// Compares the colors in order for them to be sorted according to HSV
    ///
    /// While panic if one of the colors is ['Default`](Color::Default)
    fn cmp(&self, other: &Self) -> Ordering {
        if let Self::Custom(0, 0, 0, 0) = self {
            if let Self::Custom(0, 0, 0, 0) = other {
                return Ordering::Equal;
            }
            return Ordering::Greater;
        }
        if let Self::Custom(0, 0, 0, 0) = other {
            return Ordering::Less;
        }
        if let Self::Transparent = self {
            if let Self::Transparent = other {
                return Ordering::Equal;
            }
            return Ordering::Greater;
        }
        if let Self::Transparent = other {
            return Ordering::Less;
        }

        let (hue_self, sat_self, value_self) = self.get_hsv();
        let (hue_other, sat_other, value_other) = other.get_hsv();

        if hue_self < hue_other {
            Ordering::Less
        } else if hue_self > hue_other {
            Ordering::Greater
        } else if sat_self < sat_other {
            Ordering::Less
        } else if sat_self > sat_other {
            Ordering::Greater
        } else if value_self < value_other {
            Ordering::Less
        } else if value_self > value_other {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

impl PartialOrd for Color {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl From<Color> for usvg::Color {
    fn from(color: Color) -> Self {
        let (red, green, blue, _) = color.color();
        Self { red, green, blue }
    }
}

#[cfg(feature = "gui")]
impl From<Color> for iced::Color {
    fn from(color: Color) -> Self {
        let (red, green, blue, alpha) = color.color();
        Self::from_rgba(
            red as f32 / 255.,
            green as f32 / 255.,
            blue as f32 / 255.,
            alpha as f32 / 255.,
        )
    }
}

#[cfg(feature = "gui")]
impl From<iced::Color> for Color {
    fn from(color: iced::Color) -> Self {
        Self::from_rgba(color.r, color.g, color.b, color.a)
    }
}

#[cfg(test)]
mod test_module_color {
    use super::Color;
    use crate::error::Error;

    use std::{cmp::Ordering, collections::BTreeMap};
    use strum::IntoEnumIterator;

    #[test]
    fn test_custom() {
        let s = vec!["00", "08", "80", "ff", "FF", ""];
        let i = vec![0, 8, 128, 255, 255, 255];
        for begin in ["", "\\#"] {
            for r in 0..5 {
                for g in 0..5 {
                    for b in 0..5 {
                        for a in 0..6 {
                            let color = serde_yaml::from_str::<Color>(&format!(
                                "!Custom {}{}{}{}{}",
                                begin, s[r], s[g], s[b], s[a]
                            ))
                            .expect("A test should never fail");
                            assert_eq!(color, Color::Custom(i[r], i[g], i[b], i[a]));
                        }
                    }
                }
            }
        }
        for begin in ["", "#"] {
            for r in 0..5 {
                for g in 0..5 {
                    for b in 0..5 {
                        for a in 0..6 {
                            let color = serde_yaml::from_str::<Color>(&format!(
                                "!Custom \"{}{}{}{}{}\"",
                                begin, s[r], s[g], s[b], s[a]
                            ))
                            .expect("A test should never fail");
                            assert_eq!(color, Color::Custom(i[r], i[g], i[b], i[a]));
                        }
                    }
                }
            }
        }
        for r in 0..2 {
            for g in 0..2 {
                for b in 0..2 {
                    for a in 0..2 {
                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\n- {}\n- {}\n- {}\n- {}",
                            i[r], i[g], i[b], i[a]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(i[r], i[g], i[b], i[a]));
                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\n- {}\n- {}\n- {}",
                            i[r], i[g], i[b]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(i[r], i[g], i[b], 255));

                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\nr: {}\ng: {}\nb: {}\na: {}",
                            i[r], i[g], i[b], i[a]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(i[r], i[g], i[b], i[a]));
                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\ng: {}\nb: {}\na: {}",
                            i[g], i[b], i[a]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(0, i[g], i[b], i[a]));
                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\nr: {}\nb: {}\na: {}",
                            i[r], i[b], i[a]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(i[r], 0, i[b], i[a]));
                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\nr: {}\ng: {}\n\na: {}",
                            i[r], i[g], i[a]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(i[r], i[g], 0, i[a]));
                        let color = serde_yaml::from_str::<Color>(&format!(
                            "!Custom\nr: {}\ng: {}\nb: {}",
                            i[r], i[g], i[b]
                        ))
                        .expect("A test should never fail");
                        assert_eq!(color, Color::Custom(i[r], i[g], i[b], 255));
                    }
                }
            }
        }
        let color = serde_yaml::from_str::<Color>("!Custom\nSomething stupid: with something else")
            .expect("A test should never fail");
        assert_eq!(color, Color::Custom(0, 0, 0, 255));
    }

    #[test]
    fn test_custom_fail() {
        let color = serde_yaml::from_str::<Color>("!Custom abcdefgh").map_err(Error::from);
        assert_eq!(color, Err(Error::ConversionError));
        let color = serde_yaml::from_str::<Color>("!Custom\n- 0\n- 0").map_err(Error::from);
        assert_eq!(
            color,
            Err(Error::LengthError {
                expected_size: 4,
                current_size: 2
            })
        );
        let color =
            serde_yaml::from_str::<Color>("!Custom\n- 0\n- 0\n- 0\n- 0\n- 0").map_err(Error::from);
        assert_eq!(
            color,
            Err(Error::LengthError {
                expected_size: 4,
                current_size: 5
            })
        );
        let color = serde_yaml::from_str::<Color>("!Custom\nSomething stupid").map_err(Error::from);
        assert_eq!(color, Err(Error::ConversionError));
        let color =
            serde_yaml::from_str::<Color>("!Custom: Something stupid").map_err(|e| e.to_string());
        assert_eq!(
            color,
            Err("invalid value: string \"Something stupid\", expected null".to_owned())
        );
        let color = serde_yaml::from_str::<Color>("!Something stupid").map_err(|e| e.to_string());
        assert_eq!(
            color,
            Err("invalid value: string \"stupid\", expected null".to_owned())
        );

        let err = serde_yaml::from_str::<Color>("!Custom").expect_err("A test should never fail");
        assert_eq!(Error::from(err), Error::ConversionError)
    }

    #[test]
    fn test_color() {
        assert_eq!(Color::Arrancar.color(), (0x18, 0x69, 0x8a, 0xff));
        assert_eq!(Color::ImperialBeige.color(), (0xff, 0xdd, 0xd4, 0xff));
        assert_eq!(Color::SkavenBeige.color(), (0xef, 0xc4, 0x95, 0xff));
        assert_eq!(Color::White.color(), (0xff, 0xff, 0xff, 0xff));
        assert_eq!(Color::Blue.color(), (0x00, 0x00, 0xff, 0xff));
        assert_eq!(Color::BlueBand.color(), (0x2a, 0x6c, 0xb7, 0xff));
        assert_eq!(Color::LightBlue.color(), (0xa8, 0xd0, 0xfe, 0xff));
        assert_eq!(Color::DwarfBlue.color(), (0x0e, 0x2e, 0x9d, 0xff));
        assert_eq!(Color::SpaceBlue.color(), (0x08, 0xa8, 0xdc, 0xff));
        assert_eq!(Color::Brown.color(), (0x5b, 0x2f, 0x0f, 0xff));
        assert_eq!(Color::HumanBrown.color(), (0xcd, 0x86, 0x69, 0xff));
        assert_eq!(Color::Gray.color(), (0xc0, 0xc0, 0xc0, 0xff));
        assert_eq!(Color::Yellow.color(), (0xff, 0xff, 0x00, 0xff));
        assert_eq!(Color::MonsterYellow.color(), (0xff, 0xdc, 0x00, 0xff));
        assert_eq!(Color::Black.color(), (0x00, 0x00, 0x00, 0xff));
        assert_eq!(Color::Orange.color(), (0xf2, 0x80, 0x2c, 0xff));
        assert_eq!(Color::BeigeStat.color(), (0xf1, 0xec, 0xda, 0xff));
        assert_eq!(Color::Pink.color(), (0xff, 0x5b, 0xd8, 0xff));
        assert_eq!(Color::Red.color(), (0xff, 0x00, 0x00, 0xff));
        assert_eq!(Color::RedBand.color(), (0xe2, 0x6e, 0x4c, 0xff));
        assert_eq!(Color::ChaosRed.color(), (0xba, 0x30, 0x30, 0xff));
        assert_eq!(Color::RedStat.color(), (0xda, 0x46, 0x30, 0xff));
        assert_eq!(Color::Transparent.color(), (0x00, 0x00, 0x00, 0x00));
        assert_eq!(Color::Turquoise.color(), (0x3a, 0xe2, 0xb5, 0xff));
        assert_eq!(Color::Lime.color(), (0x00, 0xff, 0x00, 0xff));
        assert_eq!(Color::Green.color(), (0x00, 0x80, 0x00, 0xff));
        assert_eq!(Color::ElfGreen.color(), (0x81, 0xe9, 0x07, 0xff));
        assert_eq!(Color::ForestGreen.color(), (0x59, 0x61, 0x01, 0xff));
        assert_eq!(Color::GoblinGreen.color(), (0x1e, 0x74, 0x1d, 0xff));
        assert_eq!(Color::Purple.color(), (0x8a, 0x39, 0xa5, 0xff));
        assert_eq!(Color::HumanPurple.color(), (0x3b, 0x25, 0x6d, 0xff));
        assert_eq!(Color::Custom(2, 10, 2, 23).color(), (2, 10, 2, 23));
    }

    #[test]
    fn test_to_usvg() {
        assert_eq!(
            usvg::Color::from(Color::Arrancar),
            usvg::Color {
                red: 0x18,
                green: 0x69,
                blue: 0x8a
            }
        );
        assert_eq!(
            usvg::Color::from(Color::ImperialBeige),
            usvg::Color {
                red: 0xff,
                green: 0xdd,
                blue: 0xd4
            }
        );
        assert_eq!(
            usvg::Color::from(Color::SkavenBeige),
            usvg::Color {
                red: 0xef,
                green: 0xc4,
                blue: 0x95
            }
        );
        assert_eq!(
            usvg::Color::from(Color::White),
            usvg::Color {
                red: 0xff,
                green: 0xff,
                blue: 0xff
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Blue),
            usvg::Color {
                red: 0x00,
                green: 0x00,
                blue: 0xff
            }
        );
        assert_eq!(
            usvg::Color::from(Color::BlueBand),
            usvg::Color {
                red: 0x2a,
                green: 0x6c,
                blue: 0xb7
            }
        );
        assert_eq!(
            usvg::Color::from(Color::LightBlue),
            usvg::Color {
                red: 0xa8,
                green: 0xd0,
                blue: 0xfe
            }
        );
        assert_eq!(
            usvg::Color::from(Color::DwarfBlue),
            usvg::Color {
                red: 0x0e,
                green: 0x2e,
                blue: 0x9d
            }
        );
        assert_eq!(
            usvg::Color::from(Color::SpaceBlue),
            usvg::Color {
                red: 0x08,
                green: 0xa8,
                blue: 0xdc
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Brown),
            usvg::Color {
                red: 0x5b,
                green: 0x2f,
                blue: 0x0f
            }
        );
        assert_eq!(
            usvg::Color::from(Color::HumanBrown),
            usvg::Color {
                red: 0xcd,
                green: 0x86,
                blue: 0x69
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Gray),
            usvg::Color {
                red: 0xc0,
                green: 0xc0,
                blue: 0xc0
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Yellow),
            usvg::Color {
                red: 0xff,
                green: 0xff,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::MonsterYellow),
            usvg::Color {
                red: 0xff,
                green: 0xdc,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Black),
            usvg::Color {
                red: 0x00,
                green: 0x00,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Orange),
            usvg::Color {
                red: 0xf2,
                green: 0x80,
                blue: 0x2c
            }
        );
        assert_eq!(
            usvg::Color::from(Color::BeigeStat),
            usvg::Color {
                red: 0xf1,
                green: 0xec,
                blue: 0xda
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Pink),
            usvg::Color {
                red: 0xff,
                green: 0x5b,
                blue: 0xd8
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Red),
            usvg::Color {
                red: 0xff,
                green: 0x00,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::RedBand),
            usvg::Color {
                red: 0xe2,
                green: 0x6e,
                blue: 0x4c
            }
        );
        assert_eq!(
            usvg::Color::from(Color::ChaosRed),
            usvg::Color {
                red: 0xba,
                green: 0x30,
                blue: 0x30
            }
        );
        assert_eq!(
            usvg::Color::from(Color::RedStat),
            usvg::Color {
                red: 0xda,
                green: 0x46,
                blue: 0x30
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Transparent),
            usvg::Color {
                red: 0x00,
                green: 0x00,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Turquoise),
            usvg::Color {
                red: 0x3a,
                green: 0xe2,
                blue: 0xb5
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Lime),
            usvg::Color {
                red: 0x00,
                green: 0xff,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Green),
            usvg::Color {
                red: 0x00,
                green: 0x80,
                blue: 0x00
            }
        );
        assert_eq!(
            usvg::Color::from(Color::ElfGreen),
            usvg::Color {
                red: 0x81,
                green: 0xe9,
                blue: 0x07
            }
        );
        assert_eq!(
            usvg::Color::from(Color::ForestGreen),
            usvg::Color {
                red: 0x59,
                green: 0x61,
                blue: 0x01
            }
        );
        assert_eq!(
            usvg::Color::from(Color::GoblinGreen),
            usvg::Color {
                red: 0x1e,
                green: 0x74,
                blue: 0x1d
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Purple),
            usvg::Color {
                red: 0x8a,
                green: 0x39,
                blue: 0xa5
            }
        );
        assert_eq!(
            usvg::Color::from(Color::HumanPurple),
            usvg::Color {
                red: 0x3b,
                green: 0x25,
                blue: 0x6d
            }
        );
        assert_eq!(
            usvg::Color::from(Color::Custom(2, 10, 2, 23)),
            usvg::Color {
                red: 2,
                green: 10,
                blue: 2
            }
        );
    }

    #[test]
    fn test_html() {
        assert_eq!(Color::Arrancar.get_html(), "#18698a");
        assert_eq!(Color::ImperialBeige.get_html(), "#ffddd4");
        assert_eq!(Color::SkavenBeige.get_html(), "#efc495");
        assert_eq!(Color::White.get_html(), "#ffffff");
        assert_eq!(Color::Blue.get_html(), "#0000ff");
        assert_eq!(Color::BlueBand.get_html(), "#2a6cb7");
        assert_eq!(Color::LightBlue.get_html(), "#a8d0fe");
        assert_eq!(Color::DwarfBlue.get_html(), "#0e2e9d");
        assert_eq!(Color::SpaceBlue.get_html(), "#08a8dc");
        assert_eq!(Color::Brown.get_html(), "#5b2f0f");
        assert_eq!(Color::HumanBrown.get_html(), "#cd8669");
        assert_eq!(Color::Gray.get_html(), "#c0c0c0");
        assert_eq!(Color::Yellow.get_html(), "#ffff00");
        assert_eq!(Color::MonsterYellow.get_html(), "#ffdc00");
        assert_eq!(Color::Black.get_html(), "#000000");
        assert_eq!(Color::Orange.get_html(), "#f2802c");
        assert_eq!(Color::BeigeStat.get_html(), "#f1ecda");
        assert_eq!(Color::Pink.get_html(), "#ff5bd8");
        assert_eq!(Color::Red.get_html(), "#ff0000");
        assert_eq!(Color::RedBand.get_html(), "#e26e4c");
        assert_eq!(Color::ChaosRed.get_html(), "#ba3030");
        assert_eq!(Color::RedStat.get_html(), "#da4630");
        assert_eq!(Color::Transparent.get_html(), "#000000");
        assert_eq!(Color::Turquoise.get_html(), "#3ae2b5");
        assert_eq!(Color::Lime.get_html(), "#00ff00");
        assert_eq!(Color::Green.get_html(), "#008000");
        assert_eq!(Color::ElfGreen.get_html(), "#81e907");
        assert_eq!(Color::ForestGreen.get_html(), "#596101");
        assert_eq!(Color::GoblinGreen.get_html(), "#1e741d");
        assert_eq!(Color::Purple.get_html(), "#8a39a5");
        assert_eq!(Color::HumanPurple.get_html(), "#3b256d");
        assert_eq!(Color::Custom(2, 10, 2, 23).get_html(), "#020a02");
    }

    #[test]
    fn test_opacity() {
        assert_eq!(Color::Arrancar.get_opacity(), 1.);
        assert_eq!(Color::ImperialBeige.get_opacity(), 1.);
        assert_eq!(Color::SkavenBeige.get_opacity(), 1.);
        assert_eq!(Color::White.get_opacity(), 1.);
        assert_eq!(Color::Blue.get_opacity(), 1.);
        assert_eq!(Color::BlueBand.get_opacity(), 1.);
        assert_eq!(Color::LightBlue.get_opacity(), 1.);
        assert_eq!(Color::DwarfBlue.get_opacity(), 1.);
        assert_eq!(Color::SpaceBlue.get_opacity(), 1.);
        assert_eq!(Color::Brown.get_opacity(), 1.);
        assert_eq!(Color::HumanBrown.get_opacity(), 1.);
        assert_eq!(Color::Gray.get_opacity(), 1.);
        assert_eq!(Color::Yellow.get_opacity(), 1.);
        assert_eq!(Color::MonsterYellow.get_opacity(), 1.);
        assert_eq!(Color::Black.get_opacity(), 1.);
        assert_eq!(Color::Orange.get_opacity(), 1.);
        assert_eq!(Color::BeigeStat.get_opacity(), 1.);
        assert_eq!(Color::Pink.get_opacity(), 1.);
        assert_eq!(Color::Red.get_opacity(), 1.);
        assert_eq!(Color::RedBand.get_opacity(), 1.);
        assert_eq!(Color::ChaosRed.get_opacity(), 1.);
        assert_eq!(Color::RedStat.get_opacity(), 1.);
        assert_eq!(Color::Transparent.get_opacity(), 0.);
        assert_eq!(Color::Turquoise.get_opacity(), 1.);
        assert_eq!(Color::Lime.get_opacity(), 1.);
        assert_eq!(Color::Green.get_opacity(), 1.);
        assert_eq!(Color::ElfGreen.get_opacity(), 1.);
        assert_eq!(Color::ForestGreen.get_opacity(), 1.);
        assert_eq!(Color::GoblinGreen.get_opacity(), 1.);
        assert_eq!(Color::Purple.get_opacity(), 1.);
        assert_eq!(Color::HumanPurple.get_opacity(), 1.);
        assert_eq!(Color::Custom(2, 10, 2, 23).get_opacity(), 23. / 255.);
    }

    #[test]
    #[should_panic(
        expected = "The default color should only be used to be replaced by another color, not as a color by itself"
    )]
    fn test_default_panic() {
        Color::Default.color();
    }

    #[test]
    fn test_default() {
        assert_eq!(Color::default(), Color::Default)
    }

    #[test]
    fn test_debug() {
        assert_eq!(format!("{:?}", Color::White), "White");
        assert_eq!(format!("{:?}", Color::Gray), "Gray");
        assert_eq!(format!("{:?}", Color::Black), "Black");
        assert_eq!(format!("{:?}", Color::Blue), "Blue");
        assert_eq!(format!("{:?}", Color::LightBlue), "LightBlue");
        assert_eq!(format!("{:?}", Color::SpaceBlue), "SpaceBlue");
        assert_eq!(format!("{:?}", Color::BlueBand), "BlueBand");
        assert_eq!(format!("{:?}", Color::DwarfBlue), "DwarfBlue");
        assert_eq!(format!("{:?}", Color::Red), "Red");
        assert_eq!(format!("{:?}", Color::RedStat), "RedStat");
        assert_eq!(format!("{:?}", Color::ChaosRed), "ChaosRed");
        assert_eq!(format!("{:?}", Color::RedBand), "RedBand");
        assert_eq!(format!("{:?}", Color::Lime), "Lime");
        assert_eq!(format!("{:?}", Color::Green), "Green");
        assert_eq!(format!("{:?}", Color::GoblinGreen), "GoblinGreen");
        assert_eq!(format!("{:?}", Color::ForestGreen), "ForestGreen");
        assert_eq!(format!("{:?}", Color::ElfGreen), "ElfGreen");
        assert_eq!(format!("{:?}", Color::Arrancar), "Arrancar");
        assert_eq!(format!("{:?}", Color::Brown), "Brown");
        assert_eq!(format!("{:?}", Color::HumanBrown), "HumanBrown");
        assert_eq!(format!("{:?}", Color::Yellow), "Yellow");
        assert_eq!(format!("{:?}", Color::MonsterYellow), "MonsterYellow");
        assert_eq!(format!("{:?}", Color::SkavenBeige), "SkavenBeige");
        assert_eq!(format!("{:?}", Color::ImperialBeige), "ImperialBeige");
        assert_eq!(format!("{:?}", Color::Purple), "Purple");
        assert_eq!(format!("{:?}", Color::HumanPurple), "HumanPurple");
        assert_eq!(format!("{:?}", Color::Turquoise), "Turquoise");
        assert_eq!(format!("{:?}", Color::Orange), "Orange");
        assert_eq!(format!("{:?}", Color::BeigeStat), "BeigeStat");
        assert_eq!(format!("{:?}", Color::Pink), "Pink");
        assert_eq!(
            format!("{:?}", Color::Custom(2, 10, 2, 23)),
            "Custom(2, 10, 2, 23)"
        );
        assert_eq!(format!("{:?}", Color::Transparent), "Transparent");
        assert_eq!(format!("{:?}", Color::Default), "Default");
    }

    #[test]
    fn test_display_color() {
        assert_eq!(format!("{}", Color::White), "White");
        assert_eq!(format!("{}", Color::Gray), "Gray");
        assert_eq!(format!("{}", Color::Black), "Black");
        assert_eq!(format!("{}", Color::Blue), "Blue");
        assert_eq!(format!("{}", Color::LightBlue), "LightBlue");
        assert_eq!(format!("{}", Color::SpaceBlue), "SpaceBlue");
        assert_eq!(format!("{}", Color::BlueBand), "BlueBand");
        assert_eq!(format!("{}", Color::DwarfBlue), "DwarfBlue");
        assert_eq!(format!("{}", Color::Red), "Red");
        assert_eq!(format!("{}", Color::RedStat), "RedStat");
        assert_eq!(format!("{}", Color::ChaosRed), "ChaosRed");
        assert_eq!(format!("{}", Color::RedBand), "RedBand");
        assert_eq!(format!("{}", Color::Lime), "Lime");
        assert_eq!(format!("{}", Color::Green), "Green");
        assert_eq!(format!("{}", Color::GoblinGreen), "GoblinGreen");
        assert_eq!(format!("{}", Color::ForestGreen), "ForestGreen");
        assert_eq!(format!("{}", Color::ElfGreen), "ElfGreen");
        assert_eq!(format!("{}", Color::Arrancar), "Arrancar");
        assert_eq!(format!("{}", Color::Brown), "Brown");
        assert_eq!(format!("{}", Color::HumanBrown), "HumanBrown");
        assert_eq!(format!("{}", Color::Yellow), "Yellow");
        assert_eq!(format!("{}", Color::MonsterYellow), "MonsterYellow");
        assert_eq!(format!("{}", Color::SkavenBeige), "SkavenBeige");
        assert_eq!(format!("{}", Color::ImperialBeige), "ImperialBeige");
        assert_eq!(format!("{}", Color::Purple), "Purple");
        assert_eq!(format!("{}", Color::HumanPurple), "HumanPurple");
        assert_eq!(format!("{}", Color::Turquoise), "Turquoise");
        assert_eq!(format!("{}", Color::Orange), "Orange");
        assert_eq!(format!("{}", Color::BeigeStat), "BeigeStat");
        assert_eq!(format!("{}", Color::Pink), "Pink");
        assert_eq!(
            format!("{}", Color::Custom(2, 10, 2, 23)),
            "Custom(2, 10, 2, 23)"
        );
        assert_eq!(format!("{}", Color::Transparent), "Transparent");
        assert_eq!(format!("{}", Color::Default), "Default");
    }

    #[test]
    fn test_serialize() {
        assert_eq!(
            &serde_yaml::to_string(&Color::Custom(2, 10, 2, 23)).expect("A test should never fail"),
            "!Custom\n- 2\n- 10\n- 2\n- 23\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Default).expect("A test should never fail"),
            "Default\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::White).expect("A test should never fail"),
            "White\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Black).expect("A test should never fail"),
            "Black\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Transparent).expect("A test should never fail"),
            "Transparent\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Red).expect("A test should never fail"),
            "Red\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Green).expect("A test should never fail"),
            "Green\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Blue).expect("A test should never fail"),
            "Blue\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Yellow).expect("A test should never fail"),
            "Yellow\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Lime).expect("A test should never fail"),
            "Lime\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Pink).expect("A test should never fail"),
            "Pink\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Orange).expect("A test should never fail"),
            "Orange\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::RedStat).expect("A test should never fail"),
            "RedStat\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::BeigeStat).expect("A test should never fail"),
            "BeigeStat\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Brown).expect("A test should never fail"),
            "Brown\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Gray).expect("A test should never fail"),
            "Gray\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::BlueBand).expect("A test should never fail"),
            "BlueBand\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::MonsterYellow).expect("A test should never fail"),
            "MonsterYellow\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::RedBand).expect("A test should never fail"),
            "RedBand\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::ChaosRed).expect("A test should never fail"),
            "ChaosRed\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::ForestGreen).expect("A test should never fail"),
            "ForestGreen\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::ElfGreen).expect("A test should never fail"),
            "ElfGreen\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::GoblinGreen).expect("A test should never fail"),
            "GoblinGreen\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Purple).expect("A test should never fail"),
            "Purple\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::HumanPurple).expect("A test should never fail"),
            "HumanPurple\n"
        );
        assert_eq!(
            &serde_yaml::to_string(&Color::Turquoise).expect("A test should never fail"),
            "Turquoise\n"
        );
    }

    #[test]
    fn test_deserialize() {
        assert_eq!(
            Color::Custom(2, 10, 2, 23),
            serde_yaml::from_str::<Color>("!Custom\n- 2\n- 10\n- 2\n- 23\n")
                .expect("A test should never fail")
        );
        assert_eq!(
            Color::Default,
            serde_yaml::from_str::<Color>("Default\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::White,
            serde_yaml::from_str::<Color>("White\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Black,
            serde_yaml::from_str::<Color>("Black\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Transparent,
            serde_yaml::from_str::<Color>("Transparent\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Red,
            serde_yaml::from_str::<Color>("Red\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Green,
            serde_yaml::from_str::<Color>("Green\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Blue,
            serde_yaml::from_str::<Color>("Blue\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Yellow,
            serde_yaml::from_str::<Color>("Yellow\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Lime,
            serde_yaml::from_str::<Color>("Lime\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Pink,
            serde_yaml::from_str::<Color>("Pink\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Orange,
            serde_yaml::from_str::<Color>("Orange\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::RedStat,
            serde_yaml::from_str::<Color>("RedStat\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::BeigeStat,
            serde_yaml::from_str::<Color>("BeigeStat\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Brown,
            serde_yaml::from_str::<Color>("Brown\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Gray,
            serde_yaml::from_str::<Color>("Gray\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::BlueBand,
            serde_yaml::from_str::<Color>("BlueBand\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::MonsterYellow,
            serde_yaml::from_str::<Color>("MonsterYellow\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::RedBand,
            serde_yaml::from_str::<Color>("RedBand\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::ChaosRed,
            serde_yaml::from_str::<Color>("ChaosRed\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::ForestGreen,
            serde_yaml::from_str::<Color>("ForestGreen\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::ElfGreen,
            serde_yaml::from_str::<Color>("ElfGreen\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::GoblinGreen,
            serde_yaml::from_str::<Color>("GoblinGreen\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Purple,
            serde_yaml::from_str::<Color>("Purple\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::HumanPurple,
            serde_yaml::from_str::<Color>("HumanPurple\n").expect("A test should never fail")
        );
        assert_eq!(
            Color::Turquoise,
            serde_yaml::from_str::<Color>("Turquoise\n").expect("A test should never fail")
        );
    }

    #[test]
    fn test_clone() {
        assert_eq!(
            Color::Custom(2, 10, 2, 23).clone(),
            Color::Custom(2, 10, 2, 23)
        );
        assert_eq!(Color::Default.clone(), Color::Default);
        assert_eq!(Color::White.clone(), Color::White);
        assert_eq!(Color::Black.clone(), Color::Black);
        assert_eq!(Color::Transparent.clone(), Color::Transparent);
        assert_eq!(Color::Red.clone(), Color::Red);
        assert_eq!(Color::Green.clone(), Color::Green);
        assert_eq!(Color::Blue.clone(), Color::Blue);
        assert_eq!(Color::Yellow.clone(), Color::Yellow);
        assert_eq!(Color::Lime.clone(), Color::Lime);
        assert_eq!(Color::Pink.clone(), Color::Pink);
        assert_eq!(Color::Orange.clone(), Color::Orange);
        assert_eq!(Color::RedStat.clone(), Color::RedStat);
        assert_eq!(Color::BeigeStat.clone(), Color::BeigeStat);
        assert_eq!(Color::Brown.clone(), Color::Brown);
        assert_eq!(Color::Gray.clone(), Color::Gray);
        assert_eq!(Color::BlueBand.clone(), Color::BlueBand);
        assert_eq!(Color::MonsterYellow.clone(), Color::MonsterYellow);
        assert_eq!(Color::RedBand.clone(), Color::RedBand);
        assert_eq!(Color::ChaosRed.clone(), Color::ChaosRed);
        assert_eq!(Color::ForestGreen.clone(), Color::ForestGreen);
        assert_eq!(Color::ElfGreen.clone(), Color::ElfGreen);
        assert_eq!(Color::GoblinGreen.clone(), Color::GoblinGreen);
        assert_eq!(Color::Purple.clone(), Color::Purple);
        assert_eq!(Color::HumanPurple.clone(), Color::HumanPurple);
        assert_eq!(Color::Turquoise.clone(), Color::Turquoise);
    }

    #[test]
    fn test_iter() {
        let mut iter = Color::iter();
        assert_eq!(iter.next(), Some(Color::White));
        assert_eq!(iter.next(), Some(Color::Gray));
        assert_eq!(iter.next(), Some(Color::Black));
        assert_eq!(iter.next(), Some(Color::Blue));
        assert_eq!(iter.next(), Some(Color::LightBlue));
        assert_eq!(iter.next(), Some(Color::SpaceBlue));
        assert_eq!(iter.next(), Some(Color::BlueBand));
        assert_eq!(iter.next(), Some(Color::DwarfBlue));
        assert_eq!(iter.next(), Some(Color::Red));
        assert_eq!(iter.next(), Some(Color::RedStat));
        assert_eq!(iter.next(), Some(Color::ChaosRed));
        assert_eq!(iter.next(), Some(Color::RedBand));
        assert_eq!(iter.next(), Some(Color::Lime));
        assert_eq!(iter.next(), Some(Color::Green));
        assert_eq!(iter.next(), Some(Color::GoblinGreen));
        assert_eq!(iter.next(), Some(Color::ForestGreen));
        assert_eq!(iter.next(), Some(Color::ElfGreen));
        assert_eq!(iter.next(), Some(Color::Arrancar));
        assert_eq!(iter.next(), Some(Color::Brown));
        assert_eq!(iter.next(), Some(Color::HumanBrown));
        assert_eq!(iter.next(), Some(Color::Yellow));
        assert_eq!(iter.next(), Some(Color::MonsterYellow));
        assert_eq!(iter.next(), Some(Color::SkavenBeige));
        assert_eq!(iter.next(), Some(Color::ImperialBeige));
        assert_eq!(iter.next(), Some(Color::Purple));
        assert_eq!(iter.next(), Some(Color::HumanPurple));
        assert_eq!(iter.next(), Some(Color::Turquoise));
        assert_eq!(iter.next(), Some(Color::Orange));
        assert_eq!(iter.next(), Some(Color::BeigeStat));
        assert_eq!(iter.next(), Some(Color::Pink));
        assert_eq!(iter.next(), Some(Color::Transparent));
        assert_eq!(iter.next(), Some(Color::Custom(0, 0, 0, 0)));
        assert_eq!(iter.next(), Some(Color::Default));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_rgba() {
        assert_eq!(Color::from_rgba(1., 1., 1., 1.), Color::White);
        assert_eq!(Color::from_rgba(0., 0., 0., 1.), Color::Black);
        assert_eq!(Color::from_rgba(0., 0., 0., 0.), Color::Transparent);
    }

    #[test]
    #[cfg(feature = "gui")]
    fn test_pick_list() {
        Color::pick_list::<_, _, iced::Theme, iced::Renderer>(Color::Black, |_| ());
    }

    #[test]
    fn test_process_custom() {
        assert_eq!(Color::Red, Color::process_custom(&Color::Red, Color::Black))
    }

    #[test]
    fn test_closest() {
        for color in Color::iter() {
            if color != Color::Default && color != Color::Custom(0, 0, 0, 0) {
                println!("{color:?}");
                assert_eq!(color, color.closest());
            }
        }
        assert_eq!(
            Color::Custom(80, 80, 80, 80),
            Color::Custom(80, 80, 80, 80).closest()
        );
        assert_eq!(Color::Transparent, Color::Custom(0, 0, 0, 0).closest());
    }

    #[test]
    fn test_ord() {
        for color in Color::iter() {
            if color != Color::Default {
                assert_eq!(color.cmp(&color), Ordering::Equal);

                if color != Color::Custom(0, 0, 0, 0) {
                    println!("{color:?}");
                    assert!(color < Color::Custom(0, 0, 0, 0));

                    if color != Color::Transparent {
                        assert!(color < Color::Transparent);
                    }
                }
            }
        }
        assert!(Color::Custom(0, 0, 0, 0) > Color::Transparent);
    }

    #[test]
    #[cfg(feature = "gui")]
    fn test_from_color_for_iced_color() {
        assert_eq!(iced::Color::from_rgba(1., 1., 1., 1.), Color::White.into());
    }

    #[test]
    #[cfg(feature = "gui")]
    fn test_from_iced_color() {
        assert_eq!(
            Color::White,
            Color::from(iced::Color::from_rgba(1., 1., 1., 1.))
        );
    }

    #[test]
    #[ignore]
    fn test_color_distance() {
        let mut map = BTreeMap::new();
        for color in Color::list() {
            for color2 in Color::list() {
                if color > color2
                    && color != Color::Custom(0, 0, 0, 0)
                    && color2 != Color::Custom(0, 0, 0, 0)
                {
                    map.insert(color.distance(&color2), (color, color2));
                }
            }
        }
        println!("Color Distances : {map:?}");
        println!(
            "Shortest : {:?}",
            map.first_key_value().expect("A test should never fail")
        );
        panic!("Because I want it")
    }
}
