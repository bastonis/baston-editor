// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! module for images, SVGs and raster images

use super::{Group, Point, Size};
use crate::color::Color;
use crate::error::{Error, LogError};

use std::ffi::OsStr;
use std::fs::File;
use std::io::{Cursor, Read, Seek, SeekFrom};
use std::path::Path;

#[cfg(feature = "gui")]
use iced::window::Icon;
use image::ImageFormat;
use image::ImageReader;
use regex::Regex;
use serde::{Deserialize, Serialize};
use tempfile::tempfile;
use usvg::{Image as ImageNode, ImageKind, ImageRendering};
use usvg::{Node, Tree, TreeParsing};
use usvg::{Options, TreePostProc};
use usvg::{Transform, Visibility};

/// Trait shared by all types that represent an image
pub trait Image {
    /// Turns the current object into a Group with the specified scale
    fn to_scale_node<S: std::fmt::Display + Clone>(
        self,
        _id: S,
        _position: Point,
        _scale: f32,
    ) -> Group;

    /// Turns the current object into a Group with the specified width
    ///
    /// If no width is provided, uses the width of the object
    fn to_width_node<S: std::fmt::Display + Clone>(
        self,
        _id: S,
        _position: Point,
        _width: Option<f32>,
    ) -> Group;

    /// Returns the size of the image
    fn size(&self) -> Size;
}

/// Represents a rasterized image (for example PNG, JPEG, BMP, ...)
#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct RasterImage {
    /// The size of the image
    size: Size,
    /// The format of the image
    format: RasterImageFormat,
    /// The data of the image in the specified format
    #[serde(with = "raster_image_serde")]
    data: Vec<u8>,
}

/// Represents the format of a rasterized image
#[derive(Debug, PartialEq, Deserialize, Serialize, Copy, Clone)]
enum RasterImageFormat {
    /// PNG format
    Png,
    /// JPEG format
    Jpeg,
    /// GIF format
    Gif,
}

/// Represents a SVG file
#[derive(Debug, PartialEq, Clone)]
pub struct Svg {
    /// The content of the SVG file
    content: String,
    /// The name of the Layer we want to use
    id: String,
}

impl Image for RasterImage {
    fn to_scale_node<S: std::fmt::Display + Clone>(
        self,
        id: S,
        position: Point,
        scale: f32,
    ) -> Group {
        let width = self.size().width;
        self.to_width_node(id, position, Some(width * scale))
    }

    fn to_width_node<S: std::fmt::Display + Clone>(
        self,
        id: S,
        position: Point,
        width: Option<f32>,
    ) -> Group {
        let mut group = Group::new(id.clone(), position);
        let image = ImageNode {
            id: format!("{}_image", id),
            visibility: Visibility::Visible,
            view_box: self.size().scale_to_width(width).into(),
            rendering_mode: ImageRendering::OptimizeQuality,
            kind: self.into(),
            abs_transform: Transform::default(),
            bounding_box: None,
        };
        group.append(Node::Image(Box::new(image)));
        group
    }

    fn size(&self) -> Size {
        self.size
    }
}

impl Image for Svg {
    fn to_scale_node<S: std::fmt::Display + Clone>(
        self,
        id: S,
        position: Point,
        scale: f32,
    ) -> Group {
        let id = if id.to_string().is_empty() {
            self.id.clone()
        } else {
            id.to_string()
        };
        let mut scale_node =
            Group::new(format!("{}scale", id), Transform::from_scale(scale, scale));
        scale_node.append(self);
        let shape: Point = scale_node
            .size()
            .critical("The SVG does not contain a size")
            .into();
        let mut position = Group::new(id.clone(), position - (shape * scale / 2.0));
        position.append(scale_node);
        position
    }

    fn to_width_node<S: std::fmt::Display + Clone>(
        self,
        id: S,
        position: Point,
        width: Option<f32>,
    ) -> Group {
        let id = if id.to_string().is_empty() {
            self.id.clone()
        } else {
            id.to_string()
        };
        let node: Node = self.into();
        let bounding_box = node
            .bounding_box()
            .unwrap_or_else(|| panic!("The Layer {} does not have a bounding box", id));
        let scale = if let Some(width) = width {
            width / bounding_box.width()
        } else {
            1.
        };
        let mut scale_node =
            Group::new(format!("{}scale", id), Transform::from_scale(scale, scale));
        scale_node.append(node);
        let shape: Point = scale_node
            .size()
            .critical("The SVG does not contain a size")
            .into();
        let mut position = Group::new(id.clone(), position - (shape * scale / 2.0));
        position.append(scale_node);
        position
    }

    /// Attention, this function is expensive
    fn size(&self) -> Size {
        let node: Node = self.into();
        let size: Size = node
            .bounding_box()
            .unwrap_or_else(|| panic!("The Layer {} does not have a bounding box", self.id))
            .into();
        size
    }
}

impl Svg {
    /// Creates a Svg Object from a String, representing a SVG File, and an id, being the name of the node of the File we want
    pub fn from_str<T: std::fmt::Display, U: std::fmt::Display>(content: T, id: U) -> Self {
        Self {
            content: content.to_string(),
            id: id.to_string(),
        }
    }

    /// Changes the fill color of the Object
    pub fn set_fill_color(&self, color: Color) -> Self {
        let re = Regex::new(r##"fill="#[a-fA-F0-9]{6}""##)
            .critical("Error with the regex, should not happen");
        let content = re
            .replace_all(&self.content, format!("fill=\"{}\"", color.get_html()))
            .to_string();

        let re =
            Regex::new(r"fill:#[a-fA-F0-9]{6}").critical("Error with the regex, should not happen");
        let content = re
            .replace_all(&content, format!("fill:{}", color.get_html()))
            .to_string();

        let re =
            Regex::new(r"fill-opacity:[0-9.]*").critical("Error with the regex, should not happen");
        let content = re
            .replace_all(&content, format!("fill-opacity:{}", color.get_opacity()))
            .to_string();

        let re = Regex::new(r#"fill-opacity="[0-9.]*""#)
            .critical("Error with the regex, should not happen");
        Self {
            content: re
                .replace_all(
                    &content,
                    format!("fill-opacity=\"{}\"", color.get_opacity()),
                )
                .to_string(),
            id: self.id.clone(),
        }
    }

    /// Changes the stroke color of the Object
    pub fn set_stroke_color(&self, color: Color) -> Self {
        let re = Regex::new(r##"stroke="#[a-fA-F0-9]{6}""##)
            .critical("Error with the regex, should not happen");
        let content = re
            .replace_all(&self.content, format!("stroke=\"{}\"", color.get_html()))
            .to_string();

        let re = Regex::new(r"stroke:#[a-fA-F0-9]{6}")
            .critical("Error with the regex, should not happen");
        let content = re
            .replace_all(&content, format!("stroke:{}", color.get_html()))
            .to_string();

        let re = Regex::new(r"stroke-opacity:[0-9.]*")
            .critical("Error with the regex, should not happen");
        let content = re
            .replace_all(&content, format!("stroke-opacity:{}", color.get_opacity()))
            .to_string();

        let re = Regex::new(r#"stroke-opacity="[0-9.]*""#)
            .critical("Error with the regex, should not happen");
        Self {
            content: re
                .replace_all(
                    &content,
                    format!("stroke-opacity=\"{}\"", color.get_opacity()),
                )
                .to_string(),
            id: self.id.clone(),
        }
    }
}

impl RasterImage {
    /// Creates a new Object from the given bytes
    ///
    /// If a format is specified, it will use it, else it will try to guess it using the image crate
    pub fn from_bytes(data: &[u8], format: Option<ImageFormat>) -> Result<Self, Error> {
        let image = match format {
            Some(format) => ImageReader::with_format(Cursor::new(data), format),
            None => ImageReader::new(Cursor::new(data)).with_guessed_format()?,
        };
        let format = match format {
            Some(format) => format,
            None => image.format().ok_or(Error::UnknownFormatError)?,
        };
        let (width, height) = image.into_dimensions()?;
        let (data, format) = match format {
            ImageFormat::Png | ImageFormat::Jpeg | ImageFormat::Gif => (
                data.to_vec(),
                format.try_into().critical("Should never happen"),
            ),
            _ => {
                let image = ImageReader::with_format(Cursor::new(data), format);
                let mut file = tempfile()?;
                image.decode()?.write_to(&mut file, ImageFormat::Png)?;
                let mut content = Vec::new();
                file.sync_data()?;
                file.seek(SeekFrom::Start(0))?;
                file.read_to_end(&mut content)?;
                (content, RasterImageFormat::Png)
            }
        };
        Ok(Self {
            data,
            size: Size {
                width: width as f32,
                height: height as f32,
            },
            format,
        })
    }

    /// Creates a new Object by reading the given file
    ///
    /// If a format is specified, it will use it, else it will try to guess it using the image crate
    pub fn open<P: AsRef<OsStr> + AsRef<Path>>(
        path: P,
        format: Option<ImageFormat>,
    ) -> Result<Self, Error> {
        let mut content = vec![];
        File::open(path)?.read_to_end(&mut content)?;
        Self::from_bytes(&content, format)
    }
}

#[cfg(feature = "gui")]
impl TryFrom<RasterImage> for Icon {
    type Error = Error;

    fn try_from(image: RasterImage) -> Result<Self, Self::Error> {
        let reader = ImageReader::with_format(Cursor::new(image.data), image.format.into());
        let data = reader.decode()?.to_rgba8().into_raw();
        Ok(iced::window::icon::from_rgba(
            data,
            image.size.width.round() as u32,
            image.size.height.round() as u32,
        )?)
    }
}

impl From<RasterImageFormat> for ImageFormat {
    fn from(format: RasterImageFormat) -> Self {
        match format {
            RasterImageFormat::Png => ImageFormat::Png,
            RasterImageFormat::Jpeg => ImageFormat::Jpeg,
            RasterImageFormat::Gif => ImageFormat::Gif,
        }
    }
}

/// Module to serialize and deserialize a `Vec<u8>` into a String
/// It works by encoding each u8 into two characters representing its hexadecimal value
mod raster_image_serde {
    use crate::error::LogError;

    use serde::Deserialize;

    /// Serialize a vector of u8 into a String using the hexadecimal notation for each byte
    pub fn serialize<S>(data: &[u8], serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut res = vec![0; data.len() * 2];
        for i in 0..data.len() {
            res[2 * i] = format!("{:1x}", data[i] / 16).as_bytes()[0];
            res[2 * i + 1] = format!("{:1x}", data[i] % 16).as_bytes()[0];
        }
        serializer.serialize_str(&String::from_utf8(res).critical("Should never fail"))
    }

    /// Deserialize a String into a vector of u8 using the hexadecimal notation for each byte
    pub fn deserialize<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let data = String::deserialize(deserializer)?;

        let mut new_data = vec![0; data.len() / 2];
        for i in (0..data.len()).step_by(2) {
            new_data[i / 2] =
                u8::from_str_radix(&data[i..i + 2], 16).map_err(|e| unreachable!("{}", e))?;
        }
        Ok(new_data)
    }
}

impl From<RasterImage> for ImageKind {
    fn from(image: RasterImage) -> Self {
        match image.format {
            RasterImageFormat::Png => ImageKind::PNG(image.data.into()),
            RasterImageFormat::Jpeg => ImageKind::JPEG(image.data.into()),
            RasterImageFormat::Gif => ImageKind::GIF(image.data.into()),
        }
    }
}

impl TryFrom<ImageFormat> for RasterImageFormat {
    type Error = Error;

    fn try_from(format: ImageFormat) -> Result<Self, Error> {
        match format {
            ImageFormat::Png => Ok(RasterImageFormat::Png),
            ImageFormat::Jpeg => Ok(RasterImageFormat::Jpeg),
            ImageFormat::Gif => Ok(RasterImageFormat::Gif),
            _ => Err(Error::ConversionError),
        }
    }
}

impl From<Svg> for Node {
    fn from(svg: Svg) -> Self {
        (&svg).into()
    }
}

impl From<&Svg> for Node {
    fn from(svg: &Svg) -> Self {
        let mut tree = Tree::from_str(&svg.content, &Options::default())
            .unwrap_or_else(|err| panic!("The file for {} seems corrupted: {}", svg.id, err));
        tree.postprocess(
            usvg::PostProcessingSteps::default(),
            &usvg::fontdb::Database::new(),
        );
        tree.node_by_id(&svg.id)
            .unwrap_or_else(|| panic!("The file for {} seems corrupted", svg.id))
            .clone()
    }
}

impl From<Svg> for Group {
    fn from(svg: Svg) -> Self {
        svg.to_scale_node("", Point::default(), 1.)
    }
}

#[cfg(test)]
mod test_module_image {
    #[cfg(test)]
    mod test_struct_raster_image {
        use super::super::RasterImage;

        use super::super::super::{Point, Size};
        use super::super::{Image, RasterImageFormat};

        use image::ImageFormat;

        #[test]
        fn test_serde_raster_image() {
            let mut data = Vec::<u8>::new();
            for i in 0..255 {
                data.push(i)
            }

            let image = RasterImage {
                size: Size {
                    width: 0.,
                    height: 0.,
                },
                format: RasterImageFormat::Png,
                data,
            };

            let string = serde_yaml::to_string(&image.clone()).expect("A test should never fail");

            assert_eq!(
                serde_yaml::from_str::<RasterImage>(&string).expect("A test should never fail"),
                image
            )
        }

        #[test]
        fn test_open_raster_image() {
            for format in vec!["bmp", "jpg", "png", "gif", "webp"] {
                let image = RasterImage::open(format!("tests/images/Alakir.{format}"), None)
                    .expect("A test should never fail");

                assert_eq!(
                    image.size(),
                    Size {
                        width: 1836.,
                        height: 1500.
                    }
                );

                assert_eq!(
                    image.format,
                    match format {
                        "jpg" | "jpeg" => RasterImageFormat::Jpeg,
                        "png" => RasterImageFormat::Png,
                        "gif" => RasterImageFormat::Gif,
                        _ => RasterImageFormat::Png,
                    }
                );

                let image2 = RasterImage::open(
                    format!("tests/images/Alakir.{format}"),
                    Some(ImageFormat::from_extension(format).expect("A test should never fail")),
                )
                .expect("A test should never fail");

                assert_eq!(image, image2);
            }
        }

        #[test]
        fn test_from_bytes_raster_image() {
            let image =
                RasterImage::from_bytes(include_bytes!("../../tests/images/Alakir.bmp"), None)
                    .expect("A test should never fail");

            assert_eq!(
                image.size(),
                Size {
                    width: 1836.,
                    height: 1500.
                }
            );

            assert_eq!(image.format, RasterImageFormat::Png);

            let image2 = RasterImage::from_bytes(
                include_bytes!("../../tests/images/Alakir.bmp"),
                Some(ImageFormat::Bmp),
            )
            .expect("A test should never fail");

            assert_eq!(image, image2);

            let image =
                RasterImage::from_bytes(include_bytes!("../../tests/images/Alakir.jpg"), None)
                    .expect("A test should never fail");

            assert_eq!(
                image.size(),
                Size {
                    width: 1836.,
                    height: 1500.
                }
            );

            assert_eq!(image.format, RasterImageFormat::Jpeg);

            let image2 = RasterImage::from_bytes(
                include_bytes!("../../tests/images/Alakir.jpg"),
                Some(ImageFormat::Jpeg),
            )
            .expect("A test should never fail");

            assert_eq!(image, image2);

            let image =
                RasterImage::from_bytes(include_bytes!("../../tests/images/Alakir.png"), None)
                    .expect("A test should never fail");

            assert_eq!(
                image.size(),
                Size {
                    width: 1836.,
                    height: 1500.
                }
            );

            assert_eq!(image.format, RasterImageFormat::Png);

            let image2 = RasterImage::from_bytes(
                include_bytes!("../../tests/images/Alakir.png"),
                Some(ImageFormat::Png),
            )
            .expect("A test should never fail");

            assert_eq!(image, image2);

            let image =
                RasterImage::from_bytes(include_bytes!("../../tests/images/Alakir.gif"), None)
                    .expect("A test should never fail");

            assert_eq!(
                image.size(),
                Size {
                    width: 1836.,
                    height: 1500.
                }
            );

            assert_eq!(image.format, RasterImageFormat::Gif);

            let image2 = RasterImage::from_bytes(
                include_bytes!("../../tests/images/Alakir.gif"),
                Some(ImageFormat::Gif),
            )
            .expect("A test should never fail");

            assert_eq!(image, image2);

            let image =
                RasterImage::from_bytes(include_bytes!("../../tests/images/Alakir.webp"), None)
                    .expect("A test should never fail");

            assert_eq!(
                image.size(),
                Size {
                    width: 1836.,
                    height: 1500.
                }
            );

            assert_eq!(image.format, RasterImageFormat::Png);

            let image2 = RasterImage::from_bytes(
                include_bytes!("../../tests/images/Alakir.webp"),
                Some(ImageFormat::WebP),
            )
            .expect("A test should never fail");

            assert_eq!(image, image2);
        }

        #[test]
        fn test_to_scale_node_raster_image() {
            let image = RasterImage::open(format!("tests/images/Alakir.png"), None)
                .expect("A test should never fail");
            let size = image.size.clone();

            let node = image.clone().to_scale_node("", Point::default(), 1.);
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_scale_node("", Point::default(), 0.5);
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image.clone().to_scale_node("", Point::default(), 2.);
            assert_eq!(node.size(), Some(size * 2.));

            let image = RasterImage::open(format!("tests/images/Alakir.jpg"), None)
                .expect("A test should never fail");
            let size = image.size.clone();

            let node = image.clone().to_scale_node("", Point::default(), 1.);
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_scale_node("", Point::default(), 0.5);
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image.clone().to_scale_node("", Point::default(), 2.);
            assert_eq!(node.size(), Some(size * 2.));

            let image = RasterImage::open(format!("tests/images/Alakir.gif"), None)
                .expect("A test should never fail");
            let size = image.size.clone();

            let node = image.clone().to_scale_node("", Point::default(), 1.);
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_scale_node("", Point::default(), 0.5);
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image.clone().to_scale_node("", Point::default(), 2.);
            assert_eq!(node.size(), Some(size * 2.));
        }

        #[test]
        fn test_to_width_node_raster_image() {
            let image = RasterImage::open(format!("tests/images/Alakir.png"), None)
                .expect("A test should never fail");
            let size = image.size.clone();

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width));
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_width_node("", Point::default(), None);
            assert_eq!(node.size(), Some(size));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 0.5));
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 2.));
            assert_eq!(node.size(), Some(size * 2.));

            let image = RasterImage::open(format!("tests/images/Alakir.jpg"), None)
                .expect("A test should never fail");
            let size = image.size.clone();

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width));
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_width_node("", Point::default(), None);
            assert_eq!(node.size(), Some(size));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 0.5));
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 2.));
            assert_eq!(node.size(), Some(size * 2.));

            let image = RasterImage::open(format!("tests/images/Alakir.gif"), None)
                .expect("A test should never fail");
            let size = image.size.clone();

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width));
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_width_node("", Point::default(), None);
            assert_eq!(node.size(), Some(size));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 0.5));
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 2.));
            assert_eq!(node.size(), Some(size * 2.));
        }

        use std::io::Write;
        use usvg::TreeWriting;

        #[test]
        #[ignore]
        fn test_show_raster_image() {
            for format in vec!["bmp", "jpg", "png", "gif", "webp"] {
                let image = RasterImage::open(format!("tests/images/Alakir.{format}"), None)
                    .expect("A test should never fail");

                let tree: usvg::Tree = image.to_scale_node("", Point::default(), 1.).into();
                let mut tempfile = tempfile::Builder::new()
                    .prefix(&format!("RasterImageTest.{}", format))
                    .suffix(".svg")
                    .tempfile()
                    .expect("A test should never fail");
                tempfile
                    .write_all(tree.to_string(&usvg::XmlOptions::default()).as_bytes())
                    .expect("A test should never fail");
                let (_, path) = tempfile.keep().expect("A test should never fail");
                open::that(path).expect("A test should never fail")
            }
        }

        #[test]
        fn test_debug_raster_image() {
            assert!(RasterImage::open(format!("tests/images/Alakir.png"), None).is_ok())
        }
    }

    #[cfg(test)]
    mod test_enum_raster_image_format {
        use super::super::RasterImageFormat;

        use crate::error::Error;

        use image::ImageFormat;

        #[test]
        fn test_conversion_raster_image_format() {
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Png).expect("A test should never fail"),
                RasterImageFormat::Png
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Jpeg).expect("A test should never fail"),
                RasterImageFormat::Jpeg
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Gif).expect("A test should never fail"),
                RasterImageFormat::Gif
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::WebP),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Pnm),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Tiff),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Tga),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Dds),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Bmp),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Ico),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Hdr),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::OpenExr),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Farbfeld),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Avif),
                Err(Error::ConversionError)
            );
            assert_eq!(
                RasterImageFormat::try_from(ImageFormat::Qoi),
                Err(Error::ConversionError)
            );

            assert_eq!(ImageFormat::from(RasterImageFormat::Png), ImageFormat::Png,);
            assert_eq!(
                ImageFormat::from(RasterImageFormat::Jpeg),
                ImageFormat::Jpeg,
            );
            assert_eq!(ImageFormat::from(RasterImageFormat::Gif), ImageFormat::Gif,)
        }

        #[test]
        fn test_debug_raster_image_format() {
            assert_eq!(format!("{:?}", RasterImageFormat::Png), "Png");
            assert_eq!(format!("{:?}", RasterImageFormat::Jpeg), "Jpeg");
            assert_eq!(format!("{:?}", RasterImageFormat::Gif), "Gif");
        }
    }

    #[cfg(test)]
    mod test_struct_svg {
        use super::super::Svg;

        use super::super::super::{Group, Point, Size};
        use super::super::Image;

        use crate::color::Color;

        #[test]
        fn test_from_str_svg() {
            let image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            let image2 = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            assert_eq!(image, image2);

            let group: Group = image.into();

            assert_eq!(
                group.size(),
                Some(Size {
                    width: 1961.8397,
                    height: 1705.7771
                })
            );
        }

        #[test]
        fn test_to_scale_node_svg() {
            let image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            let size = Size {
                width: 1961.8397,
                height: 1705.7771,
            };

            let node = image.clone().to_scale_node("", Point::default(), 1.);
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_scale_node("Image", Point::default(), 1.);
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_scale_node("", Point::default(), 0.5);
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image.clone().to_scale_node("Image", Point::default(), 0.5);
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image.clone().to_scale_node("", Point::default(), 2.);
            assert_eq!(node.size(), Some(size * 2.));

            let node = image.clone().to_scale_node("Image", Point::default(), 2.);
            assert_eq!(node.size(), Some(size * 2.));
        }

        #[test]
        fn test_to_width_node_svg() {
            let image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            let size = Size {
                width: 1961.8397,
                height: 1705.7771,
            };

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width));
            assert_eq!(node.size(), Some(size));

            let node = image
                .clone()
                .to_width_node("Image", Point::default(), Some(size.width));
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_width_node("", Point::default(), None);
            assert_eq!(node.size(), Some(size));

            let node = image.clone().to_width_node("Image", Point::default(), None);
            assert_eq!(node.size(), Some(size));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 0.5));
            assert_eq!(node.size(), Some(size * 0.5));

            let node =
                image
                    .clone()
                    .to_width_node("Image", Point::default(), Some(size.width * 0.5));
            assert_eq!(node.size(), Some(size * 0.5));

            let node = image
                .clone()
                .to_width_node("", Point::default(), Some(size.width * 2.));
            assert_eq!(node.size(), Some(size * 2.));

            let node =
                image
                    .clone()
                    .to_width_node("Image", Point::default(), Some(size.width * 2.));
            assert_eq!(node.size(), Some(size * 2.));
        }

        #[test]
        fn test_debug_svg() {
            let _image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");
        }

        #[test]
        fn test_fill_svg() {
            let image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            let image2 = image.set_fill_color(Color::Black);
            assert_ne!(image, image2);

            assert_eq!(image2, image2.set_fill_color(Color::Black));

            assert_eq!(
                image.set_fill_color(Color::Red),
                image2.set_fill_color(Color::Red)
            );

            assert_ne!(
                image.set_fill_color(Color::Custom(255, 0, 0, 255)),
                image.set_fill_color(Color::Custom(255, 0, 0, 0))
            );
        }

        #[test]
        fn test_stroke_svg() {
            let image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            let image2 = image.set_stroke_color(Color::Black);
            assert_ne!(image, image2);

            assert_eq!(image2, image2.set_stroke_color(Color::Black));

            assert_eq!(
                image.set_stroke_color(Color::Red),
                image2.set_stroke_color(Color::Red)
            );

            assert_ne!(
                image.set_stroke_color(Color::Custom(255, 0, 0, 255)),
                image.set_stroke_color(Color::Custom(255, 0, 0, 0))
            );
        }

        #[test]
        fn test_size_svg() {
            let image = Svg::from_str(include_str!("../../tests/images/Alakir.svg"), "Image");

            assert_eq!(
                image.size(),
                Size {
                    width: 1961.8397,
                    height: 1705.7771,
                }
            );
        }
    }
}
