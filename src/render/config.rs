// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for the config for rendering a [Card](crate::card::Card)

use super::{Point, Size};
use crate::card::fields::CharacteristicName;
use crate::card::illustration::{Mode, Orientation};
use crate::error::{Error, LogError};
use crate::render::Layer;

use std::collections::HashMap;

use serde::Deserialize;

/// Represents the config for rendering
#[derive(Debug)]
pub struct Config {
    /// The font for each layer that needs one
    fonts: HashMap<Layer, String>,
    /// The configuration for the things specific for the vertical orientation
    vertical: OrientationConfig,
    /// The configuration for the things specific for the horizontal orientation
    horizontal: OrientationConfig,
    /// The common part of the config
    common: CommonConfig,
    /// The orientation of the card
    ///
    /// Used to choose the right part of the config
    orientation: Orientation,
    /// The mode of the card
    ///
    /// Used to choose the right part of the config
    mode: Mode,
    /// Whether or not to use the compact fraction
    compact_fraction: bool,
}

/// Represents the part of the config specific for an orientation
#[derive(Deserialize, Debug)]
pub struct OrientationConfig {
    /// The position for each layer that needs one
    #[serde(default)]
    pub positions: HashMap<Layer, Point>,
    /// The position for each layer that needs one and is different for a specific mode
    #[serde(default)]
    pub positions_mode_dependent: HashMap<Mode, HashMap<Layer, Point>>,
    /// The configuration for each characteristic of [CharacteristicsField](crate::card::fields::CharacteristicsField)
    pub characteristics: HashMap<CharacteristicName, CharacteristicConfig>,
    /// The position for each supported number of symbols
    pub symbols_position: HashMap<u8, Vec<Point>>,
    /// The position for each supported number of pictograms
    pub pictograms_positions: HashMap<u8, Vec<Point>>,
    /// The position for each supported number of stars
    pub pictogram_stars: HashMap<u8, Vec<Point>>,
    /// The configuration for the pictograms
    pub pictogram_config: PictogramConfig,
    /// The scale of each layer that needs one
    #[serde(default)]
    pub scales: HashMap<Layer, f32>,
}

/// Represents the part of the config common for both orientation
#[derive(Deserialize, Debug)]
pub struct CommonConfig {
    /// The size of the [Card](crate::card::Card)
    pub size: Size,
    /// The view box of the [Card](crate::card::Card)
    pub view_box: Size,
    /// The dpi of the [Card](crate::card::Card)
    pub dpi: f32,
    /// The position for each layer that needs one
    #[serde(default)]
    pub positions: HashMap<Layer, Point>,
    /// The position for each layer that needs one and is different for a specific mode
    #[serde(default)]
    pub positions_mode_dependent: HashMap<Mode, HashMap<Layer, Point>>,
    /// The scale of each layer that needs one
    #[serde(default)]
    pub scales: HashMap<Layer, f32>,
    /// The width of each layer that needs one
    pub widths: HashMap<Layer, f32>,
}

/// Represents the config for a Characteristic
#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct CharacteristicConfig {
    /// The position of the name
    pub name_position: Point,
    /// The size of the name
    pub name_size: f32,
    /// The position of the value
    pub position: Point,
}

/// Represents the config for the pictograms shape
#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct PictogramConfig {
    /// The position of the text
    pub text_position: Point,
    /// The size of the text
    pub text_size: f32,
    /// The scale of the stars
    pub stars_scale: f32,
}

impl Config {
    /// Changes the orientation of the config
    pub fn set_orientation(&mut self, orientation: Orientation) {
        self.orientation = orientation
    }

    /// Changes the mode of the config
    pub fn set_mode(&mut self, mode: Mode) {
        self.mode = mode
    }

    /// Sets the compact fraction
    pub fn set_compact_fraction(&mut self) {
        self.compact_fraction = true
    }

    /// Returns whether or not the compact fraction is set
    pub fn is_compact_fraction(&self) -> bool {
        self.compact_fraction
    }

    /// Returns the size of a Card
    pub fn size(&self) -> usvg::Size {
        self.common.size.try_into().critical(
            "There is an error in src/config/rendering/common.yml; the size is not valid.",
        )
    }

    /// Returns the ViewBox of a Card
    pub fn view_box(&self) -> usvg::ViewBox {
        self.common.view_box.into()
    }

    /// Returns the dpi of a Card
    pub fn get_dpi(&self) -> f32 {
        self.common.dpi
    }

    /// Gets the font corresponding to a Layer
    pub fn get_font(&self, layer: &Layer) -> Result<usvg::Font, Error> {
        let font = match self.fonts.get(layer) {
            Some(font) => font,
            None => return Err(Error::MissingValueError(format!("the font of {}", layer))),
        };

        Ok(usvg::Font {
            families: vec![font.to_string()],
            style: usvg::FontStyle::Normal,
            stretch: usvg::FontStretch::Normal,
            weight: 400,
        })
    }

    /// Returns the positions for the given number of symbols
    pub fn get_symbols_positions(&self, number: u8) -> Result<&Vec<Point>, Error> {
        let positions = match self.orientation {
            Orientation::Vertical => &self.vertical.symbols_position,
            Orientation::Horizontal => &self.horizontal.symbols_position,
        };

        match positions.get(&number) {
            Some(position) => Ok(position),
            None => Err(Error::MissingValueError(format!(
                "the position of {} symbols in {:?} template",
                number, self.orientation
            ))),
        }
    }

    /// Returns the maximal number of symbols handled
    pub fn get_max_symbols(&self) -> usize {
        match self.orientation {
            Orientation::Horizontal => self.horizontal.symbols_position.len(),
            Orientation::Vertical => self.vertical.symbols_position.len(),
        }
    }

    /// Gets the position of the given layer
    pub fn get_position(&self, layer: &Layer) -> Result<Point, Error> {
        let position = match self.common.get_position(layer, &self.mode) {
            Some(position) => Some(position),
            None => match self.orientation {
                Orientation::Vertical => self.vertical.get_position(layer, &self.mode),
                Orientation::Horizontal => self.horizontal.get_position(layer, &self.mode),
            },
        };
        match position {
            Some(position) => Ok(*position),
            None => Err(Error::MissingValueError(format!(
                "the position of {}",
                layer
            ))),
        }
    }

    /// Gets the scale of the given layer
    pub fn get_scale(&self, layer: &Layer) -> f32 {
        let scale = match self.common.scales.get(layer) {
            Some(scale) => Some(scale),
            None => match self.orientation {
                Orientation::Vertical => self.vertical.scales.get(layer),
                Orientation::Horizontal => self.horizontal.scales.get(layer),
            },
        };
        match scale {
            Some(scale) => *scale,
            None => 1.,
        }
    }

    /// Gets the characteristics configurations
    pub fn get_characteristics(&self) -> &HashMap<CharacteristicName, CharacteristicConfig> {
        match self.orientation {
            Orientation::Vertical => &self.vertical.characteristics,
            Orientation::Horizontal => &self.horizontal.characteristics,
        }
    }

    /// Gets the shape of the pictograms (position of the text, size of the stars...)
    pub fn get_pictogram_shape(&self) -> &PictogramConfig {
        match self.orientation {
            Orientation::Vertical => &self.vertical.pictogram_config,
            Orientation::Horizontal => &self.horizontal.pictogram_config,
        }
    }

    /// Gets the positions of the stars
    pub fn get_pictogram_stars(&self) -> &HashMap<u8, Vec<Point>> {
        match self.orientation {
            Orientation::Vertical => &self.vertical.pictogram_stars,
            Orientation::Horizontal => &self.horizontal.pictogram_stars,
        }
    }

    /// Gets the positions of the given number of pictograms
    pub fn get_pictogram_positions(&self, number: u8) -> Result<&Vec<Point>, Error> {
        let positions = match self.orientation {
            Orientation::Vertical => &self.vertical.pictograms_positions,
            Orientation::Horizontal => &self.horizontal.pictograms_positions,
        };

        match positions.get(&number) {
            Some(position) => Ok(position),
            None => Err(Error::MissingValueError(format!(
                "the position of {} pictograms in {:?} template",
                number, self.orientation
            ))),
        }
    }

    /// Returns the maximal number of pictograms handled
    pub fn get_max_pictograms(&self) -> usize {
        match self.orientation {
            Orientation::Horizontal => self.horizontal.pictograms_positions.len(),
            Orientation::Vertical => self.vertical.pictograms_positions.len(),
        }
    }

    /// Returns the width of the given layer
    pub fn get_width(&self, layer: &Layer) -> Result<f32, Error> {
        match self.common.widths.get(layer) {
            Some(width) => Ok(*width),
            None => Err(Error::MissingValueError(format!("the width of {}", layer))),
        }
    }

    /// Gets the database of fonts used
    /// Useful for turning the text block in path
    pub fn get_font_database(&self) -> Result<usvg::fontdb::Database, Error> {
        let mut database = usvg::fontdb::Database::new();

        database.load_font_data(include_bytes!("../fonts/timesbd.ttf").to_vec());

        database.load_font_data(include_bytes!("../fonts/FrizQuadrataBoldBASTON05.ttf").to_vec());

        database.load_font_data(include_bytes!("../fonts/Heisei_Mincho_Std_W5.otf").to_vec());

        database.load_font_data(include_bytes!("../fonts/NewtextDemiBASTON.ttf").to_vec());

        Ok(database)
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            fonts: serde_yaml::from_str(include_str!("../fonts/config.yml")).critical("There is an error in the srd/fonts/config.yml. This Should not happen as the file is embedded in the executable."),
            common: serde_yaml::from_str(include_str!("../config/rendering/common.yml")).critical("There is an error in the srd/config/rendering/common.yml. This Should not happen as the file is embedded in the executable."),
            vertical: serde_yaml::from_str(include_str!("../config/rendering/vertical.yml")).critical("There is an error in the srd/config/rendering/vertical.yml. This Should not happen as the file is embedded in the executable."),
            horizontal: serde_yaml::from_str(include_str!("../config/rendering/horizontal.yml")).critical("There is an error in the srd/config/rendering/horizontal.yml. This Should not happen as the file is embedded in the executable."),
            orientation: Orientation::default(),
            mode: Mode::default(),
            compact_fraction: false,
        }
    }
}

impl CommonConfig {
    /// Returns the position of the given layer with the given mode
    pub fn get_position(&self, layer: &Layer, mode: &Mode) -> Option<&Point> {
        match self.positions_mode_dependent.get(mode) {
            Some(positions) => match positions.get(layer) {
                Some(position) => Some(position),
                None => self.positions.get(layer),
            },
            None => self.positions.get(layer),
        }
    }
}

impl OrientationConfig {
    /// Returns the position of the given layer with the given mode
    pub fn get_position(&self, layer: &Layer, mode: &Mode) -> Option<&Point> {
        match self.positions_mode_dependent.get(mode) {
            Some(positions) => match positions.get(layer) {
                Some(position) => Some(position),
                None => self.positions.get(layer),
            },
            None => self.positions.get(layer),
        }
    }
}

#[cfg(test)]
mod test_module_config {
    use super::{CharacteristicConfig, Config};

    use super::super::Layer;

    use crate::card::fields::CharacteristicName;
    use crate::card::illustration::{Mode, Orientation};
    use crate::error::Error;
    use crate::render::Point;
    use strum::IntoEnumIterator;

    #[test]
    fn test_compact_fraction_config() {
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                assert!(!config.compact_fraction);
                assert_eq!(config.compact_fraction, config.is_compact_fraction());

                config.set_compact_fraction();
                assert!(config.compact_fraction);
                assert_eq!(config.compact_fraction, config.is_compact_fraction());
            }
        }
    }

    #[test]
    fn test_config_view_box() {
        let config = Config::default();
        let view_box = config.view_box();
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                let view_box1 = config.view_box();
                assert_eq!(view_box1.rect, view_box.rect);
                assert_eq!(view_box1.aspect, view_box.aspect);
            }
        }
    }

    #[test]
    fn test_config_size() {
        let config = Config::default();
        let size = config.size();
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                assert_eq!(config.size(), size);
            }
        }
    }

    #[test]
    fn test_config_dpi() {
        let config = Config::default();
        let dpi = config.get_dpi();
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                assert_eq!(config.get_dpi(), dpi);
            }
        }
    }

    #[test]
    fn test_config_pictograms_shape() {
        let config = Config::default();
        let shape = config.get_pictogram_shape();
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                assert_eq!(config.get_pictogram_shape(), shape);
            }
        }
    }

    #[test]
    fn test_config_symbols_positions() {
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                for number in 1..(config.get_max_symbols() + 1) as u8 {
                    assert_eq!(
                        config
                            .get_symbols_positions(number)
                            .expect(&format!(
                        "There should be a configuration for {} symbols in {orientation} mode",
                        number
                    ))
                            .len(),
                        number as usize
                    );
                }
                assert_eq!(
                    config.get_symbols_positions(
                        (config.get_max_symbols() + 1)
                            .try_into()
                            .expect("The maximum number of symbols is too big")
                    ),
                    Err(Error::MissingValueError(format!(
                        "the position of {} symbols in {:?} template",
                        config.get_max_symbols() + 1,
                        config.orientation
                    )))
                );
                assert_eq!(
                    config.get_symbols_positions(0),
                    Err(Error::MissingValueError(format!(
                        "the position of 0 symbols in {:?} template",
                        config.orientation
                    )))
                );
            }
        }
    }

    #[test]
    fn test_config_pictograms_positions() {
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                for number in 1..(config.get_max_pictograms() + 1) as u8 {
                    assert_eq!(
                        config
                            .get_pictogram_positions(number)
                            .expect(&format!(
                        "There should be a configuration for {} pictograms in {orientation} mode",
                        number
                    ))
                            .len(),
                        number as usize
                    );
                }
                assert_eq!(
                    config.get_pictogram_positions(
                        (config.get_max_pictograms() + 1)
                            .try_into()
                            .expect("The maximum number of pictograms is too big")
                    ),
                    Err(Error::MissingValueError(format!(
                        "the position of {} pictograms in {:?} template",
                        config.get_max_pictograms() + 1,
                        config.orientation
                    )))
                );
            }
        }
    }

    #[test]
    fn test_config_pictogram_stars() {
        for mode in Mode::iter() {
            let mut config = Config::default();
            config.set_orientation(Orientation::Horizontal);
            config.set_mode(mode);
            for number in config.horizontal.pictogram_stars.keys() {
                assert_eq!(
                    config
                        .get_pictogram_stars()
                        .get(number)
                        .unwrap_or_else(|| panic!(
                            "No position for {} star for a pictogram",
                            number
                        ))
                        .len(),
                    *number as usize
                );
            }
            config.set_orientation(Orientation::Vertical);
            config.set_mode(mode);
            for number in config.vertical.pictogram_stars.keys() {
                assert_eq!(
                    config
                        .get_pictogram_stars()
                        .get(number)
                        .unwrap_or_else(|| panic!(
                            "No position for {} star for a pictogram",
                            number
                        ))
                        .len(),
                    *number as usize
                );
            }
        }
    }

    #[test]
    fn test_config_positions() {
        let layers = vec![
            Layer::Name,
            Layer::Type,
            Layer::Title,
            Layer::Faction,
            Layer::Gender,
            Layer::Era,
            Layer::Archetype,
            Layer::ExtraDeck,
            Layer::PowersTopLeft,
            Layer::PowersTopRight,
            Layer::PowersLeftOfPictograms,
            Layer::Luck,
            Layer::LuckBorder,
            Layer::Charisma,
            Layer::CharismaBorder,
            Layer::IllustrationBorder,
            Layer::Main,
            Layer::CharacteristicsBand,
            Layer::CharacteristicsBorder,
        ];
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                for layer in &layers {
                    assert!(config.get_position(&layer).is_ok());
                }
                for layer in Layer::iter() {
                    if !layers.contains(&layer) {
                        assert_eq!(
                            config.get_position(&layer),
                            Err(Error::MissingValueError(format!(
                                "the position of {}",
                                layer
                            )))
                        );
                    }
                }
            }
        }
    }

    #[test]
    fn test_config_scales() {
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                for layer in Layer::iter() {
                    config.get_scale(&layer);
                }
            }
        }
    }

    #[test]
    fn test_config_widths() {
        let layers = vec![Layer::Faction, Layer::Symbols, Layer::ExtraDeck];

        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                for layer in &layers {
                    assert!(config.get_width(&layer).is_ok());
                }
                for layer in Layer::iter() {
                    if !layers.contains(&layer) {
                        assert_eq!(
                            config.get_width(&layer),
                            Err(Error::MissingValueError(format!("the width of {}", layer)))
                        );
                    }
                }
            }
        }
    }

    #[test]
    fn test_config_font() {
        let layers = vec![
            Layer::Name,
            Layer::Type,
            Layer::Title,
            Layer::CharacteristicsName,
            Layer::CharacteristicsValue,
            Layer::Symbols,
            Layer::Powers,
            Layer::Pictograms,
        ];
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                for layer in &layers {
                    assert!(config.get_font(&layer).is_ok());
                }
                for layer in Layer::iter() {
                    if !layers.contains(&layer) {
                        assert_eq!(
                            config.get_font(&layer),
                            Err(Error::MissingValueError(format!("the font of {}", layer)))
                        );
                    }
                }
            }
        }
    }

    #[test]
    fn test_font_database() {
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                let database = config
                    .get_font_database()
                    .expect("A test should never fail");

                let layers = vec![
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                ];
                for layer in layers {
                    let font = config.get_font(&layer).expect("A test should never fail");
                    let mut families: Vec<usvg::fontdb::Family> = vec![];
                    for family in font.families.as_slice() {
                        families.push(usvg::fontdb::Family::Name(family))
                    }
                    let style = match font.style {
                        usvg::FontStyle::Normal => usvg::fontdb::Style::Normal,
                        usvg::FontStyle::Italic => usvg::fontdb::Style::Italic,
                        usvg::FontStyle::Oblique => usvg::fontdb::Style::Oblique,
                    };
                    let stretch = match font.stretch {
                        usvg::FontStretch::UltraCondensed => usvg::fontdb::Stretch::UltraCondensed,
                        usvg::FontStretch::ExtraCondensed => usvg::fontdb::Stretch::ExtraCondensed,
                        usvg::FontStretch::Condensed => usvg::fontdb::Stretch::Condensed,
                        usvg::FontStretch::SemiCondensed => usvg::fontdb::Stretch::SemiCondensed,
                        usvg::FontStretch::Normal => usvg::fontdb::Stretch::Normal,
                        usvg::FontStretch::SemiExpanded => usvg::fontdb::Stretch::SemiExpanded,
                        usvg::FontStretch::Expanded => usvg::fontdb::Stretch::Expanded,
                        usvg::FontStretch::ExtraExpanded => usvg::fontdb::Stretch::ExtraExpanded,
                        usvg::FontStretch::UltraExpanded => usvg::fontdb::Stretch::UltraExpanded,
                    };
                    database
                        .query(&usvg::fontdb::Query {
                            families: &families,
                            weight: usvg::fontdb::Weight(font.weight),
                            style,
                            stretch,
                        })
                        .expect("A test should never fail");
                }
            }
        }
    }

    #[test]
    fn test_config_characteristics() {
        for orientation in Orientation::iter() {
            for mode in Mode::iter() {
                let mut config = Config::default();
                config.set_orientation(orientation);
                config.set_mode(mode);
                let characteristics = config.get_characteristics();
                for characteristic in CharacteristicName::iter() {
                    assert_ne!(characteristics.get(&characteristic), None)
                }
            }
        }
    }

    #[test]
    fn test_debug() {
        let _ = format!("{:?}", Config::default());
    }

    #[test]
    fn test_clone_partial_eq_characteristic_config() {
        let config = CharacteristicConfig {
            name_position: Point::default(),
            name_size: 10.,
            position: Point::default(),
        };

        assert_eq!(config, config.clone())
    }
}
