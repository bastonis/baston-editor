// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! The main GUI of the Baston Editor.

use crate::app::App;
use crate::card::logos::{Archetype, Era, ExtraDeck, Faction, Gender};
use crate::card::Card;
use crate::error::LogError;
use crate::render::images::RasterImage;

// use iced::advanced::widget::Operation;
use iced::widget::pane_grid::ResizeEvent;
use iced::widget::scrollable::{Direction, Scrollbar};
use iced::widget::{container, pane_grid, PaneGrid, PickList, Scrollable};
use iced::{Element, Length, Size, Task};
use iced_aw::widgets::{tab_bar::tab_label::TabLabel, tabs::Tabs};
use std::fmt::Display;
use std::sync::{Arc, RwLock};
use strum_macros::EnumIter;

pub mod canvas;
pub mod card_part_ui;
pub mod main_tab;
pub mod pictogram_tab;
pub mod power_tab;
pub mod symbol_tab;

/// Default padding of the elements
const PADDING: u16 = 6;
/// Default spacing between elements
const SPACING: u16 = 3;

/// The messages that handle the behavior of the GUI
#[derive(Debug, Clone)]
pub enum GuiMessage {
    /// Modification done to the [name](crate::card::fields::NameField) of the [Card]
    Name(card_part_ui::fields_ui::Message),
    /// Modification done to the [title](crate::card::fields::TitleField) of the [Card]
    Title(card_part_ui::fields_ui::Message),
    /// Modification done to the [type](crate::card::fields::TypeField) of the [Card]
    Type(card_part_ui::fields_ui::Message),
    /// Modification done to the [characteristics](crate::card::fields::CharacteristicsField) of the [Card]
    Characteristics(card_part_ui::characteristics_ui::Message),
    /// Modification done to the [template](crate::card::illustration::Template) of the [Card]
    Template(card_part_ui::template_ui::Message),
    /// Modification done to the [illustration](crate::card::illustration::Illustration) of the [Card]
    Illustration(card_part_ui::illustration_ui::Message),
    /// Modification done to the [faction](crate::card::logos::Faction) of the [Card]
    Faction(card_part_ui::logo_ui::Message<Faction>),
    /// Modification done to the [gender](crate::card::fields::GenderField) of the [Card]
    Gender(card_part_ui::logo_field_ui::Message<Gender>),
    /// Modification done to the [era](crate::card::fields::EraField) of the [Card]
    Era(card_part_ui::logo_field_ui::Message<Era>),
    /// Modification done to the [archetype](crate::card::fields::ArchetypeField) of the [Card]
    Archetype(card_part_ui::logo_field_ui::Message<Archetype>),
    /// Modification done to the [extra_deck](crate::card::logos::ExtraDeck) of the [Card]
    ExtraDeck(card_part_ui::logo_ui::Message<ExtraDeck>),
    /// Modification done to the nth [symbol](crate::card::symbol::SymbolField) of the [Card]
    Symbol(card_part_ui::symbol_ui::Message),
    /// Modification done to the nth [pictogram](crate::card::pictogram::Pictogram) of the [Card]
    Pictogram(card_part_ui::pictogram_ui::Message),
    /// Modification done to the nth [power](crate::card::fields::PowerField) of the [Card]
    Power(card_part_ui::power_ui::Message),

    /// Interaction to save the [Card]
    Save(card_part_ui::save_ui::Message),

    /// When the split between the Render and the UI is moved
    SplitResized(ResizeEvent),
    /// When changing tab
    TabChanged(TabId),
    /// When the tab key is pressed
    Tab,
}

/// The id of the tabs
#[derive(Debug, Copy, Clone, PartialEq, Eq, EnumIter)]
pub enum TabId {
    /// The Id of the general tab
    MainTab,
    /// The Id of the symbols tab
    SymbolTab,
    /// The Id of the pictograms tab
    PictogramTab,
    /// The Id of the powers tab
    PowerTab,
}

/// The main GUI of Baston Editor
pub struct Gui {
    /// The card that we are modifying
    card: Arc<RwLock<Card>>,
    /// The separation between the render and the UI
    state: pane_grid::State<PaneState>,
    /// The id of the current tab shown
    current_tab: TabId,
}

/// Trait to create a pick list for a given type
pub trait AsPickList
where
    Self: Sized + PartialEq + Clone + ToString,
{
    /// Gets the list of possible values
    fn list() -> Vec<Self>;

    /// Creates a new pick list
    ///
    /// The possible values are the ones from [list](Self::list)
    ///
    /// # Arguments
    /// * `current` - The current value
    /// * `messaging` - The function to send messages
    fn pick_list<'a, Message, Messaging, Theme, Renderer>(
        current: Self,
        on_select: Messaging,
    ) -> PickList<'a, Self, Vec<Self>, Self, Message, Theme, Renderer>
    where
        Message: Clone,
        Messaging: Fn(Self) -> Message + Copy + 'static,
        Theme: iced::widget::pick_list::Catalog,
        Renderer: iced::advanced::text::Renderer,
    {
        PickList::new(Self::list(), Some(current), move |value: Self| {
            on_select(value)
        })
    }
}

/// Trait for message
pub trait Message<C, Res> {
    /// Apply the message on an object
    fn apply(&self, content: &mut C) -> Res;
}

/// State of the separation between the render and the UI
enum PaneState {
    /// The render part of the GUI
    Canvas,
    /// The UI part of the GUI
    Main,
}

/// Trait to create the UI for a part of a card
pub trait GetUI<'a, Container, Message, Theme, Renderer, Option, Name>
where
    Container: Into<Element<'a, Message, Theme, Renderer>>,
    Name: Into<Element<'a, Message, Theme, Renderer>>,
{
    /// Creates the ui for the object, with the given name
    fn get_ui(&self, name: Name, option: Option) -> Container;
}

impl Gui {
    /// Create a new GUI modifying the given card
    pub fn new(card: Card) -> Self {
        let (mut state, pane) = pane_grid::State::new(PaneState::Canvas);
        let (_, split) = state
            .split(pane_grid::Axis::Vertical, pane, PaneState::Main)
            .critical("There should be no issue with initializing the split");
        state.resize(split, 0.35);

        Self {
            card: Arc::new(RwLock::new(card)),
            state,
            current_tab: TabId::MainTab,
        }
    }

    /// Create the render part of the GUI
    fn canvas<'a, Theme, Renderer>(&self) -> Element<'a, GuiMessage, Theme, Renderer>
    where
        Renderer: iced::advanced::renderer::Renderer + iced::advanced::svg::Renderer + 'a,
        Theme: iced::widget::container::Catalog + iced::widget::svg::Catalog + 'a,
        iced::widget::Svg<'a>: iced::advanced::Widget<GuiMessage, Theme, Renderer>,
    {
        let canvas =
            canvas::Canvas::new(self.card.clone()).critical("Error while creating the canvas");
        let canvas: Element<'a, GuiMessage, Theme, Renderer> = canvas.into();
        canvas
    }

    /// Create the UI part of the GUI
    fn tabs<'a, Theme, Renderer>(&self) -> Tabs<'a, GuiMessage, TabId, Theme, Renderer>
    where
        Renderer: iced::advanced::renderer::Renderer
            + iced::advanced::svg::Renderer
            + iced::advanced::text::Renderer<Font = iced::Font>
            + 'a,
        Theme: iced::widget::pick_list::Catalog
            + iced_aw::style::card::Catalog
            + iced::widget::container::Catalog
            + iced::widget::button::Catalog
            + iced::widget::checkbox::Catalog
            + iced_aw::style::number_input::ExtendedCatalog
            + iced::widget::container::Catalog
            + iced::widget::svg::Catalog
            + iced_aw::tab_bar::Catalog
            + iced::widget::text::Catalog
            + iced_aw::menu::Catalog
            + iced_aw::style::menu_bar::Catalog
            + iced::widget::scrollable::Catalog
            + iced_aw::widget::badge::Catalog
            + 'a,
    {
        let make_tab = |content| {
            let scrollbar = Scrollbar::new();
            Scrollable::new(content)
                .direction(Direction::Vertical(scrollbar))
                .width(Length::Fill)
                .height(Length::Fill)
                .anchor_top()
                .spacing(SPACING)
        };

        let tabs = Tabs::new(GuiMessage::TabChanged)
            .push(
                TabId::MainTab,
                TabLabel::Text("Main".to_owned()),
                make_tab(Element::from(main_tab::main_tab(self))),
            )
            .push(
                TabId::SymbolTab,
                TabLabel::Text("Symbols".to_owned()),
                make_tab(Element::from(symbol_tab::symbol_tab(self))),
            )
            .push(
                TabId::PowerTab,
                TabLabel::Text("Powers".to_owned()),
                make_tab(Element::from(power_tab::power_tab(self))),
            )
            .push(
                TabId::PictogramTab,
                TabLabel::Text("Pictograms".to_owned()),
                make_tab(Element::from(pictogram_tab::pictogram_tab(self))),
            )
            .set_active_tab(&self.current_tab)
            .tab_bar_height(Length::Shrink);
        tabs
    }
}

impl App<GuiMessage> for Gui {
    fn title() -> &'static str {
        "Baston Editor"
    }

    fn icon() -> Option<iced::window::Icon> {
        let icon = RasterImage::from_bytes(include_bytes!("logos/icon.png"), Some(image::ImageFormat::Png))
            .critical("Error while loading src/logos/icon.png, this file is embed in the code so it should not fail")
            .try_into()
            .critical("Error while loading src/logos/icon.png, this file is embed in the code so it should not fail");
        Some(icon)
    }

    fn size() -> Size {
        Size {
            width: 1920.,
            height: 1080.,
        }
    }

    fn update(&mut self, message: GuiMessage) -> iced::Task<GuiMessage> {
        message.apply(self)
    }

    fn view(&self) -> Element<GuiMessage> {
        let view = move |_pane, state: &PaneState, _is_maximized| {
            pane_grid::Content::new(match state {
                PaneState::Canvas => Element::from(self.canvas()),
                PaneState::Main => Element::from(self.tabs()),
            })
        };

        let split = PaneGrid::new(&self.state, view)
            .width(Length::Fill)
            .height(Length::Fill)
            .on_resize(20, GuiMessage::SplitResized)
            .spacing(SPACING);

        container(split)
            .padding(PADDING)
            .center_x(Length::Fill)
            .center_y(Length::Fill)
            .into()
    }

    fn subscription(&self) -> iced::Subscription<GuiMessage> {
        iced::event::listen_with(|event, _status, _id| {
            if let iced::Event::Keyboard(iced::keyboard::Event::KeyPressed {
                key, modifiers, ..
            }) = event
            {
                if modifiers.is_empty() {
                    if key == iced::keyboard::Key::Named(iced::keyboard::key::Named::Tab) {
                        // println!("We caught a tab");
                        Some(GuiMessage::Tab)
                    } else {
                        None
                    }
                } else {
                    None
                }
            } else {
                None
            }
        })
    }
}

impl<T: strum::IntoEnumIterator + Clone + PartialEq + Display> AsPickList for T {
    fn list() -> Vec<Self> {
        Self::iter().collect()
    }
}

impl Message<Gui, Task<GuiMessage>> for GuiMessage {
    fn apply(&self, gui: &mut Gui) -> Task<GuiMessage> {
        let mut card = gui
            .card
            .write()
            .critical("Error while getting the write lock on the card");
        match self {
            Self::Name(ref message) => message.apply(&mut card.name),
            Self::Title(ref message) => message.apply(&mut card.title),
            Self::Type(ref message) => message.apply(&mut card.r#type),
            Self::Characteristics(ref message) => message.apply(&mut card.characteristics),
            Self::Template(ref message) => message.apply(&mut card.template),
            Self::Illustration(ref message) => message.apply(&mut card.illustration),
            Self::ExtraDeck(ref message) => message.apply(&mut card.extra_deck),
            Self::Save(ref message) => message.apply(&mut card),
            Self::Faction(ref message) => message.apply(&mut card.faction),
            Self::Gender(ref message) => message.apply(&mut card.gender),
            Self::Era(ref message) => message.apply(&mut card.era),
            Self::Archetype(ref message) => message.apply(&mut card.archetype),
            Self::Symbol(ref message) => message.apply(&mut card.symbols),
            Self::Pictogram(ref message) => message.apply(&mut card),
            Self::Power(ref message) => message.apply(&mut card),

            Self::SplitResized(event) => gui.state.resize(event.split, event.ratio),
            Self::TabChanged(tab) => gui.current_tab = *tab,
            Self::Tab => {
                // println!("We are going to the next focusable widget");
                // let count = iced::advanced::widget::operation::focusable::count().finish();
                // println!("Count: {count:?}");
                // let focused = iced::advanced::widget::operation::focusable::find_focused().finish();
                // println!("Focused: {focused:?}");
                return iced::widget::focus_next();
            }
        }
        Task::none()
    }
}

#[cfg(test)]
mod test_module_gui {
    use super::Gui;
    use super::TabId;

    use crate::card::Card;
    use crate::color::Color;
    use crate::{app::App, gui::GuiMessage};

    use iced::{Renderer, Size, Theme};
    use std::ops::Deref;

    #[test]
    fn test_new_gui() {
        let gui = Gui::new(Card::new());
        let card = gui.card.read().expect("A test should never fail");
        assert_eq!(card.deref(), &Card::new());
    }

    #[test]
    fn test_view_gui() {
        let gui = Gui::new(Card::new());
        let _view = gui.view();
    }

    #[test]
    fn test_canvas_gui() {
        let gui = Gui::new(Card::new());
        let _canvas = gui.canvas::<Theme, Renderer>();
    }

    #[test]
    fn test_main_gui() {
        let gui = Gui::new(Card::new());
        let _main = gui.tabs::<Theme, Renderer>();
    }

    #[test]
    fn test_title_gui() {
        assert_eq!(Gui::title(), "Baston Editor");
    }

    #[test]
    fn test_icon_gui() {
        assert!(Gui::icon().is_some());
    }

    #[test]
    fn test_size_gui() {
        assert_eq!(
            Gui::size(),
            Size {
                width: 1920.,
                height: 1080.,
            }
        );
    }

    #[test]
    fn test_update_gui() {
        let mut gui = Gui::new(Card::new());
        let _ = gui.update(GuiMessage::Name(
            super::card_part_ui::fields_ui::Message::Size(1),
        ));
        assert_eq!(
            gui.card.read().expect("A test should never fail").name.size,
            1
        );

        let _ = gui.update(GuiMessage::Title(
            super::card_part_ui::fields_ui::Message::Size(2),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .title
                .size,
            2
        );

        let _ = gui.update(GuiMessage::Type(
            super::card_part_ui::fields_ui::Message::Size(3),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .r#type
                .size,
            3
        );

        let _ = gui.update(GuiMessage::Characteristics(
            super::card_part_ui::characteristics_ui::Message::Size(4),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .characteristics
                .size,
            4
        );

        let _ = gui.update(GuiMessage::Template(
            super::card_part_ui::template_ui::Message::MainColor(Color::Arrancar),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .template
                .main_color,
            Color::Arrancar
        );

        let _ = gui.update(GuiMessage::Illustration(
            super::card_part_ui::illustration_ui::Message::Zoom(1),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .illustration
                .zoom,
            1
        );

        let _ = gui.update(GuiMessage::ExtraDeck(
            super::card_part_ui::logo_ui::Message::Logo(crate::card::logos::ExtraDeck::Machine),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .extra_deck,
            crate::card::logos::ExtraDeck::Machine
        );

        let _ = gui.update(GuiMessage::Faction(
            super::card_part_ui::logo_ui::Message::Logo(crate::card::logos::Faction::Balance),
        ));
        assert_eq!(
            gui.card.read().expect("A test should never fail").faction,
            crate::card::logos::Faction::Balance
        );

        let _ = gui.update(GuiMessage::Gender(
            super::card_part_ui::logo_field_ui::Message::Color(Color::Arrancar),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .gender
                .color,
            Color::Arrancar
        );

        let _ = gui.update(GuiMessage::Era(
            super::card_part_ui::logo_field_ui::Message::Color(Color::Arrancar),
        ));
        assert_eq!(
            gui.card.read().expect("A test should never fail").era.color,
            Color::Arrancar
        );

        let _ = gui.update(GuiMessage::Archetype(
            super::card_part_ui::logo_field_ui::Message::Color(Color::Arrancar),
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .archetype
                .color,
            Color::Arrancar
        );

        let _ = gui.update(GuiMessage::Symbol(
            super::card_part_ui::symbol_ui::Message::New,
        ));
        assert_eq!(
            gui.card
                .read()
                .expect("A test should never fail")
                .symbols
                .last()
                .expect("A test should never fail"),
            &crate::card::symbol::SymbolField::default()
        );

        let _ = gui.update(GuiMessage::Save(
            super::card_part_ui::save_ui::Message::Reset,
        ));
        assert_eq!(
            gui.card.read().expect("A test should never fail").deref(),
            &Card::new()
        );

        let _ = gui.update(GuiMessage::TabChanged(TabId::SymbolTab));
        assert_eq!(gui.current_tab, TabId::SymbolTab);
    }

    mod test_trait_as_pick_list {
        use super::super::AsPickList;

        use iced::Theme;
        use strum_macros::{Display, EnumIter};

        #[derive(EnumIter, PartialEq, Debug, Display, Clone)]
        enum TestAsPickList {
            Test,
        }

        #[test]
        fn test_list() {
            assert_eq!(TestAsPickList::list(), vec![TestAsPickList::Test]);
        }

        #[test]
        fn test_pick_list() {
            TestAsPickList::pick_list::<_, _, Theme, iced::Renderer>(TestAsPickList::Test, |_| ());
        }
    }
}
