// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

#![doc = include_str!("../README.md")]

#[cfg(feature = "gui")]
pub mod app;
pub mod auto_update;
pub mod card;
pub mod cli;
pub mod color;
#[cfg(feature = "gui")]
pub mod color_picker;
pub mod error;
#[cfg(feature = "gui")]
pub mod gui;
pub mod render;
pub mod types;
pub mod utilities;
