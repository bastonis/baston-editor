// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module to create rendering from cards

use crate::card::fields::Position;
use crate::card::illustration::Mode;
use crate::card::logos::ExtraDeck;
use crate::card::Card;
use crate::color::Color;
use crate::error::{Error, LogError};

use std::ffi::OsStr;
use std::fs::File;
use std::io::Write;
use std::ops::{Add, Div, Mul, Neg};
use std::path::Path;

use config::Config;
use serde::{Deserialize, Serialize};
use strum_macros::{Display, EnumIter};
use usvg::fontdb::Database;
use usvg::Rect;
use usvg::{
    AlignmentBaseline, DominantBaseline, Font, LengthAdjust, Text, TextAnchor, TextChunk,
    TextDecoration, TextFlow, TextRendering, TextSpan, WritingMode,
};
use usvg::{BlendMode, Fill, Paint, PaintOrder, Transform, Visibility};
use usvg::{Node, NonZeroPositiveF32, NormalizedF32, Tree};
use usvg::{Options, PostProcessingSteps, TreeParsing, TreePostProc, TreeWriting, XmlOptions};

pub mod config;
pub mod images;

use self::images::Svg;

/// Names of the different layer that are used (and important) in the SVG rendering
#[derive(Display, Deserialize, Debug, Clone, PartialEq, Eq, Hash, EnumIter)]
pub enum Layer {
    /// Card's delimitation
    Border,
    /// Card's name
    Name,
    /// Card's type
    Type,
    /// Card's title
    Title,
    /// Card's image's background
    Background,
    /// Card's image
    Illustration,
    /// Card's image's border
    IllustrationBorder,
    /// Card's main part
    Main,
    /// Card's image's background and border, main part, characteristics' band and border, luck and charm background and border
    Template,
    /// Card's characteristics's values and names
    Characteristics,
    /// Card's characteristics' background
    CharacteristicsBand,
    /// Card's characteristics' border
    CharacteristicsBorder,
    /// Card's luck's background
    Luck,
    /// Card's luck's value
    LuckValue,
    /// Card's charisma's background
    #[serde(alias = "Charism")]
    Charisma,
    /// Card's charisma's value
    #[serde(alias = "CharismeValue")]
    CharismaValue,
    /// Card's luck's border
    LuckBorder,
    /// Card's charisma's border
    #[serde(alias = "CharismBorder")]
    CharismaBorder,
    /// Card's characteristics' names
    CharacteristicsName,
    /// Card's characteristics' values
    CharacteristicsValue,
    /// Card's faction
    Faction,
    /// Card's symbols
    Symbols,
    /// Card's powers
    Powers,
    /// Card's powers on the top left of the card
    PowersTopLeft,
    /// Card's powers on the top right of the card
    PowersTopRight,
    /// Card's powers on the left of the pictograms
    PowersLeftOfPictograms,
    /// Card's pictograms
    Pictograms,
    /// Card's extra deck
    ExtraDeck,
    /// Card's gender
    Gender,
    /// Card's era
    Era,
    /// Card's archetype
    Archetype,
}

/// Represents the graphic aspect of a Card
pub struct RenderedCard {
    /// The render of the card
    content: Tree,
    /// The config of the rendering
    config: Config,
}

/// Represents a SVG Group
#[derive(Debug, Clone, Default)]
pub struct Group {
    /// The SVG Group
    pub node: usvg::Group,
}

/// Represents a 2D Point
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq)]
pub struct Point {
    /// The X coordinate
    pub x: f32,

    /// The Y coordinate
    pub y: f32,
}

/// Represents the 2D size of an object (in a rectangle)
#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub struct Size {
    /// The width of the rectangle
    pub width: f32,

    /// The height of the rectangle
    pub height: f32,
}

/// Trait allowing a object to be rendered
pub trait Render {
    /// Render the object in the given layer
    ///
    /// # Arguments
    ///
    /// * `layer` - The layer to render in
    /// * `config` - The config of the rendering
    /// * `card` - The card of which the object is part
    fn render(
        &self,
        layer: &Layer,
        config: &Config,
        card: Option<&RenderedCard>,
    ) -> Result<Node, Error>;
}

#[cfg(test)]
/// Trait use to check if object is as expected
pub trait HasNode {
    /// Check whether the object has a node with the given id
    fn has_node<S: ToString + ?Sized>(&self, id: &S) -> bool;
}

#[cfg(test)]
impl HasNode for Node {
    fn has_node<S: ToString + ?Sized>(&self, id: &S) -> bool {
        if self.id() == id.to_string() {
            return true;
        }

        if let Node::Group(ref group) = self {
            group.has_node(id)
        } else {
            false
        }
    }
}

#[cfg(test)]
impl HasNode for usvg::Group {
    fn has_node<S: ToString + ?Sized>(&self, id: &S) -> bool {
        for children in &self.children {
            if children.has_node(id) {
                return true;
            }
        }
        return false;
    }
}

#[cfg(test)]
impl HasNode for Tree {
    fn has_node<S: ToString + ?Sized>(&self, id: &S) -> bool {
        self.root.has_node(id)
    }
}

impl RenderedCard {
    /// Creates a new rendering of the given card
    ///
    /// The boolean `no_border` is used to specify whether or not the limits of the card should be rendered
    ///
    /// # Example
    /// ```
    /// use baston_editor::card::Card;
    /// use baston_editor::render::RenderedCard;
    ///
    /// RenderedCard::new(&Card::new(), false);
    /// ```
    pub fn new(card: &Card, no_border: bool) -> Result<Self, Error> {
        let mut res = Self::default();
        res.config.set_orientation(card.get_orientation());
        res.config.set_mode(card.get_mode());
        if card.characteristics.compact_fraction {
            res.config.set_compact_fraction()
        }

        res.add_node(
            card.illustration
                .render(&Layer::Illustration, &res.config, None)?,
        );

        if !card.name.is_empty() {
            res.add_node(card.name.render(&Layer::Name, &res.config, None)?)
        } else {
            log::warn!("The card does not have a name");
        }

        if !card.r#type.is_empty() {
            res.add_node(card.r#type.render(&Layer::Type, &res.config, None)?)
        } else {
            log::warn!("The card does not have a type");
        }

        if !card.title.is_empty() {
            res.add_node(card.title.render(&Layer::Title, &res.config, None)?)
        } else {
            log::info!("The card does not have a title");
        }

        res.add_node(card.faction.render(&Layer::Faction, &res.config, None)?);

        if card.extra_deck != ExtraDeck::None {
            res.add_node(
                card.extra_deck
                    .render(&Layer::ExtraDeck, &res.config, None)?,
            );
        }

        res.add_node(card.gender.render(&Layer::Gender, &res.config, None)?);

        res.add_node(card.era.render(&Layer::Era, &res.config, None)?);

        res.add_node(
            card.archetype
                .render(&Layer::Archetype, &res.config, None)?,
        );

        res.add_node(
            card.characteristics
                .render(&Layer::Characteristics, &res.config, None)?,
        );

        if !card.symbols.is_empty() {
            res.add_node(card.symbols.render(&Layer::Symbols, &res.config, None)?)
        }

        if !card.pictograms.is_empty() {
            res.add_node(
                card.pictograms
                    .render(&Layer::Pictograms, &res.config, None)?,
            )
        }

        if !card.powers.is_empty() {
            res.convert_text()?; // We need to convert the texts to get the real bounding box of name, type and title
            res.add_node(
                card.powers
                    .render(&Layer::Powers, &res.config, Some(&res))?,
            )
        }

        res.add_node(card.template.render(&Layer::Template, &res.config, None)?);

        if !no_border {
            res.add_overlay(255)
        }
        res.order(card.get_mode());
        Ok(res)
    }

    /// Adds the delimitation of the card
    pub fn add_overlay(&mut self, opacity: u8) {
        self.add_node(
            Svg::from_str(include_str!("templates/card_border.svg"), &Layer::Border)
                .set_stroke_color(Color::Black)
                .set_fill_color(Color::Custom(255, 255, 255, opacity)),
        )
    }

    /// Reorders the layers given the mode
    fn order(&mut self, mode: Mode) {
        let old_tree = self.content.clone();
        self.content.root = Group::default().into();

        let order: Vec<Layer> = mode.into();
        for layer in order {
            if let Some(node) = old_tree.node_by_id(&layer.to_string()) {
                self.add_node(node.clone())
            }
        }
    }

    /// Gets a node by its id
    ///
    /// If no node is found, `None` is returned
    pub(super) fn node_by_id<S: Into<String>>(&self, id: S) -> Option<&Node> {
        self.content.node_by_id(&id.into())
    }

    /// Creates a new TextSpan
    ///
    /// This function is used to create the text of the card
    ///
    /// # Arguments
    ///
    /// * `field` - The object that needed text to be rendered, is used to have mode information on error
    /// * `start` - The start of the text, usually 0
    /// * `end` - The end of the text, usually the length of the text
    /// * `color` - The color of the text
    /// * `size` - The size of the text
    /// * `font` - The font of the text
    /// * `baseline` - The baseline of the text
    pub fn create_text_span<T: std::fmt::Display + ?Sized>(
        field: &T,
        start: usize,
        end: usize,
        color: Color,
        size: f32,
        font: &Font,
        baseline: DominantBaseline,
    ) -> TextSpan {
        TextSpan {
            start,
            end,
            fill: Some(Fill::from_paint(Paint::Color(color.into()))),
            stroke: None,
            paint_order: PaintOrder::StrokeAndFill,
            font: font.clone(),
            font_size: NonZeroPositiveF32::new(size)
                .unwrap_or_else(|| panic!("The font size of field: {}, should not be null", field)),
            small_caps: false,
            apply_kerning: true,
            decoration: TextDecoration {
                underline: None,
                overline: None,
                line_through: None,
            },
            dominant_baseline: baseline,
            alignment_baseline: AlignmentBaseline::Auto,
            baseline_shift: vec![],
            visibility: Visibility::Visible,
            letter_spacing: 1.0,
            word_spacing: 1.0,
            text_length: None,
            length_adjust: LengthAdjust::SpacingAndGlyphs,
        }
    }

    /// Formats the text in order to replace commands like ":dagger:"
    pub fn string_format<S: std::fmt::Display + ?Sized>(text: &S) -> String {
        let text = text.to_string();
        let text = text.replace(":dagger:", "†");
        text.replace(":ddagger:", "‡")
    }

    /// Render a text
    ///
    /// # Arguments
    ///
    /// * `field` - The object that needed text to be rendered, while be the name of the node
    /// * `text` - The text to be rendered
    /// * `color` - The color of the text
    /// * `size` - The size of the text
    /// * `font` - The font of the text
    /// * `position` - The position of the text
    /// * `anchor` - The anchor of the text
    pub fn create_text<
        T: std::fmt::Display + ?Sized,
        S: std::fmt::Display + ?Sized,
        N: Into<f32>,
    >(
        field: &T,
        text: &S,
        color: Color,
        size: N,
        font: &Font,
        position: Point,
        anchor: TextAnchor,
    ) -> Text {
        let text = Self::string_format(text);
        let span = Self::create_text_span(
            field,
            0,
            text.len(),
            color,
            size.into(),
            font,
            DominantBaseline::Central,
        );
        let chunk = TextChunk {
            x: Some(position.x),
            y: Some(position.y),
            anchor,
            spans: vec![span],
            text_flow: TextFlow::Linear,
            text: text.to_string(),
        };
        Text {
            id: field.to_string(),
            rendering_mode: TextRendering::OptimizeLegibility,
            dx: vec![],
            dy: vec![],
            rotate: vec![0.0],
            writing_mode: WritingMode::LeftToRight,
            chunks: vec![chunk],
            bounding_box: None,
            flattened: None,
            abs_transform: Transform::default(),
            stroke_bounding_box: None,
        }
    }

    /// Adds a node
    fn add_node<N: Into<Node>>(&mut self, node: N) {
        self.content.root.children.push(node.into())
    }

    /// Converts the texts into paths
    fn convert_text(&mut self) -> Result<(), Error> {
        self.content.postprocess(
            PostProcessingSteps {
                convert_text_into_paths: true,
            },
            &self.config.get_font_database()?,
        );
        Ok(())
    }

    /// Saves the render at the given path
    pub fn save<P: AsRef<OsStr>>(&mut self, filename: P) -> Result<(), Error> {
        let filename = Path::new(&filename);
        if let Some(extension) = filename.extension() {
            self.convert_text()?;
            match extension.to_ascii_lowercase().to_str() {
                Some("svg") => {
                    let svg = self.content.to_string(&XmlOptions::default());
                    Ok(File::create(filename)?.write_all(svg.as_bytes())?)
                }
                Some("pdf") => {
                    let svg = self.content.to_string(&XmlOptions::default());
                    let options = svg2pdf::PageOptions {
                        dpi: self.config.get_dpi(),
                    };
                    let tree =
                        svg2pdf::usvg::Tree::from_str(&svg, &svg2pdf::usvg::Options::default())?;
                    let pdf =
                        svg2pdf::to_pdf(&tree, svg2pdf::ConversionOptions::default(), options)?;
                    Ok(File::create(filename)?.write_all(&pdf)?)
                }
                _ => self.rasterize(filename, false),
            }
        } else {
            Err(Error::UnknownFormatError)
        }
    }

    /// Rasterize the render
    pub fn rasterize<P: AsRef<OsStr>>(
        &mut self,
        filename: P,
        no_border: bool,
    ) -> Result<(), Error> {
        let filename = Path::new(&filename);
        if let Some(parent) = filename.parent() {
            if !parent.exists() {
                std::fs::create_dir_all(parent)?;
            }
        }
        if let Some(extension) = filename.extension() {
            self.convert_text()?;
            let mut image = resvg::tiny_skia::Pixmap::new(
                self.content.size.width().ceil() as u32,
                self.content.size.height().ceil() as u32,
            )
            .ok_or(Error::ConversionError)?;
            resvg::render(&self.content, Transform::identity(), &mut image.as_mut());
            if !no_border {
                let mut mask = resvg::tiny_skia::Pixmap::new(
                    self.content.size.width().ceil() as u32,
                    self.content.size.height().ceil() as u32,
                )
                .ok_or(Error::ConversionError)?;
                let mut border = Tree::from_str(
                    include_str!("templates/card_content.svg"),
                    &Options::default(),
                )?;
                border.postprocess(
                    PostProcessingSteps {
                        convert_text_into_paths: false,
                    },
                    &Database::default(),
                );
                resvg::render(&border, Transform::identity(), &mut mask.as_mut());
                let mask = resvg::tiny_skia::Mask::from_pixmap(
                    mask.as_ref(),
                    resvg::tiny_skia::MaskType::Alpha,
                );
                image.as_mut().apply_mask(&mask)
            }
            match extension.to_ascii_lowercase().to_str() {
                Some("png") => Ok(image.save_png(filename)?),
                Some(extension) => Err(Error::UnsupportedFormatError(extension.to_string())),
                None => Err(Error::UnknownFormatError),
            }
        } else {
            Err(Error::UnknownFormatError)
        }
    }
}

impl TryFrom<RenderedCard> for String {
    type Error = Error;

    fn try_from(mut card: RenderedCard) -> Result<Self, Self::Error> {
        card.convert_text()?;
        Ok(card.content.to_string(&XmlOptions::default()))
    }
}

impl Default for RenderedCard {
    fn default() -> Self {
        let config = Config::default();
        Self {
            content: Tree {
                size: config.size(),
                view_box: config.view_box(),
                root: Group::default().into(),
            },
            config,
        }
    }
}

impl From<Group> for usvg::Group {
    fn from(group: Group) -> Self {
        group.node
    }
}

impl From<Group> for Node {
    fn from(group: Group) -> Self {
        Node::Group(Box::new(group.node))
    }
}

impl Group {
    /// Creates a new empty Group with the given transformation
    pub fn new<S: std::fmt::Display, T: Into<Transform>>(id: S, transform: T) -> Self {
        Self {
            node: usvg::Group {
                id: id.to_string(),
                transform: transform.into(),
                abs_transform: Transform::identity(),
                opacity: NormalizedF32::ONE,
                blend_mode: BlendMode::Normal,
                isolate: false,
                clip_path: None,
                mask: None,
                filters: vec![],
                bounding_box: Rect::from_xywh(0., 0., 0., 0.),
                stroke_bounding_box: None,
                layer_bounding_box: None,
                children: vec![],
            },
        }
    }

    /// Adds a node
    pub fn append<N: Into<Node>>(&mut self, node: N) {
        self.node.children.push(node.into());
        self.node.calculate_bounding_boxes()
    }

    /// Gets the bounding box
    pub fn size(&self) -> Option<Size> {
        self.node
            .bounding_box
            .map(|bounding_box| bounding_box.into())
    }
}

impl From<Group> for Tree {
    fn from(group: Group) -> Self {
        let size = match group.size() {
            Some(size) => size,
            None => Size {
                width: 0.,
                height: 0.,
            },
        };
        let size = if size.width <= 0. || size.height <= 0. {
            Size {
                width: 1.,
                height: 1.,
            }
        } else {
            size
        };
        let view_box = size.into();
        let size: usvg::Size = size.try_into().critical(
            "An error occurred while converting a Size to usvg::Size. This should never fail",
        );
        Self {
            size,
            view_box,
            root: group.into(),
        }
    }
}

impl Size {
    /// Modify the size to match a specified width, if the width is None, do nothing
    pub fn scale_to_width(self, width: Option<f32>) -> Self {
        if let Some(width) = width {
            Self {
                width,
                height: self.height * width / self.width,
            }
        } else {
            self
        }
    }
}

impl PartialEq for Size {
    /// The partial equality is true if both dimension is less than 1 appart.
    /// It allows to avoid floating point errors.
    fn eq(&self, other: &Self) -> bool {
        (self.width - other.width).abs() < 1. && (self.height - other.height).abs() < 1.
    }
}

impl Default for Point {
    fn default() -> Self {
        Self { x: 0., y: 0. }
    }
}

impl Div<f32> for Point {
    type Output = Self;

    fn div(self, ratio: f32) -> Self {
        Self {
            x: self.x / ratio,
            y: self.y / ratio,
        }
    }
}

impl Div<Point> for Point {
    type Output = Self;

    fn div(self, other: Point) -> Self {
        Self {
            x: self.x / other.x,
            y: self.y / other.y,
        }
    }
}

impl Div<f32> for Size {
    type Output = Self;

    fn div(self, ratio: f32) -> Self {
        Self {
            width: self.width / ratio,
            height: self.height / ratio,
        }
    }
}

impl Mul<f32> for Point {
    type Output = Self;

    fn mul(self, scale: f32) -> Self {
        Self {
            x: self.x * scale,
            y: self.y * scale,
        }
    }
}

impl Mul<Point> for Point {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}

impl Mul<f32> for Size {
    type Output = Self;

    fn mul(self, scale: f32) -> Self {
        Self {
            width: self.width * scale,
            height: self.height * scale,
        }
    }
}

impl Neg for Point {
    type Output = Self;

    fn neg(self) -> Self {
        Point { x: 0., y: 0. } - self
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl From<Rect> for Size {
    fn from(rect: Rect) -> Self {
        Self {
            width: rect.width(),
            height: rect.height(),
        }
    }
}

impl From<Size> for Point {
    fn from(size: Size) -> Self {
        Self {
            x: size.width,
            y: size.height,
        }
    }
}

#[cfg(feature = "gui")]
impl From<iced::Point> for Point {
    fn from(point: iced::Point) -> Self {
        Self {
            x: point.x,
            y: point.y,
        }
    }
}

#[cfg(feature = "gui")]
impl From<iced::Rectangle> for Point {
    fn from(rectangle: iced::Rectangle) -> Self {
        Self {
            x: rectangle.width,
            y: rectangle.height,
        }
    }
}

impl From<Size> for usvg::ViewBox {
    fn from(size: Size) -> Self {
        let size = if size.width <= 0. || size.height <= 0. {
            log::error!("Size should never have one of its dimension lower or equal to 0");
            Size {
                width: 1.,
                height: 1.,
            }
        } else {
            size
        };
        Self {
            rect: usvg::NonZeroRect::from_xywh(0.0, 0.0, size.width, size.height)
                .critical("Converting a Size into a ViewBox should never fail"),
            aspect: usvg::AspectRatio::default(),
        }
    }
}

impl TryFrom<Size> for usvg::Size {
    type Error = Error;

    fn try_from(size: Size) -> Result<Self, Self::Error> {
        match usvg::Size::from_wh(size.width, size.height) {
            Some(size) => Ok(size),
            None => Err(Error::ConversionError),
        }
    }
}

impl From<Point> for Transform {
    fn from(point: Point) -> Self {
        Self::from_translate(point.x, point.y)
    }
}

impl std::ops::Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl From<Position> for Layer {
    fn from(position: Position) -> Self {
        match position {
            Position::TopLeft => Self::PowersTopLeft,
            Position::TopRight => Self::PowersTopRight,
            Position::LeftOfPictograms => Self::PowersLeftOfPictograms,
            Position::LeftOfName | Position::RightOfName => Self::Name,
            Position::LeftOfType | Position::RightOfType => Self::Type,
            Position::LeftOfTitle | Position::RightOfTitle => Self::Title,
            Position::Default => unreachable!(),
        }
    }
}

impl From<Mode> for Vec<Layer> {
    fn from(mode: Mode) -> Self {
        match mode {
            Mode::Classic => vec![
                Layer::Background,
                Layer::Illustration,
                Layer::Main,
                Layer::CharacteristicsBand,
                Layer::CharacteristicsBorder,
                Layer::IllustrationBorder,
                Layer::Luck,
                Layer::Charisma,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
            Mode::FullBellowStrip => vec![
                Layer::Background,
                Layer::Illustration,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::CharacteristicsBorder,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
            Mode::FullBellowText => vec![
                Layer::Background,
                Layer::Illustration,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
            Mode::FullArt => vec![
                Layer::Background,
                Layer::Illustration,
                Layer::CharacteristicsBand,
                Layer::CharacteristicsBorder,
                Layer::Luck,
                Layer::Charisma,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
            Mode::OverBorder => vec![
                Layer::Background,
                Layer::IllustrationBorder,
                Layer::Illustration,
                Layer::Main,
                Layer::CharacteristicsBand,
                Layer::CharacteristicsBorder,
                Layer::Luck,
                Layer::Charisma,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
            Mode::OverBack => vec![
                Layer::Background,
                Layer::IllustrationBorder,
                Layer::Main,
                Layer::Illustration,
                Layer::CharacteristicsBand,
                Layer::CharacteristicsBorder,
                Layer::Luck,
                Layer::Charisma,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
            Mode::OverBand => vec![
                Layer::Background,
                Layer::Main,
                Layer::CharacteristicsBand,
                Layer::CharacteristicsBorder,
                Layer::IllustrationBorder,
                Layer::Luck,
                Layer::Charisma,
                Layer::LuckBorder,
                Layer::CharismaBorder,
                Layer::Illustration,
                Layer::CharacteristicsName,
                Layer::CharacteristicsValue,
                Layer::LuckValue,
                Layer::CharismaValue,
                Layer::Name,
                Layer::Type,
                Layer::Title,
                Layer::Faction,
                Layer::Symbols,
                Layer::Powers,
                Layer::Pictograms,
                Layer::Gender,
                Layer::Era,
                Layer::Archetype,
                Layer::ExtraDeck,
                Layer::Border,
            ],
        }
    }
}

#[cfg(test)]
mod test_module_render {

    #[cfg(test)]
    mod test_enum_layer {
        use super::super::Layer;

        use crate::card::fields::Position;
        use crate::card::illustration::Mode;

        use std::hash::Hash;
        use strum::IntoEnumIterator;

        #[test]
        fn test_serde_layer() {
            assert_eq!(
                Layer::Border,
                serde_yaml::from_str("Border").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Name,
                serde_yaml::from_str("Name").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Type,
                serde_yaml::from_str("Type").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Title,
                serde_yaml::from_str("Title").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Background,
                serde_yaml::from_str("Background").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Illustration,
                serde_yaml::from_str("Illustration").expect("A test should never fail")
            );
            assert_eq!(
                Layer::IllustrationBorder,
                serde_yaml::from_str("IllustrationBorder").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Main,
                serde_yaml::from_str("Main").expect("A test should never fail")
            );
            assert_eq!(
                Layer::CharacteristicsBand,
                serde_yaml::from_str("CharacteristicsBand").expect("A test should never fail")
            );
            assert_eq!(
                Layer::CharacteristicsBorder,
                serde_yaml::from_str("CharacteristicsBorder").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Luck,
                serde_yaml::from_str("Luck").expect("A test should never fail")
            );
            assert_eq!(
                Layer::LuckValue,
                serde_yaml::from_str("LuckValue").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Charisma,
                serde_yaml::from_str("Charism").expect("A test should never fail")
            );
            assert_eq!(
                Layer::CharismaValue,
                serde_yaml::from_str("CharismaValue").expect("A test should never fail")
            );
            assert_eq!(
                Layer::LuckBorder,
                serde_yaml::from_str("LuckBorder").expect("A test should never fail")
            );
            assert_eq!(
                Layer::CharismaBorder,
                serde_yaml::from_str("CharismaBorder").expect("A test should never fail")
            );
            assert_eq!(
                Layer::CharacteristicsName,
                serde_yaml::from_str("CharacteristicsName").expect("A test should never fail")
            );
            assert_eq!(
                Layer::CharacteristicsValue,
                serde_yaml::from_str("CharacteristicsValue").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Faction,
                serde_yaml::from_str("Faction").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Symbols,
                serde_yaml::from_str("Symbols").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Powers,
                serde_yaml::from_str("Powers").expect("A test should never fail")
            );
            assert_eq!(
                Layer::PowersTopLeft,
                serde_yaml::from_str("PowersTopLeft").expect("A test should never fail")
            );
            assert_eq!(
                Layer::PowersTopRight,
                serde_yaml::from_str("PowersTopRight").expect("A test should never fail")
            );
            assert_eq!(
                Layer::PowersLeftOfPictograms,
                serde_yaml::from_str("PowersLeftOfPictograms").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Pictograms,
                serde_yaml::from_str("Pictograms").expect("A test should never fail")
            );
            assert_eq!(
                Layer::ExtraDeck,
                serde_yaml::from_str("ExtraDeck").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Gender,
                serde_yaml::from_str("Gender").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Era,
                serde_yaml::from_str("Era").expect("A test should never fail")
            );
            assert_eq!(
                Layer::Archetype,
                serde_yaml::from_str("Archetype").expect("A test should never fail")
            );
        }

        #[test]
        fn test_display_layer() {
            assert_eq!("Border", Layer::Border.to_string());
            assert_eq!("Name", Layer::Name.to_string());
            assert_eq!("Type", Layer::Type.to_string());
            assert_eq!("Title", Layer::Title.to_string());
            assert_eq!("Background", Layer::Background.to_string());
            assert_eq!("Illustration", Layer::Illustration.to_string());
            assert_eq!("IllustrationBorder", Layer::IllustrationBorder.to_string());
            assert_eq!("Main", Layer::Main.to_string());
            assert_eq!(
                "CharacteristicsBand",
                Layer::CharacteristicsBand.to_string()
            );
            assert_eq!(
                "CharacteristicsBorder",
                Layer::CharacteristicsBorder.to_string()
            );
            assert_eq!("Luck", Layer::Luck.to_string());
            assert_eq!("LuckValue", Layer::LuckValue.to_string());
            assert_eq!("Charisma", Layer::Charisma.to_string());
            assert_eq!("CharismaValue", Layer::CharismaValue.to_string());
            assert_eq!("LuckBorder", Layer::LuckBorder.to_string());
            assert_eq!("CharismaBorder", Layer::CharismaBorder.to_string());
            assert_eq!(
                "CharacteristicsName",
                Layer::CharacteristicsName.to_string()
            );
            assert_eq!(
                "CharacteristicsValue",
                Layer::CharacteristicsValue.to_string()
            );
            assert_eq!("Faction", Layer::Faction.to_string());
            assert_eq!("Symbols", Layer::Symbols.to_string());
            assert_eq!("Powers", Layer::Powers.to_string());
            assert_eq!("PowersTopLeft", Layer::PowersTopLeft.to_string());
            assert_eq!("PowersTopRight", Layer::PowersTopRight.to_string());
            assert_eq!(
                "PowersLeftOfPictograms",
                Layer::PowersLeftOfPictograms.to_string()
            );
            assert_eq!("Pictograms", Layer::Pictograms.to_string());
            assert_eq!("ExtraDeck", Layer::ExtraDeck.to_string());
            assert_eq!("Gender", Layer::Gender.to_string());
            assert_eq!("Era", Layer::Era.to_string());

            assert_eq!("Border", format!("{:?}", Layer::Border));
            assert_eq!("Name", format!("{:?}", Layer::Name));
            assert_eq!("Type", format!("{:?}", Layer::Type));
            assert_eq!("Title", format!("{:?}", Layer::Title));
            assert_eq!("Background", format!("{:?}", Layer::Background));
            assert_eq!("Illustration", format!("{:?}", Layer::Illustration));
            assert_eq!(
                "IllustrationBorder",
                format!("{:?}", Layer::IllustrationBorder)
            );
            assert_eq!("Main", format!("{:?}", Layer::Main));
            assert_eq!(
                "CharacteristicsBand",
                format!("{:?}", Layer::CharacteristicsBand)
            );
            assert_eq!(
                "CharacteristicsBorder",
                format!("{:?}", Layer::CharacteristicsBorder)
            );
            assert_eq!("Luck", format!("{:?}", Layer::Luck));
            assert_eq!("LuckValue", format!("{:?}", Layer::LuckValue));
            assert_eq!("Charisma", format!("{:?}", Layer::Charisma));
            assert_eq!("CharismaValue", format!("{:?}", Layer::CharismaValue));
            assert_eq!("LuckBorder", format!("{:?}", Layer::LuckBorder));
            assert_eq!("CharismaBorder", format!("{:?}", Layer::CharismaBorder));
            assert_eq!(
                "CharacteristicsName",
                format!("{:?}", Layer::CharacteristicsName)
            );
            assert_eq!(
                "CharacteristicsValue",
                format!("{:?}", Layer::CharacteristicsValue)
            );
            assert_eq!("Faction", format!("{:?}", Layer::Faction));
            assert_eq!("Symbols", format!("{:?}", Layer::Symbols));
            assert_eq!("Powers", format!("{:?}", Layer::Powers));
            assert_eq!("PowersTopLeft", format!("{:?}", Layer::PowersTopLeft));
            assert_eq!("PowersTopRight", format!("{:?}", Layer::PowersTopRight));
            assert_eq!(
                "PowersLeftOfPictograms",
                format!("{:?}", Layer::PowersLeftOfPictograms)
            );
            assert_eq!("Pictograms", format!("{:?}", Layer::Pictograms));
            assert_eq!("ExtraDeck", format!("{:?}", Layer::ExtraDeck));
            assert_eq!("Gender", format!("{:?}", Layer::Gender));
            assert_eq!("Era", format!("{:?}", Layer::Era));
            assert_eq!("Archetype", format!("{:?}", Layer::Archetype));
        }

        #[test]
        fn test_clone_layer() {
            assert_eq!(Layer::Border, Layer::Border.clone());
            assert_eq!(Layer::Name, Layer::Name.clone());
            assert_eq!(Layer::Type, Layer::Type.clone());
            assert_eq!(Layer::Title, Layer::Title.clone());
            assert_eq!(Layer::Background, Layer::Background.clone());
            assert_eq!(Layer::Illustration, Layer::Illustration.clone());
            assert_eq!(Layer::IllustrationBorder, Layer::IllustrationBorder.clone());
            assert_eq!(Layer::Main, Layer::Main.clone());
            assert_eq!(
                Layer::CharacteristicsBand,
                Layer::CharacteristicsBand.clone()
            );
            assert_eq!(
                Layer::CharacteristicsBorder,
                Layer::CharacteristicsBorder.clone()
            );
            assert_eq!(Layer::Luck, Layer::Luck.clone());
            assert_eq!(Layer::Charisma, Layer::Charisma.clone());
            assert_eq!(Layer::Faction, Layer::Faction.clone());
            assert_eq!(Layer::Symbols, Layer::Symbols.clone());
            assert_eq!(Layer::Powers, Layer::Powers.clone());
            assert_eq!(Layer::PowersTopLeft, Layer::PowersTopLeft.clone());
            assert_eq!(Layer::PowersTopRight, Layer::PowersTopRight.clone());
            assert_eq!(
                Layer::PowersLeftOfPictograms,
                Layer::PowersLeftOfPictograms.clone()
            );
            assert_eq!(Layer::Pictograms, Layer::Pictograms.clone());
            assert_eq!(Layer::ExtraDeck, Layer::ExtraDeck.clone());
            assert_eq!(Layer::Gender, Layer::Gender.clone());
            assert_eq!(Layer::Era, Layer::Era.clone());
            assert_eq!(Layer::Archetype, Layer::Archetype.clone());
        }

        #[test]
        fn test_hash_layer() {
            let mut hasher = std::collections::hash_map::DefaultHasher::new();
            Layer::Border.hash(&mut hasher);
            Layer::Name.hash(&mut hasher);
            Layer::Type.hash(&mut hasher);
            Layer::Title.hash(&mut hasher);
            Layer::Background.hash(&mut hasher);
            Layer::Illustration.hash(&mut hasher);
            Layer::IllustrationBorder.hash(&mut hasher);
            Layer::Main.hash(&mut hasher);
            Layer::CharacteristicsBand.hash(&mut hasher);
            Layer::CharacteristicsBorder.hash(&mut hasher);
            Layer::Luck.hash(&mut hasher);
            Layer::Charisma.hash(&mut hasher);
            Layer::Faction.hash(&mut hasher);
            Layer::Symbols.hash(&mut hasher);
            Layer::Powers.hash(&mut hasher);
            Layer::PowersTopLeft.hash(&mut hasher);
            Layer::PowersTopRight.hash(&mut hasher);
            Layer::PowersLeftOfPictograms.hash(&mut hasher);
            Layer::Pictograms.hash(&mut hasher);
            Layer::ExtraDeck.hash(&mut hasher);
            Layer::Gender.hash(&mut hasher);
            Layer::Era.hash(&mut hasher);
            Layer::Archetype.hash(&mut hasher);
        }

        #[test]
        fn test_mode_into_layers() {
            assert_eq!(
                Vec::<Layer>::from(Mode::Classic),
                vec![
                    Layer::Background,
                    Layer::Illustration,
                    Layer::Main,
                    Layer::CharacteristicsBand,
                    Layer::CharacteristicsBorder,
                    Layer::IllustrationBorder,
                    Layer::Luck,
                    Layer::Charisma,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
            assert_eq!(
                Vec::<Layer>::from(Mode::FullBellowStrip),
                vec![
                    Layer::Background,
                    Layer::Illustration,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::CharacteristicsBorder,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
            assert_eq!(
                Vec::<Layer>::from(Mode::FullBellowText),
                vec![
                    Layer::Background,
                    Layer::Illustration,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
            assert_eq!(
                Vec::<Layer>::from(Mode::FullArt),
                vec![
                    Layer::Background,
                    Layer::Illustration,
                    Layer::CharacteristicsBand,
                    Layer::CharacteristicsBorder,
                    Layer::Luck,
                    Layer::Charisma,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
            assert_eq!(
                Vec::<Layer>::from(Mode::OverBorder),
                vec![
                    Layer::Background,
                    Layer::IllustrationBorder,
                    Layer::Illustration,
                    Layer::Main,
                    Layer::CharacteristicsBand,
                    Layer::CharacteristicsBorder,
                    Layer::Luck,
                    Layer::Charisma,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
            assert_eq!(
                Vec::<Layer>::from(Mode::OverBack),
                vec![
                    Layer::Background,
                    Layer::IllustrationBorder,
                    Layer::Main,
                    Layer::Illustration,
                    Layer::CharacteristicsBand,
                    Layer::CharacteristicsBorder,
                    Layer::Luck,
                    Layer::Charisma,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
            assert_eq!(
                Vec::<Layer>::from(Mode::OverBand),
                vec![
                    Layer::Background,
                    Layer::Main,
                    Layer::CharacteristicsBand,
                    Layer::CharacteristicsBorder,
                    Layer::IllustrationBorder,
                    Layer::Luck,
                    Layer::Charisma,
                    Layer::LuckBorder,
                    Layer::CharismaBorder,
                    Layer::Illustration,
                    Layer::CharacteristicsName,
                    Layer::CharacteristicsValue,
                    Layer::LuckValue,
                    Layer::CharismaValue,
                    Layer::Name,
                    Layer::Type,
                    Layer::Title,
                    Layer::Faction,
                    Layer::Symbols,
                    Layer::Powers,
                    Layer::Pictograms,
                    Layer::Gender,
                    Layer::Era,
                    Layer::Archetype,
                    Layer::ExtraDeck,
                    Layer::Border,
                ]
            );
        }

        #[test]
        fn test_position_into_layer() {
            assert_eq!(Layer::PowersTopLeft, Position::TopLeft.into());
            assert_eq!(Layer::PowersTopRight, Position::TopRight.into());
            assert_eq!(
                Layer::PowersLeftOfPictograms,
                Position::LeftOfPictograms.into()
            );
            assert_eq!(Layer::Name, Position::LeftOfName.into());
            assert_eq!(Layer::Name, Position::RightOfName.into());
            assert_eq!(Layer::Type, Position::LeftOfType.into());
            assert_eq!(Layer::Type, Position::RightOfType.into());
            assert_eq!(Layer::Title, Position::LeftOfTitle.into());
            assert_eq!(Layer::Title, Position::RightOfTitle.into());
            assert!(std::panic::catch_unwind(|| Layer::from(Position::Default)).is_err());
        }

        #[test]
        fn test_iter_layer() {
            for layer in Layer::iter() {
                assert_eq!(layer.clone(), layer)
            }
        }
    }

    #[cfg(test)]
    mod test_struct_point {
        use super::super::Point;

        use super::super::Size;
        use usvg::Transform;

        #[test]
        fn test_serde_point() {
            let point = Point { x: 1.0, y: 2.0 };
            let s = "x: 1.0\ny: 2.0\n";
            assert_eq!(
                s,
                serde_yaml::to_string(&point).expect("A test should never fail")
            );
            assert_eq!(
                point,
                serde_yaml::from_str(&s).expect("A test should never fail")
            );
        }

        #[test]
        fn test_debug_point() {
            assert_eq!(
                "Point { x: 1.0, y: 2.0 }",
                format!("{:?}", Point { x: 1.0, y: 2.0 })
            );
        }

        #[test]
        fn test_clone_point() {
            assert_eq!(Point { x: 1.0, y: 2.0 }.clone(), Point { x: 1.0, y: 2.0 });
        }

        #[test]
        fn test_default_point() {
            assert_eq!(Point { x: 0.0, y: 0.0 }, Point::default());
        }

        #[test]
        fn test_math_point() {
            assert_eq!(Point { x: 2.0, y: 4.0 }, Point { x: 4.0, y: 8.0 } / 2.0);
            assert_eq!(Point { x: 8.0, y: 16.0 }, Point { x: 4.0, y: 8.0 } * 2.0);
            assert_eq!(Point { x: 2.0, y: 4.0 }, -Point { x: -2.0, y: -4.0 });
            assert_eq!(
                Point { x: 4.0, y: 8.0 },
                Point { x: 2.0, y: 4.0 } - Point { x: -2.0, y: -4.0 }
            );
            assert_eq!(
                Point { x: 4.0, y: 8.0 },
                Point { x: 2.0, y: 4.0 } + Point { x: 2.0, y: 4.0 }
            );
            assert_eq!(
                Point { x: 1.0, y: 1.0 },
                Point { x: 2.0, y: 4.0 } / Point { x: 2.0, y: 4.0 }
            );
            assert_eq!(
                Point { x: 4.0, y: 16.0 },
                Point { x: 2.0, y: 4.0 } * Point { x: 2.0, y: 4.0 }
            );
        }

        #[test]
        fn test_into_point() {
            assert_eq!(
                Point { x: 1.0, y: 2.0 },
                Size {
                    width: 1.0,
                    height: 2.0
                }
                .into()
            );
            #[cfg(feature = "gui")]
            assert_eq!(
                Point { x: 1.0, y: 2.0 },
                iced::Point { x: 1.0, y: 2.0 }.into()
            );
            #[cfg(feature = "gui")]
            assert_eq!(
                Point { x: 3.0, y: 4.0 },
                iced::Rectangle {
                    x: 1.0,
                    y: 2.0,
                    width: 3.0,
                    height: 4.0
                }
                .into()
            );
        }

        #[test]
        fn test_from_point() {
            assert_eq!(
                Transform::from_translate(1.0, 2.0),
                Point { x: 1.0, y: 2.0 }.into()
            );
        }
    }

    #[cfg(test)]
    mod test_struct_size {
        use super::super::Size;

        use super::super::Point;
        use crate::error::Error;
        #[test]
        fn test_serde_size() {
            let size = Size {
                width: 1.0,
                height: 2.0,
            };
            let s = "width: 1.0\nheight: 2.0\n";
            assert_eq!(
                s,
                serde_yaml::to_string(&size).expect("A test should never fail")
            );
            assert_eq!(
                size,
                serde_yaml::from_str(&s).expect("A test should never fail")
            );
        }

        #[test]
        fn test_debug_size() {
            assert_eq!(
                "Size { width: 1.0, height: 2.0 }",
                format!(
                    "{:?}",
                    Size {
                        width: 1.0,
                        height: 2.0
                    }
                )
            );
        }

        #[test]
        fn test_clone_size() {
            assert_eq!(
                Size {
                    width: 1.0,
                    height: 2.0
                }
                .clone(),
                Size {
                    width: 1.0,
                    height: 2.0
                }
            );
        }

        #[test]
        fn test_scale_to_width_size() {
            assert_eq!(
                Size {
                    width: 2.0,
                    height: 4.0
                },
                Size {
                    width: 1.0,
                    height: 2.0
                }
                .scale_to_width(Some(2.0))
            );
            assert_eq!(
                Size {
                    width: 1.0,
                    height: 2.0
                },
                Size {
                    width: 1.0,
                    height: 2.0
                }
                .scale_to_width(None)
            );
        }

        #[test]
        fn test_partial_eq_size() {
            assert_ne!(
                Size {
                    width: 1.0,
                    height: 2.0
                },
                Size {
                    width: 2.0,
                    height: 4.0
                }
            );
            assert_eq!(
                Size {
                    width: 1.0,
                    height: 2.0
                },
                Size {
                    width: 1.9,
                    height: 2.9
                }
            );
        }

        #[test]
        fn test_math_size() {
            assert_eq!(
                Size {
                    width: 2.0,
                    height: 4.0
                },
                Size {
                    width: 4.0,
                    height: 8.0
                } / 2.0
            );
            assert_eq!(
                Size {
                    width: 8.0,
                    height: 16.0
                },
                Size {
                    width: 4.0,
                    height: 8.0
                } * 2.0
            );
        }

        #[test]
        fn test_into_size() {
            assert_eq!(
                Size {
                    width: 1.0,
                    height: 2.0
                },
                usvg::Rect::from_xywh(0., 0., 1.0, 2.0)
                    .expect("A test should never fail")
                    .into()
            );
            assert_eq!(
                Size {
                    width: 1.0,
                    height: 2.0
                },
                usvg::Rect::from_xywh(5., 1., 1.0, 2.0)
                    .expect("A test should never fail")
                    .into()
            );
        }

        #[test]
        fn test_from_size() {
            assert_eq!(
                usvg::Size::from_wh(1.0, 2.0).expect("A test should never fail"),
                usvg::Size::try_from(Size {
                    width: 1.0,
                    height: 2.0
                })
                .expect("A test should never fail")
            );
            assert_eq!(
                usvg::Size::try_from(Size {
                    width: 0.0,
                    height: 0.0
                }),
                Err(Error::ConversionError)
            );

            assert_eq!(
                Point { x: 1.0, y: 2.0 },
                Size {
                    width: 1.0,
                    height: 2.0
                }
                .into()
            );

            let view_box = usvg::ViewBox {
                rect: usvg::NonZeroRect::from_xywh(0., 0., 1.0, 2.0)
                    .expect("A test should never fail"),
                aspect: usvg::AspectRatio::default(),
            };
            let size: usvg::ViewBox = Size {
                width: 1.0,
                height: 2.0,
            }
            .into();
            assert_eq!(view_box.rect, size.rect);
            assert_eq!(view_box.aspect, size.aspect);
        }
    }

    #[cfg(test)]
    mod test_struct_group {
        use super::super::Group;

        use super::super::{config::Config, HasNode, Layer, Point, RenderedCard, Size};
        use crate::color::Color;

        #[test]
        fn test_default_group() {
            let _ = Group::default();
        }

        #[test]
        fn test_clone_group() {
            let _ = Group::default().clone();
        }

        #[test]
        fn test_debug_group() {
            let group = Group::default();

            assert_eq!(
                format!("{:?}", group),
                format!("Group {{ node: {:?} }}", group.node)
            );
        }

        #[test]
        fn test_from_group() {
            let _ = usvg::Group::from(Group::default());

            let _ = usvg::Node::from(Group::default());

            let tree: usvg::Tree = Group::default().into();
            assert_eq!(
                tree.size,
                usvg::Size::from_wh(1., 1.).expect("A test should never fail")
            );
            let view_box: usvg::ViewBox = Size {
                width: 0.,
                height: 0.,
            }
            .into();
            assert_eq!(tree.view_box.rect, view_box.rect);
            assert_eq!(tree.view_box.aspect, view_box.aspect);
            assert_eq!(tree.root.children.len(), 0);

            let mut group = Group::default();
            let path = usvg::Path::new(
                usvg::tiny_skia_path::PathBuilder::from_rect(
                    usvg::Rect::from_xywh(0., 0., 10., 10.).expect("A test should never fail"),
                )
                .into(),
            );
            group.append(usvg::Node::Path(Box::new(path)));

            let tree: usvg::Tree = group.into();
            assert_eq!(
                tree.size,
                usvg::Size::from_wh(10., 10.).expect("A test should never fail")
            );
            let view_box: usvg::ViewBox = Size {
                width: 10.,
                height: 10.,
            }
            .into();
            assert_eq!(tree.view_box.rect, view_box.rect);
            assert_eq!(tree.view_box.aspect, view_box.aspect);
            assert_eq!(tree.root.children.len(), 1);
        }

        #[test]
        fn test_new_group() {
            let group = Group::new("Test", usvg::Transform::identity());

            let node: usvg::Node = group.into();

            assert!(node.has_node("Test"));
        }

        #[test]
        fn test_append_group() {
            let mut group = Group::default();

            group.append(Group::new("Group", usvg::Transform::identity()));

            let mut usvg_group = usvg::Group::default();
            usvg_group.id = "UsvgGroup".to_owned();
            group.append(usvg::Node::Group(Box::new(usvg_group)));

            let mut path = usvg::Path::new(
                usvg::tiny_skia_path::PathBuilder::from_rect(
                    usvg::Rect::from_xywh(0., 0., 10., 10.).expect("A test should never fail"),
                )
                .into(),
            );
            path.id = "Path".to_owned();
            group.append(usvg::Node::Path(Box::new(path)));

            let data = include_bytes!("../tests/images/Alakir.png");
            let image = image::ImageReader::with_format(
                std::io::Cursor::new(data),
                image::ImageFormat::Png,
            );
            let (width, height) = image.into_dimensions().expect("A test should never fail.");
            let size = Size {
                width: width as f32,
                height: height as f32,
            };
            let image = usvg::Image {
                id: "Image".to_owned(),
                visibility: usvg::Visibility::Visible,
                view_box: size.into(),
                rendering_mode: usvg::ImageRendering::OptimizeQuality,
                kind: usvg::ImageKind::PNG(data.to_vec().into()),
                abs_transform: usvg::Transform::default(),
                bounding_box: None,
            };
            group.append(usvg::Node::Image(Box::new(image)));

            let text = RenderedCard::create_text(
                "Text",
                "Test",
                Color::Pink,
                100.,
                &Config::default()
                    .get_font(&Layer::CharacteristicsName)
                    .expect("A test should never fail"),
                Point::default(),
                usvg::TextAnchor::Middle,
            );
            group.append(usvg::Node::Text(Box::new(text)));

            let group: usvg::Node = group.into();
            assert!(group.has_node("Group"));
            assert!(group.has_node("UsvgGroup"));
            assert!(group.has_node("Path"));
            assert!(group.has_node("Image"));
            assert!(group.has_node("Text"));
        }

        #[test]
        fn test_size_group() {
            let mut node = Group::default();
            let group = Group::default();
            node.append(group.clone());

            assert_eq!(node.size(), group.size());

            let mut node = Group::default();
            let mut group = usvg::Group::default();
            node.append(usvg::Node::Group(Box::new(group.clone())));

            group.calculate_bounding_boxes();
            assert!(node.size().is_none());
            assert!(group.bounding_box.is_none());

            let mut node = Group::default();
            let path = usvg::Path::new(
                usvg::tiny_skia_path::PathBuilder::from_rect(
                    usvg::Rect::from_xywh(0., 0., 10., 10.).expect("A test should never fail"),
                )
                .into(),
            );
            node.append(usvg::Node::Path(Box::new(path.clone())));

            assert_eq!(
                node.size(),
                Some(Size {
                    width: 10.,
                    height: 10.
                })
            );
            assert!(path.bounding_box.is_none());

            let mut node = Group::default();
            let data = include_bytes!("../tests/images/Alakir.png");
            let image = image::ImageReader::with_format(
                std::io::Cursor::new(data),
                image::ImageFormat::Png,
            );
            let (width, height) = image.into_dimensions().expect("A test should never fail.");
            let size = Size {
                width: width as f32,
                height: height as f32,
            };
            let image = usvg::Image {
                id: "Image".to_owned(),
                visibility: usvg::Visibility::Visible,
                view_box: size.into(),
                rendering_mode: usvg::ImageRendering::OptimizeQuality,
                kind: usvg::ImageKind::PNG(data.to_vec().into()),
                abs_transform: usvg::Transform::default(),
                bounding_box: None,
            };
            node.append(usvg::Node::Image(Box::new(image)));

            assert_eq!(node.size(), Some(size));

            let mut node = Group::default();
            let text = RenderedCard::create_text(
                "Text",
                "Test",
                Color::Pink,
                100.,
                &Config::default()
                    .get_font(&Layer::CharacteristicsName)
                    .expect("A test should never fail"),
                Point::default(),
                usvg::TextAnchor::Middle,
            );
            node.append(usvg::Node::Text(Box::new(text)));

            assert!(node.size().is_none());
        }
    }

    mod test_struct_rendered_card {
        use super::super::RenderedCard;

        use super::super::{HasNode, Layer};
        use crate::card::fields::PowerField;
        use crate::card::illustration::{Mode, Orientation};
        use crate::card::logos::ExtraDeck;
        use crate::card::pictogram::Pictogram;
        use crate::card::symbol::Symbol;
        use crate::card::Card;
        use crate::error::Error;

        use strum::IntoEnumIterator;
        use tempfile::{tempdir, Builder};

        #[test]
        fn test_default_rendered_card() {
            let _ = RenderedCard::default();
        }

        #[test]
        fn to_string_rendered_card() {
            let rendered_card = RenderedCard::default();
            let _: String = rendered_card.try_into().expect("A test should never fail");
        }

        #[test]
        fn test_new_rendered_card() {
            let mut card = Card::new();
            card.symbols.clear();
            card.pictograms.clear();
            card.powers.clear();
            card.extra_deck = ExtraDeck::None;
            for orientation in Orientation::iter() {
                card.set_orientation(orientation);
                for mode in Mode::iter() {
                    card.set_mode(mode);
                    for no_border in [false, true] {
                        let rendered_card =
                            RenderedCard::new(&card, no_border).expect("A test should never fail");

                        for layer in Vec::<Layer>::from(mode) {
                            match layer {
                                Layer::Border => {
                                    assert_eq!(rendered_card.content.has_node(&layer), !no_border)
                                }
                                Layer::Name => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.name.is_empty()
                                ),
                                Layer::Type => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.r#type.is_empty()
                                ),
                                Layer::Title => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.title.is_empty()
                                ),
                                Layer::Symbols => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.symbols.is_empty()
                                ),
                                Layer::Pictograms => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.pictograms.is_empty()
                                ),
                                Layer::Powers => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.powers.is_empty()
                                ),
                                Layer::ExtraDeck => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    card.extra_deck != ExtraDeck::None
                                ),
                                _ => assert!(rendered_card.content.has_node(&layer)),
                            }
                        }
                    }
                }
            }
            card.characteristics.compact_fraction = true;
            card.name.value = "".to_owned();
            card.r#type.value = "".to_owned();
            card.title.value = "Test".to_owned();
            card.symbols.push(Symbol::default().into());
            card.pictograms.push(Pictogram::default());
            card.powers.push(PowerField::default());
            card.extra_deck = ExtraDeck::Machine;

            for orientation in Orientation::iter() {
                card.set_orientation(orientation);
                for mode in Mode::iter() {
                    card.set_mode(mode);
                    for no_border in [false, true] {
                        let rendered_card =
                            RenderedCard::new(&card, no_border).expect("A test should never fail");

                        for layer in Vec::<Layer>::from(mode) {
                            match layer {
                                Layer::Border => {
                                    assert_eq!(rendered_card.content.has_node(&layer), !no_border)
                                }
                                Layer::Name => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.name.is_empty()
                                ),
                                Layer::Type => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.r#type.is_empty()
                                ),
                                Layer::Title => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.title.is_empty()
                                ),
                                Layer::Symbols => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.symbols.is_empty()
                                ),
                                Layer::Pictograms => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.pictograms.is_empty()
                                ),
                                Layer::Powers => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    !card.powers.is_empty()
                                ),
                                Layer::ExtraDeck => assert_eq!(
                                    rendered_card.content.has_node(&layer),
                                    card.extra_deck != ExtraDeck::None
                                ),
                                _ => assert!(rendered_card.content.has_node(&layer)),
                            }
                        }
                    }
                }
            }
        }

        #[test]
        fn test_save_rendered_card() {
            let card = Card::new();
            let mut rendered_card =
                RenderedCard::new(&card, false).expect("A test should never fail");

            let file = Builder::new()
                .suffix(".svg")
                .tempfile()
                .expect("A test should never fail");

            assert!(rendered_card.save(file.path()).is_ok());

            let mut rendered_card =
                RenderedCard::new(&card, false).expect("A test should never fail");

            let file = Builder::new()
                .suffix(".pdf")
                .tempfile()
                .expect("A test should never fail");

            assert!(rendered_card.save(file.path()).is_ok());

            let mut rendered_card =
                RenderedCard::new(&card, false).expect("A test should never fail");

            let file = Builder::new()
                .suffix("")
                .tempfile()
                .expect("A test should never fail");

            assert_eq!(
                rendered_card.save(file.path()),
                Err(Error::UnknownFormatError)
            );

            let mut rendered_card =
                RenderedCard::new(&card, false).expect("A test should never fail");

            let file = Builder::new()
                .suffix(".png")
                .tempfile()
                .expect("A test should never fail");

            assert!(rendered_card.save(file.path()).is_ok());
        }

        #[test]
        fn test_rasterize_rendered_card() {
            let card = Card::new();
            for no_border in [true, false] {
                let mut rendered_card =
                    RenderedCard::new(&card, no_border).expect("A test should never fail");

                let file = Builder::new()
                    .suffix(".png")
                    .tempfile()
                    .expect("A test should never fail");

                assert!(rendered_card.rasterize(file.path(), no_border).is_ok());

                let file = Builder::new()
                    .suffix("")
                    .tempfile()
                    .expect("A test should never fail");

                assert_eq!(
                    rendered_card.rasterize(file.path(), no_border),
                    Err(Error::UnknownFormatError)
                );

                let file = Builder::new()
                    .suffix(".pdf")
                    .tempfile()
                    .expect("A test should never fail");

                assert_eq!(
                    rendered_card.rasterize(file.path(), no_border),
                    Err(Error::UnsupportedFormatError("pdf".to_owned()))
                );

                let filename: &std::ffi::OsStr = unsafe {
                    std::ffi::OsStr::from_encoded_bytes_unchecked(b"tests/output.\xc3\x28")
                };
                assert_eq!(
                    rendered_card.rasterize(filename, no_border),
                    Err(Error::UnknownFormatError)
                );

                let dir = tempdir().expect("A test should never fail");
                let file = dir.path().join(filename);
                assert_eq!(
                    rendered_card.rasterize(&file, no_border),
                    Err(Error::UnknownFormatError)
                );
                assert!(dir.path().join("tests").exists());
            }
        }
    }
}
