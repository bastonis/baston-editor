// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Color picker app

use crate::app::App;
use crate::color::Color;
use crate::error::LogError;
use crate::render::images::RasterImage;

use std::env::current_exe;
use std::process;

use iced::widget::{container, Button, Text};
use iced::{Element, Length};
use iced_aw::widgets::color_picker::ColorPicker as ColorPickerWidget;
use log::info;

const PADDING: u16 = 6;
// const SPACING: u16 = 3;

/// Message for the color picker
#[derive(Debug, PartialEq, Clone)]
pub enum Message {
    /// Message when a color is validated
    Color(Color),
    /// Message when the user chose not to pick a color
    Close,
}

/// Color picker app
#[derive(Debug, PartialEq)]
pub struct ColorPicker {
    /// The current color
    color: Color,
}

impl ColorPicker {
    /// Create a new color picker
    pub fn new(color: Color) -> Self {
        Self { color }
    }
}

impl App<Message> for ColorPicker {
    fn title() -> &'static str {
        "Baston Editor - Color Picker"
    }

    fn icon() -> Option<iced::window::Icon> {
        let icon = RasterImage::from_bytes(include_bytes!("logos/icon.png"), Some(image::ImageFormat::Png))
            .critical("Error while loading src/logos/icon.png, this file is embed in the code so it should not fail")
            .try_into()
            .critical("Error while loading src/logos/icon.png, this file is embed in the code so it should not fail");
        Some(icon)
    }

    fn update(&mut self, message: Message) -> iced::Task<Message> {
        match message {
            Message::Color(color) => {
                self.color = color;
                serde_yaml::to_writer(std::io::stdout(), &self.color)
                    .critical("Error while writing the color");
            }
            Message::Close => (),
        }
        iced::exit()
    }

    fn view(&self) -> Element<Message> {
        container(ColorPickerWidget::new(
            true,
            self.color.into(),
            Button::new(Text::new("Pick color")),
            Message::Close,
            |color| Message::Color(color.into()),
        ))
        .padding(PADDING)
        .center_x(Length::Fill)
        .center_y(Length::Fill)
        .into()
    }
}

/// Open the color picker to select a color
///
/// Returns the selected color if any, None otherwise
pub fn select_color(color: Color) -> Option<Color> {
    info!("Opening color picker");
    let exe = current_exe().critical("There should be a executable path");

    let mut cmd = process::Command::new(exe);

    cmd.arg("color-picker").arg(
        serde_yaml::to_string(&color).critical("We should never have issue serializing a color"),
    );

    cmd.stderr(process::Stdio::inherit());

    let output = cmd
        .output()
        .critical("Color picker should have finished with an output");

    if !output.status.success() {
        panic!("{:?}", output);
    }

    info!("Color picker exited with {:?}", output);

    if output.stdout.is_empty() {
        None
    } else {
        Some(
            serde_yaml::from_str(
                std::str::from_utf8(&output.stdout).critical("The output should be a valid utf8"),
            )
            .critical("We should never have issue deserializing a color"),
        )
    }
}

#[cfg(test)]
mod test_color_picker {
    use super::ColorPicker;
    use super::Message;

    use crate::app::App;
    use crate::color::Color;

    #[test]
    fn test_new() {
        let color_picker = ColorPicker::new(Color::default());
        assert_eq!(
            color_picker,
            ColorPicker {
                color: Color::default()
            }
        )
    }

    #[test]
    fn test_update() {
        let mut color_picker = ColorPicker::new(Color::default());
        let _ = color_picker.update(Message::Color(Color::Black));
        assert_eq!(
            color_picker,
            ColorPicker {
                color: Color::Black
            }
        );
        let _ = color_picker.update(Message::Close);
        assert_eq!(
            color_picker,
            ColorPicker {
                color: Color::Black
            }
        )
    }

    #[test]
    fn test_view() {
        let color_picker = ColorPicker::new(Color::White);
        color_picker.view();
    }

    #[test]
    fn test_title() {
        assert_eq!(ColorPicker::title(), "Baston Editor - Color Picker");
    }

    #[test]
    fn test_icon() {
        assert!(ColorPicker::icon().is_some());
    }
}
