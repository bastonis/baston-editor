// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Module for types used in the library

use crate::error::Error;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

use serde::{de::Error as SerdeError, Deserialize, Deserializer, Serialize, Serializer};
use serde_yaml::Value;

/// Represents the value of a characteristic
#[derive(Debug, Clone, PartialEq, Copy)]
pub enum CharacteristicValue {
    /// A dices throw
    DiceThrow(DiceThrow),
    /// A value
    Value(FixedPointNumber),
    /// An empty value
    Empty,
}

/// Represents a Dice
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Dice {
    /// Represents a dice with two faces
    D2,

    /// Represents a dice with three faces
    D3,

    /// Represents a dice with four faces
    D4,

    /// Represents a dice with six faces
    D6,

    /// Represents a dice with eight faces
    D8,

    /// Represents a dice with ten faces
    D10,

    /// Represents a dice with twelve faces
    D12,

    /// Represents a dice with twenty faces
    D20,

    /// Represents a dice with thirty faces
    D30,

    /// Represents a dice with forty faces
    D40,

    /// Represents a dice with one hundred faces
    D100,
}

/// Represents the throw of identical dices
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct DiceThrow {
    /// Type of dices thrown
    pub dice: Dice,
    /// Number of dices thrown
    pub throw: u8,
}

/// Represents a value between 0 and 127.5 incremented by 0.5
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct FixedPointNumber {
    /// The double of the value
    value: i16,
}

impl CharacteristicValue {
    /// Returns the string representing the value in a nice way, respecting the compact fraction
    pub fn to_nice_string(&self, compact_fraction: bool) -> String {
        match self {
            Self::DiceThrow(dice_throw) => dice_throw.to_string(),
            Self::Value(value) => value.to_nice_string(compact_fraction),
            Self::Empty => " ".to_owned(),
        }
    }
}

impl FixedPointNumber {
    /// Returns the string representing the value in a nice way, respecting the compact fraction
    pub fn to_nice_string(self, compact_fraction: bool) -> String {
        if self.value % 2 != 0 {
            if (self.value / 2) == 0 {
                format!(
                    "{}{}",
                    if self.value > 0 { "" } else { "-" },
                    if compact_fraction { "¼" } else { "½" }
                )
            } else if compact_fraction {
                format!(" {}¼", (self.value / 2))
            } else {
                format!("{}½", (self.value / 2))
            }
        } else {
            (self.value / 2).to_string()
        }
    }
}

impl Display for Dice {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            Self::D2 => write!(f, "d2"),
            Self::D3 => write!(f, "d3"),
            Self::D4 => write!(f, "d4"),
            Self::D6 => write!(f, "d6"),
            Self::D8 => write!(f, "d8"),
            Self::D10 => write!(f, "d10"),
            Self::D12 => write!(f, "d12"),
            Self::D20 => write!(f, "d20"),
            Self::D30 => write!(f, "d30"),
            Self::D40 => write!(f, "d40"),
            Self::D100 => write!(f, "d100"),
        }
    }
}

impl FromStr for Dice {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "D2" | "d2" => Ok(Self::D2),
            "D3" | "d3" => Ok(Self::D3),
            "D4" | "d4" => Ok(Self::D4),
            "D6" | "d6" => Ok(Self::D6),
            "D8" | "d8" => Ok(Self::D8),
            "D10" | "d10" => Ok(Self::D10),
            "D12" | "d12" => Ok(Self::D12),
            "D20" | "d20" => Ok(Self::D20),
            "D30" | "d30" => Ok(Self::D30),
            "D40" | "d40" => Ok(Self::D40),
            "D100" | "d100" => Ok(Self::D100),
            s => Err(Error::ValueError {
                expected: "valide dice".to_owned(),
                current: s.to_string(),
            }),
        }
    }
}

impl Display for DiceThrow {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        if self.throw != 1 {
            write!(f, "{}{}", self.throw, self.dice)
        } else {
            write!(f, "{}", self.dice)
        }
    }
}

impl FromStr for DiceThrow {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let value_up = value.to_uppercase();
        let fields: Vec<&str> = value_up.split('D').collect();
        if fields.len() != 2 {
            return Err(Error::ValueError {
                expected: "XDY with X and Y u8".to_owned(),
                current: value.to_string(),
            });
        }
        let throw = if fields[0].is_empty() {
            1
        } else {
            match fields[0].parse::<u8>() {
                Ok(v) => v,
                Err(_) => {
                    return Err(Error::ValueError {
                        expected: "XDY with X and Y u8".to_owned(),
                        current: value.to_string(),
                    })
                }
            }
        };
        let dice = match ("D".to_owned() + fields[1]).parse::<Dice>() {
            Ok(v) => v,
            Err(_) => {
                return Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: value.to_string(),
                })
            }
        };
        Ok(Self { dice, throw })
    }
}

impl Display for FixedPointNumber {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        if self.value % 2 != 0 {
            write!(f, "{}.5", self.value / 2)
        } else {
            write!(f, "{}", self.value / 2)
        }
    }
}

impl FromStr for FixedPointNumber {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value.parse::<i16>() {
            Ok(v) => Ok(v.into()),
            Err(_) => match value.parse::<f32>() {
                Ok(v) => Ok(v.into()),
                Err(_) => Err(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: value.to_string(),
                }),
            },
        }
    }
}

impl From<i16> for FixedPointNumber {
    fn from(value: i16) -> Self {
        if value > i16::MAX / 2 {
            Self { value: i16::MAX }
        } else if value < i16::MIN / 2 {
            Self { value: i16::MIN }
        } else {
            Self { value: value * 2 }
        }
    }
}

impl From<f32> for FixedPointNumber {
    fn from(value: f32) -> Self {
        Self::from(value as f64)
    }
}

impl From<f64> for FixedPointNumber {
    fn from(value: f64) -> Self {
        let value_rounded = value.round();
        let mut nb: FixedPointNumber = (value_rounded as i16).into();
        if value - value_rounded > 0.25 {
            nb.value += 1
        } else if value_rounded - value > 0.25 {
            nb.value -= 1
        }
        nb
    }
}

impl From<i32> for FixedPointNumber {
    fn from(value: i32) -> Self {
        (value as i16).into()
    }
}

impl From<i64> for FixedPointNumber {
    fn from(value: i64) -> Self {
        (value as i16).into()
    }
}

impl Display for CharacteristicValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}",
            match self {
                Self::DiceThrow(v) => v.to_string(),
                Self::Value(v) => v.to_string(),
                Self::Empty => "".to_owned(),
            }
        )
    }
}

impl FromStr for CharacteristicValue {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        if value.is_empty() || value == " " {
            return Ok(Self::Empty);
        }
        match value.parse::<DiceThrow>() {
            Ok(v) => Ok(Self::DiceThrow(v)),
            Err(err) => match value.parse::<FixedPointNumber>() {
                Ok(v) => Ok(Self::Value(v)),
                Err(err2) => Err(err.merge(err2)),
            },
        }
    }
}

impl TryFrom<&str> for CharacteristicValue {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::from_str(value)
    }
}

impl From<i16> for CharacteristicValue {
    fn from(value: i16) -> Self {
        Self::Value(value.into())
    }
}

impl From<f64> for CharacteristicValue {
    fn from(value: f64) -> Self {
        Self::Value(value.into())
    }
}

impl<'de> Deserialize<'de> for CharacteristicValue {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value: Value = Deserialize::deserialize(deserializer)?;

        match value {
            Value::Null => Ok(Self::default()),
            Value::String(s) => Self::from_str(&s).map_err(D::Error::custom),
            Value::Number(val) => {
                if let Some(val) = val.as_f64() {
                    Ok(Self::Value(val.into()))
                } else if let Some(val) = val.as_i64() {
                    Ok(Self::Value((val as i16).into()))
                } else {
                    Err(D::Error::custom(Error::ValueError {
                        expected: "valide fixed point number (i15.X) or XDY with X and Y u8"
                            .to_string(),
                        current: val.to_string(),
                    }))
                }
            }
            _ => Err(D::Error::custom(Error::ValueError {
                expected: "valide fixed point number (i15.X) or XDY with X and Y u8".to_owned(),
                current: format!("{:?}", value),
            })),
        }
    }
}

impl Serialize for CharacteristicValue {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Self::Value(v) => v.serialize(serializer),
            Self::DiceThrow(dice_throw) => serializer.serialize_str(&dice_throw.to_string()),
            Self::Empty => serializer.serialize_str(""),
        }
    }
}

impl Serialize for FixedPointNumber {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        if self.value % 2 == 0 {
            serializer.serialize_i16(self.value / 2)
        } else {
            serializer.serialize_f64(self.value as f64 / 2.)
        }
    }
}

impl Default for CharacteristicValue {
    fn default() -> Self {
        Self::Empty
    }
}

#[cfg(test)]
mod test_module_types {

    #[cfg(test)]
    mod test_enum_dice {
        use super::super::Dice;

        use crate::error::Error;
        use std::str::FromStr;

        #[test]
        fn test_init_dice() {
            assert_eq!(
                Dice::from_str("D2").expect("A test should never fail"),
                Dice::D2
            );
            assert_eq!(
                Dice::from_str("D3").expect("A test should never fail"),
                Dice::D3
            );
            assert_eq!(
                Dice::from_str("D4").expect("A test should never fail"),
                Dice::D4
            );
            assert_eq!(
                Dice::from_str("D6").expect("A test should never fail"),
                Dice::D6
            );
            assert_eq!(
                Dice::from_str("D8").expect("A test should never fail"),
                Dice::D8
            );
            assert_eq!(
                Dice::from_str("D10").expect("A test should never fail"),
                Dice::D10
            );
            assert_eq!(
                Dice::from_str("D12").expect("A test should never fail"),
                Dice::D12
            );
            assert_eq!(
                Dice::from_str("D20").expect("A test should never fail"),
                Dice::D20
            );
            assert_eq!(
                Dice::from_str("D30").expect("A test should never fail"),
                Dice::D30
            );
            assert_eq!(
                Dice::from_str("D40").expect("A test should never fail"),
                Dice::D40
            );
            assert_eq!(
                Dice::from_str("D100").expect("A test should never fail"),
                Dice::D100
            );
            assert_eq!(
                Dice::from_str("d2").expect("A test should never fail"),
                Dice::D2
            );
            assert_eq!(
                Dice::from_str("d3").expect("A test should never fail"),
                Dice::D3
            );
            assert_eq!(
                Dice::from_str("d4").expect("A test should never fail"),
                Dice::D4
            );
            assert_eq!(
                Dice::from_str("d6").expect("A test should never fail"),
                Dice::D6
            );
            assert_eq!(
                Dice::from_str("d8").expect("A test should never fail"),
                Dice::D8
            );
            assert_eq!(
                Dice::from_str("d10").expect("A test should never fail"),
                Dice::D10
            );
            assert_eq!(
                Dice::from_str("d12").expect("A test should never fail"),
                Dice::D12
            );
            assert_eq!(
                Dice::from_str("d20").expect("A test should never fail"),
                Dice::D20
            );
            assert_eq!(
                Dice::from_str("d30").expect("A test should never fail"),
                Dice::D30
            );
            assert_eq!(
                Dice::from_str("d40").expect("A test should never fail"),
                Dice::D40
            );
            assert_eq!(
                Dice::from_str("d100").expect("A test should never fail"),
                Dice::D100
            );
            assert_eq!(
                Dice::from_str("D7"),
                Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: "D7".to_owned()
                })
            );
            assert_eq!(
                Dice::from_str("d9"),
                Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: "d9".to_owned()
                })
            );
        }

        #[test]
        fn test_clone_dice() {
            assert_eq!(Dice::D2.clone(), Dice::D2);
            assert_eq!(Dice::D3.clone(), Dice::D3);
            assert_eq!(Dice::D4.clone(), Dice::D4);
            assert_eq!(Dice::D6.clone(), Dice::D6);
            assert_eq!(Dice::D8.clone(), Dice::D8);
            assert_eq!(Dice::D10.clone(), Dice::D10);
            assert_eq!(Dice::D12.clone(), Dice::D12);
            assert_eq!(Dice::D20.clone(), Dice::D20);
            assert_eq!(Dice::D30.clone(), Dice::D30);
            assert_eq!(Dice::D40.clone(), Dice::D40);
            assert_eq!(Dice::D100.clone(), Dice::D100);
        }

        #[test]
        fn test_display_dice() {
            assert_eq!(Dice::D2.to_string(), "d2");
            assert_eq!(Dice::D3.to_string(), "d3");
            assert_eq!(Dice::D4.to_string(), "d4");
            assert_eq!(Dice::D6.to_string(), "d6");
            assert_eq!(Dice::D8.to_string(), "d8");
            assert_eq!(Dice::D10.to_string(), "d10");
            assert_eq!(Dice::D12.to_string(), "d12");
            assert_eq!(Dice::D20.to_string(), "d20");
            assert_eq!(Dice::D30.to_string(), "d30");
            assert_eq!(Dice::D40.to_string(), "d40");
            assert_eq!(Dice::D100.to_string(), "d100");
        }
    }

    #[cfg(test)]
    mod test_struct_dice_throw {
        use super::super::DiceThrow;

        use super::super::Dice;
        use crate::error::Error;
        use std::str::FromStr;

        #[test]
        fn test_init_dice_throw() {
            assert_eq!(
                DiceThrow::from_str("1D2").expect("A test should never fail"),
                DiceThrow {
                    dice: Dice::D2,
                    throw: 1
                }
            );
            assert_eq!(
                DiceThrow::from_str("D2").expect("A test should never fail"),
                DiceThrow {
                    dice: Dice::D2,
                    throw: 1
                }
            );
            assert_eq!(
                DiceThrow::from_str("1d2").expect("A test should never fail"),
                DiceThrow {
                    dice: Dice::D2,
                    throw: 1
                }
            );
            assert_eq!(
                DiceThrow::from_str("d2").expect("A test should never fail"),
                DiceThrow {
                    dice: Dice::D2,
                    throw: 1
                }
            );
            assert_eq!(
                DiceThrow::from_str("1D7"),
                Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: "1D7".to_owned()
                })
            );
            assert_eq!(
                DiceThrow::from_str("aD7"),
                Err(Error::ValueError {
                    expected: "XDY with X and Y u8".to_owned(),
                    current: "aD7".to_owned()
                })
            );
        }

        #[test]
        fn test_debug_dice_throw() {
            assert_eq!(
                format!(
                    "{:?}",
                    DiceThrow {
                        dice: Dice::D2,
                        throw: 1
                    }
                ),
                "DiceThrow { dice: D2, throw: 1 }"
            );
        }

        #[test]
        fn test_clone_dice_throw() {
            assert_eq!(
                DiceThrow {
                    dice: Dice::D2,
                    throw: 1
                }
                .clone(),
                DiceThrow {
                    dice: Dice::D2,
                    throw: 1
                }
            );
        }

        #[test]
        fn test_display_dice_throw() {
            assert_eq!(
                DiceThrow::from_str("1D2")
                    .expect("A test should never fail")
                    .to_string(),
                "d2"
            );
            assert_eq!(
                DiceThrow::from_str("2D2")
                    .expect("A test should never fail")
                    .to_string(),
                "2d2"
            );
        }
    }

    #[cfg(test)]
    mod test_struct_fixed_point_number {

        use super::super::FixedPointNumber;

        use crate::error::Error;
        use std::str::FromStr;

        #[test]
        fn test_init_fixed_point_number() {
            assert_eq!(
                FixedPointNumber::from_str("12").expect("A test should never fail"),
                FixedPointNumber { value: 24 }
            );
            assert_eq!(
                FixedPointNumber::from_str("17.2").expect("A test should never fail"),
                FixedPointNumber { value: 34 }
            );
            assert_eq!(
                FixedPointNumber::from_str("17.5").expect("A test should never fail"),
                FixedPointNumber { value: 35 }
            );
            assert_eq!(
                FixedPointNumber::from_str("17.8").expect("A test should never fail"),
                FixedPointNumber { value: 36 }
            );
            assert_eq!(
                FixedPointNumber::from_str("17.a"),
                Err(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: "17.a".to_owned()
                })
            );
            assert_eq!(
                FixedPointNumber::from_str("a.5"),
                Err(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: "a.5".to_owned()
                })
            );
        }

        #[test]
        fn test_display_fixed_point_number() {
            assert_eq!(
                FixedPointNumber::from_str("12")
                    .expect("A test should never fail")
                    .to_string(),
                "12"
            );
            assert_eq!(
                FixedPointNumber::from_str("12.5")
                    .expect("A test should never fail")
                    .to_string(),
                "12.5"
            );
        }

        #[test]
        fn test_debug_fixed_point_number() {
            assert_eq!(
                format!("{:?}", FixedPointNumber { value: 24 }),
                "FixedPointNumber { value: 24 }"
            );
            assert_eq!(
                format!("{:?}", FixedPointNumber { value: -24 }),
                "FixedPointNumber { value: -24 }"
            );
        }

        #[test]
        fn test_clone_fixed_point_number() {
            assert_eq!(
                FixedPointNumber { value: 24 }.clone(),
                FixedPointNumber { value: 24 }
            );
            assert_eq!(
                FixedPointNumber { value: -24 }.clone(),
                FixedPointNumber { value: -24 }
            );
        }

        #[test]
        fn test_from_i32_fixed_point_number() {
            assert_eq!(FixedPointNumber::from(12), FixedPointNumber { value: 24 });
            assert_eq!(FixedPointNumber::from(-12), FixedPointNumber { value: -24 });
        }

        #[test]
        fn test_from_i64_fixed_point_number() {
            assert_eq!(
                FixedPointNumber::from(12 as i64),
                FixedPointNumber { value: 24 }
            );
            assert_eq!(
                FixedPointNumber::from(-12 as i64),
                FixedPointNumber { value: -24 }
            );
        }
    }

    #[cfg(test)]
    mod test_struct_characteristic_value {

        use super::super::CharacteristicValue;

        use super::super::{Dice, DiceThrow, FixedPointNumber};
        use crate::error::Error;
        use std::str::FromStr;

        #[test]
        fn test_deserialize_characteristic_value() {
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("12")
                    .expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("-12")
                    .expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: -24 })
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("\"12\"")
                    .expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("1D12")
                    .expect("A test should never fail"),
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("D12")
                    .expect("A test should never fail"),
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("12.12")
                    .expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("''")
                    .expect("A test should never fail"),
                CharacteristicValue::Empty
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("1D29").map_err(Error::from),
                Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: "1D29".to_owned()
                }
                .merge(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: "1D29".to_owned()
                }))
            );
            assert_eq!(
                serde_yaml::from_str::<CharacteristicValue>("").expect("A test should never fail"),
                CharacteristicValue::Empty
            );
            assert!(serde_yaml::from_str::<CharacteristicValue>("300000000000000000000").is_err());

            assert!(serde_yaml::from_str::<CharacteristicValue>("Something: Wrong").is_err());
        }

        #[test]
        fn test_deserialize_option_characteristic_value() {
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("12")
                    .expect("A test should never fail"),
                Some(CharacteristicValue::Value(FixedPointNumber { value: 24 }))
            );
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("\"12\"")
                    .expect("A test should never fail"),
                Some(CharacteristicValue::Value(FixedPointNumber { value: 24 }))
            );
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("1D12")
                    .expect("A test should never fail"),
                Some(CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                }))
            );
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("D12")
                    .expect("A test should never fail"),
                Some(CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                }))
            );
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("12.12")
                    .expect("A test should never fail"),
                Some(CharacteristicValue::Value(FixedPointNumber { value: 24 }))
            );
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("''")
                    .expect("A test should never fail"),
                Some(CharacteristicValue::Empty)
            );
            assert_eq!(
                serde_yaml::from_str::<Option<CharacteristicValue>>("1D29").map_err(Error::from),
                Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: "1D29".to_owned()
                }
                .merge(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: "1D29".to_owned()
                }))
            );
        }

        #[test]
        fn test_serialize_characteristic_value() {
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::Value(FixedPointNumber { value: 24 }))
                    .expect("A test should never fail"),
                "12\n"
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::Value(FixedPointNumber { value: 25 }))
                    .expect("A test should never fail"),
                "12.5\n"
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::Value(FixedPointNumber { value: -24 }))
                    .expect("A test should never fail"),
                "-12\n"
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::Value(FixedPointNumber { value: -25 }))
                    .expect("A test should never fail"),
                "-12.5\n"
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                }))
                .expect("A test should never fail"),
                "d12\n"
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 2
                }))
                .expect("A test should never fail"),
                "2d12\n"
            );
            assert_eq!(
                serde_yaml::to_string(&CharacteristicValue::Empty)
                    .expect("A test should never fail"),
                "''\n"
            );
        }

        #[test]
        fn test_init_characteristic_value() {
            assert_eq!(
                CharacteristicValue::from_str("12").expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                CharacteristicValue::from_str("12.5").expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: 25 })
            );
            assert_eq!(
                CharacteristicValue::from_str("-12").expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: -24 })
            );
            assert_eq!(
                CharacteristicValue::from_str("-12.5").expect("A test should never fail"),
                CharacteristicValue::Value(FixedPointNumber { value: -25 })
            );
            assert_eq!(
                CharacteristicValue::from_str("1d12").expect("A test should never fail"),
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
            );
            assert_eq!(
                CharacteristicValue::from_str("d12").expect("A test should never fail"),
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
            );
            assert_eq!(
                CharacteristicValue::from_str("").expect("A test should never fail"),
                CharacteristicValue::Empty
            );
            assert_eq!(
                CharacteristicValue::from_str("test"),
                Err(Error::ValueError {
                    expected: "XDY with X and Y u8".to_owned(),
                    current: "test".to_owned()
                }
                .merge(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: "test".to_owned()
                }))
            );
            assert_eq!(CharacteristicValue::default(), CharacteristicValue::Empty)
        }

        #[test]
        fn test_display_characteristic_value() {
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 24 }).to_string(),
                "12"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 25 }).to_string(),
                "12.5"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: -24 }).to_string(),
                "-12"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: -25 }).to_string(),
                "-12.5"
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
                .to_string(),
                "d12"
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 2
                })
                .to_string(),
                "2d12"
            );
            assert_eq!(CharacteristicValue::Empty.to_string(), "");
        }

        #[test]
        fn test_to_nice_string_characteristic_value() {
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 24 }).to_nice_string(false),
                "12"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 24 }).to_nice_string(true),
                "12"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 25 }).to_nice_string(false),
                "12½"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 25 }).to_nice_string(true),
                " 12¼"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: -24 }).to_nice_string(false),
                "-12"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: -24 }).to_nice_string(true),
                "-12"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: -25 }).to_nice_string(false),
                "-12½"
            );
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: -25 }).to_nice_string(true),
                " -12¼"
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
                .to_nice_string(false),
                "d12"
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
                .to_nice_string(true),
                "d12"
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 2
                })
                .to_nice_string(false),
                "2d12"
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 2
                })
                .to_nice_string(true),
                "2d12"
            );
            assert_eq!(CharacteristicValue::Empty.to_nice_string(true), " ");
            assert_eq!(CharacteristicValue::Empty.to_nice_string(true), " ");
        }

        #[test]
        fn test_from_characteristic_value() {
            assert_eq!(
                CharacteristicValue::from(1. / 3.),
                CharacteristicValue::Value(FixedPointNumber { value: 1 })
            );
            assert_eq!(
                CharacteristicValue::from(-1. / 3.),
                CharacteristicValue::Value(FixedPointNumber { value: -1 })
            );
            assert_eq!(
                CharacteristicValue::from(2. / 3.),
                CharacteristicValue::Value(FixedPointNumber { value: 1 })
            );
            assert_eq!(
                CharacteristicValue::from(-2. / 3.),
                CharacteristicValue::Value(FixedPointNumber { value: -1 })
            );
            assert_eq!(
                CharacteristicValue::from(12.6 as f64),
                CharacteristicValue::Value(FixedPointNumber { value: 25 })
            );
            assert_eq!(
                CharacteristicValue::from(12 as i16),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                CharacteristicValue::from(12),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                CharacteristicValue::from(-10),
                CharacteristicValue::Value(FixedPointNumber { value: -20 })
            );
            assert_eq!(
                CharacteristicValue::from(150),
                CharacteristicValue::Value(FixedPointNumber { value: 300 })
            );
            assert_eq!(
                CharacteristicValue::from(i16::MAX),
                CharacteristicValue::Value(FixedPointNumber { value: i16::MAX })
            );
            assert_eq!(
                CharacteristicValue::from(i16::MIN),
                CharacteristicValue::Value(FixedPointNumber { value: i16::MIN })
            );
            assert_eq!(
                CharacteristicValue::try_from("D12").expect("A test should never fail"),
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
            );
            assert_eq!(
                CharacteristicValue::try_from("D13"),
                Err(Error::ValueError {
                    expected: "valide dice".to_owned(),
                    current: "D13".to_owned()
                }
                .merge(Error::ValueError {
                    expected: "valide fixed point number (i15.X)".to_owned(),
                    current: "D13".to_owned()
                }))
            );
        }

        #[test]
        fn test_debug_characteristic_value() {
            assert_eq!(
                format!(
                    "{:?}",
                    CharacteristicValue::Value(FixedPointNumber { value: 24 })
                ),
                "Value(FixedPointNumber { value: 24 })"
            );
            assert_eq!(
                format!(
                    "{:?}",
                    CharacteristicValue::DiceThrow(DiceThrow {
                        dice: Dice::D12,
                        throw: 1
                    })
                ),
                "DiceThrow(DiceThrow { dice: D12, throw: 1 })"
            );
            assert_eq!(format!("{:?}", CharacteristicValue::Empty), "Empty");
        }

        #[test]
        fn test_clone_characteristic_value() {
            assert_eq!(
                CharacteristicValue::Value(FixedPointNumber { value: 24 }).clone(),
                CharacteristicValue::Value(FixedPointNumber { value: 24 })
            );
            assert_eq!(
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
                .clone(),
                CharacteristicValue::DiceThrow(DiceThrow {
                    dice: Dice::D12,
                    throw: 1
                })
            );
            assert_eq!(
                CharacteristicValue::Empty.clone(),
                CharacteristicValue::Empty
            );
        }
    }
}
