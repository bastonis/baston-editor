// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Some utilities that are used across the project

use std::ffi::OsStr;
use std::path::Path;

/// Checks if the given path as an extension matching one of the given extensions
///
/// # Examples
///
/// ```rust
/// use baston_editor::utilities::check_extension;
///
/// assert!(check_extension("tests/test.svg", &["svg"]));
/// assert!(!check_extension("tests/test.svg", &["pdf"]));
/// assert!(!check_extension("tests/test", &["svg"]));
/// ```
pub fn check_extension<P: AsRef<OsStr> + AsRef<Path>>(path: P, extensions: &[&str]) -> bool {
    let path = Path::new(&path);
    if let Some(extension) = path.extension() {
        if let Some(extension) = extension.to_ascii_lowercase().to_str() {
            for ext in extensions {
                if extension == *ext {
                    return true;
                }
            }
        }
    }
    false
}

#[cfg(test)]
mod test_module_utilities {
    use super::check_extension;

    use std::ffi::OsStr;

    #[test]
    fn test_check_extension() {
        assert!(check_extension("tests/test.svg", &["svg"]));
        assert!(!check_extension("tests/test.svg", &["pdf"]));
        assert!(!check_extension("tests/test", &["svg"]));
        let filename: &OsStr =
            unsafe { OsStr::from_encoded_bytes_unchecked(b"tests/output.\xc3\x28") };
        assert!(!check_extension(filename, &["svg"]));
    }
}
