// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Keeps the binary up to date

use crate::error::{Error, LogError};

use std::cmp::Ordering;
#[cfg(feature = "auto_update")]
use std::collections::BTreeMap;
use std::str::FromStr;

#[cfg(feature = "auto_update")]
use log::{info, warn};
use regex::RegexBuilder;
#[cfg(feature = "gui")]
use rfd::{MessageButtons, MessageDialog, MessageDialogResult, MessageLevel};
#[cfg(feature = "auto_update")]
use self_update::backends::gitlab;
#[cfg(feature = "auto_update")]
use self_update::update::Release;
use serde::{Deserialize, Serialize};

/// The version of the binary
pub fn current_version() -> Version {
    Version::try_from(env!("CARGO_PKG_VERSION")).critical("Current version should be right")
}

/// If possible updates the binary to the last version available
///
/// If dry_run is specified, the binary will not be updated but will output a message if the binary is updatable
///
/// If gui is specified, the user will be asked to confirm the update using a message dialog instead of a prompt in the console
///
/// Returns true if the binary was updated
///
/// # Exemple
///
/// ```rust
/// use baston_editor::auto_update;
///
/// auto_update::update(true, false).unwrap();
/// ```
///
#[cfg(feature = "auto_update")]
#[allow(unused_variables)]
pub fn update(dry_run: bool, gui: bool) -> Result<bool, Error> {
    let current_version = current_version();
    let releases = get_releases()?;
    let releases = get_release_map(&releases);
    let (last_version, _) = get_last_release(&releases);
    if last_version > current_version {
        info!(
            "New unstable version available: {} (current version: {})",
            last_version, current_version
        );
    }
    let (last_version, release) = get_last_stable_release(&releases);
    if last_version > current_version {
        warn!(
            "New stable version available: {} (current version: {}), starting update",
            last_version, current_version
        );
        let mut updater = gitlab::Update::configure();
        updater
            .repo_owner("bastonis")
            .repo_name("baston-editor")
            .bin_name("baston_editor")
            .target_version_tag(&format!("v{}", release.version.as_str()))
            .current_version(env!("CARGO_PKG_VERSION"));

        if !dry_run {
            #[cfg(feature = "gui")]
            if gui {
                let confirmation = MessageDialog::new()
                    .set_level(MessageLevel::Warning)
                    .set_title("Update Available")
                    .set_description(format!(
                        "New version available: {}\nCurrent version: {}\n\nDo you want to update?",
                        last_version, current_version
                    ))
                    .set_buttons(MessageButtons::YesNo)
                    .show();
                match confirmation {
                    MessageDialogResult::Yes => updater.no_confirm(true),
                    MessageDialogResult::No => return Ok(false),
                    _ => unreachable!(),
                };
            }

            match updater.build()?.update() {
                Ok(status) => Ok(status.updated()),
                Err(self_update::errors::Error::Update(msg)) => {
                    if msg == "Update aborted" {
                        Ok(false)
                    } else {
                        Err(self_update::errors::Error::Update(msg))?
                    }
                }
                Err(err) => Err(err)?,
            }
        } else {
            Ok(false)
        }
    } else {
        Ok(false)
    }
}

/// Get the list of releases from gitlab
///
/// # Exemple
///
/// ```rust
/// use baston_editor::auto_update::get_releases;
///
/// let releases = get_releases().unwrap();
///
/// println!("{:?}", releases);
/// ```
///
#[cfg(feature = "auto_update")]
pub fn get_releases() -> Result<Vec<Release>, Error> {
    Ok(gitlab::ReleaseList::configure()
        .repo_owner("bastonis")
        .repo_name("baston-editor")
        .build()?
        .fetch()?)
}

/// Turns a list of releases into a map with the version as key and the release as value
///
/// # Exemple
///
/// ```rust
/// use baston_editor::auto_update::{get_releases, get_release_map};
///
/// let releases = get_releases().unwrap();
/// println!("{:?}", get_release_map(&releases));
/// ```
#[cfg(feature = "auto_update")]
pub fn get_release_map(releases: &[Release]) -> BTreeMap<Version, &Release> {
    BTreeMap::from_iter(releases.iter().map(|release| {
        (
            release
                .name
                .as_str()
                .try_into()
                .critical("Release version from gitlab should be right"),
            release,
        )
    }))
}

/// Returns the last release including pre-releases
///
/// # Exemple
///
/// ```rust
/// use baston_editor::auto_update::{get_releases, get_release_map, get_last_release};
///
/// let releases = get_releases().unwrap();
/// let releases_map = get_release_map(&releases);
///
/// println!("{:?}", get_last_release(&releases_map));
/// ```
///
#[cfg(feature = "auto_update")]
pub fn get_last_release<'a>(releases: &BTreeMap<Version, &'a Release>) -> (Version, &'a Release) {
    let (version, release) = releases
        .last_key_value()
        .critical("There should be at least one release");
    (*version, *release)
}

/// Returns the last stable release
///
/// # Exemple
///
/// ```rust
/// use baston_editor::auto_update::{get_releases, get_release_map, get_last_stable_release};
///
/// let releases = get_releases().unwrap();
/// let releases_map = get_release_map(&releases);
///
/// println!("{:?}", get_last_stable_release(&releases_map));
/// ```
///
#[cfg(feature = "auto_update")]
pub fn get_last_stable_release<'a>(
    releases: &BTreeMap<Version, &'a Release>,
) -> (Version, &'a Release) {
    let stable_releases: BTreeMap<_, _> = releases
        .iter()
        .filter(|(version, _)| version.prerelease.is_none())
        .map(|(version, release)| (*version, *release))
        .collect();
    let (version, release) = stable_releases
        .last_key_value()
        .critical("There should be at least one stable release");
    (*version, *release)
}

/// Represents a version
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize, Default)]
pub struct Version {
    /// Main part of the version
    main: u16,

    /// Minor part of the version
    minor: u16,

    /// Patch part of the version
    patch: u16,

    /// Prerelease part of the version
    prerelease: Option<u16>,
}

impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Version {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.main > other.main {
            Ordering::Greater
        } else if self.main < other.main {
            Ordering::Less
        } else if self.minor > other.minor {
            Ordering::Greater
        } else if self.minor < other.minor {
            Ordering::Less
        } else if self.patch > other.patch {
            Ordering::Greater
        } else if self.patch < other.patch {
            Ordering::Less
        } else {
            match (self.prerelease, other.prerelease) {
                (None, Some(_)) => Ordering::Greater,
                (Some(_), None) => Ordering::Less,
                (None, None) => Ordering::Equal,
                (Some(prerelease), Some(other_prerelease)) => prerelease.cmp(&other_prerelease),
            }
        }
    }
}

impl TryFrom<&str> for Version {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let re = RegexBuilder::new(
            r"v?(?<main>[0-9]+)\.(?<minor>[0-9]+)\.(?<patch>[0-9]+)(-rc(?<prerelease>[0-9]+))?",
        )
        .ignore_whitespace(true)
        .case_insensitive(true)
        .build()
        .critical("Error with the regex, should not happen");

        if let Some(capture) = re.captures(value) {
            Ok(Self {
                main: u16::from_str(&capture["main"])?,
                minor: u16::from_str(&capture["minor"])?,
                patch: u16::from_str(&capture["patch"])?,
                prerelease: match capture.name("prerelease") {
                    None => None,
                    Some(prerelease) => Some(u16::from_str(prerelease.as_str())?),
                },
            })
        } else {
            Err(Self::Error::ConversionError)
        }
    }
}

impl std::fmt::Display for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "v{}.{}.{}{}",
            self.main,
            self.minor,
            self.patch,
            match self.prerelease {
                None => "".to_owned(),
                Some(prerelease) => format!("-rc{}", prerelease),
            }
        )
    }
}

#[cfg(test)]
mod test_module_auto_update {
    #[cfg(test)]
    mod test_version {
        use super::super::Version;

        use crate::error::Error;

        use std::cmp::Ordering;

        #[test]
        fn test_from_str() {
            for release in ["", "Release "] {
                for v in ["", "v"] {
                    for main in [0, 1, 10, 100, 1000] {
                        for minor in [0, 1, 10, 100, 1000] {
                            for patch in [0, 1, 10, 100, 1000] {
                                for prerelease in [None, Some(0), Some(1), Some(10), Some(100)] {
                                    assert_eq!(
                                        Version {
                                            main,
                                            minor,
                                            patch,
                                            prerelease
                                        },
                                        format!(
                                            "{}{}{}.{}.{}{}",
                                            release,
                                            v,
                                            main,
                                            minor,
                                            patch,
                                            match prerelease {
                                                None => "".to_owned(),
                                                Some(prerelease) => format!("-rc{}", prerelease),
                                            }
                                        )
                                        .as_str()
                                        .try_into()
                                        .expect("A test should never fail")
                                    );
                                }
                            }
                        }
                    }
                }
            }

            assert_eq!(
                Err(Error::ConversionError),
                Version::try_from("Something wrong")
            );
        }

        #[test]
        fn test_display() {
            for main in [0, 1, 10, 100, 1000] {
                for minor in [0, 1, 10, 100, 1000] {
                    for patch in [0, 1, 10, 100, 1000] {
                        for prerelease in [None, Some(0), Some(1), Some(10), Some(100)] {
                            assert_eq!(
                                format!(
                                    "v{}.{}.{}{}",
                                    main,
                                    minor,
                                    patch,
                                    match prerelease {
                                        None => "".to_owned(),
                                        Some(prerelease) => format!("-rc{}", prerelease),
                                    }
                                ),
                                Version {
                                    main,
                                    minor,
                                    patch,
                                    prerelease
                                }
                                .to_string()
                            );
                        }
                    }
                }
            }
        }

        #[test]
        fn test_cmp() {
            assert!(
                Version {
                    main: 0,
                    minor: 2,
                    patch: 0,
                    prerelease: None
                } < Version {
                    main: 1,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 1,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                } > Version {
                    main: 0,
                    minor: 2,
                    patch: 0,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 2,
                    prerelease: None
                } < Version {
                    main: 0,
                    minor: 1,
                    patch: 0,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 1,
                    patch: 0,
                    prerelease: None
                } > Version {
                    main: 0,
                    minor: 0,
                    patch: 2,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                } < Version {
                    main: 0,
                    minor: 0,
                    patch: 1,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 1,
                    prerelease: None
                } > Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(2)
                } < Version {
                    main: 0,
                    minor: 0,
                    patch: 1,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 1,
                    prerelease: None
                } > Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(2)
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(0)
                } < Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                } > Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(0)
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(0)
                } < Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(2)
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(2)
                } > Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: Some(0)
                }
            );

            assert!(
                Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                } == Version {
                    main: 0,
                    minor: 0,
                    patch: 0,
                    prerelease: None
                }
            );

            let v = Version {
                main: 0,
                minor: 0,
                patch: 0,
                prerelease: None,
            };

            assert_eq!(v.cmp(&v), Ordering::Equal);
        }
    }

    #[cfg(feature = "auto_update")]
    #[test]
    fn test_gets() {
        let releases = super::get_releases().expect("A test should never fail");

        let release_map = super::get_release_map(&releases);
        let _ = super::get_last_release(&release_map);
        let _ = super::get_last_stable_release(&release_map);
    }

    #[cfg(feature = "auto_update")]
    #[test]
    fn test_update_dry_run() {
        assert!(!super::update(true, false).expect("A test should never fail"))
    }
}
