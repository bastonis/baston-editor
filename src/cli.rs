// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Command line interface for Baston Editor

#[cfg(feature = "gui")]
use crate::app::App;
#[cfg(feature = "auto_update")]
use crate::auto_update::update;
use crate::card::Card;
#[cfg(feature = "gui")]
use crate::color::Color;
#[cfg(feature = "gui")]
use crate::color_picker::ColorPicker;
use crate::error::{Error, LogError};
#[cfg(feature = "gui")]
use crate::gui::Gui;
use crate::utilities::check_extension;

#[cfg(feature = "auto_update")]
use std::env::args_os;
use std::ops::Add;
use std::path::{Path, PathBuf};
#[cfg(feature = "auto_update")]
use std::process::{exit, Command};
#[cfg(feature = "gui")]
use std::sync::RwLock;
use std::thread::{self, JoinHandle};

use clap::{
    error::ErrorKind, ColorChoice, CommandFactory, Parser, Subcommand, ValueEnum, ValueHint,
};
use env_logger::Env;
#[cfg(feature = "gui")]
use lazy_static::lazy_static;
use log::info;
use strum_macros::Display;

/// Command line interface for Baston Editor
#[derive(Parser, PartialEq, Debug, Default)]
#[command(
    author,
    version,
    about,
    long_about,
    args_override_self(true),
    propagate_version = true,
    color = ColorChoice::Auto,
)]
pub struct Cli {
    /// Prevent the different outputs from showing the border
    #[arg(long, default_value_t = false)]
    pub no_border: bool,

    /// Prevent any modification and starting the GUI
    #[arg(long, default_value_t = false)]
    pub dry_run: bool,

    /// Configure the logging level
    #[arg(long, default_value_t = LogLevel::default(), ignore_case(true), value_enum)]
    log_level: LogLevel,

    /// Verbose mode
    ///
    /// Every times it is specify, it increase the verbosity
    #[arg(short, long, default_value_t = 0, action = clap::ArgAction::Count)]
    verbose: u8,

    /// Commands
    #[command(subcommand)]
    pub command: Option<Commands>,
}

/// Available commands
#[derive(Subcommand, PartialEq, Debug)]
pub enum Commands {
    #[cfg(feature = "gui")]
    /// GUI mode
    Gui {
        /// Input file
        ///
        /// If not provided, use the default card
        #[arg(value_name = "FILE", value_hint(ValueHint::FilePath))]
        input: Option<String>,
    },

    /// Headless mode (CLI)
    Headless {
        /// Input file or folder
        ///
        /// If not provided, use the default card
        ///
        /// If there is a folder or several files, the option out cannot be used
        #[arg(value_name = "FILE, FOLDER", value_hint(ValueHint::AnyPath))]
        inputs: Vec<String>,

        /// Recursively search for input files if a folder is specified
        #[arg(short, long, default_value_t = false)]
        recursive: bool,

        /// Number of concurrent jobs
        ///
        /// If N is not specified or is null, an unlimited number of concurrent jobs will be run
        #[arg(short, long, value_name = "N", default_value = "1", default_missing_value = "0", num_args(0..=1))]
        jobs: usize,

        /// Update the input file
        ///
        /// Only work if there is an input file
        #[arg(short = 'U', long, default_value_t = false, requires("inputs"))]
        update: bool,

        /// Show the input file
        #[arg(short, long, default_value_t = false)]
        show: bool,

        /// Output file
        ///
        /// The format will be determined from the extension
        ///
        /// Can be specified multiple times
        ///
        /// Cannot be used with output-type
        ///
        /// Requires at most one input file
        #[arg(
            short,
            long,
            visible_alias("out"),
            value_name = "FILE",
            value_hint(ValueHint::FilePath),
            conflicts_with("output_type"),
            conflicts_with("output_dir")
        )]
        outputs: Vec<String>,

        /// Output type
        ///
        /// Can be specified multiple times
        ///
        /// Needs at least an input and cannot be used with output
        #[arg(
            long,
            value_name = "TYPE",
            value_enum,
            conflicts_with("outputs"),
            requires("inputs")
        )]
        output_type: Vec<OutputType>,

        /// Output folder
        ///
        /// If not specified, the current directory will be used
        ///
        /// Needs at least one input and one output-type and cannot be used with output
        #[arg(
            long,
            value_name = "FOLDER",
            conflicts_with("outputs"),
            requires("inputs"),
            requires("output_type"),
            value_hint(ValueHint::DirPath)
        )]
        output_dir: Option<String>,
    },

    #[cfg(feature = "gui")]
    /// Small utility to pick a color
    ColorPicker {
        /// Input color
        #[arg(value_name = "COLOR", value_hint(ValueHint::Other))]
        color: Option<String>,
    },
}

/// Represents a level of logging
#[derive(Copy, Clone, PartialEq, ValueEnum, Display, Debug, Default)]
pub enum LogLevel {
    /// Does not show any log
    Off,
    #[default]
    /// Shows only the errors
    Error,
    /// Shows the errors and the warnings
    Warn,
    /// Shows the errors, the warnings and the info
    Info,
    /// Shows the debug information
    Debug,
    /// Shows everything
    Trace,
}

/// Represents a type of file that the software can output
#[derive(Copy, Clone, PartialEq, ValueEnum, Display, Debug)]
pub enum OutputType {
    /// PDF format
    Pdf,

    /// SVG format
    Svg,

    /// PNG format
    Png,

    /// YAML format
    Yml,
}

/// Pool of Thread of a given size
///
/// Use a best effort policy to have the most thread active
#[derive(Debug)]
struct ThreadPool {
    /// Maximum number of thread
    capacity: usize,
    /// List of currently active threads
    threads: Vec<JoinHandle<Result<(), Error>>>,
}

/// Information needed to run in headless mode with a specific card
#[derive(PartialEq, Debug, Clone)]
pub struct Headless {
    /// Whether or not to update the input file
    update: bool,
    /// Whether or not to show the input file
    show: bool,
    /// Files to be output
    outputs: Vec<PathBuf>,
    /// Path of the input file if any
    filename: Option<PathBuf>,
}

/// Trait representing the ability of an object to be executed on a card
trait RunCard {
    /// Run the object on the card specified
    fn run_card(self, card: Card, no_border: bool) -> Result<(), Error>;
}

#[cfg(feature = "gui")]
lazy_static! {
    /// Flag specifying whether or not we are running a GUI
    pub static ref GUI: RwLock<bool> = RwLock::new(false);
}

impl Add<u8> for LogLevel {
    type Output = Self;

    fn add(self, value: u8) -> Self::Output {
        if value == 0 {
            self
        } else {
            match self {
                LogLevel::Off => LogLevel::Error,
                LogLevel::Error => LogLevel::Warn,
                LogLevel::Warn => LogLevel::Info,
                LogLevel::Info => LogLevel::Debug,
                LogLevel::Debug => LogLevel::Trace,
                LogLevel::Trace => LogLevel::Trace,
            }
            .add(value - 1)
        }
    }
}

impl OutputType {
    /// Turns the output type in the corresponding file extension
    pub fn to_extension(&self) -> String {
        self.to_string().to_lowercase()
    }
}

impl Cli {
    /// Parse the command and the argument provided
    ///
    /// It also check for there correctness and start the logging process
    pub fn read() -> Self {
        let args = Cli::parse();
        let args = if args.command.is_none() {
            Cli {
                command: Some(Commands::default()),
                ..args
            }
        } else {
            args
        };
        args.check();
        args.start_logging();
        args
    }

    /// Run the software based on the command
    pub fn run(self) -> Result<(), Error> {
        #[cfg(feature = "auto_update")]
        match update(
            self.dry_run,
            !matches!(self.command, Some(Commands::Headless { .. })),
        ) {
            Ok(true) => {
                let mut current_cmd = args_os();
                let exec = current_cmd
                    .next()
                    .critical("There is at least the executable in a command");
                let mut cmd = Command::new(exec);
                cmd.args(current_cmd);

                exit(cmd.status()?.code().critical("Exit code should be set"));
            }
            Ok(_) => (),
            Err(err) => {
                err.error("An error occurred while updating");
            }
        }

        self.command
            .critical("The command should be set")
            .run(self.no_border, self.dry_run)
    }

    /// Start the logging process
    fn start_logging(&self) {
        let env = Env::default().default_filter_or((self.log_level + self.verbose).to_string());
        env_logger::Builder::from_env(env).init();

        info!("Starting to load and update the config file");
        let _ = Card::new(); // Updating the config and loading the default card
        info!("Config file updated and loaded");

        #[cfg(feature = "gui")]
        if let Some(Commands::Gui { .. }) = self.command {
            if !self.dry_run {
                *GUI.write()
                    .expect("Error while getting the write lock on the GUI flag") = true;
            }
            log::warn!("GUI is still in alpha, it is a work in progress");
        }
    }

    /// Check the correctness of the command
    fn check(&self) {
        self.command
            .as_ref()
            .critical("Command should not be null at this point")
            .check()
    }
}

impl Commands {
    /// Check the correctness of the command
    pub fn check(&self) {
        match self {
            #[cfg(feature = "gui")]
            Commands::Gui { .. } => {
                let inputs = self.inputs();
                assert!(inputs.len() <= 1)
            }
            Commands::Headless {
                output_type,
                outputs,
                output_dir,
                update,
                ..
            } => {
                let inputs = self.inputs();
                if inputs.is_empty() && *update {
                    Cli::command()
                        .error(
                            ErrorKind::MissingRequiredArgument,
                            "No input were found despite the update option".to_owned(),
                        )
                        .exit();
                }
                if inputs.is_empty() && !output_type.is_empty() {
                    Cli::command()
                        .error(
                            ErrorKind::MissingRequiredArgument,
                            "No input were found despite the output-type option".to_owned(),
                        )
                        .exit();
                }
                if inputs.len() > 1 && !outputs.is_empty() {
                    Cli::command()
                        .error(
                            ErrorKind::InvalidValue,
                            "Multiple inputs cannot be used with output",
                        )
                        .exit();
                }
                if let Some(dir) = output_dir {
                    if !Path::new(dir).is_dir() {
                        Cli::command()
                            .error(
                                ErrorKind::InvalidValue,
                                format!("The output-dir : {} is not a directory", dir),
                            )
                            .exit();
                    }
                }
            }
            #[cfg(feature = "gui")]
            Commands::ColorPicker { color } => {
                if let Some(color) = color {
                    let color = serde_yaml::from_str::<Color>(color);
                    if let Err(err) = color {
                        Cli::command()
                            .error(ErrorKind::InvalidValue, format!("Invalid color: {}", err))
                            .exit();
                    } else if let Ok(Color::Default) = color {
                        Cli::command()
                            .error(ErrorKind::InvalidValue, "Invalid color")
                            .exit();
                    }
                }
            }
        }
    }

    /// Get the input files of the command
    ///
    /// It also check for the existence of the input and that they are YAML files
    fn inputs(&self) -> Vec<PathBuf> {
        match self {
            #[cfg(feature = "gui")]
            Commands::Gui { input } => match input {
                Some(input) => {
                    let path = Path::new(input);
                    if !path.exists() {
                        log::error!("Input {} does not exist", input);
                        Cli::command()
                            .error(
                                ErrorKind::InvalidValue,
                                format!("Input {} does not exist", input),
                            )
                            .exit()
                    }
                    if path.is_dir() {
                        log::error!("The GUI does not support input directories");
                        Cli::command()
                            .error(
                                ErrorKind::InvalidValue,
                                "The GUI does not support input directories".to_owned(),
                            )
                            .exit();
                    } else if check_extension(path, &["yml", "yaml"]) {
                        vec![path.to_path_buf()]
                    } else {
                        log::error!("Input file {} is not a YAML file", path.display());
                        Cli::command()
                            .error(
                                ErrorKind::InvalidValue,
                                format!("Input file {} is not a YAML file", input),
                            )
                            .exit();
                    }
                }
                None => Vec::new(),
            },
            Commands::Headless {
                inputs, recursive, ..
            } => {
                let mut res = Vec::new();
                let mut inputs: Vec<_> =
                    inputs.iter().map(|i| Path::new(&i).to_path_buf()).collect();
                while let Some(path) = inputs.pop() {
                    if !path.exists() {
                        log::error!("Input {} does not exist", path.display());
                        Cli::command()
                            .error(
                                ErrorKind::InvalidValue,
                                format!("Input {} does not exist", path.display()),
                            )
                            .exit()
                    }
                    if path.is_dir() {
                        for entry in path.read_dir().unwrap_or_else(|_| {
                            panic!("Failed to read directory: {}", path.display())
                        }) {
                            let entry = entry.critical("Failed to read directory entry").path();
                            if entry.is_dir() {
                                if *recursive {
                                    inputs.push(entry);
                                }
                            } else if check_extension(&entry, &["yml", "yaml"]) {
                                res.push(entry);
                            } else {
                                log::warn!("Input file {} is not a YAML file", entry.display());
                            }
                        }
                    } else if check_extension(&path, &["yml", "yaml"]) {
                        res.push(path.to_path_buf())
                    } else {
                        log::error!("Input file {} is not a YAML file", path.display());
                        Cli::command()
                            .error(
                                ErrorKind::InvalidValue,
                                format!("Input file {} is not a YAML file", path.display()),
                            )
                            .exit();
                    }
                }
                res
            }
            #[cfg(feature = "gui")]
            Commands::ColorPicker { .. } => panic!("This should not be used"),
        }
    }

    /// Run the software based on the command
    pub fn run(&self, no_border: bool, dry_run: bool) -> Result<(), Error> {
        match self {
            #[cfg(feature = "gui")]
            Commands::Gui { .. } => {
                let inputs = self.inputs();

                let card = if inputs.is_empty() {
                    Card::new()
                } else {
                    Card::open(&inputs[0])?
                };
                log::info!("Starting the GUI");
                if !dry_run {
                    Gui::new(card).run()?;
                } else {
                    info!("Dry run, the GUI was not started");
                }
                log::info!("The GUI stopped");
                Ok(())
            }
            Commands::Headless {
                jobs,
                ref output_dir,
                ref output_type,
                ..
            } => {
                let inputs = self.inputs();
                let config: Headless = (self).try_into()?;
                if inputs.is_empty() {
                    let card = Card::new();
                    if !dry_run {
                        config.run_card(card, no_border)?;
                    } else {
                        info!("Dry run, nothing was modified");
                    }
                } else {
                    let mut threads = ThreadPool::new(*jobs);
                    for input in inputs {
                        let card = Card::open(&input)?;
                        let mut config = config.clone();
                        config.filename = Some(input.clone());
                        if config.outputs.is_empty() {
                            let output_dir = output_dir.as_ref().map(PathBuf::from);
                            let mut filename = input;
                            for output_type in output_type {
                                if filename.set_extension(output_type.to_extension()) {
                                    if let Some(output_dir) = &output_dir {
                                        config.outputs.push(output_dir.join(filename.clone()));
                                    } else {
                                        config.outputs.push(filename.clone());
                                    }
                                } else {
                                    log::error!(
                                        "The file {} does not have a valid name",
                                        filename.display()
                                    );
                                    return Err(Error::UnknownFormatError);
                                }
                            }
                        }
                        if !dry_run {
                            threads.spawn(move || config.run_card(card, no_border))?;
                        } else {
                            info!("Dry run, nothing was modified");
                        }
                    }
                    threads.join()?
                }
                Ok(())
            }
            #[cfg(feature = "gui")]
            Commands::ColorPicker { color } => {
                if !dry_run {
                    match color {
                        Some(color) => Ok(ColorPicker::new(serde_yaml::from_str(color)?).run()?),
                        None => Ok(ColorPicker::new(Color::Black).run()?),
                    }
                } else {
                    info!("Dry run, color picker GUI was not started");
                    Ok(())
                }
            }
        }
    }
}

impl Default for Commands {
    #[cfg(feature = "gui")]
    fn default() -> Self {
        Self::Gui { input: None }
    }

    #[cfg(not(feature = "gui"))]
    fn default() -> Self {
        Self::Headless {
            inputs: vec![],
            recursive: false,
            jobs: 1,
            update: false,
            show: false,
            outputs: vec![],
            output_dir: None,
            output_type: vec![],
        }
    }
}

impl TryFrom<&Commands> for Headless {
    type Error = Error;

    fn try_from(value: &Commands) -> Result<Self, Self::Error> {
        match value {
            Commands::Headless {
                outputs,
                update,
                show,
                ..
            } => {
                let mut res = Vec::new();
                for output in outputs {
                    let path = Path::new(&output);
                    res.push(path.to_path_buf());
                }
                Ok(Headless {
                    update: *update,
                    show: *show,
                    outputs: res,
                    filename: None,
                })
            }
            #[cfg(feature = "gui")]
            _ => Err(Self::Error::ConversionError),
        }
    }
}

impl ThreadPool {
    /// Create a new thread pool of the given capacity
    ///
    /// If none is provided create an endless pool
    pub fn new(capacity: usize) -> Self {
        Self {
            capacity,
            threads: Vec::with_capacity(capacity),
        }
    }

    /// Spawn a new thread
    ///
    /// Can be blocking if no thread are available.
    ///
    /// In this case, it waits for the first thread of the pool to finish, adds the new thread and returns the old thread return value
    pub fn spawn<F>(&mut self, f: F) -> Result<(), Error>
    where
        F: FnOnce() -> Result<(), Error> + Send + 'static,
    {
        if self.capacity > 0 && self.threads.len() == self.capacity {
            let mut finished = self.threads.iter_mut().position(|e| e.is_finished());
            while finished.is_none() {
                std::thread::sleep(std::time::Duration::from_millis(100));
                finished = self.threads.iter_mut().position(|e| e.is_finished());
            }
            if let Some(finished) = finished {
                let old = self.threads.remove(finished);
                self.threads.push(thread::spawn(f));
                old.join().expect("Unrecoverable thread error")
            } else {
                Err(Error::ValueError {
                    expected: "a usize".to_owned(),
                    current: "None".to_owned(),
                })
            }
        } else {
            self.threads.push(thread::spawn(f));
            Ok(())
        }
    }

    /// Joins all the thread remaining in the pool
    pub fn join(&mut self) -> Result<(), Error> {
        let mut res = Ok(());

        while let Some(thread) = self.threads.pop() {
            match thread.join().expect("Unrecoverable thread error") {
                Ok(()) => (),
                Err(err) => {
                    if let Ok(()) = res {
                        res = Err(err);
                    }
                }
            }
        }
        res
    }
}

impl RunCard for Headless {
    fn run_card(self, card: Card, no_border: bool) -> Result<(), Error> {
        if self.update {
            card.save(
                self.filename
                    .as_ref()
                    .critical("Filename should be set at this point"),
            )
            .error(&format!(
                "An error occurred while saving the card {}",
                self.filename
                    .as_ref()
                    .critical("Filename should be set at this point")
                    .display(),
            ))?;
        }
        if self.show {
            card.show(no_border, true)
                .error(&if let Some(filename) = &self.filename {
                    format!(
                        "An error occurred while showing the card {}",
                        filename.display()
                    )
                } else {
                    "An error occurred while showing the card".to_owned()
                })?;
        }
        for output in self.outputs {
            let res = if output.display().to_string().ends_with(".yml") {
                log::info!("to yaml");
                card.save(&output)
            } else {
                log::info!("to something other than yaml");
                card.print(&output, no_border)
            };
            res.error(&format!(
                "An error occurred while saving the card {}",
                output.display()
            ))?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod test_module_cli {

    #[cfg(test)]
    mod test_enum_output_type {
        use super::super::OutputType;

        #[test]
        fn test_display_output_type() {
            assert_eq!(OutputType::Pdf.to_string(), "Pdf");
            assert_eq!(OutputType::Svg.to_string(), "Svg");
            assert_eq!(OutputType::Png.to_string(), "Png");
            assert_eq!(OutputType::Yml.to_string(), "Yml");

            assert_eq!(format!("{:?}", OutputType::Pdf), "Pdf");
            assert_eq!(format!("{:?}", OutputType::Svg), "Svg");
            assert_eq!(format!("{:?}", OutputType::Png), "Png");
            assert_eq!(format!("{:?}", OutputType::Yml), "Yml");
        }

        #[test]
        fn test_to_extension_output_type() {
            assert_eq!(OutputType::Pdf.to_extension(), "pdf");
            assert_eq!(OutputType::Svg.to_extension(), "svg");
            assert_eq!(OutputType::Png.to_extension(), "png");
            assert_eq!(OutputType::Yml.to_extension(), "yml");
        }

        #[test]
        fn test_clone_output_type() {
            assert_eq!(OutputType::Pdf, OutputType::Pdf.clone());
            assert_eq!(OutputType::Svg, OutputType::Svg.clone());
            assert_eq!(OutputType::Png, OutputType::Png.clone());
            assert_eq!(OutputType::Yml, OutputType::Yml.clone());
        }
    }

    #[cfg(test)]
    mod test_enum_log_level {
        use super::super::LogLevel;

        #[test]
        fn test_display_log_level() {
            assert_eq!(LogLevel::Off.to_string(), "Off");
            assert_eq!(LogLevel::Error.to_string(), "Error");
            assert_eq!(LogLevel::Warn.to_string(), "Warn");
            assert_eq!(LogLevel::Info.to_string(), "Info");
            assert_eq!(LogLevel::Debug.to_string(), "Debug");
            assert_eq!(LogLevel::Trace.to_string(), "Trace");

            assert_eq!(format!("{:?}", LogLevel::Off), "Off");
            assert_eq!(format!("{:?}", LogLevel::Error), "Error");
            assert_eq!(format!("{:?}", LogLevel::Warn), "Warn");
            assert_eq!(format!("{:?}", LogLevel::Info), "Info");
            assert_eq!(format!("{:?}", LogLevel::Debug), "Debug");
            assert_eq!(format!("{:?}", LogLevel::Trace), "Trace");
        }

        #[test]
        fn test_clone_log_level() {
            assert_eq!(LogLevel::Off, LogLevel::Off.clone());
            assert_eq!(LogLevel::Error, LogLevel::Error.clone());
            assert_eq!(LogLevel::Warn, LogLevel::Warn.clone());
            assert_eq!(LogLevel::Info, LogLevel::Info.clone());
            assert_eq!(LogLevel::Debug, LogLevel::Debug.clone());
            assert_eq!(LogLevel::Trace, LogLevel::Trace.clone());
        }

        #[test]
        fn test_add_log_level() {
            assert_eq!(LogLevel::Off + 0, LogLevel::Off);
            assert_eq!(LogLevel::Off + 1, LogLevel::Error);
            assert_eq!(LogLevel::Off + 2, LogLevel::Warn);
            assert_eq!(LogLevel::Off + 3, LogLevel::Info);
            assert_eq!(LogLevel::Off + 4, LogLevel::Debug);
            assert_eq!(LogLevel::Off + 5, LogLevel::Trace);
            assert_eq!(LogLevel::Off + 6, LogLevel::Trace);

            assert_eq!(LogLevel::Error + 0, LogLevel::Error);
            assert_eq!(LogLevel::Error + 1, LogLevel::Warn);
            assert_eq!(LogLevel::Error + 2, LogLevel::Info);
            assert_eq!(LogLevel::Error + 3, LogLevel::Debug);
            assert_eq!(LogLevel::Error + 4, LogLevel::Trace);
            assert_eq!(LogLevel::Error + 5, LogLevel::Trace);

            assert_eq!(LogLevel::Warn + 0, LogLevel::Warn);
            assert_eq!(LogLevel::Warn + 1, LogLevel::Info);
            assert_eq!(LogLevel::Warn + 2, LogLevel::Debug);
            assert_eq!(LogLevel::Warn + 3, LogLevel::Trace);
            assert_eq!(LogLevel::Warn + 4, LogLevel::Trace);

            assert_eq!(LogLevel::Info + 0, LogLevel::Info);
            assert_eq!(LogLevel::Info + 1, LogLevel::Debug);
            assert_eq!(LogLevel::Info + 2, LogLevel::Trace);
            assert_eq!(LogLevel::Info + 3, LogLevel::Trace);

            assert_eq!(LogLevel::Debug + 0, LogLevel::Debug);
            assert_eq!(LogLevel::Debug + 1, LogLevel::Trace);
            assert_eq!(LogLevel::Debug + 2, LogLevel::Trace);

            assert_eq!(LogLevel::Trace + 0, LogLevel::Trace);
            assert_eq!(LogLevel::Trace + 1, LogLevel::Trace);
        }

        #[test]
        fn test_default_log_level() {
            assert_eq!(LogLevel::default(), LogLevel::Error);
        }
    }

    #[cfg(test)]
    mod test_struct_thread_pool {
        use super::super::ThreadPool;

        #[test]
        fn test_new_pool() {
            let pool = ThreadPool::new(0);
            assert_eq!(pool.threads.len(), 0);
            assert_eq!(pool.capacity, 0);

            let pool = ThreadPool::new(10);
            assert_eq!(pool.threads.len(), 0);
            assert_eq!(pool.capacity, 10);
            assert!(pool.threads.capacity() >= 10);
        }

        #[test]
        fn test_endless_pool() {
            let mut pool = ThreadPool::new(0);

            let begin = std::time::Instant::now();

            pool.spawn(|| Ok(std::thread::sleep(std::time::Duration::from_secs(2))))
                .expect("A test should never fail");
            assert_eq!(pool.threads.len(), 1);
            assert_eq!(pool.capacity, 0);

            pool.spawn(|| Ok(std::thread::sleep(std::time::Duration::from_secs(2))))
                .expect("A test should never fail");
            assert_eq!(pool.threads.len(), 2);
            assert_eq!(pool.capacity, 0);

            pool.join().expect("A test should never fail");
            assert_eq!(pool.threads.len(), 0);
            assert_eq!(pool.capacity, 0);

            let time = begin.elapsed();
            assert!(time.as_secs() >= 2);
            assert!(time.as_secs() < 4);
        }

        #[test]
        fn test_small_pool() {
            let mut pool = ThreadPool::new(1);

            let begin = std::time::Instant::now();

            pool.spawn(|| Ok(std::thread::sleep(std::time::Duration::from_secs(2))))
                .expect("A test should never fail");
            assert_eq!(pool.threads.len(), 1);
            assert_eq!(pool.capacity, 1);

            pool.spawn(|| Ok(std::thread::sleep(std::time::Duration::from_secs(2))))
                .expect("A test should never fail");
            assert_eq!(pool.threads.len(), 1);
            assert_eq!(pool.capacity, 1);

            pool.join().expect("A test should never fail");
            assert_eq!(pool.threads.len(), 0);
            assert_eq!(pool.capacity, 1);

            let time = begin.elapsed();
            assert!(time.as_secs() >= 4);
        }
    }

    #[cfg(test)]
    mod test_struct_headless {
        use super::super::Commands;
        use super::super::Headless;
        use super::super::RunCard;

        use crate::card::Card;
        use crate::error::Error;

        use std::path::Path;
        use tempfile::Builder;

        #[test]
        fn test_debug() {
            for update in [true, false] {
                for show in [true, false] {
                    for outputs in [vec![], vec![Path::new("tests/").to_path_buf()]] {
                        for filename in [None, Some(Path::new("tests/").to_path_buf())] {
                            assert_eq!(
                                format!(
                                    "Headless {{ update: {:?}, show: {:?}, outputs: {:?}, filename: {:?} }}",
                                    update, show, outputs.clone(), filename.clone()),
                                format!(
                                    "{:?}",
                                    Headless{
                                        update,
                                        show,
                                        outputs: outputs.clone(),
                                        filename
                                    }),
                            )
                        }
                    }
                }
            }
        }

        #[test]
        fn test_clone() {
            for update in [true, false] {
                for show in [true, false] {
                    for outputs in [vec![], vec![Path::new("tests/").to_path_buf()]] {
                        for filename in [None, Some(Path::new("tests/").to_path_buf())] {
                            let headless = Headless {
                                update,
                                show,
                                outputs: outputs.clone(),
                                filename,
                            };
                            assert_eq!(headless.clone(), headless,)
                        }
                    }
                }
            }
        }

        #[test]
        fn test_run_card() {
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![],
                filename: None,
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let tempfile = Builder::new()
                .suffix(".yml")
                .tempfile()
                .expect("A test should never fail");
            let path = tempfile.path().to_path_buf();
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![],
                filename: Some(path.clone()),
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let headless = Headless {
                update: true,
                show: false,
                outputs: vec![],
                filename: Some(path.clone()),
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let headless = Headless {
                update: true,
                show: false,
                outputs: vec![path.clone()],
                filename: Some(path.clone()),
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![path.clone()],
                filename: Some(path.clone()),
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![path.clone()],
                filename: None,
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let tempfile = Builder::new()
                .suffix(".svg")
                .tempfile()
                .expect("A test should never fail");
            let path = tempfile.path().to_path_buf();
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![path.clone()],
                filename: None,
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let tempfile = Builder::new()
                .suffix(".pdf")
                .tempfile()
                .expect("A test should never fail");
            let path = tempfile.path().to_path_buf();
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![path.clone()],
                filename: None,
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
            let tempfile = Builder::new()
                .suffix(".png")
                .tempfile()
                .expect("A test should never fail");
            let path = tempfile.path().to_path_buf();
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![path.clone()],
                filename: None,
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
        }

        #[test]
        fn test_run_card_error() {
            let tempfile = Builder::new()
                .suffix(".odt")
                .tempfile()
                .expect("A test should never fail");
            let path = tempfile.path().to_path_buf();
            let headless = Headless {
                update: false,
                show: false,
                outputs: vec![path.clone()],
                filename: None,
            };
            let res = headless.run_card(Card::new(), false);
            assert!(res.is_err());
            assert_eq!(res, Err(Error::UnsupportedFormatError("odt".to_owned())),);
            let headless = Headless {
                update: true,
                show: false,
                outputs: vec![],
                filename: Some(path.clone()),
            };
            let res = headless.run_card(Card::new(), false);
            assert!(res.is_err());
            assert_eq!(res, Err(Error::UnsupportedFormatError("odt".to_owned())),);
        }

        #[test]
        #[should_panic(expected = "Filename should be set at this point")]
        fn test_run_card_update_no_filename() {
            let headless = Headless {
                update: true,
                show: false,
                outputs: vec![],
                filename: None,
            };
            let _ = headless.run_card(Card::new(), false);
        }

        #[test]
        #[ignore]
        fn test_run_card_show() {
            let headless = Headless {
                update: false,
                show: true,
                outputs: vec![],
                filename: None,
            };
            assert!(headless.run_card(Card::new(), false).is_ok());
        }

        #[test]
        fn test_from_command() {
            for outputs in [vec![], vec!["".to_owned()]] {
                for update in [true, false] {
                    for show in [true, false] {
                        let command = Commands::Headless {
                            inputs: vec![],
                            recursive: false,
                            jobs: 0,
                            update,
                            show,
                            outputs: outputs.clone(),
                            output_type: vec![],
                            output_dir: None,
                        };
                        assert_eq! {
                            Headless{
                                update,
                                show,
                                outputs: outputs.iter().map(|path| Path::new(path).to_path_buf()).collect(),
                                filename: None,
                            },
                            TryInto::<Headless>::try_into(&command).expect("A test should never fail")
                        };
                    }
                }
            }
            #[cfg(feature = "gui")]
            let command = Commands::Gui { input: None };
            #[cfg(feature = "gui")]
            assert_eq!(
                TryInto::<Headless>::try_into(&command),
                Err(Error::ConversionError)
            );
            #[cfg(feature = "gui")]
            let command = Commands::ColorPicker { color: None };
            #[cfg(feature = "gui")]
            assert_eq!(
                TryInto::<Headless>::try_into(&command),
                Err(Error::ConversionError)
            );
        }
    }

    #[cfg(test)]
    mod test_enum_commands {
        use super::super::Commands;

        use super::super::OutputType;
        #[cfg(feature = "gui")]
        use crate::color::Color;

        use std::fs::remove_file;
        #[cfg(feature = "gui")]
        use std::path::Path;
        use std::path::PathBuf;
        use std::process::Command;

        #[test]
        #[cfg(feature = "gui")]
        fn test_default_commands() {
            assert_eq!(Commands::default(), Commands::Gui { input: None })
        }

        #[test]
        #[cfg(not(feature = "gui"))]
        fn test_default_commands() {
            assert_eq!(
                Commands::default(),
                Commands::Headless {
                    inputs: vec![],
                    recursive: false,
                    jobs: 1,
                    update: false,
                    show: false,
                    outputs: vec![],
                    output_type: vec![],
                    output_dir: None
                }
            )
        }

        #[test]
        fn test_debug_commands() {
            #[cfg(feature = "gui")]
            assert_eq!(
                format!("{:?}", Commands::Gui { input: None }),
                format!("Gui {{ input: {:?} }}", None::<String>),
            );
            #[cfg(feature = "gui")]
            assert_eq!(
                format!(
                    "{:?}",
                    Commands::Gui {
                        input: Some("".to_owned())
                    }
                ),
                format!("Gui {{ input: {:?} }}", Some("".to_owned())),
            );

            #[cfg(feature = "gui")]
            assert_eq!(
                format!("{:?}", Commands::ColorPicker { color: None }),
                format!("ColorPicker {{ color: {:?} }}", None::<String>),
            );
            #[cfg(feature = "gui")]
            assert_eq!(
                format!(
                    "{:?}",
                    Commands::ColorPicker {
                        color: Some("".to_owned())
                    }
                ),
                format!("ColorPicker {{ color: {:?} }}", Some("".to_owned())),
            );

            for inputs in [vec![], vec!["".to_owned()]] {
                for recursive in [true, false] {
                    for jobs in [0, 1] {
                        for update in [true, false] {
                            for show in [true, false] {
                                for outputs in [vec![], vec!["".to_owned()]] {
                                    for output_type in [vec![], vec![OutputType::Yml]] {
                                        for output_dir in [None, Some("".to_owned())] {
                                            let command = Commands::Headless {
                                                inputs: inputs.clone(),
                                                recursive,
                                                jobs,
                                                update,
                                                show,
                                                outputs: outputs.clone(),
                                                output_type: output_type.clone(),
                                                output_dir: output_dir.clone(),
                                            };
                                            assert_eq!(
                                                format!("{:?}", command),
                                                format!(
                                                    "Headless {{ inputs: {:?}, recursive: {:?}, jobs: {:?}, update: {:?}, show: {:?}, outputs: {:?}, output_type: {:?}, output_dir: {:?} }}",
                                                    inputs,
                                                    recursive,
                                                    jobs,
                                                    update,
                                                    show,
                                                    outputs,
                                                    output_type,
                                                    output_dir
                                                ),
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #[test]
        fn test_inputs() {
            #[cfg(feature = "gui")]
            assert_eq!(
                Commands::Gui { input: None }.inputs(),
                Vec::<PathBuf>::new()
            );
            #[cfg(feature = "gui")]
            assert_eq!(
                Commands::Gui {
                    input: Some("tests/input_ugly.yml".to_owned())
                }
                .inputs(),
                vec![Path::new("tests/input_ugly.yml").to_path_buf()]
            );

            let command = Commands::Headless {
                inputs: vec!["tests".to_owned(), "src/config/default_card.yml".to_owned()],
                recursive: false,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_type: vec![],
                output_dir: None,
            };

            let inputs = command.inputs();
            assert_eq!(inputs.len(), 4);
            for input in vec![
                "tests/input_ugly.yml",
                "tests/input.yml",
                "tests/input_ugly_char.yml",
                "src/config/default_card.yml",
            ]
            .iter()
            .map(|f| PathBuf::from(f))
            {
                assert!(inputs.contains(&input));
            }

            let command = Commands::Headless {
                inputs: vec!["tests".to_owned(), "src/config/default_card.yml".to_owned()],
                recursive: true,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_type: vec![],
                output_dir: None,
            };

            let inputs = command.inputs();
            assert_eq!(inputs.len(), 18);
            for input in vec![
                "tests/input_ugly.yml",
                "tests/input.yml",
                "tests/input_ugly_char.yml",
                "tests/input_field_missing/characteristics.yml",
                "tests/input_field_missing/era.yml",
                "tests/input_field_missing/archetype.yml",
                "tests/input_field_missing/extra_deck.yml",
                "tests/input_field_missing/faction.yml",
                "tests/input_field_missing/gender.yml",
                "tests/input_field_missing/illustration.yml",
                "tests/input_field_missing/name.yml",
                "tests/input_field_missing/pictograms.yml",
                "tests/input_field_missing/powers.yml",
                "tests/input_field_missing/symbols.yml",
                "tests/input_field_missing/template.yml",
                "tests/input_field_missing/title.yml",
                "tests/input_field_missing/type.yml",
                "src/config/default_card.yml",
            ]
            .iter()
            .map(|f| PathBuf::from(f))
            {
                assert!(inputs.contains(&input));
            }

            let command = Commands::Headless {
                inputs: vec!["src".to_owned()],
                recursive: true,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_type: vec![],
                output_dir: None,
            };
            let inputs = command.inputs();
            assert_eq!(inputs.len(), 7);
            for input in vec![
                "src/config/default_card.yml",
                "src/config/symbol_classification.yml",
                "src/config/rendering/common.yml",
                "src/config/rendering/vertical.yml",
                "src/config/rendering/horizontal.yml",
                "src/fonts/config.yml",
                "src/logos/symbols/Config.yml",
            ]
            .iter()
            .map(|f| PathBuf::from(f))
            {
                assert!(inputs.contains(&input));
            }

            let command = Commands::Headless {
                inputs: vec!["src".to_owned()],
                recursive: false,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_type: vec![],
                output_dir: None,
            };
            let inputs = command.inputs();
            assert_eq!(inputs.len(), 0);

            let command = Commands::Headless {
                inputs: vec![],
                recursive: false,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_type: vec![],
                output_dir: None,
            };
            let inputs = command.inputs();
            assert_eq!(inputs.len(), 0);
        }

        #[test]
        #[should_panic(expected = "This should not be used")]
        #[cfg(feature = "gui")]
        fn test_inputs_color_picker_none() {
            let _ = Commands::ColorPicker { color: None }.inputs();
        }

        #[test]
        #[should_panic(expected = "This should not be used")]
        #[cfg(feature = "gui")]
        fn test_inputs_color_picker_some() {
            let _ = Commands::ColorPicker {
                color: Some("".to_owned()),
            }
            .inputs();
        }

        #[test]
        fn test_check() {
            #[cfg(feature = "gui")]
            Commands::Gui { input: None }.check();
            #[cfg(feature = "gui")]
            Commands::Gui {
                input: Some("tests/input.yml".to_owned()),
            }
            .check();

            #[cfg(feature = "gui")]
            Commands::ColorPicker { color: None }.check();
            #[cfg(feature = "gui")]
            Commands::ColorPicker {
                color: Some(
                    serde_yaml::to_string(&Color::Black).expect("A test should never fail"),
                ),
            }
            .check();
            #[cfg(feature = "gui")]
            Commands::ColorPicker {
                color: Some(
                    serde_yaml::to_string(&Color::Custom(12, 13, 14, 15))
                        .expect("A test should never fail"),
                ),
            }
            .check();

            Commands::Headless {
                inputs: vec![],
                recursive: false,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_type: vec![],
                output_dir: None,
            }
            .check();
        }

        #[test]
        fn test_run() {
            let dir = tempfile::tempdir().expect("A test should never fail");
            let output_dir = dir
                .path()
                .to_str()
                .expect("A test should never fail")
                .to_string();
            let exit_status = Command::new("touch")
                .arg("tmp")
                .current_dir(&dir)
                .status()
                .expect("A test should never fail");
            assert!(exit_status.success());
            for jobs in [0, 1, 2] {
                for inputs in [vec![], vec!["tests/input.yml".to_owned()]] {
                    for output_dir in [None, Some(output_dir.clone())] {
                        for output_type in [vec![], vec![OutputType::Png]] {
                            let command = Commands::Headless {
                                inputs: inputs.clone(),
                                recursive: false,
                                jobs,
                                update: false,
                                show: false,
                                outputs: vec![],
                                output_dir: output_dir.clone(),
                                output_type: output_type.clone(),
                            };
                            let res = command.run(false, false);
                            assert!(res.is_ok());
                        }
                    }
                }
            }
            dir.close().expect("A test should never fail");
            remove_file("tests/input.png").expect("A test should never fail");
        }
    }

    #[cfg(test)]
    mod test_struct_cli {
        use super::super::Cli;

        use super::super::Commands;
        use super::super::LogLevel;
        use clap::CommandFactory;

        #[test]
        fn verify_cli() {
            Cli::command().debug_assert()
        }

        #[test]
        fn test_default_cli() {
            assert_eq!(
                Cli::default(),
                Cli {
                    no_border: false,
                    dry_run: false,
                    log_level: LogLevel::default(),
                    verbose: 0,
                    command: None,
                }
            )
        }

        #[test]
        fn test_debug_cli() {
            assert_eq!(
                format!("{:?}", Cli::default()),
                format!(
                    "Cli {{ no_border: {:?}, dry_run: {:?}, log_level: {:?}, verbose: {:?}, command: {:?} }}",
                    false,
                    false,
                    LogLevel::default(),
                    0,
                    None::<Commands>,
                )
            )
        }

        #[test]
        #[should_panic(expected = "The command should be set")]
        fn test_run_fail() {
            Cli {
                dry_run: true,
                ..Cli::default()
            }
            .run()
            .expect("A test should never fail")
        }

        #[test]
        fn test_run() {
            let command = Commands::Headless {
                inputs: vec![],
                recursive: false,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_dir: None,
                output_type: vec![],
            };
            let cli = Cli {
                command: Some(command),
                dry_run: true,
                ..Cli::default()
            };
            cli.run().expect("A test should never fail")
        }

        #[test]
        fn test_check() {
            let command = Commands::Headless {
                inputs: vec![],
                recursive: false,
                jobs: 0,
                update: false,
                show: false,
                outputs: vec![],
                output_dir: None,
                output_type: vec![],
            };
            let cli = Cli {
                command: Some(command),
                dry_run: true,
                ..Cli::default()
            };
            cli.check()
        }

        #[test]
        fn test_logging_default() {
            Cli::default().start_logging();
        }

        #[test]
        #[ignore]
        #[cfg(feature = "gui")]
        fn test_logging_gui() {
            Cli {
                command: Some(Commands::Gui { input: None }),
                dry_run: true,
                ..Cli::default()
            }
            .start_logging()
        }
    }
}
