// @Author: Ultraxime
//
// This file is part of Baston Editor.
//
// Baston Editor is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Baston Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

//! Wrapper for iced app

use iced::{settings::Settings, window, Element, Size};

/// Wrapper for iced app
pub trait App<Message>
where
    Message: std::fmt::Debug + std::marker::Send + Clone + 'static,
    Self: 'static,
{
    /// Update the app state given a message
    fn update(&mut self, message: Message) -> iced::Task<Message>;

    /// Iced rendering function
    fn view(&self) -> Element<Message>;

    /// Title of the app
    fn title() -> &'static str;

    /// Icon of the app
    fn icon() -> Option<iced::window::Icon> {
        None
    }

    /// Size of the app
    fn size() -> Size {
        Size {
            width: 600.,
            height: 300.,
        }
    }

    /// Sets the subscription logic of the app
    fn subscription(&self) -> iced::Subscription<Message> {
        iced::Subscription::none()
    }

    /// Run the app
    fn run(self) -> iced::Result
    where
        Self: Sized,
    {
        let app = iced::application(Self::title(), Self::update, Self::view);

        let settings = Settings {
            default_text_size: 12.into(),
            ..Settings::default()
        };

        let window_settings = window::Settings {
            size: Self::size(),
            icon: Self::icon(),
            ..window::Settings::default()
        };

        let app = app
            .settings(settings)
            .window(window_settings)
            .font(iced_fonts::BOOTSTRAP_FONT_BYTES)
            .font(iced_fonts::REQUIRED_FONT_BYTES)
            .subscription(Self::subscription);

        app.run_with(move || (self, iced::Task::none()))
    }
}

#[cfg(test)]
mod test_app {
    use super::App;

    use iced::Size;

    struct Test;

    impl App<()> for Test {
        fn update(&mut self, _message: ()) -> iced::Task<()> {
            unimplemented!()
        }

        fn view(&self) -> iced::Element<()> {
            unimplemented!()
        }

        fn title() -> &'static str {
            unimplemented!()
        }
    }

    #[test]
    fn test_size() {
        assert_eq!(
            Test::size(),
            Size {
                width: 600.,
                height: 300.
            }
        )
    }

    #[test]
    fn test_icon() {
        assert!(Test::icon().is_none())
    }

    #[test]
    #[should_panic]
    fn test_update() {
        let _ = Test::update(&mut Test {}, ());
    }

    #[test]
    #[should_panic]
    fn test_view() {
        let _ = Test::view(&Test {});
    }

    #[test]
    #[should_panic]
    fn test_title() {
        Test::title();
    }

    #[test]
    fn test_subscription() {
        let _ = Test::subscription(&Test {});
    }
}
