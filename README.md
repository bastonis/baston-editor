|[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)|
|-|

|![gplv3-or-later](https://www.gnu.org/graphics/gplv3-or-later.png)|
|-|

# Baston-Editor

## Description

Baston Editor is a software designed to create cards for the game "Baston".

## Installation

To install it you can download the corresponding binary from the [releases](https://gitlab.com/bastonis/baston-editor/-/releases)

### From Source

You can also install the software by :

- cloning the repository (`git clone https://gitlab.com/bastonis/baston-editor.git`)
- pull the submodules (`git submodule update --init --recursive`)
- build the binary (`cargo build --release`)
- you will find the binary in `target/release`

## Usage

```bash, ignore
Usage: baston_editor [OPTIONS] [COMMAND]

Commands:
  gui           GUI mode
  headless      Headless mode (CLI)
  color-picker
  help          Print this message or the help of the given subcommand(s)

Options:
      --no-border
          Prevent the different outputs from showing the border

      --dry-run
          Prevent any modification and starting the GUI

      --log-level <LOG_LEVEL>
          Configure the logging level
          [default: error]

          Possible values:
          - off:   Does not show any log
          - error: Shows only the errors
          - warn:  Shows the errors and the warnings
          - info:  Shows the errors, the warnings and the info
          - debug: Shows the debug information
          - trace: Shows everything

  -v, --verbose...
          Verbose mode
          Every times it is specify, it increase the verbosity

  -h, --help
          Print help (see a summary with '-h')

  -V, --version
          Print version

```

For more information, please visit the [wiki](https://gitlab.com/bastonis/baston-editor/-/wikis/home)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Pre-Commit

The project already contains a pre-commit-config.

## Authors

- [@Ultraxime](https://gitlab.com/Ultraxime)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
